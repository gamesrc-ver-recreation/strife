@echo off
if "%MSG%" == "" goto error
set CHOICE=
cls
echo [1] Strife v1.0/v1.1
echo [2] Strife v1.2
echo [3] Strife v1.3
echo [4] Strife v1.31
echo.
echo [0] Cancel and quit
echo.
echo * Watcom 9.5c should be used for a more accurate code generation,
echo along with TASM 3.1.
echo.
echo %MSG%
set MSG=
choice /S /C:12340 /N
echo.

if ERRORLEVEL 5 goto end
if ERRORLEVEL 4 goto STRF131
if ERRORLEVEL 3 goto STRF13
if ERRORLEVEL 2 goto STRF12
if ERRORLEVEL 1 goto STRF11

:STRF11
set CHOICE=STRF11
goto end
:STRF12
set CHOICE=STRF12
goto end
:STRF13
set CHOICE=STRF13
goto end
:STRF131
set CHOICE=STRF131
goto end

:error
echo This script shouldn't be run independently

:end
