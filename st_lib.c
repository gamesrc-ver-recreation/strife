//
// Copyright (C) 1993-1996 Id Software, Inc.
// Copyright (C) 2022 Nuke.YKT
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
//

// ST_lib.c
#include "DoomDef.h"
#include "ST_lib.h"


// in AM_map.c
extern boolean		automapactive; 

patch_t *sttminus;

void STlib_init(void)
{
 sttminus = (patch_t *) W_CacheLumpName("STCFN045", PU_STATIC);
}


void STlib_initNum (st_number_t *n, int x, int y, patch_t **pl, int *num, int width)
{
  n->x = x;
  n->y = y;
  n->width = width;
  n->num = num;
  n->p = pl;
}


void STlib_drawNum (st_number_t *n, boolean refresh)
{

  int numdigits = n->width;
  int num = *n->num;
    
  int w = SHORT(n->p[0]->width) + 1;
  int h = SHORT(n->p[0]->height);
  int x = n->x;
    
  int neg;

  neg = num < 0;

  if (neg)
  {
    if (numdigits == 2 && num < -9)
      num = -9;
    else if (numdigits == 3 && num < -99)
      num = -99;
	
    num = -num;
  }

// if non-number, do not draw it
  if (num == 1994)
    return;

  x = n->x;

// in the special case of 0, you draw 0
  if (!num)
    V_DrawPatch(x - w, n->y, FG, n->p[ 0 ]);

// draw the new number
  while (num && numdigits--)
  {
    x -= w;
    V_DrawPatch(x, n->y, FG, n->p[ num % 10 ]);
    num /= 10;
  }

// draw a minus sign if necessary
  if (neg)
    V_DrawPatch(x - 8, n->y, FG, sttminus);
}


void STlib_updateNum (st_number_t *n)
{

  int numdigits = n->width;
  int num = *n->num;
    
  int w = SHORT(n->p[0]->width) + 1;
  int h = SHORT(n->p[0]->height);
  int x = n->x;

  if (num < 0)
    num = 0;

// if non-number, do not draw it
  if (num == 1994)
    return;

  x = n->x;

// in the special case of 0, you draw 0
  if (!num)
    V_DrawPatchDirect(x - w, n->y, FG, n->p[ 0 ]);

// draw the new number
  while (num && numdigits--)
  {
    x -= w;
    V_DrawPatchDirect(x, n->y, FG, n->p[ num % 10 ]);
    num /= 10;
  }
}

