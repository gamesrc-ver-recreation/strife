//
// Copyright (C) 1993-1996 Id Software, Inc.
// Copyright (C) 2022 Nuke.YKT
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
//

// ST_stuff.c

#include "DoomDef.h"
#include "P_local.h"
extern int _wp1, _wp2, _wp3, _wp4, _wp5, _wp6;
#include "DUtils.h"
#include "AM_map.h"
#include "ST_lib.h"
#include "ST_stuff.h"


//
// STATUS BAR CODE
//
void ST_Stop(void);

extern int key_invpop, key_mission, key_invkey, key_invquery, key_invL,
key_invR, key_invH, key_invE;


// Respond to keyboard input events,
//  intercept cheats.
boolean ST_Responder (event_t *ev)
{
  int		i;
  static int _int_88610 = 0;
  static char buf[ST_MSGWIDTH];
    
  // Filter automap on/off.
  if (ev->type == ev_keyup)
  {
	  if ((ev->data1 & 0xffff0000) == AM_MSGHEADER)
	  {
		  switch (ev->data1)
		  {
		  case AM_MSGENTERED:
			  st_gamestate = AutomapState;
			  st_firsttime = true;
			  break;

		  case AM_MSGEXITED:
			  //	fprintf(stderr, "AM exited\n");
			  st_gamestate = FirstPersonState;
			  break;
		  }
	  }
	  else if (ev->data1 == key_invpop || ev->data1 == key_mission || ev->data1 == key_invkey)
	  {
		  if (ev->data1 == key_invpop)
		  {
			  _int_884A8 = 0;
		  }
		  else if (ev->data1 == key_mission)
		  {
			  _int_884B0 = 0;
		  }
		  else if (ev->data1 == key_invkey)
		  {
			  _int_884AC = 0;
			  _int_88610 = 0;
		  }
		  if (!(_int_884AC || _int_884B0 || _int_884A8) && !_int_8848C)
		  {
			  _int_884A4 = 0;
			  if (_int_884A0)
			  {
				  M_SizeDisplay(1);
			  }
			  _int_884A0 = 0;
		  }
		  return true;
	  }
  }

  // if a user keypress...
  else if (ev->type == ev_keydown && plyr->mo->health > 0)
  {
	if (ev->data1 == key_invquery)
	{
		if (plyr->f_59[plyr->f_1df].f_8)
		{
			sprintf(buf, "%d %s", plyr->f_59[plyr->f_1df].f_8,
				mobjinfo[plyr->f_59[plyr->f_1df].f_4].name);
			plyr->message = buf;
		}
	}
	if (ev->data1 == key_invpop || ev->data1 == key_invkey || ev->data1 == key_mission)
	{
		if (ev->data1 == key_invkey)
		{
			_int_884B0 = 0;
			_int_884A8 = 0;
			if (!_int_88610)
			{
				_int_88610 = 1;
				_int_88490++;
				if (_int_88490 > 2)
				{
					_int_88490 = -1;
					_int_8848C = 0;
					_int_884AC = 0;
					_int_884A4 = 0;
					return true;
				}
			}
			if (!netgame)
				_int_8848C = 50;
			else
				_int_8848C = 20;
			_int_884AC = 1;
		}
		else if (ev->data1 == key_mission && !netgame)
		{
			_int_88490 = -1;
			_int_8848C = 0;
			_int_884AC = 0;
			_int_884B0 = 1;
			_int_884A8 = 0;
		}
		else if (ev->data1 == key_invpop)
		{
			_int_88490 = -1;
			_int_8848C = 0;
			_int_884AC = 0;
			_int_884B0 = 0;
			_int_884A8 = 1;
		}
		if (_int_884AC || _int_884B0 || _int_884A8)
		{
			_int_884A4 = 1;
			if (viewheight == 200)
			{
				M_SizeDisplay(0);
				_int_884A0 = 1;
			}
		}
	}
	if (ev->data1 == key_invL)
	{
		if (plyr->f_1df > 0)
			plyr->f_1df--;
		return true;
	}
	if (ev->data1 == key_invR)
	{
		if (plyr->f_1df < plyr->f_1dd-1)
			plyr->f_1df++;
		return true;
	}
	if (ev->data1 == key_invH)
	{
		plyr->f_1df = 0;
		return true;
	}
	if (ev->data1 == key_invE)
	{
		plyr->f_1df = plyr->f_1dd == 0 ? 0 : plyr->f_1dd - 1;
		return true;
	}
	// 'mus' cheat for changing music
	if (cht_CheckCheat(&cheat_mus, ev->data1))
	{
	
	  char	buf[3];
	  int		musnum;
	
	  plyr->message = STSTR_MUS;
	  cht_GetParam(&cheat_mus, buf);
	
	  musnum = (buf[0]-'0')*10 + buf[1]-'0';
	  
	  if (musnum > 35)
	     plyr->message = STSTR_NOMUS;
	  else
	    S_ChangeMusic(musnum, 1);
	}
	else if (cht_CheckCheat(&cheat_88580, ev->data1))
	{
		devparm = !devparm;
		if (devparm)
			plyr->message = "devparm ON";
		else
			plyr->message = "devparm OFF";
	}
    if (!netgame && usergame)
    {
	// 'dqd' cheat for toggleable god mode
	if (cht_CheckCheat(&cheat_god, ev->data1))
	{
	  plyr->cheats ^= CF_GODMODE;
	  if (plyr->cheats & CF_GODMODE)
	  {
	    if (plyr->mo)
	      plyr->mo->health = 100;
	  
	    plyr->health = 100;
		plyr->f_1d9 = 1;
	    plyr->message = STSTR_DQDON;
	  }
	  else 
	  {
		plyr->f_1d9 = 1;
	    plyr->message = STSTR_DQDOFF;
	  }
	}
	// 'fa' cheat for killer fucking arsenal
	else if (cht_CheckCheat(&cheat_ammonokey, ev->data1))
	{
	  plyr->armorpoints = 200;
	  plyr->armortype = 2;
	
	  for (i=0;i<NUMWEAPONS;i++)
	  {
			plyr->weaponowned[i] = !shareware || (shareware && weaponinfo[i].f_18);
	  }

	  plyr->weaponowned[wp_7] = 0;

	  for (i=0;i<NUMAMMO;i++)
	    plyr->ammo[i] = plyr->maxammo[i];
	
	  plyr->message = STSTR_FAADDED;
	}
	else if (cht_CheckCheat(&cheat_885B0, ev->data1))
	{
		if (!plyr->cards[it_15])
		{
			for (i = 0; i < 16; i++)
				plyr->cards[i] = 1;
			plyr->message = "Cheater Keys Added";
		}
		else if (!plyr->cards[it_26] && !shareware)
		{
			for (i = 0; i < 27; i++)
				plyr->cards[i] = 1;
			plyr->message = "Cheater Keys Added";
		}
		else
		{
			for (i = 0; i < 27; i++)
				plyr->cards[i] = 0;
			plyr->message = "Keys removed";
		}
	}
	else if (cht_CheckCheat(&cheat_noclip, ev->data1) )
	{	
	  plyr->cheats ^= CF_NOCLIP;
	
	  if (plyr->cheats & CF_NOCLIP)
	  {
		  plyr->message = STSTR_NCON;
		  plyr->mo->flags |= MF_NOCLIP;
	  }
	  else
	  {
		  plyr->mo->flags &= ~MF_NOCLIP;
		  plyr->message = STSTR_NCOFF;
	  }
	}
	else if (cht_CheckCheat(&cheat_88598, ev->data1))
	{
		plyr->cheats ^= CF_NOMOMENTUM;

		if (plyr->cheats & CF_NOMOMENTUM)
			plyr->message = "STEALTH BOOTS ON";
		else
			plyr->message = "STEALTH BOOTS OFF";
	}
	// 'behold?' power-up cheats
	for (i=0;i<3;i++)
	{
	  if (cht_CheckCheat(&cheat_powerup[i], ev->data1))
	  {
	    if (!plyr->powers[i])
	      P_GivePower( plyr, i);
	    else if (plyr->powers[i] != 1)
	      plyr->powers[i] = 1;
	    else
	      plyr->powers[i] = 0;
	  
	    plyr->message = STSTR_BEHOLDX;
	  }
	}
	if (cht_CheckCheat(&cheat_powerup[3], ev->data1))
	{
		func_2DF40(plyr, 180, 125);
		func_2DF40(plyr, 181, 126);
		func_2DF40(plyr, 130, 127);
		plyr->message = "you got the stuff!";
	}
	if (cht_CheckCheat(&cheat_powerup[4], ev->data1))
	{
		if (!plyr->backpack)
		{
			for (i = 0; i < NUMAMMO; i++)
				plyr->maxammo[i] *= 2;
			plyr->backpack = 1;
		}
		for (i = 0; i < NUMAMMO; i++)
			P_GiveAmmo(plyr, i, 1);
		plyr->message = "you got the stuff!";
	}
	if (cht_CheckCheat(&cheat_powerup[5], ev->data1))
	{
		func_2DF40(plyr, 126, 306);
		func_2DF40(plyr, 126, 307);
		plyr->message = "you got the stuff!";
	}
	if (cht_CheckCheat(&cheat_powerup[6], ev->data1))
	{
		P_GivePower(plyr, pw_5);
		plyr->message = "you got the stuff!";
	}
	// 'behold' power-up menu
	if (cht_CheckCheat(&cheat_powerup[7], ev->data1))
	{
	  plyr->message = STSTR_BEHOLD;
	}
	else if (!shareware && cht_CheckCheat(&cheat_88578, ev->data1))
	{
		plyr->f_1d9 = 1;
		if (!plyr->weaponowned[wp_7])
		{
			plyr->weaponowned[wp_7] = 1;
			plyr->f_45 = 0;
		}
		else
		{
			plyr->weaponowned[wp_7] = 1;
			plyr->f_45++;
			if (plyr->f_45 > 4)
			{
				plyr->pendingweapon = wp_0;
				plyr->weaponowned[wp_7] = 0;
				plyr->f_45 = -1;
			}
		}
		plyr->pendingweapon = wp_7;
	}
	// 'mypos' for player position
	else if (cht_CheckCheat(&cheat_mypos, ev->data1))
	{
	  sprintf(buf, "ang=0x%x;x,y=(0x%x,0x%x)",
		  players[consoleplayer].mo->angle,
		  players[consoleplayer].mo->x,
		  players[consoleplayer].mo->y);
	  plyr->message = buf;
      }
      // 'clev' change-level cheat
      else if (cht_CheckCheat(&cheat_clev, ev->data1))
      {
	char		buf[3];
	int		map;
      
	cht_GetParam(&cheat_clev, buf);
      
	  map = (buf[0] - '0')*10 + buf[1] - '0';

#if (APPVER_STRIFEREV >= AV_SR_STRF13)
	if ( (shareware && map > 31 && map < 35 ) || (registered && map > 0 && map < 35 ) )
#else
	if ( map > 0 && map <= 40 )
#endif
	{
	  plyr->message = STSTR_CLEV;
	  G_ExitLevel(map, 0, plyr->mo->angle);
	}
    }    
      else if (cht_CheckCheat(&cheat_88590, ev->data1))
      {
	char		buf[2];
	int		spot;
      
	cht_GetParam(&cheat_88590, buf);
      
	  spot = buf[0] - '0';

	  if (spot <= 10)
	  {
		  plyr->message = "Spawning to spot";
		  func_16A2C(spot);
	  }
    }
	  else if (cht_CheckCheat(&cheat_885C0, ev->data1))
	{
		killfl ^= 1;
		plyr->message = "Kill 'em.  Kill 'em All";
	}
	  else if (cht_CheckCheat(&cheat_88588, ev->data1))
	{
		plyr->message = "YOU GOT THE MIDAS TOUCH, BABY";
		func_2DF40(plyr, 128, 309);
	}
    }
  }
  return false;
}

void ST_Ticker (void)
{
	static int largeammo = 1994; // means "n/a"
	int i;

	if (weaponinfo[plyr->readyweapon].ammo == am_noammo)
		w_ready.num = &largeammo;
	else
		w_ready.num = &plyr->ammo[weaponinfo[plyr->readyweapon].ammo];

	w_ready.data = plyr->readyweapon;

	// get rid of chat window if up because of message
	if (!--st_msgcounter)
		st_chat = st_oldchat;

	if (_int_8848C)
	{
		if (!--_int_8848C)
		{
			_int_884A4 = 0;
			_int_884AC = 0;
			_int_88490 = -1;
			if (_int_884A0)
				M_SizeDisplay(1);
			_int_884A0 = 0;
		}
	}
}

void ST_doPaletteStuff(void)
{

  int palette;
  byte *pal;
  int cnt, bzc;

  cnt = plyr->damagecount;

  if (plyr->powers[pw_0])
  {
    // slowly fade the berzerk out
    bzc = 12 - (plyr->powers[pw_0]>>6);

    if (bzc > cnt)
      cnt = bzc;
  }
	
  if (cnt)
  {
    palette = (cnt+7)>>3;
	
    if (palette >= NUMREDPALS)
      palette = NUMREDPALS-1;

    palette += STARTREDPALS;
  }

  else if (plyr->bonuscount)
  {
    palette = (plyr->bonuscount+7)>>3;

    if (palette >= NUMBONUSPALS)
      palette = NUMBONUSPALS-1;

    palette += STARTBONUSPALS;
  }

  else if ( plyr->f_49 > 16*35 || plyr->f_49&8)
    palette = RADIATIONPAL;
  else
    palette = 0;

  if (palette != st_palette)
  {
    st_palette = palette;
    pal = (byte *) W_CacheLumpNum (lu_palette, PU_CACHE)+palette*768;
    I_SetPalette (pal);
  }

}

void func_383D4(int a1, int a2, int a3)
{
	if (!a3)
		V_DrawPatch(a1, a2, 0, invynum[0]);
	while (a3)
	{
		V_DrawPatch(a1, a2, 0, invynum[a3%10]);
		a1 -= invynum[0]->width+1;
		a3 /= 10;
	}
}

int func_38434(int a1, int a2, int a3)
{
	int w;
	w = invynum[0]->width + 1;
	if (!a3)
		V_DrawPatchDirect(a1, a2, 0, invynum[0]);
	if (a3 < 0)
		a3 = 0;
	while (a3)
	{
		V_DrawPatchDirect(a1, a2, 0, invynum[a3%10]);
		a1 -= w;
		a3 /= 10;
	}
	return a1;
}

void func_384A4(int a1, int a2, int a3, int a4)
{
	int i;
	byte *buf;

	buf = screens[0] + a2 * SCREENWIDTH + a1;

	for (i = 0; i < a3; i++)
	{
		*buf++ = a4;
	}
}

void ST_drawWidgets(boolean refresh)
{
	static int _int_88618 = -1;
	static int _int_DC878;
	static int _int_DC874;
	static int _int_DC870;

	int j;
	int x;
	int l;
	int k;
	patch_t *p;
	int i;
	int y;
	char buf[8];

	if (!plyr->f_1d9 && !refresh && plyr->health == _int_88618
		&& weaponinfo[plyr->readyweapon].ammo == _int_DC874 && plyr->f_1df == _int_DC878
		&& plyr->armortype == _int_DC870)
		return;

	plyr->f_1d9 = 0;
	_int_DC878 = plyr->f_1df;
	_int_DC874 = weaponinfo[plyr->readyweapon].ammo;
	_int_DC870 = plyr->armortype;
	_int_88618 = plyr->health;

	st_firsttime = 0;

	V_DrawPatch(0, 168, 0, invback);

	if (netgame)
		V_DrawPatch(0, 173, 0, faceback);

	i = plyr->f_1df < 6 ? 0 : plyr->f_1df - 5;
	if (plyr->f_1dd)
	{
		V_DrawPatch(42 + (plyr->f_1df - i) * 35, 180, 0, invcursor);
	}
	for (j = 0; j < 6 && j < plyr->f_1dd; j++)
	{
		sprintf(buf, "I_%s", sprnames[plyr->f_59[i+j].f_0]);
		k = W_CheckNumForName(buf);
		if (k != -1)
			p = W_CacheLumpNum(k, PU_STATIC);
		else
			p = W_CacheLumpName("STCFN063", PU_CACHE);
		V_DrawPatch(48 + j * 35, 182, 0, p);
		func_383D4(68 + j * 35, 191, plyr->f_59[i+j].f_8);
	}
	if (plyr->weaponowned[wp_7])
	{
		V_DrawPatch(253, 175, 0, sigil[plyr->f_45]);
	}
	if (_int_DC874 < 7)
	{
		V_DrawPatch(290, 180, 0, inventory[_int_DC874]);
	}
	if (plyr->armortype)
	{
		V_DrawPatch(2, 177, 0, armor[plyr->armortype-1]);
		func_383D4(20, 191, plyr->armorpoints);
	}

	x = ((plyr->health > 100) ? (200 - plyr->health) *2 : plyr->health * 2);

	if (plyr->health < 11)
		y = 64;
	else if (plyr->health < 21)
		y = 80;
	else
		y = 96;

	if (plyr->cheats & CF_GODMODE)
		y = 226;

	func_384A4(49, 172, x, y);
	func_384A4(49, 173, x, y + 3);
	func_384A4(49, 175, x, y);
	func_384A4(49, 176, x, y + 3);

	if (plyr->health > 100)
	{
		func_384A4(49 + x, 172, 200 - x, 112);
		func_384A4(49 + x, 173, 200 - x, 115);
		func_384A4(49 + x, 175, 200 - x, 112);
		func_384A4(49 + x, 176, 200 - x, 115);
	}
}

void ST_Drawer (boolean fullscreen, boolean refresh)
{
  st_statusbaron = (!fullscreen) || automapactive;

// Do red-/gold-shifts from damage/items
  ST_doPaletteStuff();

  if (st_statusbaron)
  {
	  if (refresh || st_firsttime)
		  ST_drawWidgets(true);
	  else ST_drawWidgets(false);
  }
}

int func_38988(int a1)
{
	int i;
	int va = 0;
	for (i = 0; i < 8; i++)
	{
		if (i != a1)
			va += players[a1].frags[i];
		else
			va -= players[a1].frags[i];
	}
	return va;
}

void func_389BC(int a1, int a2, int a3)
{
	char buf[12];
	sprintf(buf, "%02d:%02d:%02d", a3 / 3600, (a3 / 60) % 60, a3 % 60);
	func_3A2F8(a1, a2, buf);
}

int func_38A1C(void)
{
	int i;
	int vs = 0;
	if (!st_statusbaron)
	{
		func_38434(15, 194, plyr->health);
		if (weaponinfo[plyr->readyweapon].ammo != am_noammo)
			func_38434(310, 194, plyr->ammo[weaponinfo[plyr->readyweapon].ammo]);
	}
	else
	{
		V_DrawPatchDirect(0, 160, 0, invtop);
		STlib_updateNum(&w_health);
		STlib_updateNum(&w_ready);
	}
	if (!_int_884A4)
		return 0;
	if (_int_884B0)
	{
		func_36F94(0, 56, 0, invppbak2);
		V_DrawPatchDirect(0, 56, 0, invpop2);
		func_19A70(24, 74, _char_A1300, 1);
		func_3A2F8(24, 74, _char_A1300);
		func_389BC(210, 64, leveltime / 35);
		return 1;
	}
	if (_int_884AC || _int_8848C)
		return func_38D6C(0);
	func_36F94(0, 56, 0, invppback);
	V_DrawPatchDirect(0, 56, 0, invpop);
	for (i = 0; i < NUMCARDS; i++)
		if (plyr->cards[i])
			vs++;
	func_38434(261, 132, vs);
	if (plyr->weaponowned[wp_1])
		V_DrawPatchDirect(38, 86, 0, W_CacheLumpName(_int_88458[0], PU_CACHE));
	if (plyr->weaponowned[wp_2])
		V_DrawPatchDirect(40, 107, 0, W_CacheLumpName(_int_88458[1], PU_CACHE));
	if (plyr->weaponowned[wp_3])
		V_DrawPatchDirect(39, 131, 0, W_CacheLumpName(_int_88458[2], PU_CACHE));
	if (plyr->weaponowned[wp_4])
		V_DrawPatchDirect(78, 87, 0, W_CacheLumpName(_int_88458[3], PU_CACHE));
	if (plyr->weaponowned[wp_5])
		V_DrawPatchDirect(80, 117, 0, W_CacheLumpName(_int_88458[4], PU_CACHE));
	if (plyr->weaponowned[wp_6])
		V_DrawPatchDirect(75, 142, 0, W_CacheLumpName(_int_88458[5], PU_CACHE));

	for (i = 0; i < 7; i++)
	{
		STlib_updateNum(&w_ammo[i]);
		STlib_updateNum(&w_maxammo[i]);
	}

	func_38434(261, 84, plyr->f_1e1);
	func_38434(261, 108, plyr->f_1e3);
	if (plyr->powers[pw_4])
		V_DrawPatchDirect(280, 130, 0, W_CacheLumpName("I_COMM", PU_CACHE));
	return 1;
}

extern char player_names[][16];

int func_38D6C(int a1)
{
	int i, j;
	int x, y;
	int vd;
	char buffer[20];

	func_36F94(0, 56, 0, invppbak2);
	V_DrawPatchDirect(0, 56, 0, invpop2);
	if (deathmatch)
	{
		for (i = 0; i < 4; i++)
		{
			sprintf(buffer, "stcolor%d", i+1);
			V_DrawPatchDirect(28, 64+i*17, 0, W_CacheLumpName(buffer, PU_CACHE));
			sprintf(buffer, "%s%d", player_names[i], func_38988(i));
			func_3A2F8(38, 66+i*17, buffer);
			if (!playeringame[i])
				func_3A2F8(28, 65+i*17, "X");
		}
		for (i = 4; i < 8; i++)
		{
			sprintf(buffer, "stcolor%d", i+1);
			V_DrawPatchDirect(158, -4+i*17, 0, W_CacheLumpName(buffer, PU_CACHE));
			sprintf(buffer, "%s%d", player_names[i], func_38988(i));
			func_3A2F8(168, -2+i*17, buffer);
			if (!playeringame[i])
				func_3A2F8(158, -3+i*17, "X");
		}
	}
	else
	{
		if (_int_88490 < 0 || _int_88490 > 2)
		{
			_int_88490 = -1;
			_int_8848C = 0;
			_int_884A4 = 0;
			return 0;
		}
		if (_int_88490 > 0)
		{
			vd = 0;
			for (i = 0, j = 10 * _int_88490; i < 10 && j < NUMCARDS; i++, j++)
			{
				if (plyr->cards[j])
					vd = 1;
			}
			if (!vd)
			{
				_int_88490 = -1;
				_int_884A4 = 0;
				_int_884AC = 0;
				return 0;
			}
		}
		for (j = 10 * _int_88490, i = 0, x = 20, y = 63; i < 10 && j < NUMCARDS; i++, j++)
		{
			if (plyr->cards[j])
			{
				sprintf(buffer, "I_%s", sprnames[states[mobjinfo[MT_133+j].spawnstate].sprite]);
				V_DrawPatchDirect(x, y, 0, W_CacheLumpName(buffer, PU_CACHE));
				func_3A2F8(x+17, y+4, mobjinfo[MT_133+j].name);
			}
			if (i == 4)
			{
				x = 160;
				y = 63;
			}
			else
				y += 17;
		}
	}
	return 1;
}

void ST_loadGraphics(void)
{
  int i, j, facenum;   
  char namebuf[9];

// Load the numbers, tall and short
  for (i=0;i<10;i++)
  {
    sprintf(namebuf, "INVFONG%d", i);
	invgnum[i] = (patch_t *) W_CacheLumpName(namebuf, PU_STATIC);

    sprintf(namebuf, "INVFONY%d", i);
    invynum[i] = (patch_t *) W_CacheLumpName(namebuf, PU_STATIC);
  }

  invcursor = (patch_t*)W_CacheLumpName("INVCURS", PU_STATIC);

	if (!shareware)
	{
		for (i = 0; i < 5; i++)
		{
			sprintf(namebuf, "I_SGL%d", i+1);
			sigil[i] = (patch_t*)W_CacheLumpName(namebuf, PU_STATIC);
		}
	}

	for (i = 0; i < 7; i++)
	{
		inventory[i] = (patch_t*)W_CacheLumpName(_int_88470[i], PU_STATIC);
	}

	armor[0] = (patch_t*)W_CacheLumpName("I_ARM2", PU_STATIC);
	armor[1] = (patch_t*)W_CacheLumpName("I_ARM1", PU_STATIC);

	sprintf(namebuf, "STBACK0%d", consoleplayer+1);
	if (netgame)
		faceback = (patch_t*)W_CacheLumpName(namebuf, PU_STATIC);

	invback = (patch_t*)W_CacheLumpName("INVBACK", PU_STATIC);
	invtop = (patch_t*)W_CacheLumpName("INVTOP", PU_STATIC);
	invpop = (patch_t*)W_CacheLumpName("INVPOP", PU_STATIC);
	invpop2 = (patch_t*)W_CacheLumpName("INVPOP2", PU_STATIC);
	invppback = (patch_t*)W_CacheLumpName("INVPBAK", PU_STATIC);
	invppbak2 = (patch_t*)W_CacheLumpName("INVPBAK2", PU_STATIC);
}

void ST_initData(void)
{

  int i;

  st_firsttime = true;
  plyr = &players[consoleplayer];

  st_chatstate = StartChatState;
  st_gamestate = FirstPersonState;

  st_statusbaron = true;
  st_oldchat = st_chat = false;
  st_cursoron = false;

  st_palette = -1;

  STlib_init();

}



void ST_createWidgets(void)
{

  int i;

// ready weapon ammo
  STlib_initNum(&w_ready, 311, 162, invgnum,
    &plyr->ammo[weaponinfo[plyr->readyweapon].ammo], 3 );

// the last weapon type
  w_ready.data = plyr->readyweapon; 

// health percentage
  STlib_initNum(&w_health, 79, 162, invgnum,
    &plyr->health, 3);

// ammo count (all four kinds)
  STlib_initNum(&w_ammo[0], 206, 75, invynum,
    &plyr->ammo[0], 3);

  STlib_initNum(&w_ammo[1], 206, 99, invynum,
    &plyr->ammo[1], 3);

  STlib_initNum(&w_ammo[2], 206, 91, invynum,
    &plyr->ammo[2], 2);
    
  STlib_initNum(&w_ammo[3], 206, 139, invynum,
    &plyr->ammo[3], 3);

  STlib_initNum(&w_ammo[4], 206, 131, invynum,
    &plyr->ammo[4], 3);

  STlib_initNum(&w_ammo[5], 206, 115, invynum,
    &plyr->ammo[5], 2);
    
  STlib_initNum(&w_ammo[6], 206, 123, invynum,
    &plyr->ammo[6], 3);
  
// max ammo count (all four kinds)
  STlib_initNum(&w_maxammo[0], 239, 75, invynum,
    &plyr->maxammo[0], 3);

  STlib_initNum(&w_maxammo[1], 239, 99, invynum,
    &plyr->maxammo[1], 3);

  STlib_initNum(&w_maxammo[2], 239, 91, invynum,
    &plyr->maxammo[2], 2);
    
  STlib_initNum(&w_maxammo[3], 239, 139, invynum,
    &plyr->maxammo[3], 3);

  STlib_initNum(&w_maxammo[4], 239, 131, invynum,
    &plyr->maxammo[4], 3);

  STlib_initNum(&w_maxammo[5], 239, 115, invynum,
    &plyr->maxammo[5], 2);
    
  STlib_initNum(&w_maxammo[6], 239, 123, invynum,
    &plyr->maxammo[6], 3);
}

static boolean	st_stopped = true;


void ST_Start (void)
{

	if (!st_stopped)
		return;

  ST_initData();
  ST_createWidgets();
  st_stopped = false;

}

void ST_Stop (void)
{
  if (st_stopped)
    return;

  I_SetPalette (W_CacheLumpNum (lu_palette, PU_CACHE));

  st_stopped = true;
}

void ST_Init (void)
{
  veryfirsttime = 0;
  lu_palette = W_GetNumForName ("PLAYPAL");
  ST_loadGraphics();
}
