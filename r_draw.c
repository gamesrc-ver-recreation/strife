//
// Copyright (C) 1993-1996 Id Software, Inc.
// Copyright (C) 2022 Nuke.YKT
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
//

// R_draw.c

#include "DoomDef.h"
extern int _wp1, _wp2, _wp3, _wp4, _wp5, _wp6, _wp7, _wp8, _wp9, _wp10, _wp11;
#include "R_local.h"

#define SC_INDEX			0x3c4
#define SC_MAPMASK			2
#define GC_INDEX			0x3ce
#define GC_READMAP			4
#define GC_MODE				5

/*

All drawing to the view buffer is accomplished in this file.  The other refresh
files only know about ccordinates, not the architecture of the frame buffer.

*/

byte *viewimage;
int viewwidth, scaledviewwidth, viewheight, viewwindowx, viewwindowy;
byte *ylookup[MAXHEIGHT];
int columnofs[MAXWIDTH];
byte *xlatable;
// byte translations[3][256]; // color tables for different players

/*
==================
=
= R_DrawColumn
=
= Source is the top of the column to scale
=
==================
*/

lighttable_t	*dc_colormap;
int				dc_x;
int				dc_yl;
int				dc_yh;
fixed_t			dc_iscale;
fixed_t			dc_texturemid;
byte			*dc_source;		// first pixel in a column (possibly virtual)

int				dccount;		// just for profiling

#ifndef __WATCOMC__
#ifndef __i386
#ifndef __m68k
void R_DrawColumn (void)
{
	int			count;
	byte		*dest;
	fixed_t		frac, fracstep;	

	count = dc_yh - dc_yl;
	if (count < 0)
		return;
				
#ifdef RANGECHECK
	if ((unsigned)dc_x >= SCREENWIDTH || dc_yl < 0 || dc_yh >= SCREENHEIGHT)
		I_Error ("R_DrawColumn: %i to %i at %i", dc_yl, dc_yh, dc_x);
#endif

	dest = ylookup[dc_yl] + columnofs[dc_x]; 
	
	fracstep = dc_iscale;
	frac = dc_texturemid + (dc_yl-centery)*fracstep;

	do
	{
		*dest = dc_colormap[dc_source[(frac>>FRACBITS)&127]];
		dest += SCREENWIDTH;
		frac += fracstep;
	} while (count--);
}
#endif		// __m68k
#endif		// __i386
#endif

#ifndef __WATCOMC__
#ifndef __i386
#ifndef __m68k
void R_DrawColumnLow (void)
{
	int			count;
	byte		*dest;
	fixed_t		frac, fracstep;	

	count = dc_yh - dc_yl;
	if (count < 0)
		return;
				
#ifdef RANGECHECK
	if ((unsigned)dc_x >= SCREENWIDTH || dc_yl < 0 || dc_yh >= SCREENHEIGHT)
		I_Error ("R_DrawColumn: %i to %i at %i", dc_yl, dc_yh, dc_x);
//	dccount++;
#endif

	dest = ylookup[dc_yl] + columnofs[dc_x]; 
	
	fracstep = dc_iscale;
	frac = dc_texturemid + (dc_yl-centery)*fracstep;

	do
	{
		*dest = dc_colormap[dc_source[(frac>>FRACBITS)&127]];
		dest += SCREENWIDTH;
		frac += fracstep;
	} while (count--);
}
#endif		// __m68k
#endif		// __i386
#endif


void func_326A0(void)
{
	int			count;
	byte		*dest;
	fixed_t		frac, fracstep;
		
	count = dc_yh - dc_yl;
	if (count < 0)
		return;

	outp (SC_INDEX+1,1<<(dc_x&3)); 
	dest = destview + dc_yl*80 + (dc_x>>2); 

	fracstep = pspriteiscale;
	frac = dc_texturemid + (dc_yl-centery)*fracstep;

	do
	{
		*dest = dc_colormap[dc_source[(frac>>16)%200]];
		dest += SCREENWIDTH/4;
		frac += fracstep;
	} while (count--);
}


void R_DrawFuzzColumn (void)
{
	int			count;
	byte		*dest;
	fixed_t		frac, fracstep;	

	if (!dc_yl)
		dc_yl = 1;
	if (dc_yh == viewheight-1)
		dc_yh = viewheight - 2;
		
	count = dc_yh - dc_yl;
	if (count < 0)
		return;
				
#ifdef RANGECHECK
	if ((unsigned)dc_x >= SCREENWIDTH || dc_yl < 0 || dc_yh >= SCREENHEIGHT)
		I_Error ("R_DrawFuzzColumn: %i to %i at %i", dc_yl, dc_yh, dc_x);
#endif

#ifdef __WATCOMC__
	outpw (GC_INDEX,GC_READMAP+((dc_x&3)<<8) ); 
	outp (SC_INDEX+1,1<<(dc_x&3)); 
	dest = destview + dc_yl*80 + (dc_x>>2); 
#else
	dest = ylookup[dc_yl] + columnofs[dc_x];
#endif

	fracstep = dc_iscale;
	frac = dc_texturemid + (dc_yl-centery)*fracstep;

	R_DrawFuzzColumnLoop(dest, frac, count);
}


void R_DrawFuzzColumnAlt (void)
{
	int			count;
	byte		*dest;
	fixed_t		frac, fracstep;	

	if (!dc_yl)
		dc_yl = 1;
	if (dc_yh == viewheight-1)
		dc_yh = viewheight - 2;
		
	count = dc_yh - dc_yl;
	if (count < 0)
		return;
				
#ifdef RANGECHECK
	if ((unsigned)dc_x >= SCREENWIDTH || dc_yl < 0 || dc_yh >= SCREENHEIGHT)
		I_Error ("R_DrawFuzzColumn2: %i to %i at %i", dc_yl, dc_yh, dc_x);
#endif

#ifdef __WATCOMC__
	outpw (GC_INDEX,GC_READMAP+((dc_x&3)<<8) ); 
	outp (SC_INDEX+1,1<<(dc_x&3)); 
	dest = destview + dc_yl*80 + (dc_x>>2); 
#else
	dest = ylookup[dc_yl] + columnofs[dc_x];
#endif

	fracstep = dc_iscale;
	frac = dc_texturemid + (dc_yl-centery)*fracstep;

	R_DrawAltFuzzColumnLoop(dest, frac, count);
}

/*
========================
=
= R_DrawTranslatedColumn
=
========================
*/

byte *dc_translation;
byte *translationtables;

void R_DrawTranslatedColumn (void)
{
	int			count;
	byte		*dest;
	fixed_t		frac, fracstep;	

	count = dc_yh - dc_yl;
	if (count < 0)
		return;
				
#ifdef RANGECHECK
	if ((unsigned)dc_x >= SCREENWIDTH || dc_yl < 0 || dc_yh >= SCREENHEIGHT)
		I_Error ("R_DrawXColumn: %i to %i at %i", dc_yl, dc_yh, dc_x);
#endif

#ifdef __WATCOMC__
	outp (SC_INDEX+1,1<<(dc_x&3));
	dest = destview + dc_yl*80 + (dc_x>>2);
#else
	dest = ylookup[dc_yl] + columnofs[dc_x];
#endif
	
	fracstep = dc_iscale;
	frac = dc_texturemid + (dc_yl-centery)*fracstep;

	do
	{
		*dest = dc_colormap[dc_translation[dc_source[frac>>FRACBITS]]];
#ifdef __WATCOMC__
		dest += SCREENWIDTH/4;
#else
		dest += SCREENWIDTH;
#endif
		frac += fracstep;
	} while (count--);
}

void func_3298C(void)
{
	int			count;
	byte		*dest;
	fixed_t		frac, fracstep;	

	count = dc_yh - dc_yl;
	if (count < 0)
		return;
				
#ifdef RANGECHECK
	if ((unsigned)dc_x >= SCREENWIDTH || dc_yl < 0 || dc_yh >= SCREENHEIGHT)
		I_Error ("R_DrawXFuzzColumn: %i to %i at %i", dc_yl, dc_yh, dc_x);
#endif

#ifdef __WATCOMC__
	outpw (GC_INDEX,GC_READMAP+((dc_x&3)<<8) );
	outp (SC_INDEX+1,1<<(dc_x&3));
	dest = destview + dc_yl*80 + (dc_x>>2);
#else
	dest = ylookup[dc_yl] + columnofs[dc_x];
#endif
	
	fracstep = dc_iscale;
	frac = dc_texturemid + (dc_yl-centery)*fracstep;

	do
	{
		*dest = xlatable[(*dest << 8) + dc_colormap[dc_translation[dc_source[(frac >> FRACBITS)&127]]]];
#ifdef __WATCOMC__
		dest += SCREENWIDTH/4;
#else
		dest += SCREENWIDTH;
#endif
		frac += fracstep;
	} while (count--);
}

/*
========================
=
= R_InitTranslationTables
=
========================
*/

void R_InitTranslationTables (void)
{
	int		i;
	char *xt;

	translationtables = Z_Malloc (256*7+255, PU_STATIC, 0);
	translationtables = (byte *)(( (int)translationtables + 255 )& ~255);

	xt = W_CacheLumpName("XLATAB", PU_CACHE);
	xlatable = Z_Malloc(0x20000, PU_STATIC, 0);
	xlatable = (byte *)(( (int)xlatable + 65535 )& ~65535);
	memcpy(xlatable, xt, 65536);

//
// translate just the 16 green colors
//
	for(i = 0; i < 256; i++)
	{
		if (i >= 0x20 && i < 0x40)
		{
			translationtables[i] = i - 0x20;
			translationtables[i+256] = i - 0x20;
			translationtables[i+512] = 0xd0 + (i&0xf);
			translationtables[i+768] = 0xd0 + (i&0xf);
			translationtables[i+1024] = i - 0x20;
			translationtables[i+1280] = i - 0x20;
			translationtables[i+1536] = i - 0x20;
		}
		else if (i >= 0x50 && i <= 0x5f)
		{
			translationtables[i] = i;
			translationtables[i+256] = i;
			translationtables[i+512] = i;
			translationtables[i+768] = i;
			translationtables[i+1024] = 0x80 + (i&0xf);
			translationtables[i+1280] = 0x10 + (i&0xf);
			translationtables[i+1536] = 0x40 + (i&0xf);
		}
		else if (i >= 0xf1 && i <= 0xf6)
		{
			translationtables[i] = 0xdf + (i&0xf);
			translationtables[i+256] = i;
			translationtables[i+512] = i;
			translationtables[i+768] = i;
			translationtables[i+1024] = i;
			translationtables[i+1280] = i;
			translationtables[i+1536] = i;
		}
		else if (i >= 0xf7 && i <= 0xfb)
		{
			translationtables[i] = i-6;
			translationtables[i+256] = i;
			translationtables[i+512] = i;
			translationtables[i+768] = i;
			translationtables[i+1024] = i;
			translationtables[i+1280] = i;
			translationtables[i+1536] = i;
		}
		else if (i >= 0x80 && i <= 0x8f)
		{
			translationtables[i] = 0x40 + (i&0xf);
			translationtables[i+256] = 0xb0 + (i&0xf);
			translationtables[i+512] = 0x10 + (i&0xf);
			translationtables[i+768] = 0x30 + (i&0xf);
			translationtables[i+1024] = 0x50 + (i&0xf);
			translationtables[i+1280] = 0x60 + (i&0xf);
			translationtables[i+1536] = 0x90 + (i&0xf);
		}
		else if (i >= 0xc0 && i <= 0xcf)
		{
			translationtables[i] = i;
			translationtables[i+256] = i;
			translationtables[i+512] = i;
			translationtables[i+768] = i;
			translationtables[i+1024] = 0xa0 + (i&0xf);
			translationtables[i+1280] = 0x20 + (i&0xf);
			translationtables[i+1536] = (i&0xf);
			if (!translationtables[i+1536])
				translationtables[i+1536]=1;
		}
		else if (i >= 0xd0 && i <= 0xdf)
		{
			translationtables[i] = i;
			translationtables[i+256] = i;
			translationtables[i+512] = i;
			translationtables[i+768] = i;
			translationtables[i+1024] = 0xb0 + (i&0xf);
			translationtables[i+1280] = 0x30 + (i&0xf);
			translationtables[i+1536] = 0x10 + (i&0xf);
		}
		else
		{
			translationtables[i] = translationtables[i+256] 
			= translationtables[i+512] = translationtables[i+768]
			= translationtables[i+1024] = translationtables[i+1280]
			= translationtables[i+1536] = i;
		}
	}
}

/*
================
=
= R_DrawSpan
=
================
*/

int				ds_y;
int				ds_x1;
int				ds_x2;
lighttable_t	*ds_colormap;
fixed_t			ds_xfrac;
fixed_t			ds_yfrac;
fixed_t			ds_xstep;
fixed_t			ds_ystep;
byte			*ds_source;		// start of a 64*64 tile image

int				dscount;		// just for profiling

#ifndef __WATCOMC__
#ifndef __i386
#ifndef __m68k
void R_DrawSpan (void)
{
	fixed_t		xfrac, yfrac;
	byte		*dest;
	int			count, spot;
	
#ifdef RANGECHECK
	if (ds_x2 < ds_x1 || ds_x1<0 || ds_x2>=SCREENWIDTH 
	|| (unsigned)ds_y>SCREENHEIGHT)
		I_Error ("R_DrawSpan: %i to %i at %i",ds_x1,ds_x2,ds_y);
//	dscount++;
#endif
	
	xfrac = ds_xfrac;
	yfrac = ds_yfrac;
	
	dest = ylookup[ds_y] + columnofs[ds_x1];	
	count = ds_x2 - ds_x1;
	do
	{
		spot = ((yfrac>>(16-6))&(63*64)) + ((xfrac>>16)&63);
		*dest++ = ds_colormap[ds_source[spot]];
		xfrac += ds_xstep;
		yfrac += ds_ystep;
	} while (count--);
}
#endif
#endif
#endif

#ifndef __WATCOMC__
#ifndef __i386
#ifndef __m68k
void R_DrawSpanLow (void)
{
	fixed_t		xfrac, yfrac;
	byte		*dest;
	int			count, spot;
	
#ifdef RANGECHECK
	if (ds_x2 < ds_x1 || ds_x1<0 || ds_x2>=SCREENWIDTH 
	|| (unsigned)ds_y>SCREENHEIGHT)
		I_Error ("R_DrawSpan: %i to %i at %i",ds_x1,ds_x2,ds_y);
//	dscount++;
#endif
	
	xfrac = ds_xfrac;
	yfrac = ds_yfrac;
	
	dest = ylookup[ds_y] + columnofs[ds_x1];	
	count = ds_x2 - ds_x1;
	do
	{
		spot = ((yfrac>>(16-6))&(63*64)) + ((xfrac>>16)&63);
		*dest++ = ds_colormap[ds_source[spot]];
		xfrac += ds_xstep;
		yfrac += ds_ystep;
	} while (count--);
}
#endif
#endif
#endif



/*
================
=
= R_InitBuffer
=
=================
*/

void R_InitBuffer (int width, int height)
{
	int		i;
	
	viewwindowx = (SCREENWIDTH-width) >> 1;
	for (i=0 ; i<width ; i++)
		columnofs[i] = viewwindowx + i;
	if (width == SCREENWIDTH)
		viewwindowy = 0;
	else
		viewwindowy = (SCREENHEIGHT-SBARHEIGHT-height) >> 1;
	for (i=0 ; i<height ; i++)
		ylookup[i] = screens[0] + (i+viewwindowy)*SCREENWIDTH;
}

 


/*
================
=
= R_FillBackScreen
=
= Fills the back screen with a pattern for variable screen sizes
= Also draws a beveled edge.
=================
*/
char *back_flat;

void R_FillBackScreen (void)
{
	byte		*src, *dest;
	int			i, j;
	int			x, y;
	patch_t		*patch;
	char		*name;
	
	if (scaledviewwidth == 320)
		return;
	
	name = back_flat;
    
	src = W_CacheLumpName (name, PU_CACHE);
	dest = screens[1];
	 
	for (y=0 ; y<SCREENHEIGHT-SBARHEIGHT ; y++)
	{
		for (x=0 ; x<SCREENWIDTH/64 ; x++)
		{
			memcpy (dest, src+((y&63)<<6), 64);
			dest += 64;
		}
		if (SCREENWIDTH&63)
		{
			memcpy (dest, src+((y&63)<<6), SCREENWIDTH&63);
			dest += (SCREENWIDTH&63);
		}
	}
	
	patch = W_CacheLumpName ("brdr_t",PU_CACHE);
	for (x=0 ; x<scaledviewwidth ; x+=8)
		V_DrawPatch (viewwindowx+x,viewwindowy-8,1,patch);
	patch = W_CacheLumpName ("brdr_b",PU_CACHE);
	for (x=0 ; x<scaledviewwidth ; x+=8)
		V_DrawPatch (viewwindowx+x,viewwindowy+viewheight,1,patch);
	patch = W_CacheLumpName ("brdr_l",PU_CACHE);
	for (y=0 ; y<viewheight ; y+=8)
		V_DrawPatch (viewwindowx-8,viewwindowy+y,1,patch);
	patch = W_CacheLumpName ("brdr_r",PU_CACHE);
	for (y=0 ; y<viewheight ; y+=8)
		V_DrawPatch (viewwindowx+scaledviewwidth,viewwindowy+y,1,patch);

	V_DrawPatch (viewwindowx-8, viewwindowy-8, 1,
		W_CacheLumpName ("brdr_tl",PU_CACHE));
	V_DrawPatch (viewwindowx+scaledviewwidth, viewwindowy-8, 1,
		W_CacheLumpName ("brdr_tr",PU_CACHE));
	V_DrawPatch (viewwindowx-8, viewwindowy+viewheight, 1,
		W_CacheLumpName ("brdr_bl",PU_CACHE));
	V_DrawPatch (viewwindowx+scaledviewwidth, viewwindowy+viewheight, 1,
		W_CacheLumpName ("brdr_br",PU_CACHE));

#ifdef __WATCOMC__
	dest = (byte*)0xac000;
	src = screens[1];
	for (i = 0; i < 4; i++, src++)
	{
		outp (SC_INDEX, 2);
		outp (SC_INDEX+1, 1<<i);
		for (j = 0; j < (SCREENHEIGHT-SBARHEIGHT)*SCREENWIDTH/4; j++)
			dest[j] = src[j*4];
	}
#endif
}


void R_VideoErase (unsigned ofs, int count)
{ 
#ifdef __WATCOMC__
	int		i;
	byte	*src, *dest;
	outp (SC_INDEX, SC_MAPMASK);
	outp (SC_INDEX+1, 15);
	outp (GC_INDEX, GC_MODE);
	outp (GC_INDEX+1, inp (GC_INDEX+1)|1);
	src = (byte*)0xac000+(ofs>>2);
	dest = destscreen+(ofs>>2);
	for (i = (count>>2)-1; i >= 0; i--)
	{
		dest[i] = src[i];
	}
	outp (GC_INDEX, GC_MODE);
	outp (GC_INDEX+1, inp (GC_INDEX+1)&~1);
#else
	memcpy (screens[0]+ofs, screens[1]+ofs, count);
#endif
}


/*
================
=
= R_DrawViewBorder
=
= Draws the border around the view for different size windows
=
=================
*/

void V_MarkRect (int x, int y, int width, int height);
 
void R_DrawViewBorder (void)
{ 
    int		top, side, ofs, i;
 
	if (scaledviewwidth == SCREENWIDTH)
		return;
  
	top = ((SCREENHEIGHT-SBARHEIGHT)-viewheight)/2;
	side = (SCREENWIDTH-scaledviewwidth)/2;

//
// copy top and one line of left side
//
	R_VideoErase (0, top*SCREENWIDTH+side);
 
//
// copy one line of right side and bottom
//
	ofs = (viewheight+top)*SCREENWIDTH-side;
	R_VideoErase (ofs, top*SCREENWIDTH+side);
 
//
// copy sides using wraparound
//
	ofs = top*SCREENWIDTH + SCREENWIDTH-side;
	side <<= 1;
    
	for (i=1 ; i<viewheight ; i++)
	{
		R_VideoErase (ofs, side);
		ofs += SCREENWIDTH;
	}

#ifndef __WATCOMC__
	V_MarkRect (0,0,SCREENWIDTH, SCREENHEIGHT-SBARHEIGHT);
#endif
}
 
 
