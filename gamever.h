// *** VERSIONS RESTORATION ***
#ifndef GAMEVER_H
#define GAMEVER_H

// It is assumed here that:
// 1. The compiler is set up to appropriately define APPVER_EXEDEF
// as an EXE identifier.
// 2. This header is included (near) the beginning of every compilation unit,
// in order to have an impact in any place where it's expected to.

// APPVER_STRIFEREV definitions
#define AV_SR_STRF11 199604180
#define AV_SR_STRF12 199605010
#define AV_SR_STRF13 199609120
#define AV_SR_STRF131 199703190

// Now define APPVER_STRIFEREV to one of the above, based on APPVER_EXEDEF

#define APPVER_CONCAT1(x,y) x ## y
#define APPVER_CONCAT2(x,y) APPVER_CONCAT1(x,y)
#define APPVER_STRIFEREV APPVER_CONCAT2(AV_SR_,APPVER_EXEDEF)

#endif // GAMEVER_H
