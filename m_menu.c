//
// Copyright (C) 1993-1996 Id Software, Inc.
// Copyright (C) 2022 Nuke.YKT
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
//

// M_menu.c

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <ctype.h>
#include "DoomDef.h"
#include "R_local.h"
#include "soundst.h"

#if (APPVER_STRIFEREV >= AV_SR_STRF13)
extern int _wp1, _wp2, _wp3, _wp4;
#else
extern int _wp1, _wp2, _wp3;
#endif

extern patch_t *hu_font[HU_FONTSIZE];
extern patch_t *hu_font2[HU_FONTSIZE];
extern boolean message_dontfuckwithme;

extern boolean chat_on;		// in heads-up code

extern boolean sendsave;
extern int gamekeydown[];
//
// defaulted values
//
int mouseSensitivity;       // has default

// Show messages has default, 0 = off, 1 = on
int showMessages;
	
int sfxVolume;
int musicVolume;
int voiceVolume;

// Blocky mode, has default, 0 = high, 1 = normal
int screenblocks;		// has default

// temp for screenblocks (0-9)
int screenSize;		

// -1 = no quicksave slot picked!
//int quickSaveSlot;          

 // 1 = message to be printed
int messageToPrint;
// ...and here is the message string!
char *messageString;		

// message x & y
int messx;			
int messy;
int messageLastMenuActive;

// timed message = no input from user
boolean messageNeedsInput;     

void (*messageRoutine)(int response);

#if (APPVER_STRIFEREV >= AV_SR_STRF13)
boolean _int_8632C = false;
#endif

char gammamsg[5][26] =
{
	GAMMALVL0,
	GAMMALVL1,
	GAMMALVL2,
	GAMMALVL3,
	GAMMALVL4
};

// we are going to be entering a savegame string
int saveStringEnter;              
int saveSlot; // which slot to save in
int saveCharIndex; // which char we're editing
// old save description before edit
char saveOldString[SAVESTRINGSIZE];  

boolean inhelpscreens;
boolean menuactive;
boolean menu2active;
boolean talkactive;

#define SKULLXOFF -28
#define LINEHEIGHT 19

extern boolean sendpause;
char savegamestrings[7][SAVESTRINGSIZE];

char endstring[160];

short itemOn; // menu item skull is on
short skullAnimCounter; // skull animation counter
short whichSkull; // which skull to draw

// graphic name of skulls
char    skullName[8][8] = {"M_CURS1","M_CURS2","M_CURS3","M_CURS4","M_CURS5","M_CURS6","M_CURS7","M_CURS8"};

// current menudef
menu_t *currentMenu;

//
// PROTOTYPES
//
void M_NewGame(int choice);
void M_Episode(int choice);
void M_ChooseSkill(int choice);
void M_LoadGame(int choice);
void M_SaveGame(int choice);
void M_Options(int choice);
void M_EndGame(int choice);
void M_ReadThis(int choice);
void M_ReadThis2(int choice);
void M_ReadThis3(int choice);
void M_QuitDOOM(int choice);

void M_ChangeMessages(int choice);
void M_ChangeSensitivity(int choice);
void M_SfxVol(int choice);
void M_MusicVol(int choice);
void M_VoiceVol(int choice);
void M_ChangeDetail(int choice);
void M_SizeDisplay(int choice);
void M_StartGame(int choice);
void M_Sound(int choice);

void M_FinishReadThis(int choice);
void M_LoadSelect(int choice);
void M_SaveSelect(int choice);
void M_ReadSaveStrings(void);
void M_QuickSave(void);
void M_QuickLoad(void);

void M_DrawMainMenu(void);
void M_DrawReadThis1(void);
void M_DrawReadThis2(void);
void M_DrawReadThis3(void);
void M_DrawNewGame(void);
void M_DrawEpisode(void);
void M_DrawOptions(void);
void M_DrawSound(void);
void M_DrawLoad(void);
void M_DrawSave(void);
void M_DrawProfile(void);

void M_DrawSaveLoadBorder(int x, int y);
void M_SetupNextMenu(menu_t *menudef);
void M_DrawThermo(int x, int y, int thermWidth, int thermDot);
void M_DrawEmptyCell(menu_t *menu, int item);
void M_DrawSelCell(menu_t *menu, int item);
int M_WriteText(int x, int y, char* string);
int M_StringWidth(char *string);
int M_StringHeight(char *string);
void M_StartControlPanel(void);
void M_StartMessage(char *string, void *routine, boolean input);
void M_StopMessage(void);
void M_ClearMenus(void);




//
// DOOM MENU
//
enum
{
	newgame,
	options,
	loadgame,
	savegame,
	readthis,
	quitdoom,
	main_end
} main_e;

menuitem_t MainMenu[]=
{
	{1,"M_NGAME", M_NewGame, 'n'},
	{1,"M_OPTION",M_Options, 'o'},
	{1,"M_LOADG", M_LoadGame,'l'},
	{1,"M_SAVEG", M_SaveGame,'s'},
	{1,"M_RDTHIS",M_ReadThis,'h'},
	{1,"M_QUITG", M_QuitDOOM,'q'}
};

menu_t  MainDef =
{
	main_end,
	NULL,
	MainMenu,
	M_DrawMainMenu,
	97,45,
	0
};

//
// NEW GAME
//
enum
{
	killthings,
	toorough,
	hurtme,
	violence,
	nightmare,
	newg_end
} newgame_e;

menuitem_t NewGameMenu[]=
{
	{1,"M_JKILL",	M_ChooseSkill, 't'},
	{1,"M_ROUGH",	M_ChooseSkill, 'r'},
	{1,"M_HURT",	M_ChooseSkill, 'v'},
	{1,"M_ULTRA",	M_ChooseSkill, 'e'},
	{1,"M_NMARE",	M_ChooseSkill, 'b'}
};

menu_t  NewDef =
{
	newg_end,			// # of menu items
	&MainDef,			// previous menu
	NewGameMenu,		// menuitem_t ->
	M_DrawNewGame,		// drawing routine ->
	48,63,              // x,y
	toorough				// lastOn
};



//
// OPTIONS MENU
//
enum
{
	endgame,
	scrnsize,
	option_empty1,
	soundvol,
	opt_end
} options_e;

menuitem_t OptionsMenu[]=
{
	{1,"M_ENDGAM",	M_EndGame,'e'},
	{2,"M_SCRNSZ",	M_SizeDisplay,'s'},
	{-1,"",0},
	{1,"M_SVOL",	M_Sound,'s'}
};

menu_t  OptionsDef =
{
	opt_end,
	&MainDef,
	OptionsMenu,
	M_DrawOptions,
	60,37,
	0
};

//
// Read This! MENU 1 & 2
//
enum
{
	rdthsempty1,
	read1_end
} read_e;

menuitem_t ReadMenu1[] =
{
	{1,"",M_ReadThis2,0}
};

menu_t  ReadDef1 =
{
	read1_end,
	&MainDef,
	ReadMenu1,
	M_DrawReadThis1,
	250,185,
	0
};

menuitem_t ReadMenu2[]=
{
	{1,"",M_ReadThis3,0}
};

menu_t  ReadDef2 =
{
	read1_end,
	&ReadDef1,
	ReadMenu2,
	M_DrawReadThis2,
	250,185,
	0
};

menuitem_t ReadMenu3[]=
{
	{1,"",M_FinishReadThis,0}
};

menu_t  ReadDef3 =
{
	read1_end,
	&ReadDef2,
	ReadMenu3,
	M_DrawReadThis3,
	250,185,
	0
};

//
// SOUND VOLUME MENU
//
enum
{
	sfx_vol,
	sfx_empty1,
	music_vol,
	sfx_empty2,
	voice_vol,
	sfx_empty3,
	mousesens,
	sfx_empty4,
	sound_end
} sound_e;

menuitem_t SoundMenu[]=
{
	{2,"M_SFXVOL",M_SfxVol,'s'},
	{-1,"",0},
	{2,"M_MUSVOL",M_MusicVol,'m'},
	{-1,"",0},
	{2,"M_VOIVOL",M_VoiceVol,'v'},
	{-1,"",0},
	{2,"M_MSENS",M_ChangeSensitivity,'m'},
	{-1,"",0}
};

menu_t  SoundDef =
{
	sound_end,
	&OptionsDef,
	SoundMenu,
	M_DrawSound,
	80,35,
	0
};

//
// LOAD GAME MENU
//
enum
{
	load1,
	load2,
	load3,
	load4,
	load5,
	load6,
	load_end
} load_e;

menuitem_t LoadMenu[]=
{
	{1,"", M_LoadSelect,'1'},
	{1,"", M_LoadSelect,'2'},
	{1,"", M_LoadSelect,'3'},
	{1,"", M_LoadSelect,'4'},
	{1,"", M_LoadSelect,'5'},
	{1,"", M_LoadSelect,'6'}
};

menu_t  LoadDef =
{
	load_end,
	&MainDef,
	LoadMenu,
	M_DrawLoad,
	80,54,
	0
};

//
// SAVE GAME MENU
//
menuitem_t SaveMenu[]=
{
	{1,"", M_SaveSelect,'1'},
	{1,"", M_SaveSelect,'2'},
	{1,"", M_SaveSelect,'3'},
	{1,"", M_SaveSelect,'4'},
	{1,"", M_SaveSelect,'5'},
	{1,"", M_SaveSelect,'6'}
};

menu_t  SaveDef =
{
	load_end,
	&MainDef,
	SaveMenu,
	M_DrawSave,
	80,54,
	0
};

menu_t  ProfileDef =
{
	load_end,
	&NewDef,
	SaveMenu,
	M_DrawProfile,
	80,54,
	0
};


//
// M_ReadSaveStrings
//  read the strings from the savegame files
//
void M_ReadSaveStrings(void)
{
	int             handle;
	int             count;
	int             i;
	char    name[256];

	for (i = 0;i < load_end;i++)
	{
		if (M_CheckParm("-cdrom"))
			sprintf(name, "c:\\strife.cd\\strfsav%d.ssg\\name", i);
		else
			sprintf(name, "strfsav%d.ssg\\name", i);

		handle = open(name, O_RDONLY | 0, 0666);
		if (handle == -1)
		{
			strcpy(&savegamestrings[i][0], EMPTYSTRING);
			LoadMenu[i].status = 0;
			continue;
		}
		count = read(handle, &savegamestrings[i], SAVESTRINGSIZE);
		close(handle);
		LoadMenu[i].status = 1;
	}
}


void M_DrawProfile(void)
{
	int             i;

	M_WriteText(72, 28, "Name Your Character");
	for (i = 0;i < load_end; i++)
	{
		M_DrawSaveLoadBorder(LoadDef.x,LoadDef.y+LINEHEIGHT*i);
		M_WriteText(LoadDef.x,LoadDef.y+LINEHEIGHT*i,savegamestrings[i]);
	}
	
	if (saveStringEnter)
	{
		i = M_StringWidth(savegamestrings[saveSlot]);
		M_WriteText(LoadDef.x + i,LoadDef.y+LINEHEIGHT*saveSlot,"_");
	}
}

int menuskill;

void func_18A00(int a1)
{
#if (APPVER_STRIFEREV >= AV_SR_STRF13)
	_int_8632C = false;
#endif
	sendsave = true;
	func_1B2A8();
	G_SaveGame(a1, savegamestrings[a1]);
	saveSlot = a1;
	SaveDef.lastOn = a1;
	func_1B370();
	func_1B428(a1);
	if (shareware)
		G_DeferedInitNew(menuskill, 33);
	else
		G_DeferedInitNew(menuskill, 2);
	M_ClearMenus();
}


//
// M_LoadGame & Cie.
//
void M_DrawLoad(void)
{
	int             i;

	V_DrawPatchDirect (72,28,0,W_CacheLumpName("M_LOADG",PU_CACHE));
	for (i = 0;i < load_end; i++)
	{
		M_DrawSaveLoadBorder(LoadDef.x,LoadDef.y+LINEHEIGHT*i);
		M_WriteText(LoadDef.x,LoadDef.y+LINEHEIGHT*i,savegamestrings[i]);
	}
}



//
// Draw border for the savegame description
//
void M_DrawSaveLoadBorder(int x,int y)
{
	int             i;
	
	V_DrawPatchDirect (x-8,y+7,0,W_CacheLumpName("M_LSLEFT",PU_CACHE));

	for (i = 0;i < 24;i++)
	{
		V_DrawPatchDirect (x,y+7,0,W_CacheLumpName("M_LSCNTR",PU_CACHE));
		x += 8;
	}

	V_DrawPatchDirect (x,y+7,0,W_CacheLumpName("M_LSRGHT",PU_CACHE));
}



//
// User wants to load this game
//
void M_LoadSelect(int choice)
{
	char    name[256];
	
	G_SaveGame(choice, savegamestrings[choice]);
	func_1B524(choice);

	if (M_CheckParm("-cdrom"))
		sprintf(name,"c:\\strife.cd\\strfsav%d.ssg",choice);
	else
		sprintf(name,"strfsav%d.ssg",choice);
	G_LoadGame (name);
	saveSlot = choice;
	M_ClearMenus ();
}

//
// Selected from DOOM menu
//
void M_LoadGame (int choice)
{
	if (netgame)
	{
		M_StartMessage(LOADNET,NULL,false);
		return;
	}
	
	M_SetupNextMenu(&LoadDef);
	M_ReadSaveStrings();
}


//
//  M_SaveGame & Cie.
//
void M_DrawSave(void)
{
	int             i;
	
	V_DrawPatchDirect (72,28,0,W_CacheLumpName("M_SAVEG",PU_CACHE));
	for (i = 0;i < load_end; i++)
	{
		M_DrawSaveLoadBorder(LoadDef.x,LoadDef.y+LINEHEIGHT*i);
		M_WriteText(LoadDef.x,LoadDef.y+LINEHEIGHT*i,savegamestrings[i]);
	}
	
	if (saveStringEnter)
	{
		i = M_StringWidth(savegamestrings[saveSlot]);
		M_WriteText(LoadDef.x + i,LoadDef.y+LINEHEIGHT*saveSlot,"_");
	}
}

//
// M_Responder calls this when user is finished
//
void M_DoSave(int slot)
{
	if (slot < 0)
	{
		M_StartMessage(QSAVESPOT,NULL,false);
		return;
	}
	sendsave = true;
	G_SaveGame (slot,savegamestrings[slot]);
	M_ClearMenus ();
	saveSlot = slot;
	func_1B428(slot);
}

//
// User wants to save. Start string input for M_Responder
//
void M_SaveSelect(int choice)
{
	// we are going to be intercepting all chars
	saveStringEnter = 1;
    
	saveSlot = choice;
	strcpy(saveOldString,savegamestrings[choice]);
	if (!strcmp(savegamestrings[choice],EMPTYSTRING))
		savegamestrings[choice][0] = 0;
	saveCharIndex = strlen(savegamestrings[choice]);
}

//
// Selected from DOOM menu
//
void M_SaveGame (int choice)
{
	if (netgame)
	{
		M_StartMessage("You can't save a netgame",NULL,false);
		return;
	}
	if (!usergame)
	{
		M_StartMessage(SAVEDEAD,NULL,false);
		return;
	}
	
	if (gamestate != GS_LEVEL)
		return;

#if (APPVER_STRIFEREV >= AV_SR_STRF13)
	M_SetupNextMenu(&SaveDef);
#endif
	M_ReadSaveStrings();
#if (APPVER_STRIFEREV < AV_SR_STRF13)
	M_DoSave(saveSlot);
#endif
}



//
//      M_QuickSave
//
char    tempstring[80];

void M_QuickSaveResponse(int ch)
{
	if (ch == 'y')
	{
		M_DoSave(saveSlot);
		S_StartSound(NULL,sfx_mtalht);
	}
}

void M_QuickSave(void)
{
	if (netgame)
	{
		M_StartMessage("You can't save a netgame",NULL,false);
		return;
	}
	if (!usergame)
	{
		S_StartSound(NULL,sfx_oof);
		return;
	}

	if (gamestate != GS_LEVEL)
		return;
	
	if (saveSlot < 0)
	{
		M_StartControlPanel();
		M_ReadSaveStrings();
		M_SetupNextMenu(&SaveDef);
		saveSlot = -2;	// means to pick a slot now
		return;
	}
	sprintf(tempstring,QSPROMPT,savegamestrings[saveSlot]);
	M_StartMessage(tempstring,M_QuickSaveResponse,true);
}



//
// M_QuickLoad
//
void M_QuickLoadResponse(int ch)
{
	if (ch == 'y')
	{
		M_LoadSelect(saveSlot);
		S_StartSound(NULL,sfx_mtalht);
	}
}


void M_QuickLoad(void)
{
	if (netgame)
	{
		M_StartMessage(QLOADNET,NULL,false);
		return;
	}
	
	if (saveSlot < 0)
	{
		M_StartMessage(QSAVESPOT,NULL,false);
		return;
	}
	sprintf(tempstring,QLPROMPT,savegamestrings[saveSlot]);
	M_StartMessage(tempstring,M_QuickLoadResponse,true);
}




//
// Read This Menus
//
void M_DrawReadThis1(void)
{
	inhelpscreens = true;
	V_DrawPatchDirect (0,0,0,W_CacheLumpName("HELP1",PU_CACHE));
}

//
// Read This Menus - optional second page.
//
void M_DrawReadThis2(void)
{
	inhelpscreens = true;
	V_DrawPatchDirect (0,0,0,W_CacheLumpName("HELP2",PU_CACHE));
}

void M_DrawReadThis3(void)
{
	inhelpscreens = true;
	V_DrawPatchDirect (0,0,0,W_CacheLumpName("HELP3",PU_CACHE));
}


//
// Change Sfx & Music volumes
//
void M_DrawSound(void)
{
	V_DrawPatchDirect (100,10,0,W_CacheLumpName("M_SVOL",PU_CACHE));

	M_DrawThermo(SoundDef.x,SoundDef.y+LINEHEIGHT*(sfx_vol+1),
				 16,sfxVolume);

	M_DrawThermo(SoundDef.x,SoundDef.y+LINEHEIGHT*(music_vol+1),
				 16,musicVolume);

	M_DrawThermo(SoundDef.x,SoundDef.y+LINEHEIGHT*(voice_vol+1),
				 16,voiceVolume);

	M_DrawThermo(SoundDef.x,SoundDef.y+LINEHEIGHT*(mousesens+1),
				 10,mouseSensitivity);
}

void M_Sound(int choice)
{
	M_SetupNextMenu(&SoundDef);
}

void M_SfxVol(int choice)
{
	switch (choice)
	{
		case 0:
			if (sfxVolume)
				sfxVolume--;
			break;
		case 1:
			if (sfxVolume < 15)
				sfxVolume++;
			break;
	}
	
	S_SetSfxVolume(sfxVolume*8);
}

void M_MusicVol(int choice)
{
	switch (choice)
	{
		case 0:
			if (musicVolume)
				musicVolume--;
			break;
		case 1:
			if (musicVolume < 15)
				musicVolume++;
			break;
	}
	
    S_SetMusicVolume(musicVolume*8);
}

void M_VoiceVol(int choice)
{
	switch (choice)
	{
		case 0:
			if (voiceVolume)
				voiceVolume--;
			break;
		case 1:
			if (voiceVolume < 15)
				voiceVolume++;
			break;
	}
	
    S_SetVoiceVolume(voiceVolume*8);
}




//
// M_DrawMainMenu
//
void M_DrawMainMenu(void)
{
	V_DrawPatchDirect (84,2,0,W_CacheLumpName("M_STRIFE",PU_CACHE));
}




//
// M_NewGame
//
void M_DrawNewGame(void)
{
	V_DrawPatchDirect (96,14,0,W_CacheLumpName("M_NGAME",PU_CACHE));
	V_DrawPatchDirect (54,38,0,W_CacheLumpName("M_SKILL",PU_CACHE));
}

void M_NewGame(int choice)
{
	if (netgame && !demoplayback)
	{
		M_StartMessage(NEWGAME,NULL,false);
		return;
	}

#if (APPVER_STRIFEREV >= AV_SR_STRF13)
	_int_8632C = true;
#endif
	M_SetupNextMenu(&NewDef);
}


//
//      M_Episode
//

void M_ChooseSkill(int choice)
{
	menuskill = choice;
	M_SetupNextMenu(&ProfileDef);
	M_ReadSaveStrings();
}



//
// M_Options
//
char	msgNames[2][9]		= {"M_MSGOFF","M_MSGON"};


void M_DrawOptions(void)
{
	V_DrawPatchDirect (108,15,0,W_CacheLumpName("M_OPTION",PU_CACHE));
	
	M_DrawThermo(OptionsDef.x,OptionsDef.y+LINEHEIGHT*(scrnsize+1),
				 9,screenSize);
}

void M_Options(int choice)
{
	M_SetupNextMenu(&OptionsDef);
}



//
//      Toggle messages on/off
//

void func_19500(int choice)
{
	if (netgame || !usergame)
		return;

	players[consoleplayer].cheats ^= CF_4;

	if (players[consoleplayer].cheats & CF_4)
		players[consoleplayer].message = "Auto use health ON";
	else
		players[consoleplayer].message = "Auto use health OFF" ;
}

void M_ChangeMessages(int choice)
{
	// warning: unused parameter `int choice'
	choice = 0;
	showMessages ^= 1;

	if (!showMessages)
		players[consoleplayer].message = MSGOFF;
	else
		players[consoleplayer].message = MSGON ;
}


//
// M_EndGame
//
void M_EndGameResponse(int ch)
{
	if (ch != 'y')
		return;
		
	currentMenu->lastOn = itemOn;
	M_ClearMenus ();
	D_StartTitle ();
}

void M_EndGame(int choice)
{
	choice = 0;
	if (!usergame)
	{
		S_StartSound(NULL,sfx_oof);
		return;
	}

	if (netgame)
	{
		M_StartMessage(NETEND,NULL,false);
		return;
	}

	M_StartMessage(ENDGAME,M_EndGameResponse,true);
}




//
// M_ReadThis
//
void M_ReadThis(int choice)
{
	choice = 0;
	M_SetupNextMenu(&ReadDef1);
}

void M_ReadThis2(int choice)
{
	choice = 0;
	M_SetupNextMenu(&ReadDef2);
}

void M_ReadThis3(int choice)
{
	choice = 0;
	M_SetupNextMenu(&ReadDef3);
}

void M_FinishReadThis(int choice)
{
	choice = 0;
	M_ClearMenus();
}




//
// M_QuitDOOM
//
int     quitsounds[8] =
{
	sfx_pldeth,
	sfx_rebdth,
	sfx_pespna,
	sfx_slop,
	sfx_telept,
	sfx_agrdth,
	sfx_mislht,
	sfx_rifle
};


void func_196C0(int a1)
{
	if (usergame)
	{
		M_StartMessage("You have to end your game first.",NULL,false);
		return;
	}
	F_StartCast();
	M_ClearMenus();
}

void M_QuitResponse(int ch)
{
	char buf[9];
	if (ch != 'y')
		return;
	if (!netgame)
	{
		sprintf(buf, "qfmrm%i", (gametic % 8) + 1);
		I_StartVoice(buf);
		func_12A78();
		return;
	}
	I_Quit ();
}




void M_QuitDOOM(int choice)
{
	sprintf(endstring,"Do you really want to leave?\n\n(press y to quit)");

	M_StartMessage(endstring,M_QuitResponse,true);
}




void M_ChangeSensitivity(int choice)
{
	switch (choice)
	{
		case 0:
			if (mouseSensitivity)
				mouseSensitivity--;
			break;
		case 1:
			if (mouseSensitivity < 9)
				mouseSensitivity++;
			break;
	}
}




void M_SizeDisplay(int choice)
{
	switch (choice)
	{
	case 0:
		if (screenSize > 0)
		{
			screenblocks--;
			screenSize--;
		}
		break;
	case 1:
		if (screenSize < 8)
		{
			screenblocks++;
			screenSize++;
		}
		break;
	}
	

	R_SetViewSize (screenblocks);
}




//
//      Menu Functions
//
void M_DrawThermo(int x, int y, int thermWidth, int thermDot)
{
	int		xx;
	int		i;

	xx = x;
	V_DrawPatchDirect (xx,y+6,0,W_CacheLumpName("M_THERML",PU_CACHE));
	xx += 8;
	for (i=0;i<thermWidth;i++)
	{
		V_DrawPatchDirect (xx,y+6,0,W_CacheLumpName("M_THERMM",PU_CACHE));
		xx += 8;
	}
	V_DrawPatchDirect (xx,y+6,0,W_CacheLumpName("M_THERMR",PU_CACHE));

	V_DrawPatchDirect ((x+8) + thermDot*8,y+2,
						0,W_CacheLumpName("M_THERMO",PU_CACHE));
}


void M_StartMessage(char *string, void *routine, boolean input)
{
	messageLastMenuActive = menuactive;
	messageToPrint = 1;
	messageString = string;
	messageRoutine = routine;
	messageNeedsInput = input;
	menuactive = true;
	return;
}



void M_StopMessage(void)
{
	menuactive = messageLastMenuActive;
	messageToPrint = 0;
}



//
// Find string width from hu_font chars
//
int M_StringWidth(char *string)
{
	int             i;
	int             w = 0;
	int             c;
	
	for (i = 0;i < strlen(string);i++)
	{
		c = toupper(string[i]) - HU_FONTSTART;
		if (c < 0 || c >= HU_FONTSIZE)
			w += 4;
		else
			w += SHORT (hu_font[c]->width);
	}
		
	return w;
}



//
//      Find string height from hu_font chars
//
int M_StringHeight(char *string)
{
	int             i;
	int             h;
	int             height = SHORT(hu_font[0]->height);
	
	h = height;
	for (i = 0;i < strlen(string);i++)
		if (string[i] == '\n')
			h += height;
		
	return h;
}


//
//      Write a string using the hu_font
//
int M_WriteText(int x, int y, char *string)
{
	int		w;
	char*	ch;
	int		c;
	int		cx;
	int		cy;
		

	ch = string;
	cx = x;
	cy = y;
	
	while(1)
	{
		c = *ch++;
		if (!c)
			break;
		if (c == '\n')
		{
			cx = x;
			cy += 11;
			continue;
		}

		if (c == ' ' && cx == x)
			continue;
		
		c = toupper(c) - HU_FONTSTART;
		if (c < 0 || c>= HU_FONTSIZE)
		{
			cx += 4;
			continue;
		}
		
		w = SHORT (hu_font[c]->width);
		if (cx+w > 300)
		{
			cx = x;
			cy += 11;
			ch--;
			continue;
		}
		V_DrawPatchDirect(cx, cy, 0, hu_font[c]);
		cx+=w;
	}
	return cy+12;
}

void func_19A70(int x, int y, char *string, int vc)
{
	int		w;
	char*	ch;
	char	c;
	int		cx;
	int		cy;
	int vd;
	int v4;

	patch_t **vbp;

	if (vc)
		vbp = hu_font2;
	else
		vbp = hu_font;
		
	cx = 0;
	v4 = 300 - x;

	vd = 0;
	c = toupper(string[vd]);
	
	while(c != 0)
	{
		if (c == '\n')
		{
			cx = 0;
			c = toupper(string[++vd]);
			continue;
		}
		
		if (c < HU_FONTSTART || c> HU_FONTEND)
			w = 4;
		else
			w = SHORT (vbp[c-HU_FONTSTART]->width);
		if (cx+w < v4)
		{
			if (c != ' ' || cx != 0)
				cx += w;
		}
		else
		{
			while (string[vd] != ' ' && vd > 0)
			{
				cx -= w;
				c = toupper(string[--vd]);
				if (c < HU_FONTSTART || c > HU_FONTEND)
					w = 4;
				else
					w = SHORT (vbp[c-HU_FONTSTART]->width);
			}
			string[vd] = '\n';
			cx = 0;
		}
		c = toupper(string[++vd]);
	}
}



//
// CONTROL PANEL
//

//
// M_Responder
//
extern int mousefreeze;
extern void func_2EB54(int choice);
boolean M_Responder (event_t* ev)
{
	int             ch;
	int             i;
	static  int     joywait = 0;
	static  int     mousey = 0;
	static  int     lasty = 0;
	static  int     mousex = 0;
	static  int     lastx = 0;
	static  int     mousewait = 0;
	
	ch = -1;
	
	if (ev->type == ev_joystick && joywait < I_GetTime())
	{
		if (ev->data3 == -1)
		{
			ch = KEY_UPARROW;
			joywait = I_GetTime() + 5;
		}
		else if (ev->data3 == 1)
		{
			ch = KEY_DOWNARROW;
			joywait = I_GetTime() + 5;
		}
		
		if (ev->data2 == -1)
		{
			ch = KEY_LEFTARROW;
			joywait = I_GetTime() + 2;
		}
		else if (ev->data2 == 1)
		{
			ch = KEY_RIGHTARROW;
			joywait = I_GetTime() + 2;
		}
		
		if (ev->data1&1)
		{
			ch = KEY_ENTER;
			joywait = I_GetTime() + 5;
		}
		if (ev->data1&2)
		{
			ch = KEY_BACKSPACE;
			joywait = I_GetTime() + 5;
		}
	}
	else
	{
		if (ev->type == ev_mouse && mousewait < I_GetTime())
		{
			mousey += ev->data3;
			if (mousey < lasty-30)
			{
				ch = KEY_DOWNARROW;
				mousewait = I_GetTime() + 5;
				mousey = lasty -= 30;
			}
			else if (mousey > lasty+30)
			{
				ch = KEY_UPARROW;
				mousewait = I_GetTime() + 5;
				mousey = lasty += 30;
			}
		
			mousex += ev->data2;
			if (mousex < lastx-30)
			{
				ch = KEY_LEFTARROW;
				mousewait = I_GetTime() + 5;
				mousex = lastx -= 30;
			}
			else if (mousex > lastx+30)
			{
				ch = KEY_RIGHTARROW;
				mousewait = I_GetTime() + 5;
				mousex = lastx += 30;
			}
		
			if (ev->data1&1)
			{
				ch = KEY_ENTER;
				mousewait = I_GetTime() + 15;
				mousefreeze = 5;
			}
			
			if (ev->data1&2)
			{
			ch = KEY_BACKSPACE;
			mousewait = I_GetTime() + 15;
			}
		}
		else
			if (ev->type == ev_keydown)
			{
				ch = ev->data1;
			}
	}
    
	if (ch == -1)
		return false;

    
	// Save Game string input
	if (saveStringEnter)
	{
		switch(ch)
		{
			case KEY_BACKSPACE:
				if (saveCharIndex > 0)
				{
					saveCharIndex--;
					savegamestrings[saveSlot][saveCharIndex] = 0;
				}
				break;
				
			case KEY_ESCAPE:
				saveStringEnter = 0;
				strcpy(&savegamestrings[saveSlot][0],saveOldString);
				break;
				
			case KEY_ENTER:
				saveStringEnter = 0;
#if (APPVER_STRIFEREV >= AV_SR_STRF13)
				if (_int_8632C)
				{
					if (savegamestrings[saveSlot][0])
						func_18A00(saveSlot);
				}
				else
					M_DoSave(saveSlot);
#else
				if (savegamestrings[saveSlot][0])
					func_18A00(saveSlot);
#endif
				break;
				
			default:
				ch = toupper(ch);
				if (ch != 32)
					if (ch-HU_FONTSTART < 0 || ch-HU_FONTSTART >= HU_FONTSIZE)
						break;
				if (ch >= 32 && ch <= 127 &&
					saveCharIndex < SAVESTRINGSIZE-1 &&
					M_StringWidth(savegamestrings[saveSlot]) <
					(SAVESTRINGSIZE-2)*8)
				{
					savegamestrings[saveSlot][saveCharIndex++] = ch;
					savegamestrings[saveSlot][saveCharIndex] = 0;
				}
				break;
		}
		return true;
	}
    
	// Take care of any messages that need input
	if (messageToPrint)
	{
		if (messageNeedsInput == true &&
			!(ch == ' ' || ch == 'n' || ch == 'y' || ch == KEY_ESCAPE))
			return false;
		
		menuactive = messageLastMenuActive;
		messageToPrint = 0;
		if (messageRoutine)
			messageRoutine(ch);
			
		menuactive = menu2active = false;
		S_StartSound(NULL,sfx_mtalht);
		return true;
	}
		
    
	// F-Keys
	if (!menuactive)
		switch(ch)
		{
			case '1':
				if (devparm && !netgame && gamekeydown[KEY_RSHIFT])
				{
					G_DeferedInitNew(gameskill, gamemap);
					return true;
				}
				return false;

			case KEY_MINUS:         // Screen size down
				if (automapactive || chat_on)
					return false;
				M_SizeDisplay(0);
				S_StartSound(NULL,sfx_stnmov);
				return true;
				
			case KEY_EQUALS:        // Screen size up
				if (automapactive || chat_on)
					return false;
				M_SizeDisplay(1);
				S_StartSound(NULL,sfx_stnmov);
				return true;
				
			case KEY_F1:            // Help key
				M_StartControlPanel ();

				currentMenu = &ReadDef1;
	    
				itemOn = 0;
				S_StartSound(NULL,sfx_swtchn);
				return true;
				
			case KEY_F2:            // Save
#if (APPVER_STRIFEREV >= AV_SR_STRF13)
				_int_8632C = false;
#endif
				if (!netgame && players[consoleplayer].health > 0 && (players[consoleplayer].cheats & CF_3) == 0)
				{
					M_StartControlPanel();
					S_StartSound(NULL,sfx_swtchn);
					M_SaveGame(0);
				}
				else
					S_StartSound(NULL,sfx_oof);
				return true;
				
			case KEY_F3:            // Load
#if (APPVER_STRIFEREV >= AV_SR_STRF13)
				_int_8632C = false;
#endif
#if (APPVER_STRIFEREV >= AV_SR_STRF131)
				M_StartControlPanel();
				M_LoadGame(0);
#endif
				S_StartSound(NULL,sfx_swtchn);
#if (APPVER_STRIFEREV < AV_SR_STRF131)
				M_QuickLoad();
#endif
				return true;
				
			case KEY_F4:            // Sound Volume
				M_StartControlPanel ();
				currentMenu = &SoundDef;
				itemOn = sfx_vol;
				S_StartSound(NULL,sfx_swtchn);
				return true;
				
			case KEY_F5:            // Detail toggle
				func_19500(0);
				S_StartSound(NULL,sfx_swtchn);
				return true;
				
			case KEY_F6:            // Quicksave
#if (APPVER_STRIFEREV >= AV_SR_STRF13)
				_int_8632C = false;
#endif
				if (!netgame && players[consoleplayer].health > 0 && (players[consoleplayer].cheats & CF_3) == 0)
				{
					S_StartSound(NULL,sfx_swtchn);
					M_QuickSave();
				}
				else
					S_StartSound(NULL,sfx_oof);
				return true;
				
			case KEY_F7:            // End game
				S_StartSound(NULL,sfx_swtchn);
				M_EndGame(0);
				return true;
				
			case KEY_F8:            // Toggle messages
				M_ChangeMessages(0);
				S_StartSound(NULL,sfx_swtchn);
				return true;
				
			case KEY_F9:            // Quickload
#if (APPVER_STRIFEREV < AV_SR_STRF131)
				G_ScreenShot ();
#else
				_int_8632C = false;
				S_StartSound(NULL,sfx_swtchn);
				M_QuickLoad();
#endif
				return true;
				
			case KEY_F10:           // Quit DOOM
				S_StartSound(NULL,sfx_swtchn);
				M_QuitDOOM(0);
				return true;
				
			case KEY_F11:           // gamma toggle
				usegamma++;
				if (usegamma > 4)
					usegamma = 0;
				players[consoleplayer].message = gammamsg[usegamma];
				I_SetPalette (W_CacheLumpName ("PLAYPAL",PU_CACHE));
				return true;

#if (APPVER_STRIFEREV >= AV_SR_STRF131)
			case KEY_F12:
				G_ScreenShot();
				return true;
#endif
		}

    
	// Pop-up menu?
	if (!menuactive)
	{
		if (ch == KEY_ESCAPE)
		{
			M_StartControlPanel ();
			S_StartSound(NULL,sfx_swtchn);
			return true;
		}
		return false;
	}

    
	// Keys usable within menu
	switch (ch)
	{
		case KEY_DOWNARROW:
			do
			{
				if (itemOn+1 > currentMenu->numitems-1)
					itemOn = 0;
				else itemOn++;
				S_StartSound(NULL,sfx_pstop);
			} while(currentMenu->menuitems[itemOn].status==-1);
			return true;
		
		case KEY_UPARROW:
			do
			{
				if (!itemOn)
					itemOn = currentMenu->numitems-1;
				else itemOn--;
				S_StartSound(NULL,sfx_pstop);
			} while(currentMenu->menuitems[itemOn].status==-1);
			return true;

		case KEY_LEFTARROW:
			if (currentMenu->menuitems[itemOn].routine &&
				currentMenu->menuitems[itemOn].status == 2)
			{
				S_StartSound(NULL,sfx_stnmov);
				currentMenu->menuitems[itemOn].routine(0);
			}
			return true;
		
		case KEY_RIGHTARROW:
			if (currentMenu->menuitems[itemOn].routine &&
				currentMenu->menuitems[itemOn].status == 2)
			{
				S_StartSound(NULL,sfx_stnmov);
				currentMenu->menuitems[itemOn].routine(1);
			}
			return true;

		case KEY_ENTER:
		case 0xd1:
			if (currentMenu->menuitems[itemOn].routine &&
				currentMenu->menuitems[itemOn].status)
			{
				currentMenu->lastOn = itemOn;
				if (currentMenu->menuitems[itemOn].status == 2)
				{
					currentMenu->menuitems[itemOn].routine(1);      // right arrow
					S_StartSound(NULL,sfx_stnmov);
				}
				else
				{
					currentMenu->menuitems[itemOn].routine(itemOn);
					//S_StartSound(NULL,sfx_pistol);
				}
			}
			return true;
		
		case KEY_ESCAPE:
#if (APPVER_STRIFEREV >= AV_SR_STRF13)
			_int_8632C = false;
#endif
			if (talkactive)
				func_2EB54(-1);
			currentMenu->lastOn = itemOn;
			M_ClearMenus ();
			S_StartSound(NULL,sfx_mtalht);
			return true;
		
		case 0xc9:
		case KEY_BACKSPACE:
			currentMenu->lastOn = itemOn;
			if (currentMenu->prevMenu)
			{
				currentMenu = currentMenu->prevMenu;
				itemOn = currentMenu->lastOn;
				S_StartSound(NULL,sfx_swtchn);
			}
			return true;
	
		default:
			for (i = itemOn+1;i < currentMenu->numitems;i++)
			if (currentMenu->menuitems[i].alphaKey == ch)
			{
				itemOn = i;
				S_StartSound(NULL,sfx_pstop);
				return true;
			}
			for (i = 0;i <= itemOn;i++)
				if (currentMenu->menuitems[i].alphaKey == ch)
				{
					itemOn = i;
					S_StartSound(NULL,sfx_pstop);
					return true;
				}
			break;
	
	}

	return false;
}



//
// M_StartControlPanel
//
void M_StartControlPanel (void)
{
	// intro might call this repeatedly
	if (menuactive)
		return;

	menuactive = 1;
	menu2active = 1;
	currentMenu = &MainDef;         // JDC
	itemOn = currentMenu->lastOn;   // JDC
}


//
// M_Drawer
// Called after the view has been rendered,
// but before it has been blitted.
//
void M_Drawer (void)
{
	static short	x;
	static short	y;
	short		i;
	short		max;
	char		string[40];
	int			start;

	inhelpscreens = false;

    
	// Horiz. & Vertically center string and print it.
	if (messageToPrint)
	{
		start = 0;
		y = 100 - M_StringHeight(messageString)/2;
		while(*(messageString+start))
		{
			for (i = 0;i < strlen(messageString+start);i++)
			if (*(messageString+start+i) == '\n')
			{
				memset(string,0,40);
				strncpy(string,messageString+start,i);
				break;
			}
				
			if (i == strlen(messageString+start))
			{
				strcpy(string,messageString+start);
				start += i;
			}
			else
			{
				start += i + 1;
			}
				
			x = 160 - M_StringWidth(string)/2;
			M_WriteText(x,y,string);
			y += SHORT(hu_font[0]->height);
		}
		return;
	}

	if (!menuactive)
		return;

	if (currentMenu->routine)
		currentMenu->routine();         // call Draw routine
    
	// DRAW MENU
	x = currentMenu->x;
	y = currentMenu->y;
	max = currentMenu->numitems;

	for (i=0;i<max;i++)
	{
		if (currentMenu->menuitems[i].name[0])
			V_DrawPatchDirect (x,y,0,
		W_CacheLumpName(currentMenu->menuitems[i].name ,PU_CACHE));
		y += LINEHEIGHT;
	}

    
	// DRAW SKULL
	V_DrawPatchDirect(x + SKULLXOFF,currentMenu->y - 5 + itemOn*LINEHEIGHT, 0,
	W_CacheLumpName(skullName[whichSkull],PU_CACHE));

}


//
// M_ClearMenus
//
void M_ClearMenus (void)
{
	menuactive = 0;
	menu2active = 0;
	// if (!netgame && usergame && paused)
	//       sendpause = true;
}




//
// M_SetupNextMenu
//
void M_SetupNextMenu(menu_t *menudef)
{
	currentMenu = menudef;
	itemOn = currentMenu->lastOn;
}


//
// M_Ticker
//
void M_Ticker (void)
{
	if (--skullAnimCounter <= 0)
	{
		whichSkull = (whichSkull+1)%8;
		skullAnimCounter = 5;
	}
}


//
// M_Init
//
void M_Init (void)
{
	currentMenu = &MainDef;
	menuactive = 0;
	itemOn = currentMenu->lastOn;
	whichSkull = 0;
	skullAnimCounter = 10;
	screenSize = screenblocks - 3;
	messageToPrint = 0;
	messageString = NULL;
	messageLastMenuActive = menuactive;
	saveSlot = -1;

	G_SaveGame(5, "ME");
	func_1B2A8();
}
