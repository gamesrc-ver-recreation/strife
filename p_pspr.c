//
// Copyright (C) 1993-1996 Id Software, Inc.
// Copyright (C) 2022 Nuke.YKT
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
//

// P_pspr.c

#include "DoomDef.h"
#include "P_local.h"
#include "soundst.h"

#define LOWERSPEED		FRACUNIT*6
#define RAISESPEED		FRACUNIT*6

#define WEAPONBOTTOM	128*FRACUNIT
#define WEAPONTOP		32*FRACUNIT


#define	BFGCELLS		40			// plasma cells for a bfg attack


/* 
================== 
= 
= P_SetPsprite
= 
================== 
*/ 

void P_SetPsprite (player_t *player, int position, statenum_t stnum) 
{
	pspdef_t	*psp;
	state_t	*state;
	
	psp = &player->psprites[position];
	
	do
	{
		if (!stnum)
		{
			psp->state = NULL;
			break;		// object removed itself
		}
		state = &states[stnum];
		psp->state = state;
		psp->tics = state->tics;  // could be 0

#if 0
		if (state->misc1)
		{
			// coordinate set
			psp->sx = state->misc1 << FRACBITS;
			psp->sy = state->misc2 << FRACBITS;
		}
#endif

		// call action routine
		if (state->action)
		{
			state->action (player, psp);
			if (!psp->state)
				break;
		}
		stnum = psp->state->nextstate;
	} while (!psp->tics);	// an initial state of 0 could cycle through
}


/*
===============================================================================
						PSPRITE ACTIONS
===============================================================================
*/

const weaponinfo_t	weaponinfo[NUMWEAPONS] =
{
	{	// fist
		am_noammo,		// ammo
		S_PNCH_15,		// upstate 
		S_PNCH_14,	// downstate
		S_PNCH_13,		// readystate
		S_PNCH_16,		// atkstate
		S_NULL,			// flashstate
		true
	},
	{	// pistol
		am_1,		// ammo
		S_XBOW_23,		// upstate 
		S_XBOW_22,	// downstate
		S_XBOW_21,		// readystate
		S_XBOW_24,		// atkstate
		S_NULL,	// flashstate
		true
	},
	{	// shotgun
		am_0,		// ammo
		S_RIFG_56,		// upstate 
		S_RIFG_55,		// downstate
		S_RIFG_54,			// readystate
		S_RIFF_57,		// atkstate
		S_NULL,	// flashstate
		true
	},
	{	// chaingun
		am_4,		// ammo
		S_MMIS_46,		// upstate 
		S_MMIS_45,	// downstate
		S_MMIS_44,		// readystate
		S_MMIS_47,		// atkstate
		S_NULL,	// flashstate
		false
	},
	{	// missile
		am_5,		// ammo
		S_GREN_97,	// upstate 
		S_GREN_96,	// downstate
		S_GREN_95,		// readystate
		S_GREN_98,		// atkstate
		S_GREF_103,	// flashstate
		false
	},
	{	// plasma
		am_3,		// ammo
		S_FLMT_65,		// upstate 
		S_FLMT_64,	// downstate
		S_FLMT_62,		// readystate
		S_FLMF_66,		// atkstate
		S_NULL,	// flashstate
		true
	},
	{	// bfg 9000
		am_3,		// ammo
		S_BLST_73,		// upstate 
		S_BLST_72,		// downstate
		S_BLST_68,			// readystate
		S_BLSF_74,			// atkstate
		S_NULL,		// flashstate
		false
	},
	{	// saw
		am_noammo,		// ammo
		S_SIGH_123,		// upstate 
		S_SIGH_122,		// downstate
		S_SIGH_117,			// readystate
		S_SIGH_124,			// atkstate
		S_SIGF_128,			// flashstate
		false
	},
	{
		// super shotgun
		am_2,		// ammo
		S_XBOW_36,		// upstate 
		S_XBOW_35,	// downstate
		S_XBOW_34,		// readystate
		S_XBOW_37,		// atkstate
		S_NULL,	// flashstate
		true
	},
	{
		//
		am_6,		// ammo
		S_GREN_108,		// upstate 
		S_GREN_107,	// downstate
		S_GREN_106,		// readystate
		S_GREN_109,		// atkstate
		S_GREF_114,	// flashstate
		false
	},
	{
		//
		am_3,		// ammo
		S_BLST_87,		// upstate 
		S_BLST_86,	// downstate
		S_BLST_82,		// readystate
		S_BLST_88,		// atkstate
		S_NULL,	// flashstate
		false
	},
};

/*
================
=
= P_BringUpWeapon
=
= Starts bringing the pending weapon up from the bottom of the screen
= Uses player
================
*/

void P_BringUpWeapon (player_t *player)
{
	statenum_t	new;
	
	if (player->pendingweapon == wp_nochange)
		player->pendingweapon = player->readyweapon;
		
	if (player->pendingweapon == wp_5)
		S_StartSound (player->mo, sfx_flidl);
		
	new = weaponinfo[player->pendingweapon].upstate;

	player->psprites[ps_weapon].sy = WEAPONBOTTOM;
	P_SetPsprite (player, ps_weapon, new);

	if (player->pendingweapon == wp_1)
		P_SetPsprite(player, ps_flash, S_XBOW_31);
	else if (player->pendingweapon == wp_7 && player->f_45)
		P_SetPsprite(player, ps_flash, S_SIGH_117+player->f_45);
	else
		P_SetPsprite(player, ps_flash, S_NULL);
	player->pendingweapon = wp_nochange;
}

/*
================
=
= P_CheckAmmo
=
= returns true if there is enough ammo to shoot
= if not, selects the next weapon to use
================
*/

boolean P_CheckAmmo (player_t *player)
{
	ammotype_t	ammo;
	int			count;

	ammo = weaponinfo[player->readyweapon].ammo;
	if (player->readyweapon == wp_10)
		count = 30;
	else if (player->readyweapon == wp_6)
		count = 20;
	else
		count = 1;
	if (ammo == am_noammo || player->ammo[ammo] >= count)
		return true;
		
// out of ammo, pick a weapon to change to
	if (player->weaponowned[wp_6] && player->ammo[am_3] >= 20)
		player->pendingweapon = wp_6;
	else if (player->weaponowned[wp_2] && player->ammo[am_0])
		player->pendingweapon = wp_2;
	else if (player->weaponowned[wp_1] && player->ammo[am_1])
		player->pendingweapon = wp_1;
	else if (player->weaponowned[wp_3] && player->ammo[am_4])
		player->pendingweapon = wp_3;
	else if (player->weaponowned[wp_5] && player->ammo[am_3])
		player->pendingweapon = wp_5;
	else if (player->weaponowned[wp_4] && player->ammo[am_5])
		player->pendingweapon = wp_4;
	else if (player->weaponowned[wp_8] && player->ammo[am_2])
		player->pendingweapon = wp_8;
	else if (player->weaponowned[wp_9] && player->ammo[am_6])
		player->pendingweapon = wp_9;
	else if (player->weaponowned[wp_10] && player->ammo[am_3] >= 30)
		player->pendingweapon = wp_10;
	else
		player->pendingweapon = wp_0;
	
	P_SetPsprite (player, ps_weapon
		, weaponinfo[player->readyweapon].downstate);

	return false;	
}


/*
================
=
= P_FireWeapon
=
================
*/

void P_FireWeapon (player_t *player)
{
	statenum_t	new;
	
	if (!P_CheckAmmo (player))
		return;

	P_SetMobjState (player->mo, S_PLAY_292);
	new = weaponinfo[player->readyweapon].atkstate;
	P_SetPsprite (player, ps_weapon, new);
	switch (player->readyweapon)
	{
		case wp_0:
		case wp_1:
			return;
		case wp_8:
			return;
	}
	P_NoiseAlert (player->mo, player->mo);
}


/*
================
=
= P_DropWeapon
=
= Player died, so put the weapon away
================
*/

void P_DropWeapon (player_t *player)
{
	P_SetPsprite (player, ps_weapon
	, weaponinfo[player->readyweapon].downstate);
}


/*
=================
=
= A_WeaponReady
=
= The player can fire the weapon or change to another weapon at this time
=
=================
*/
	
void A_WeaponReady (player_t *player, pspdef_t *psp)
{	
	statenum_t	new;
	int			angle;
	
    // get out of attack state
	if (player->mo->state == &states[S_PLAY_292] || player->mo->state == &states[S_PLAY_293] )
	{
		P_SetMobjState (player->mo, S_PLAY_287);
	}

	if (player->readyweapon == wp_5 && psp->state == &states[S_FLMT_62])
	{
		S_StartSound (player->mo, sfx_flidl);
	}

//
// check for change
//  if player is dead, put the weapon away
//
	if (player->pendingweapon != wp_nochange || !player->health)
	{
	// change weapon (pending weapon should allready be validated)
		new = weaponinfo[player->readyweapon].downstate;
		P_SetPsprite (player, ps_weapon, new);
		return;	
	}
	
//
// check for fire
//
// the missile launcher and bfg do not auto fire
	if (player->cmd.buttons & BT_ATTACK)
	{
		if ( !player->attackdown
			|| (player->readyweapon != wp_3 && player->readyweapon != wp_10) )
		{
			player->attackdown = true;
			P_FireWeapon (player);		
			return;
		}
	}
	else
		player->attackdown = false;
	
//
// bob the weapon based on movement speed
//
	angle = (128*leveltime)&FINEMASK;
	psp->sx = FRACUNIT + FixedMul (player->bob, finecosine[angle]);
	angle &= FINEANGLES/2-1;
	psp->sy = WEAPONTOP + FixedMul (player->bob, finesine[angle]);
}


/*
=================
=
= A_ReFire
=
= The player can re fire the weapon without lowering it entirely
=
=================
*/
	
void A_ReFire (player_t *player, pspdef_t *psp)
{	
//
// check for fire (if a weaponchange is pending, let it go through instead)
//
	if ( (player->cmd.buttons & BT_ATTACK)
	&& player->pendingweapon == wp_nochange && player->health)
	{
		player->refire++;
		P_FireWeapon (player);
	}
	else
	{
		player->refire = 0;
		P_CheckAmmo (player);
	}
}


void A_CheckReload (player_t *player, pspdef_t *psp)
{
	P_CheckAmmo (player);
	if (player->readyweapon == wp_1)
		P_SetPsprite (player, ps_flash, S_XBOW_31);
#if 0
	if (player->ammo[am_shell]<2)
		P_SetPsprite (player, ps_weapon, S_DSNR1);
#endif
}


/*
=================
=
= A_Lower
=
=================
*/
	
void A_Lower (player_t *player, pspdef_t *psp)
{	
	psp->sy += LOWERSPEED;
	if (psp->sy < WEAPONBOTTOM )
		return;
	if (player->playerstate == PST_DEAD)
	{
		psp->sy = WEAPONBOTTOM;
		return;		// don't bring weapon back up
	}
		
//
// The old weapon has been lowered off the screen, so change the weapon
// and start raising it
//
	if (!player->health)
	{	// player is dead, so keep the weapon off screen
		P_SetPsprite (player,  ps_weapon, S_NULL);
		return;	
	}
	
	player->readyweapon = player->pendingweapon; 

	P_BringUpWeapon (player);
}


/*
=================
=
= A_Raise
=
=================
*/
	
void A_Raise (player_t *player, pspdef_t *psp)
{
	statenum_t	new;
	
	psp->sy -= RAISESPEED;

	if (psp->sy > WEAPONTOP )
		return;
	psp->sy = WEAPONTOP;	
	
//
// the weapon has been raised all the way, so change to the ready state
//
	new = weaponinfo[player->readyweapon].readystate;

	P_SetPsprite (player, ps_weapon, new);
}


/*
================
=
= A_GunFlash
=
=================
*/

void A_GunFlash (player_t *player, pspdef_t *psp) 
{
	P_SetMobjState (player->mo, S_PLAY_293);
	P_SetPsprite (player,ps_flash,weaponinfo[player->readyweapon].flashstate);
}


/*
===============================================================================
						WEAPON ATTACKS
===============================================================================
*/

void func_27114(player_t *player, pspdef_t *psp)
{
	angle_t		angle;
	int			damage;
	int			slope;
	
	damage = (P_Random ()&(7+(player->f_1e3/10)))*(player->f_1e3/10+2);
	if (player->powers[pw_0])	
		damage *= 10;
	angle = player->mo->angle+((P_Random()-P_Random())<<18);
	slope = P_AimLineAttack (player->mo, angle, 80*FRACUNIT);
	P_LineAttack (player->mo, angle, 80*FRACUNIT, slope, damage);
// turn to face target
	if (linetarget)
	{
		if (linetarget->flags & MF_NOBLOOD)
			S_StartSound (player->mo, sfx_mtalht);
		else
			S_StartSound (player->mo, sfx_meatht);
		player->mo->angle = R_PointToAngle2 (player->mo->x, player->mo->y,
		linetarget->x, linetarget->y);
		player->mo->flags |= MF_JUSTATTACKED;
		func_1EAE0(player->mo, linetarget);
	}
	else
		S_StartSound(player->mo, sfx_swish);
}

void func_27204(player_t *player, pspdef_t *psp)
{
	mobj_t *th;
	P_SetMobjState(player->mo, S_PLAY_293);
	player->ammo[weaponinfo[player->readyweapon].ammo]--;
	player->mo->angle += (P_Random()-P_Random())<<18;
	th = P_SpawnPlayerMissile (player->mo, MT_112);
	th->momz += 5*FRACUNIT;
}

void func_27264(player_t *player, pspdef_t *psp)
{
	int v4;
	angle_t oa;
	v4 = 19 - (player->f_1e1*5)/100;
	oa = player->mo->angle;
	player->mo->angle += (P_Random()-P_Random())<<v4;
	P_SetMobjState(player->mo, S_PLAY_293);
	player->ammo[weaponinfo[player->readyweapon].ammo]--;
	P_SpawnPlayerMissile (player->mo, MT_99);
	player->mo->angle = oa;
}

void func_272F4(player_t *player, pspdef_t *psp)
{
	P_SetMobjState(player->mo, S_PLAY_293);
	P_DamageMobj(player->mo, player->mo, NULL, 20);
	player->ammo[weaponinfo[player->readyweapon].ammo] -= 30;
	P_SpawnPlayerMissile(player->mo, MT_110);
	P_Thrust(player, player->mo->angle+ANG180, 0x7d000);
}

void func_2735C(player_t *player, pspdef_t *psp)
{
	mobjtype_t vbp;
	angle_t angle;
	mobj_t *th;
	int radius;
	if (player->readyweapon == wp_4)
		vbp = MT_106;
	else if (player->readyweapon == wp_9)
		vbp = MT_107;
	player->ammo[weaponinfo[player->readyweapon].ammo]--;
	P_SetPsprite(player, ps_flash, (psp->state
		+ weaponinfo[player->readyweapon].flashstate) - &states[weaponinfo[player->readyweapon].atkstate]);
	player->mo->z += 32*FRACUNIT;
	th = func_2D96C(player->mo, vbp);
	player->mo->z -= 32*FRACUNIT;
	th->momz = FixedMul(th->info->speed, (player->f_51 << 16) / 160) + 8*FRACUNIT;
	if (th->info->seesound)
		S_StartSound(th, th->info->seesound);

	angle = player->mo->angle >> ANGLETOFINESHIFT;
	radius = player->mo->info->radius+mobjinfo[vbp].radius+4*FRACUNIT;

	th->x += FixedMul(radius, finecosine[angle]);
	th->y += FixedMul(radius, finesine[angle]);

	radius = 15*FRACUNIT;
	if (psp->state == &states[weaponinfo[player->readyweapon].atkstate])
		angle = player->mo->angle-ANG90;
	else
		angle = player->mo->angle+ANG90;

	angle >>= ANGLETOFINESHIFT;
	th->x += FixedMul(radius, finecosine[angle]);
	th->y += FixedMul(radius, finesine[angle]);

	th->flags |= MF_24;

}

void func_27544(player_t *player, pspdef_t *psp)
{
	int s = 18 - (player->f_1e1 * 5) / 100;
	int oa = player->mo->angle;
	player->mo->angle += (P_Random()-P_Random())<<s;
	player->ammo[weaponinfo[player->readyweapon].ammo]--;
	P_SpawnPlayerMissile (player->mo, MT_102);
	player->mo->angle = oa;
	S_StartSound(player->mo, sfx_xbow);
}

void func_275C8(player_t *player, pspdef_t *psp)
{
	int s = 18 - (player->f_1e1 * 5) / 100;
	int oa = player->mo->angle;
	player->mo->angle += (P_Random()-P_Random())<<s;
	player->ammo[weaponinfo[player->readyweapon].ammo]--;
	P_SpawnPlayerMissile (player->mo, MT_103);
	player->mo->angle = oa;
	S_StartSound(player->mo, sfx_xbow);
}

/*
===============
=
= P_BulletSlope
=
= Sets a slope so a near miss is at aproximately the height of the
= intended target
=
===============
*/
fixed_t bulletslope;

void P_BulletSlope (mobj_t *mo)
{
	angle_t		an;

//
// see which target is to be aimed at
//
	an = mo->angle;
	bulletslope = P_AimLineAttack (mo, an, 16*64*FRACUNIT);
	if (!linetarget)
	{
		an += 1<<26;
		bulletslope = P_AimLineAttack (mo, an, 16*64*FRACUNIT);
		if (!linetarget)
		{
			an -= 2<<26;
			bulletslope = P_AimLineAttack (mo, an, 16*64*FRACUNIT);
		}
	}
	if (linetarget)
		mo->target = linetarget;
}

/*
===============
=
= P_GunShot
=
===============
*/

void P_GunShot (mobj_t *mo, boolean accurate)
{
	angle_t		angle;
	int s;
	
	s = 20 - (mo->player->f_1e1 * 5) / 100;
	angle = mo->angle;
	if (!accurate)
		angle += (P_Random()-P_Random())<<s;
	s = 4*(P_Random ()%3+1);
	P_LineAttack (mo, angle, MISSILERANGE, bulletslope, s);
}

void func_2773C(player_t *player, pspdef_t *psp)
{
	S_StartSound (player->mo, sfx_rifle);

	if (!player->ammo[weaponinfo[player->readyweapon].ammo])
		return;
	
	P_SetMobjState (player->mo, S_PLAY_293);	
	player->ammo[weaponinfo[player->readyweapon].ammo]--;

	P_BulletSlope (player->mo);

	P_GunShot (player->mo, !player->refire);
}

void func_277B8(player_t *player, pspdef_t *psp)
{
	int			i;
	angle_t		angle;
	int			damage;

	if (player->ammo[weaponinfo[player->readyweapon].ammo] < 20)
		return;
	player->ammo[weaponinfo[player->readyweapon].ammo] -= 20;

	P_BulletSlope (player->mo);
	S_StartSound(player->mo, sfx_pgrdat);

	for (i = 0; i < 20; i++)
	{
		damage = 5*(P_Random ()%3+1);
		angle = player->mo->angle;
		angle += (P_Random()-P_Random())<<19;
		P_LineAttack (player->mo, angle, 0x8400000,
			bulletslope + ((P_Random()-P_Random())<<5), damage);
	}
}

void func_27864(player_t *player, pspdef_t *psp)
{
	S_StartSound(player->mo, sfx_siglup);
	A_Light2(player, psp);
}

void func_27894(player_t *player, pspdef_t *psp)
{
	int oa;
	mobj_t *th;
	angle_t angle;
	int i;

	oa = player->armortype;
	player->armortype = 0;

	P_DamageMobj(player->mo,  player->mo, NULL, (player->f_45+1)*4);
	player->armortype = oa;
	S_StartSound(player->mo, sfx_siglup);
	switch (player->f_45)
	{
		case 0:
			P_BulletSlope(player->mo);
			if (linetarget)
			{
				th = P_SpawnMobj(linetarget->x, linetarget->y, ONFLOORZ, MT_88);
				th->tracer = linetarget;
			}
			else
			{
				th = P_SpawnMobj(player->mo->x, player->mo->y, player->mo->z, MT_88);
				angle = player->mo->angle;
				angle >>= ANGLETOFINESHIFT;
				th->momx += FixedMul(28 * FRACUNIT, finecosine[angle]);
				th->momy += FixedMul(28 * FRACUNIT, finesine[angle]);
			}
			th->target = player->mo;
			th->health = -1;
			break;
		case 1:
			th = P_SpawnPlayerMissile(player->mo, MT_78);
			th->health = -1;
			break;
		case 2:
			player->mo->angle -= ANG90;
			for (i = 0; i < 20; i++)
			{
				player->mo->angle += ANG180/20;
				th = func_2D96C(player->mo, MT_80);
				th->z = player->mo->z+32*FRACUNIT;
				th->health = -1;
			}
			player->mo->angle -= ANG90;
			break;
		case 3:
			P_BulletSlope(player->mo);
			if (linetarget)
			{
				th = P_SpawnPlayerMissile(player->mo, MT_89);
				th->tracer = linetarget;
			}
			else
			{
				th = P_SpawnPlayerMissile(player->mo, MT_89);
				angle = player->mo->angle;
				angle >>= ANGLETOFINESHIFT;
				th->momx += FixedMul(th->info->speed, finecosine[angle]);
				th->momy += FixedMul(th->info->speed, finesine[angle]);
			}
			th->health = -1;
			break;
		case 4:
			th = P_SpawnPlayerMissile(player->mo, MT_84);
			th->health = -1;
			if (!linetarget)
			{
				angle = player->f_51;
				angle >>= ANGLETOFINESHIFT;
				th->momz += FixedMul(th->info->speed, finesine[angle]);
			}
			break;
	}

}

void func_27AD0(player_t *player, pspdef_t *psp)
{
	if (player->readyweapon == wp_7 && player->f_45)
		P_SetPsprite(player, ps_flash, S_SIGH_117+player->f_45);
	else
		P_SetPsprite(player, ps_flash, S_NULL);
}


/*============================================================================= */


void A_Light0 (player_t *player, pspdef_t *psp)
{
	player->extralight = 0;
}

void A_Light1 (player_t *player, pspdef_t *psp)
{
	player->extralight = 1;
}

void A_Light2 (player_t *player, pspdef_t *psp)
{
	player->extralight = 2;
}

void A_Light3 (player_t *player, pspdef_t *psp)
{
	player->extralight = -3;
}


void func_27B34(mobj_t *mo)
{
	int i;
	mobj_t *th;
	mo->angle += ANG180;
	for (i = 0; i < 80; i++)
	{
		mo->angle += ANG180/40;
		th = func_2D96C(mo, MT_111);
		th->target = mo->target;
	}
}


void func_27B90(player_t *player, pspdef_t *psp)
{
	S_StartSound(player->mo, MT_87);
	psp->sx += (P_Random()-P_Random())<<10;
	psp->sy += (P_Random()-P_Random())<<10;
}

/*============================================================================= */

/* 
================== 
= 
= P_SetupPsprites
= 
= Called at start of level for each player
================== 
*/ 
 
void P_SetupPsprites (player_t *player) 
{
	int	i;
	
// remove all psprites

	for (i=0 ; i<1 ; i++)
		player->psprites[i].state = NULL;
		
// spawn the gun
	player->pendingweapon = player->readyweapon;
	P_BringUpWeapon (player);
}



/* 
================== 
= 
= P_MovePsprites 
= 
= Called every tic by player thinking routine
================== 
*/ 

void P_MovePsprites (player_t *player) 
{
	int			i;
	pspdef_t	*psp;
	state_t		*state;

	psp = &player->psprites[0];
	for (i=0 ; i<NUMPSPRITES ; i++, psp++)
		if ( psp->state != 0)		// a null state means not active
		{
		// drop tic count and possibly change state
			if (psp->tics != -1)	// a -1 tic count never changes
			{
				psp->tics--;
				if (!psp->tics)
					P_SetPsprite (player, i, psp->state->nextstate);
			}				
		}
	
	player->psprites[ps_flash].sx = player->psprites[ps_weapon].sx;
	player->psprites[ps_flash].sy = player->psprites[ps_weapon].sy;

	player->psprites[ps_3].sx = (160<<16) - ((100 - player->f_1e1) << 16);
	player->psprites[ps_4].sx = (160<<16) + ((100 - player->f_1e1) << 16);
}
