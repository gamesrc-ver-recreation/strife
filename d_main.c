//
// Copyright (C) 1993-1996 Id Software, Inc.
// Copyright (C) 2022 Nuke.YKT
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
//

// D_main.c

#define	BGCOLOR		2
#define	FGCOLOR		0

#ifdef __WATCOMC__
#include <dos.h>
#include <graph.h>
#include <sys\types.h>
#include <unistd.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include "DoomDef.h"
#include "soundst.h"
#include "DUtils.h"

extern int _wp1, _wp2, _wp3, _wp4, _wp5, _wp6, _wp7;

#define MAXWADFILES 20
char *wadfiles[MAXWADFILES];

boolean shareware;		// true if only episode 1 present
boolean registered;

boolean devparm;            // started game with -devparm
boolean nomonsters;			// checkparm of -nomonsters
boolean respawnparm;			// checkparm of -respawn
boolean fastparm;				// checkparm of -fastparm

boolean drone;

boolean modifiedgame;

boolean singletics = false; // debug flag to cancel adaptiveness

int loadcounter = 0;
boolean graph = true;

byte *startbot;

boolean testgame;
boolean randomp;
boolean flipparm;
boolean killfl;


extern int soundVolume;
extern  int	sfxVolume;
extern  int	musicVolume;
extern  int	voiceVolume;

extern  boolean	inhelpscreens;

skill_t startskill;
int startmap;
boolean autostart;

FILE *debugfile;

boolean advancedemo;




char wadfile[1024];		// primary wad file
char mapdir[1024];      // directory of development maps
char basedefault[1024]; // default file


void D_CheckNetGame(void);
void D_ProcessEvents(void);
void G_BuildTiccmd(ticcmd_t *cmd);
void D_DoAdvanceDemo(void);
void D_PageDrawer (void);
void D_AdvanceDemo (void);
void F_Drawer(void);
boolean F_Responder(event_t *ev);

/*
===============================================================================

							EVENT HANDLING

Events are asyncronous inputs generally generated by the game user.

Events can be discarded if no responder claims them

===============================================================================
*/

event_t events[MAXEVENTS];
int eventhead;
int eventtail;

/*
================
=
= D_PostEvent
=
= Called by the I/O functions when input is detected
=
================
*/

void D_PostEvent (event_t *ev)
{
	events[eventhead] = *ev;
	eventhead = (++eventhead)&(MAXEVENTS-1);
}

/*
================
=
= D_ProcessEvents
=
= Send all the events of the given timestamp down the responder chain
=
================
*/

void D_ProcessEvents (void)
{
	event_t		*ev;

	for ( ; eventtail != eventhead ; eventtail = (++eventtail)&(MAXEVENTS-1) )
	{
		ev = &events[eventtail];
		if (M_Responder(ev))
			continue;               // menu ate the event
		G_Responder(ev);
	}
}

/*
================
=
= FixedDiv
=
================
*/

fixed_t FixedDiv (fixed_t a, fixed_t b)
{
	if ( (abs(a)>>14) >= abs(b))
		return (a^b)<0 ? MININT : MAXINT;
	return FixedDiv2 (a,b);
}

/*
================
=
= D_Display
=
= draw current display, possibly wiping it from the previous
=
================
*/

// wipegamestate can be set to -1 to force a wipe on the next draw
gamestate_t wipegamestate = 1;
extern boolean setsizeneeded;
extern int showMessages;
void R_ExecuteSetViewSize (void);

void D_Display (void)
{
	static boolean viewactivestate = false;
	static boolean menuactivestate = false;
	static boolean inhelpscreensstate = false;
	static boolean fullscreen = false;
	static boolean _int_861D0 = false;
	static gamestate_t oldgamestate = -1;
	static int borderdrawcount;
	int nowtime;
	int tics;
	int wipestart;
	int y;
	boolean done;
	boolean wipe;
	boolean redrawsbar;

	redrawsbar = false;

	// Change the view size if needed
	if (setsizeneeded)
	{
		R_ExecuteSetViewSize ();
		oldgamestate = -1;                      // force background redraw
		borderdrawcount = 3;
	}

	// save the current screen if about to wipe
	if (gamestate != wipegamestate)
	{
		wipe = true;
		wipe_StartScreen(0, 0, SCREENWIDTH, SCREENHEIGHT);
	}
	else
		wipe = false;

	if (gamestate == GS_LEVEL && gametic)
		HU_Erase();

//
// do buffered drawing
//
	switch (gamestate)
	{
	case GS_LEVEL:
		if (!gametic)
			break;
		if (automapactive)
			AM_Drawer ();
		if (wipe || (viewheight != 200 && fullscreen) )
			redrawsbar = true;
		if (menuactivestate || (inhelpscreensstate && !inhelpscreens) )
			redrawsbar = true;              // just put away the help screen
		fullscreen = viewheight == 200;
		ST_Drawer (fullscreen, redrawsbar );
		break;
	case GS_INTERMISSION:
		break;
	case GS_FINALE:
		F_Drawer ();
		break;
	case GS_DEMOSCREEN:
		D_PageDrawer ();
		break;
	}
    
	// draw buffered stuff to screen
	I_UpdateNoBlit ();
	
	// draw the view directly
	if (gamestate == GS_LEVEL && !automapactive && gametic)
		R_RenderPlayerView (&players[displayplayer]);
	
	// clean up border stuff
	if (gamestate != oldgamestate && gamestate != GS_LEVEL)
		I_SetPalette (W_CacheLumpName ("PLAYPAL",PU_CACHE));
	
	// see if the border needs to be initially drawn
	if (gamestate == GS_LEVEL && oldgamestate != GS_LEVEL)
	{
		viewactivestate = false;        // view was not active
		R_FillBackScreen ();    // draw the pattern into the back screen
	}
	
	// see if the border needs to be updated to the screen
	if (gamestate == GS_LEVEL && !automapactive && scaledviewwidth != 320)
	{
		if (menuactive || menuactivestate || !viewactivestate)
		{
			borderdrawcount = 3;
			_int_861D0 = false;
		}
		if (borderdrawcount)
		{
			R_DrawViewBorder ();    // erase old menu stuff
			borderdrawcount--;
		}
	
	}
	
	menuactivestate = menuactive;
	viewactivestate = viewactive;
	inhelpscreensstate = inhelpscreens;
	oldgamestate = wipegamestate = gamestate;
	
	if (gamestate == GS_LEVEL && gametic)
	{
		HU_Drawer ();
		if (func_38A1C ())
			_int_861D0 = true;
		else if (_int_861D0)
		{
			_int_861D0 = false;
			menuactivestate = true;
		}
	}
	
	// draw pause pic
	if (paused)
	{
		if (automapactive)
			y = 4;
		else
			y = viewwindowy+4;
		V_DrawPatchDirect(viewwindowx+(scaledviewwidth-68)/2,
						  y,0,W_CacheLumpName ("M_PAUSE", PU_CACHE));
	}
	
	
	// menus go directly to the screen
	M_Drawer ();          // menu is drawn even on top of everything
	NetUpdate ();         // send out any new accumulation
	
	
	// normal update
	if (!wipe)
	{
		I_FinishUpdate ();              // page flip or blit buffer
		return;
	}
	
	// wipe update
	wipe_EndScreen(0, 0, SCREENWIDTH, SCREENHEIGHT);
	
	wipestart = I_GetTime () - 1;
	
	do
	{
		do
		{
			nowtime = I_GetTime ();
			tics = nowtime - wipestart;
		} while (tics < 3);
		wipestart = nowtime;
		done = wipe_ScreenWipe(wipe_ColorXForm
							   , 0, 0, SCREENWIDTH, SCREENHEIGHT, tics);
		I_UpdateNoBlit ();
		M_Drawer ();                            // menu is drawn even on top of wipes
		I_FinishUpdate ();                      // page flip or blit buffer
	} while (!done);
}

/*
================
=
= D_DoomLoop
=
================
*/

void D_DoomLoop (void)
{
	if (demorecording)
		G_BeginRecording ();

	if (M_CheckParm ("-debugfile"))
	{
		char	filename[20];
		sprintf (filename, "debug%i.txt", consoleplayer);
		printf ("debug output to: %s\n", filename);
		debugfile = fopen (filename,"w");
	}
	I_InitGraphics ();
	while (1)
	{
		// frame syncronous IO operations
		I_StartFrame();

		// process one or more tics
		if (singletics)
		{
			I_StartTic ();
			D_ProcessEvents ();
			G_BuildTiccmd (&netcmds[consoleplayer][maketic%BACKUPTICS]);
			if (advancedemo)
				D_DoAdvanceDemo ();
			M_Ticker ();
			G_Ticker ();
			gametic++;
			maketic++;
		}
		else
		{
			// will run at least one tic
			TryRunTics ();
		}

		// move positional sounds
		S_UpdateSounds(players[consoleplayer].mo);
		D_Display();
	}
}

/*
===============================================================================

						DEMO LOOP

===============================================================================
*/

int             demosequence;
int             pagetic;
char            *pagename;


/*
================
=
= D_PageTicker
=
= Handles timing for warped projection
=
================
*/

void D_PageTicker (void)
{
	if (--pagetic < 0)
		D_AdvanceDemo ();
}


/*
================
=
= D_PageDrawer
=
================
*/

void D_PageDrawer (void)
{
	V_DrawPatch (0,0, 0, W_CacheLumpName(pagename, PU_CACHE));
}

/*
=================
=
= D_AdvanceDemo
=
= Called after each demo or intro demosequence finishes
=================
*/

void D_AdvanceDemo (void)
{
	advancedemo = true;
}

void D_DoAdvanceDemo (void)
{
	players[consoleplayer].playerstate = PST_LIVE;  // don't reborn
	advancedemo = false;
	usergame = false;               // can't save / end game here
	paused = false;
	gameaction = ga_nothing;
	switch (demosequence)
	{
		case -5:
			I_Quit ();
			return;
		case -4:
			menuactive = false;
			pagetic = 35 * 3;
			gamestate = GS_DEMOSCREEN;
			pagename = "PANEL7";
			S_StartMusic(mus_fast_5);
			if (shareware)
				demosequence = -3;
			else
				demosequence = -5;
			return;
		case -3:
			pagetic = 35 * 6;
			gamestate = GS_DEMOSCREEN;
			pagename = "vellogo";
			demosequence = -5;
			return;
		case -2:
			pagetic = 35 * 6;
			gamestate = GS_DEMOSCREEN;
			pagename = "TITLEPIC";
			S_StartMusic(mus_logo_1);
			demosequence = -1;
			return;
		case -1:
			pagetic = 10;
			gamestate = GS_DEMOSCREEN;
			pagename = "PANEL0";
			S_StartSound(NULL, sfx_rb2act);
			wipegamestate = -1;
			break;
		case 0:
			pagetic = 35 * 4;
			gamestate = GS_DEMOSCREEN;
			pagename = "RGELOGO";
			wipegamestate = -1;
			break;
		case 1:
			pagetic = 35 * 7;
			gamestate = GS_DEMOSCREEN;
			pagename = "PANEL1";
			I_StartVoice("pro1");
			S_StartMusic(mus_intro_6);
			break;
		case 2:
			pagetic = 35 * 9;
			gamestate = GS_DEMOSCREEN;
			pagename = "PANEL2";
			I_StartVoice("pro2");
			break;
		case 3:
			pagetic = 35 * 12;
			gamestate = GS_DEMOSCREEN;
			pagename = "PANEL3";
			I_StartVoice("pro3");
			break;
		case 4:
			pagetic = 35 * 11;
			gamestate = GS_DEMOSCREEN;
			pagename = "PANEL4";
			I_StartVoice("pro4");
			break;
		case 5:
			pagetic = 35 * 10;
			gamestate = GS_DEMOSCREEN;
			pagename = "PANEL5";
			I_StartVoice("pro5");
			break;
		case 6:
			pagetic = 35 * 16;
			gamestate = GS_DEMOSCREEN;
			pagename = "PANEL6";
			I_StartVoice("pro6");
			break;
		case 7:
			pagetic = 35 * 9;
			gamestate = GS_DEMOSCREEN;
			pagename = "TITLEPIC";
			wipegamestate = -1;
			break;
		case 8:
			func_1B2A8();
			pagetic = 35 * 9;
			G_DeferedPlayDemo("demo1");
			break;
		case 9:
			pagetic = 35 * 6;
			gamestate = GS_DEMOSCREEN;
			pagename = "vellogo";
			wipegamestate = -1;
			break;
		case 10:
			gamestate = GS_DEMOSCREEN;
			pagetic = 35 * 12;
			pagename = "CREDIT";
			wipegamestate = -1;
			break;
	}
	demosequence++;
	if (demosequence > 11)
		demosequence = -2;
	if (demosequence == 7)
		demosequence++;
	if (demosequence == 9)
		demosequence++;
}


/*
=================
=
= D_StartTitle
=
=================
*/

void D_StartTitle (void)
{
	gamestate = GS_DEMOSCREEN;
	gameaction = ga_nothing;
	demosequence = -2;
	D_AdvanceDemo ();
}

void func_12A78 (void)
{
	gameaction = ga_nothing;
	demosequence = -4;
	D_AdvanceDemo ();
}

char title[128] = "Conversation ON"; //      print title for every printed line

int func_2DAB0(void)
{
	union REGS regs;
	regs.h.ah = 3;
	regs.h.bh = 0;
	int386(0x10, &regs, &regs);
	return regs.h.dl;
}

int func_2DAE0(void)
{
	union REGS regs;
	regs.h.ah = 3;
	regs.h.bh = 0;
	int386(0x10, &regs, &regs);
	return regs.h.dh;
}

void SetCursor (int a1, int a2)
{
	union REGS regs;
	regs.h.ah = 2;
	regs.h.bh = 0;
	regs.h.dh = a2;
	regs.h.dl = a1;
	int386(0x10, &regs, &regs);
}

void func_2DB40(char *a1, int a2, int a3)
{
	union REGS regs;
	byte v4;
	int vsi;
	int v8;
	int vc;

	v4 = (a3 << 4) | a2;
	vsi = func_2DAB0();
	v8 = func_2DAE0();

	for (vc = 0; vc < strlen(a1); vc++)
	{
		regs.h.ah = 9;
		regs.h.al = a1[vc];
		regs.w.cx = 1;
		regs.h.bl = v4;
		regs.h.bh = 0;
		int386(0x10, &regs, &regs);
		if (++vsi > 79)
			vsi = 0;

		SetCursor (vsi, v8);
	}
}

void mprintf(char *a1)
{
	int vc, vsi;
	if (devparm)
	{
		vc = func_2DAB0 ();
		vsi = func_2DAE0 ();

		SetCursor (0, 0);

		func_2DB40 (title, FGCOLOR, BGCOLOR);

		SetCursor (vc, vsi);

		printf (a1);
	}
	else
	{
		DrawLoadBackground ();
		UpdateLoadScreen ();
		if (!graph)
		{
			SetCursor (0, 20);
			func_2DB40 (a1, FGCOLOR, BGCOLOR);
		}
	}
}

byte *startlogo;
byte *startp[4];
byte *startlz[2];

void DrawLoadBackground (void)
{
	char buffer[80];
	if (!graph)
	{
		memset(0xb8000, 0, 4000);
		SetCursor (0, 0);
		func_2DB40 (title, FGCOLOR, BGCOLOR);

		sprintf(buffer, "Rogue Entertainment");
		SetCursor (40 - (strlen(buffer)>>1), 5);
		func_2DB40 (buffer, 2, 1);

		sprintf(buffer, "and");
		SetCursor (40 - (strlen(buffer)>>1), 7);
		func_2DB40 (buffer, 2, 1);

		sprintf(buffer, "Velocity Games");
		SetCursor (40 - (strlen(buffer)>>1), 9);
		func_2DB40 (buffer, 2, 1);

		sprintf(buffer, "present");
		SetCursor (40 - (strlen(buffer)>>1), 11);
		func_2DB40 (buffer, 2, 1);

		sprintf(buffer, "S T R I F E");
		SetCursor (40 - (strlen(buffer)>>1), 14);
		func_2DB40 (buffer, 2, 1);

		sprintf(buffer, "Loading...");
		SetCursor (40 - (strlen(buffer)>>1), 17);
		func_2DB40 (buffer, 2, 1);

		sprintf(buffer, "[                                                  ]");
		SetCursor (14, 18);
		func_2DB40 (buffer, 2, 1);
	}
	else
	{
		startlogo = W_CacheLumpName("STARTUP0", PU_STATIC);
		startp[0] = W_CacheLumpName("STRTPA1", PU_STATIC);
		startp[1] = W_CacheLumpName("STRTPB1", PU_STATIC);
		startp[2] = W_CacheLumpName("STRTPC1", PU_STATIC);
		startp[3] = W_CacheLumpName("STRTPD1", PU_STATIC);
		startlz[0] = W_CacheLumpName("STRTLZ1", PU_STATIC);
		startlz[1] = W_CacheLumpName("STRTLZ2", PU_STATIC);
		startbot = W_CacheLumpName("STRTBOT", PU_STATIC);
		memset(0xa0000, 240, SCREENWIDTH*SCREENHEIGHT);
		memcpy(0xa0000+320*41, startlogo+320*57, 320*95);
	}
}

void UpdateLoadScreen(void)
{
	char *dest;
	char *vp;
	int vd;
	int v8;
	int vbp;
	if (!graph)
	{
		vd = (loadcounter * 50) / 69;
		if (vd > 50)
			vd = 50;
		for (vbp = 0; vbp < vd; vbp++)
		{
			SetCursor (15+vbp, 18);
			func_2DB40 ("#", 1, 2);
		}
	}
	else
	{
		vd = (loadcounter * 200) / 69 + 60;
		if (vd > 200)
			vd = 200;
		dest = (byte*)(0xa0000+320*156+60);
		for (vbp = 0; vbp < 16; vbp++)
		{
			memset(dest, 240, 200);
		}
		vp = startlz[vd % 2];
		dest = (byte*)(0xa0000+320*156+vd);
		for (vbp = 0; vbp < 16; vbp++)
		{
			memcpy(dest, vp, 16);
			dest += 320; vp += 16;
		}
		if (vd > 10)
		{
			vp = startbot;
			v8 = vd % 5 - 2;
			dest = (byte*)(0xa0000+320*138+14);
			for (vbp = 0; vbp < v8; vbp++)
			{
				memset(dest, 240, 48);
				dest += 320;
			}
			for (vbp = 0; vbp < 48; vbp++)
			{
				memcpy(dest, vp, 48);
				dest += 320; vp += 48;
			}
			for (vbp = 0; vbp < 5 - v8; vbp++)
			{
				memset(dest, 240, 48);
				dest += 320;
			}
		}
		vp = startp[vd % 4];
		dest = (byte*)(0xa0000+320*136+262);
		for (vbp = 0; vbp < 64; vbp++)
		{
			memcpy(dest, vp, 32);
			dest += 320; vp += 32;
		}
	}
}

void AdvanceLoadScreen(void)
{
	if (devparm)
		return;
	loadcounter++;
	if (loadcounter < 69)
		UpdateLoadScreen();
	else
		S_StartSound(NULL, sfx_psdtha);
}

/*
===============
=
= D_AddFile
=
===============
*/

void D_AddFile(char *file)
{
	int numwadfiles;
	char *new;

	for(numwadfiles = 0; wadfiles[numwadfiles]; numwadfiles++);
	new = malloc(strlen(file)+1);
	strcpy(new, file);

	wadfiles[numwadfiles] = new;
}


#define DEVMAPS "f:/st/data/"
#define DEVDATA "c:/localid/"
#define DEVTALK "f:/st/talk/"

boolean novoice = false;

/*
=================
=
= IdentifyVersion
=
= Checks availability of IWAD files by name,
= to determine whether registered/commercial features
= should be executed (notably loading PWAD's).
=================
*/
void IdentifyVersion (void)
{
	char	*strife0wad, *strife1wad;
	strcpy(basedefault,"strife.cfg");
	strife0wad = "strife0.wad";
	strife1wad = "strife1.wad";
	if (M_CheckParm ("-shdev"))
	{
		autostart = true;
		registered = false;
		shareware = true;
		devparm = true;
		D_AddFile (DEVDATA"strife0.wad");
		D_AddFile (DEVTALK"script00.lmp");
		D_AddFile (DEVTALK"script32.lmp");
		D_AddFile (DEVTALK"script33.lmp");
		D_AddFile (DEVTALK"script34.lmp");
		D_AddFile (DEVMAPS"map32.wad");
		D_AddFile (DEVMAPS"map33.wad");
		D_AddFile (DEVMAPS"map34.wad");
		strcpy (basedefault,DEVDATA"strife.cfg");
		return;
	}
	
	if (M_CheckParm ("-regdev"))
	{
		autostart = true;
		registered = true;
		devparm = true;
		D_AddFile (DEVDATA"voices.wad");
		D_AddFile (DEVDATA"strife1.wad");
		D_AddFile (DEVTALK"script02.lmp");
		D_AddFile (DEVTALK"script07.lmp");
		D_AddFile (DEVTALK"script08.lmp");
		D_AddFile (DEVTALK"script10.lmp");
		D_AddFile (DEVTALK"script11.lmp");
		D_AddFile (DEVTALK"script12.lmp");
		D_AddFile (DEVTALK"script14.lmp");
		D_AddFile (DEVTALK"script15.lmp");
		D_AddFile (DEVTALK"script16.lmp");
		D_AddFile (DEVTALK"script18.lmp");
		D_AddFile (DEVTALK"script23.lmp");
		strcpy (basedefault,DEVDATA"strife.cfg");
		return;
	}
	
	if ( !access (strife1wad,R_OK) )
	{
		registered = true;
		if ( !access ("voices.wad",R_OK) && !novoice )
			D_AddFile ("voices.wad");
		else
		{
			novoice = true;
			if (devparm)
				printf("Voices disabled.\n");
		}
		D_AddFile (strife1wad);
		return;
	}
	
	if ( !access (strife0wad,R_OK) )
	{
		shareware = true;
		D_AddFile (strife0wad);
		return;
	}
	
	printf ("Game mode indeterminate.\nI cannot find any .WAD files here!\n");
	exit (1);
}

void CheckBetaTest (void)
{
	struct dosdate_t date;
	int serial;
	int d, y, m;

	serial = atoi (W_CacheLumpName ("serial", PU_CACHE));
	_dos_getdate (&date);
	if (date.year > 1996 || (date.day > 15 && date.month > 4) )
		I_Error("Data error! Corrupted WAD File!");

	y = serial / 10000;
	m = serial / 100 - y * 100;
	d = serial - m * 100 - y * 10000;
	if (date.year < y || (date.day < d && date.month < m) )
		I_Error("Bad wadfile");
}

/*
=================
=
= Find a Response File
=
=================
*/
void FindResponseFile (void)
{
	int i;
#define MAXARGVS 100
	
	for (i = 1;i < myargc;i++)
		if (myargv[i][0] == '@')
		{
			FILE *handle;
			int size;
			int k;
			int index;
			int indexinfile;
			char *infile;
			char *file;
			char *moreargs[20];
			char *firstargv;
		
			// READ THE RESPONSE FILE INTO MEMORY
			handle = fopen (&myargv[i][1],"rb");
			if (!handle)
			{
				printf ("\nNo such response file!");
				exit(1);
			}
			printf("Found response file %s!\n",&myargv[i][1]);
			fseek (handle,0,SEEK_END);
			size = ftell(handle);
			fseek (handle,0,SEEK_SET);
			file = malloc (size);
			fread (file,size,1,handle);
			fclose (handle);
		
			// KEEP ALL CMDLINE ARGS FOLLOWING @RESPONSEFILE ARG
			for (index = 0,k = i+1; k < myargc; k++)
				moreargs[index++] = myargv[k];
		
			firstargv = myargv[0];
			myargv = malloc(sizeof(char *)*MAXARGVS);
			memset(myargv,0,sizeof(char *)*MAXARGVS);
			myargv[0] = firstargv;
		
			infile = file;
			indexinfile = k = 0;
			indexinfile++;  // SKIP PAST ARGV[0] (KEEP IT)
			do
			{
				myargv[indexinfile++] = infile+k;
				while(k < size &&
					((*(infile+k)>= ' '+1) && (*(infile+k)<='z')))
					k++;
				*(infile+k) = 0;
				while(k < size &&
					((*(infile+k)<= ' ') || (*(infile+k)>'z')))
					k++;
			} while(k < size);
		
			for (k = 0;k < index;k++)
				myargv[indexinfile++] = moreargs[k];
			myargc = indexinfile;
	
			// DISPLAY ARGS
			printf("%d command-line args:\n",myargc);
			for (k=1;k<myargc;k++)
				printf("%s\n",myargv[k]);
	
			break;
		}
}

/*
=================
=
= D_DoomMain
=
=================
*/

void D_DoomMain (void)
{
	union REGS regs;
	char file[256];
	int p;
	int i;
	int serial;

	FindResponseFile ();

	if (M_CheckParm ("-nograph"))
		graph = false;

	if (M_CheckParm ("-cdrom"))
	{
		printf ("CD-ROM Version: \nAccessing strife.cd\n");
		strcpy (basedefault,"c:\\strife.cd\\strife.cfg");
		mkdir ("c:\\strife.cd", 0);
		for (i = 0; i < 8; i++)
		{
			sprintf (file, "c:\\strife.cd\\strfsav%d.ssg", i);
			mkdir (file, 0);
		}
	}
	else
	{
		for (i = 0; i <= 6; i++)
		{
			sprintf (file, "strfsav%d.ssg", i);
			mkdir (file, 0);
		}
	}
	devparm = M_CheckParm ("-devparm");
	if (devparm)
		graph = false;

	IdentifyVersion ();
	
	setbuf (stdout, NULL);
	modifiedgame = false;
	
	nomonsters = M_CheckParm ("-nomonsters") > 0;
	testgame = M_CheckParm ("-work");
	flipparm = M_CheckParm ("-flip");
	respawnparm = M_CheckParm ("-respawn") > 0;
	randomp = M_CheckParm ("-random") > 0;
	fastparm = M_CheckParm ("-fast");
	if (M_CheckParm ("-altdeath"))
		deathmatch = 2;
	else if (M_CheckParm ("-net"))
		deathmatch = 1;

#if (APPVER_STRIFEREV < AV_SR_STRF12)
	sprintf (title,
			 "                      "
			 "STRIFE:  Quest for the Sigil v%i.%i"
			 "                                  ",
			 VERSION/100, VERSION%100);
#elif (APPVER_STRIFEREV < AV_SR_STRF13)
	sprintf (title,
			 "                      "
			 "STRIFE:  Quest for the Sigil v1.2"
			 "                                  ");
#elif (APPVER_STRIFEREV < AV_SR_STRF131)
	sprintf (title,
			 "                      "
			 "STRIFE:  Quest for the Sigil v1.3"
			 "                                  ");
#else
	sprintf (title,
			 "                      "
			 "STRIFE:  Quest for the Sigil v1.31"
			 "                                 ");
#endif

	printf ("\n%s\n", title);
	
	if (devparm)
		printf(D_DEVSTR);
	
	// turbo option
	if ( (p=M_CheckParm ("-turbo")) )
	{
		int  scale = 200;
		extern int forwardmove[2];
		extern int sidemove[2];
	
		if (p<myargc-1)
			scale = atoi (myargv[p+1]);
		if (scale < 10)
			scale = 10;
		if (scale > 400)
			scale = 400;
		if (devparm)
			printf ("turbo scale: %i%%\n",scale);
		forwardmove[0] = forwardmove[0]*scale/100;
		forwardmove[1] = forwardmove[1]*scale/100;
		sidemove[0] = sidemove[0]*scale/100;
		sidemove[1] = sidemove[1]*scale/100;
	}
	
	// add any files specified on the command line with -file wadfile
	// to the wad list
	//
	// convenience hack to allow -wart e m to add a wad file
	// prepend a tilde to the filename so wadfile will be reloadable
	p = M_CheckParm ("-wart");
	if (p)
	{
		myargv[p][4] = 'p';     // big hack, change to -warp

		// Map name handling.
		p = atoi (myargv[p+1]);
		if (p<10)
			sprintf (file,"~"DEVMAPS"map0%i.wad", p);
		else
			sprintf (file,"~"DEVMAPS"map%i.wad", p);
		D_AddFile (file);
	}
	
	p = M_CheckParm ("-file");
	if (p)
	{
		// the parms after p are wadfile/lump names,
		// until end of parms or another - preceded parm
		modifiedgame = true;            // homebrew levels
		while (++p != myargc && myargv[p][0] != '-')
			D_AddFile (myargv[p]);
	}
	
	p = M_CheckParm ("-playdemo");
	
	if (!p)
		p = M_CheckParm ("-timedemo");
	
	if (p && p < myargc-1)
	{
		sprintf (file,"%s.lmp", myargv[p+1]);
		D_AddFile (file);
		if (devparm)
			printf("Playing demo %s.lmp.\n",myargv[p+1]);
	}
	
	// get skill / episode / map from parms
	startskill = sk_easy;
	if (shareware)
		startmap = 33;
	else
		startmap = 2;
		
	p = M_CheckParm ("-skill");
	if (p && p < myargc-1)
	{
		startskill = myargv[p+1][0]-'1';
		autostart = true;
	}
	
	p = M_CheckParm ("-timer");
	if (p && p < myargc-1 && deathmatch)
	{
		int time;
		time = atoi(myargv[p+1]);
		if (devparm)
			printf("Cannot change levels until after %d minute",time);
		if (time>1)
			printf("s");
		if (devparm)
			printf(".\n");
	}
	
	p = M_CheckParm ("-avg");
	if (p && p < myargc-1 && deathmatch && devparm)
		printf("Austin Virtual Gaming: Levels will end after 20 minutes\n");
	
	p = M_CheckParm ("-warp");
	if (p && p < myargc-1)
	{
		startmap = atoi (myargv[p+1]);
		if (shareware && startmap < 32)
		{
			startmap += 31;
			if (startmap > 34)
				startmap = 34;
		}
		autostart = true;
	}
	
	// init subsystems
	V_Init ();
	
	M_LoadDefaults ();              // load before initing other systems
	
	Z_Init ();
	
	if (devparm)
		printf ("W_Init: Init WADfiles.\n");
	W_InitMultipleFiles (wadfiles);

	if (!devparm)
	{
		if (graph)
		{
			regs.h.al = 0x13;
			regs.h.ah = 0;
			int386 (0x10, &regs, &regs);
			I_SetPalette (W_CacheLumpName("PLAYPAL", PU_CACHE) );
		}
		DrawLoadBackground ();
	}

	AdvanceLoadScreen();
	
	serial = atoi (W_CacheLumpName ("serial", PU_CACHE));

	if (devparm)
	{
		sprintf(file, "Wad Serial Number: %d:", serial);
		mprintf(file);
	}
	
	// Check for -file in shareware
	if (modifiedgame)
	{
		// These are the lumps that will be checked in IWAD,
		// if any one is not present, execution will be aborted.
		char name[3][8]=
		{
			"map23","map30","ROB3E1"
		};
		int i;
	
		if (shareware)
			I_Error("\nYou cannot -file with the demo "
				"version. You must buy the real game!");
	
		// Check for fake IWAD with right name,
		// but w/o all the lumps of the registered version. 
		if (registered)
			for (i = 0;i < 3; i++)
				if (W_CheckNumForName(name[i])<0)
					I_Error("\nThis is not the retail version.");

		if (devparm)
			mprintf(
				"\n===========================================================================\n"
				"ATTENTION:  This version of STRIFE has extra files added to it.\n"
				"        You will not receive technical support for modified games.\n"
				"===========================================================================\n"
			);
	}
	
	// Check and print which version is executed.
	if (registered && devparm)
	{
		mprintf("\tretail version.\n");
		mprintf (
			"===========================================================================\n"
			"             This version is NOT SHAREWARE, do not distribute!\n"
			"         Please report software piracy to the SPA: 1-800-388-PIR8\n"
			"===========================================================================\n"
		);
	}
	if (shareware)
	{
		mprintf("\tdemo version.\n");
	}

	AdvanceLoadScreen();
	
	if (devparm)
		mprintf ("R_Init: Loading Graphics - ");
	R_Init ();
	AdvanceLoadScreen();

	if (devparm)
		mprintf ("\nP_Init: Init Playloop state.\n");
	P_Init ();
	AdvanceLoadScreen();

	if (devparm)
		mprintf ("I_Init: Setting up machine state.\n");
	I_Init ();
	AdvanceLoadScreen();

	if (devparm)
		mprintf ("D_CheckNetGame: Checking network game status.\n");
	D_CheckNetGame ();
	AdvanceLoadScreen();

	if (devparm)
		mprintf ("M_Init: Init Menu.\n");
	M_Init ();
	AdvanceLoadScreen();

	if (devparm)
		mprintf ("S_Init: Setting up sound.\n");
	S_Init (sfxVolume*8, musicVolume*8, voiceVolume*8);
	AdvanceLoadScreen();

	if (devparm)
		mprintf ("HU_Init: Setting up heads up display.\n");
	HU_Init ();
	AdvanceLoadScreen();

	if (devparm)
		mprintf ("ST_Init: Init status bar.\n");
	ST_Init ();
	AdvanceLoadScreen();
	
	// check for a driver that wants intermission stats
	p = M_CheckParm ("-statcopy");
	if (p && p<myargc-1)
	{
		// for statistics driver
		extern void *statcopy;                            
	
		statcopy = (void*)atoi(myargv[p+1]);
		if (devparm)
			mprintf ("External statistics registered.\n");
	}
	AdvanceLoadScreen();
	
	// start the apropriate game based on parms
	p = M_CheckParm ("-record");
	
	if (p && p < myargc-1)
	{
		G_RecordDemo (myargv[p+1]);
		autostart = true;
	}
	AdvanceLoadScreen();
	
	p = M_CheckParm ("-playdemo");
	if (p && p < myargc-1)
	{
		singledemo = true;              // quit after one demo
		G_DeferedPlayDemo (myargv[p+1]);
		D_DoomLoop ();  // never returns
	}
	AdvanceLoadScreen();
	
	p = M_CheckParm ("-timedemo");
	if (p && p < myargc-1)
	{
		G_TimeDemo (myargv[p+1]);
		D_DoomLoop ();  // never returns
	}
	AdvanceLoadScreen();
	
	p = M_CheckParm ("-loadgame");
	if (p && p < myargc-1)
	{
		M_LoadSelect(atoi(myargv[p+1]));
	}
	AdvanceLoadScreen();
	
	
	if ( gameaction != ga_loadgame )
	{
		if (autostart || netgame)
			G_InitNew (startskill, startmap);
		else
			D_StartTitle ();                // start up intro loop
	
	}
	
	D_DoomLoop ();  // never returns
}
