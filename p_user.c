//
// Copyright (C) 1993-1996 Id Software, Inc.
// Copyright (C) 2022 Nuke.YKT
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
//

// P_user.c

#include "DoomDef.h"
#include "P_local.h"
#include "soundst.h"

/*
===============================================================================

						movement

===============================================================================
*/

#define MAXBOB			0x100000		// 16 pixels of bob

boolean onground;

/*
==================
=
= P_Thrust
=
= moves the given origin along a given angle
=
==================
*/

void P_Thrust (player_t *player, angle_t angle, fixed_t move) 
{
	angle >>= ANGLETOFINESHIFT;
	player->mo->momx += FixedMul(move,finecosine[angle]); 
	player->mo->momy += FixedMul(move,finesine[angle]);
}


/*
==================
=
= P_CalcHeight
=
=Calculate the walking / running height adjustment
=
==================
*/

void P_CalcHeight (player_t *player)
{
	int			angle;
	fixed_t		bob;
	

//
// regular movement bobbing (needs to be calculated for gun swing even
// if not on ground)
// OPTIMIZE: tablify angle 
	player->bob = (FixedMul (player->mo->momx, player->mo->momx)+
	FixedMul (player->mo->momy,player->mo->momy)) >> 2;
	if (player->bob>MAXBOB)
		player->bob = MAXBOB;

	if (!onground)
	{
		player->viewz = player->mo->z + player->viewheight;
		return;
	}
	
//
// move viewheight
//
	if (player->playerstate == PST_LIVE)
	{
		player->viewheight += player->deltaviewheight;
		if (player->viewheight > VIEWHEIGHT)
		{
			player->viewheight = VIEWHEIGHT;
			player->deltaviewheight = 0;
		}
		if (player->viewheight < VIEWHEIGHT/2)
		{
			player->viewheight = VIEWHEIGHT/2;
			if (player->deltaviewheight <= 0)
				player->deltaviewheight = 1;
		}
	
		if (player->deltaviewheight)	
		{
			player->deltaviewheight += FRACUNIT/4;
			if (!player->deltaviewheight)
				player->deltaviewheight = 1;
		}
	}
		
	angle = (FINEANGLES/20*leveltime)&FINEMASK;
	bob = FixedMul ( player->bob>>1, finesine[angle]);

	player->viewz = player->mo->z + player->viewheight + bob;
	if (player->mo->flags & MF_13)
		player->viewz -= 13*FRACUNIT;
	if (player->viewz > player->mo->ceilingz-4*FRACUNIT)
		player->viewz = player->mo->ceilingz-4*FRACUNIT;

	if (player->viewz < player->mo->floorz)
		player->viewz = player->mo->floorz;
}

/*
=================
=
= P_MovePlayer
=
=================
*/

void P_MovePlayer (player_t *player)
{
	ticcmd_t		*cmd;
	
    cmd = &player->cmd;
	player->mo->angle += (cmd->angleturn<<16);

	// don't let the player control movement if not onground
	onground = (player->mo->z <= player->mo->floorz);
	
	if (onground)
	{
		if ((cmd->f_8 & BT2_5) && player->deltaviewheight == 0)
			player->mo->momz += 8*FRACUNIT;
		if (cmd->forwardmove)
			P_Thrust (player, player->mo->angle, cmd->forwardmove*2048);
		if (cmd->sidemove)
			P_Thrust (player, player->mo->angle-ANG90, cmd->sidemove*2048);
	}
	else
	{
		if (cmd->forwardmove)
			P_Thrust (player, player->mo->angle, 256);
	}

	if ( (cmd->forwardmove || cmd->sidemove)
	&& player->mo->state == &states[S_PLAY_287] )
		P_SetMobjState (player->mo, S_PLAY_288);

	if (cmd->f_8 & BT2_2)
		player->f_55 = 1;
	if (player->f_55)
	{
		if (player->f_51 > 0)
			player->f_51 -= 8;
		else if (player->f_51 < 0)
			player->f_51 += 8;
		if (abs(player->f_51) < 8)
		{
			player->f_51 = 0;
			player->f_55 = 0;
		}
	}
	if (cmd->f_8 & BT2_0)
	{
		player->f_51 += 6;
		if (player->f_51 > 90 || player->f_51 < -110)
			player->f_51 -= 6;
	}
	else if (cmd->f_8 & BT2_1)
	{
		player->f_51 -= 6;
		if (player->f_51 > 90 || player->f_51 < -110)
			player->f_51 += 6;
	}
}

/*
=================
=
= P_DeathThink
=
=================
*/

#define         ANG5    (ANG90/18)

void P_DeathThink (player_t *player)
{
	angle_t		angle, delta;

	P_MovePsprites (player);
	
// fall to the ground
	if (player->viewheight > 6*FRACUNIT)
		player->viewheight -= FRACUNIT;
	if (player->viewheight < 6*FRACUNIT)
		player->viewheight = 6*FRACUNIT;
	player->deltaviewheight = 0;
	onground = (player->mo->z <= player->mo->floorz);
	P_CalcHeight (player);
	
	if (player->attacker && player->attacker != player->mo)
	{
		angle = R_PointToAngle2 (player->mo->x, player->mo->y
		, player->attacker->x, player->attacker->y);
		delta = angle - player->mo->angle;
		if (delta < ANG5 || delta > (unsigned)-ANG5)
		{	// looking at killer, so fade damage flash down
			player->mo->angle = angle;
			if (player->damagecount)
				player->damagecount--;
		}
		else if (delta < ANG180)
			player->mo->angle += ANG5;
		else
			player->mo->angle -= ANG5;
	}
	else if (player->damagecount)
		player->damagecount--;
	
	if (player->f_51 <= 90)
		player->f_51 += 3;

	if (player->cmd.buttons & BT_USE)
		player->playerstate = PST_REBORN;
}



/*
=================
=
= P_PlayerThink
=
=================
*/

void P_PlayerThink (player_t *player)
{
	ticcmd_t		*cmd;
	weapontype_t	newweapon;
	
	if (player->playerstate == PST_DEAD)
	{
		P_DeathThink (player);
		return;
	}

//
// chain saw run forward
//
	cmd = &player->cmd;
	if (player->mo->flags & MF_JUSTATTACKED)
	{
		cmd->angleturn = 0;
		cmd->forwardmove = 0xc800/512;
		cmd->sidemove = 0;
		player->mo->flags &= ~MF_JUSTATTACKED;
	}
		
//
// move around
// reactiontime is used to prevent movement for a bit after a teleport
	if (player->mo->reactiontime)
		player->mo->reactiontime--;
	else
		P_MovePlayer (player);
	P_CalcHeight (player);
	if (player->mo->subsector->sector->special)
		P_PlayerInSpecialSector (player);

	if (cmd->f_8 & (BT2_7|BT2_4|BT2_3))
	{
		if (!player->f_25d)
		{
			if (cmd->f_8 & BT2_7)
			{
				func_30F48(player, 130);
			}
			else if (cmd->f_8 & BT2_3)
			{
				func_30F48(player, cmd->f_9);
			}
			else
			{
				func_30A2C(player, cmd->f_9);
				if (testgame)
				{
					player->cheats ^= CF_NOCLIP;
					if (player->cheats & CF_NOCLIP)
					{
						player->message = "No Clipping Mode ON";
						player->mo->flags |= MF_NOCLIP;
					}
					else
					{
						player->mo->flags &= ~MF_NOCLIP;
						player->message = "No Clipping Mode OFF";
					}
				}
			}
			player->f_25d = 1;
		}
	}
	else
		player->f_25d = 0;
		
//
// check for weapon change
//
	if (cmd->buttons & BT_SPECIAL)
		cmd->buttons = 0;			// A special event has no other buttons
	
	if (cmd->buttons & BT_CHANGE)
	{
		// The actual changing of the weapon is done
		//  when the weapon psprite can do it
		//  (read: not in the middle of an attack).
		newweapon = (cmd->buttons&BT_WEAPONMASK)>>BT_WEAPONSHIFT;

		if (newweapon == wp_1 && player->weaponowned[wp_8] &&
			player->readyweapon == wp_1 && player->ammo[weaponinfo[wp_8].ammo])
			newweapon = wp_8;
		if (newweapon == wp_4 && player->weaponowned[wp_9] &&
			player->readyweapon == wp_4 && player->ammo[weaponinfo[wp_9].ammo])
			newweapon = wp_9;
		if (newweapon == wp_6 && player->weaponowned[wp_10] &&
			player->readyweapon == wp_6 && player->ammo[am_3] >= 30)
			newweapon = wp_10;
	
		if (player->weaponowned[newweapon]
			&& newweapon != player->readyweapon)
		{	// Do not go to plasma or BFG in shareware,  even if cheated.
			if (weaponinfo[newweapon].f_18 || !shareware)
			{
				if (player->ammo[weaponinfo[newweapon].ammo])
					player->pendingweapon = newweapon;
				else if (newweapon == wp_1 && player->ammo[am_2] && player->readyweapon != wp_8)
					player->pendingweapon = wp_8;
				else if (newweapon == wp_4 && player->ammo[am_6] && player->readyweapon != wp_9)
					player->pendingweapon = wp_9;
			}
		}
	}
	
//
// check for use
//
	if (cmd->buttons & BT_USE)
	{
		if (!player->usedown)
		{
			func_2EDEC(player);
			P_UseLines (player);
			player->usedown = true;
		}
	}
	else
		player->usedown = false;
			
//
// cycle psprites
//
	P_MovePsprites (player);
	
	
//
// counters
//
	if (player->powers[pw_0])
		player->powers[pw_0]++;	// strength counts up to diminish fade

	if (player->powers[pw_5])
	{
		player->powers[pw_5]--;
		if (!player->powers[pw_5])
		{
			P_SetPsprite(player, ps_2, S_NULL);
			P_SetPsprite(player, ps_3, S_NULL);
			P_SetPsprite(player, ps_4, S_NULL);
		}
		else if (player->powers[pw_5] < 175)
		{
			if (player->powers[pw_5]&32)
			{
				P_SetPsprite(player, ps_4, S_NULL);
				P_SetPsprite(player, ps_3, S_TRGT_11);
			}
			else if (player->powers[pw_5]&16)
			{
				P_SetPsprite(player, ps_4, S_TRGT_12);
				P_SetPsprite(player, ps_3, S_NULL);
			}
		}
	}

	if (player->powers[pw_1])
		if (! --player->powers[pw_1] )
			player->mo->flags &= ~(MF_SHADOW|MF_27);

	if (player->powers[pw_2])
	{
		player->powers[pw_2]--;
		if (!(leveltime&63))
			S_StartSound(player->mo, sfx_mask);
	}

	if (player->powers[pw_3] > 1)
		player->powers[pw_3]--;

	if (player->f_49)
	{
		player->f_49--;
		if (!(leveltime&31) && player->f_49 > 560)
			P_DamageMobj(player->mo, NULL, NULL, 5);
	}
		
	if (player->damagecount)
		player->damagecount--;
		
	if (player->bonuscount)
		player->bonuscount--;

	if (player->extralight < 0)
		player->fixedcolormap = INVERSECOLORMAP;
	else if (player->cheats & CF_3)
		player->fixedcolormap = 1;
	else
		player->fixedcolormap = 0;
}

char *func_30950(player_t *player, int a2, int a3)
{
	int v4;
	int j;

	v4 = player->f_59[a2].f_4;
	player->f_59[a2].f_8 -= a3;
	player->f_1d9 = 1;
	if (!player->f_59[a2].f_8)
	{
		for (j = a2 + 1; j < player->f_1dd; j++)
		{
			player->f_59[j-1] = player->f_59[j];
		}
		player->f_1dd--;
		player->f_59[player->f_1dd].f_4 = NUMMOBJTYPES;
		player->f_59[player->f_1dd].f_0 = -1;
		if (player->f_1df && a2 <= player->f_1df)
			player->f_1df--;
	}
	return mobjinfo[v4].name;
}

void func_30A2C(player_t *player, int a2)
{
	int i;
	int vc;
	int v4;
	angle_t an;
	mobj_t *th;
	int radius;
	int x;
	int y;
	int z;
	for (i = 0; i < player->f_1dd; i++)
	{
		if (player->f_59[i].f_0 == a2)
			break;
	}

	if (!player->f_59[i].f_8)
		return;


	if (player->f_59[i].f_4 == 168)
	{
		if (player->f_59[i].f_8 >= 50)
		{
			vc = 171;
			v4 = 50;
		}
		else if (player->f_59[i].f_8 >= 25)
		{
			v4 = 25;
			vc = 170;
		}
		else if (player->f_59[i].f_8 >= 10)
		{
			v4 = 10;
			vc = 169;
		}
		else
		{
			v4 = 1;
			vc = 168;
		}
	}
	else
	{
		v4 = 1;
		vc = player->f_59[i].f_4;
	}

#if (APPVER_STRIFEREV >= AV_SR_STRF12)
	if (vc >= NUMMOBJTYPES)
		return;
#endif

	an = player->mo->angle;
	an += ((P_Random() - P_Random()) << 18);

	an >>= ANGLETOFINESHIFT;

	if (an < 1474 && an >= 574)
		an = 1474;
	else if (an < 3522 && an >= 2622)
		an = 3522;
	else if (an < 5570 && an >= 4670)
		an = 5570;
	else if (an < 7618 && an >= 6718)
		an = 7618;

	radius = player->mo->info->radius + mobjinfo[vc].radius + 4*FRACUNIT;

	x = player->mo->x + FixedMul(radius, finecosine[an]);
	y = player->mo->y + FixedMul(radius, finesine[an]);
	z = player->mo->z + 10 * FRACUNIT;

	th = P_SpawnMobj(x, y, z, vc);
	th->flags |= (MF_SPECIAL|MF_DROPPED);
	if (!P_CheckPosition(th, x, y))
	{
		P_RemoveMobj(th);
		return;
	}
	th->angle = an;
	th->momx = player->mo->momx+FixedMul(5*FRACUNIT, finecosine[an]);
	th->momy = player->mo->momy+FixedMul(5*FRACUNIT, finesine[an]);
	th->momz = FRACUNIT;
	func_30950(player, i, v4);
}

boolean func_30C60(player_t *player)
{
	angle_t an;
	mobj_t *th;
	int radius;
	int x;
	int y;
	int z;

	an = player->mo->angle;

	an >>= ANGLETOFINESHIFT;

	if (an < 1474 && an >= 574)
		an = 1474;
	else if (an < 3522 && an >= 2622)
		an = 3522;
	else if (an < 5570 && an >= 4670)
		an = 5570;
	else if (an < 7618 && an >= 6718)
		an = 7618;

	radius = player->mo->info->radius + mobjinfo[MT_128].radius + 4*FRACUNIT;

	x = player->mo->x + FixedMul(radius, finecosine[an]);
	y = player->mo->y + FixedMul(radius, finesine[an]);
	z = player->mo->z + 10 * FRACUNIT;

	th = P_SpawnMobj(x, y, z, MT_128);
	if (!P_CheckPosition(th, x, y))
	{
		P_RemoveMobj(th);
		return false;
	}
	th->target = player->mo;
	th->angle = an;
	th->momx = player->mo->momx+FixedMul(5*FRACUNIT, finecosine[an]);
	th->momy = player->mo->momy+FixedMul(5*FRACUNIT, finesine[an]);
	th->momz = FRACUNIT;
	return true;
}

boolean func_30DC0(player_t *player)
{
	angle_t an;
	mobj_t *th;
	int radius;
	int x;
	int y;
	int z;

	an = player->mo->angle;
	an += ((P_Random() - P_Random()) << 18);

	an >>= ANGLETOFINESHIFT;

	if (an < 1474 && an >= 574)
		an = 1474;
	else if (an < 3522 && an >= 2622)
		an = 3522;
	else if (an < 5570 && an >= 4670)
		an = 5570;
	else if (an < 7618 && an >= 6718)
		an = 7618;

	radius = player->mo->info->radius + mobjinfo[MT_166].radius + 4*FRACUNIT;

	x = player->mo->x + FixedMul(radius, finecosine[an]);
	y = player->mo->y + FixedMul(radius, finesine[an]);
	z = player->mo->z + 10 * FRACUNIT;

	th = P_SpawnMobj(x, y, z, MT_166);
	if (!P_CheckPosition(th, x, y))
	{
		P_RemoveMobj(th);
		return false;
	}
	th->target = player->mo;
#if (APPVER_STRIFEREV >= AV_SR_STRF12)
	th->f_9a = player->f_30f;
#endif
	th->angle = an;
	th->momx = player->mo->momx+FixedMul(5*FRACUNIT, finecosine[an]);
	th->momy = player->mo->momy+FixedMul(5*FRACUNIT, finesine[an]);
	th->momz = FRACUNIT;
	P_SetMobjState(th, th->info->seestate);
	return true;
}

boolean func_30F48(player_t *player, int a2)
{
	static char buffer[40];
	int i;
	char *va;
	if (player->cheats & CF_3)
		return 0;

	for (i = 0; i < player->f_1dd; i++)
	{
		if (player->f_59[i].f_0 != a2)
			continue;

		if (!func_30FF4(player, a2))
			break;

		va = func_30950(player, i, 1);
		if (!va)
			va = "item";
		sprintf(buffer, "You used the %s.", va);
		player->message = buffer;
		if (player == &players[consoleplayer])
			S_StartSound(NULL, sfx_itemup);
		return 1;
	}
	return 0;
}


boolean func_30FF4(player_t *player, int a2)
{
	switch (a2)
	{
		case 108:
			if (!P_GivePower(player, pw_5))
				return 0;
			break;
		case 130:
			if (!P_GiveBody(player, 200))
				return 0;
			break;
#if (APPVER_STRIFEREV < AV_SR_STRF12)
		case 33:
			return func_30C60(player);
#endif
		case 135:
			return func_30DC0(player);
		case 136:
			if (!P_GiveArmor(player, 2))
				return 0;
			break;
		case 137:
			if (!P_GiveArmor(player, 1))
				return 0;
			break;
		case 180:
			if (!P_GiveBody(player, 10))
				return 0;
			break;
		case 181:
			if (!P_GiveBody(player, 25))
				return 0;
			break;
		case 186:
			if (!P_GivePower(player, pw_1))
				return 0;
			break;
		case 187:
			if (!P_GivePower(player, pw_2))
				return 0;
			break;
		case 191:
			if (!player->powers[pw_3])
			{
				player->message = "The scanner won't work without a map!";
				return 0;
			}
			player->powers[pw_3] = 80*35;
			break;
		default:
			return 0;
	}
	return 1;
}

