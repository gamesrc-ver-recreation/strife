//
// Copyright (C) 1993-1996 Id Software, Inc.
// Copyright (C) 2022 Nuke.YKT
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
//

#include "DoomDef.h"
#include "P_local.h"
#include "soundst.h"

//==================================================================
//
//	CHANGE THE TEXTURE OF A WALL SWITCH TO ITS OPPOSITE
//
//==================================================================
switchlist_t alphSwitchList[] =
{
	{"GLASS01", "GLASS02", 1, 43},
	{"GLASS03", "GLASS04", 1, 43},
	{"GLASS05", "GLASS06", 1, 43},
	{"GLASS07", "GLASS08", 1, 43},
	{"GLASS17", "GLASS18", 1, 43},
	{"GLASS19", "GLASS20", 1, 43},
	{"SWKNOB01", "SWKNOB02", 1, 76},
	{"SWLITE01", "SWLITE02", 1, 0},
	{"SWCHN01", "SWCHN02", 1, 75},
	{"COMP01", "COMP04B", 1, 43},
	{"COMP05", "COMP12B", 1, 43},
	{"COMP09", "COMP12B", 1, 43},
	{"COMP12", "COMP04B", 1, 43},
	{"COMP13", "COMP12B", 1, 43},
	{"COMP17", "COMP20B", 1, 43},
	{"COMP21", "COMP28B", 1, 43},
	{"WALTEK09", "WALTEKB1", 1, 0},
	{"WALTEK10", "WALTEKB1", 1, 0},
	{"WALTEK15", "WALTEKB1", 1, 0},
	{"SWFORC01", "SWFORC02", 1, 0},
	{"SWEXIT01", "SWEXIT02", 1, 0},
	{"DORSBK01", "DORSBK02", 1, 78},
	{"SWSLD01", "SWSLD02", 1, 0},
	{"DORWS04", "DORWS05", 1, 36},
	{"SWIRON01", "SWIRON02", 1, 0},
	{"GLASS09", "GLASS10", 2, 43},
	{"GLASS11", "GLASS12", 2, 43},
	{"GLASS13", "GLASS14", 2, 43},
	{"GLASS15", "GLASS16", 2, 43},
	{"SWFORC03", "SWFORC04", 2, 0},
	{"SWCIT01", "SWCIT02", 2, 0},
	{"SWTRMG01", "SWTRMG04", 2, 0},
	{"SWMETL01", "SWMETL02", 2, 0},
	{"SWWOOD01", "SWWOOD02", 2, 0},
	{"SWTKBL01", "SWTKBL02", 2, 0},
	{"AZWAL21", "AZWAL22", 2, 0},
	{"SWINDT01", "SWINDT02", 2, 0},
	{"SWRUST01", "SWRUST02", 2, 0},
	{"SWCHAP01", "SWCHAP02", 2, 0},
	{"SWALIN01", "SWALIN02", 2, 0},
	{"SWWALG01", "SWWALG02", 2, 0},
	{"SWWALG03", "SWWALG04", 2, 0},
	{"SWTRAM01", "SWTRAM02", 2, 0},
	{"SWTRAM03", "SWTRAM04", 2, 0},
	{"SWORC01", "SWORC02", 2, 0},
	{"SWBRIK01", "SWBRIK02", 2, 0},
	{"SWIRON03", "SWIRON04", 2, 0},
	{"SWIRON05", "SWIRON06", 2, 0},
	{"SWIRON07", "SWIRON08", 2, 0},
	{"SWCARD01", "SWCARD02", 2, 77},
	{"SWSIGN01", "SWSIGN02", 2, 0},
	{"SWLEV01", "SWLEV02", 2, 0},
	{"SWLEV03", "SWLEV04", 2, 0},
	{"SWLEV05", "SWLEV06", 2, 0},
	{"SWBRN01", "SWBRN02", 2, 77},
	{"SWPIP01", "SWPIP02", 2, 135},
	{"SWPALM01", "SWPALM02", 2, 37},
	{"SWKNOB03", "SWKNOB04", 2, 76},
	{"ALTSW01", "ALTSW02", 2, 0},
	{"COMP25", "COMP28B", 2, 43},
	{"COMP29", "COMP20B", 2, 43},
	{"COMP33", "COMP50", 2, 43},
	{"COMP42", "COMP51", 2, 43},
	{"GODSCRN1", "GODSCRN2", 2, 114},
	{"ALIEN04", "ALIEN05", 2, 0},
	{"CITADL04", "CITADL05", 2, 0},
	{"SWITE03", "SWITE04", 2, 0},
	{"SWTELP01", "SWTELP02", 2, 0},
	{"BRNSCN01", "BRNSCN05", 2, 69},
	
	{"\0",			"\0",		0}
};

int			switchlist[MAXSWITCHES * 2];
int			numswitches;
button_t	buttonlist[MAXBUTTONS];

/*
===============
=
= P_InitSwitchList
=
= Only called at game initialization
=
===============
*/

void P_InitSwitchList(void)
{
	int		i;
	int		index;
	int		episode;
	
	episode = 1;
	if (registered)
		episode = 2;
		
	for (index = 0,i = 0;i < MAXSWITCHES;i++)
	{
		if (!alphSwitchList[i].episode)
		{
			numswitches = index/2;
			switchlist[index] = -1;
			break;
		}
		
		if (alphSwitchList[i].episode <= episode)
		{
#if 0
			int		value;
			
			if (R_CheckTextureNumForName(alphSwitchList[i].name1) < 0)
			{
				I_Error("Can't find switch texture '%s'!", alphSwitchList[i].name1);
				continue;
			}
	    
			value = R_TextureNumForName(alphSwitchList[i].name1);
#endif
			switchlist[index++] = R_TextureNumForName(alphSwitchList[i].name1);
			switchlist[index++] = R_TextureNumForName(alphSwitchList[i].name2);
		}
	}
}

//==================================================================
//
//	Start a button counting down till it turns off.
//
//==================================================================
void P_StartButton(line_t *line,bwhere_e w,int texture,int time)
{
	int		i;

// See if button is already pressed
	for (i = 0; i < MAXBUTTONS; i++)
		if (buttonlist[i].btimer && buttonlist[i].line == line)
			return;
	
	for (i = 0;i < MAXBUTTONS;i++)
		if (!buttonlist[i].btimer)
		{
			buttonlist[i].line = line;
			buttonlist[i].where = w;
			buttonlist[i].btexture = texture;
			buttonlist[i].btimer = time;
			buttonlist[i].soundorg = (mobj_t *)&line->frontsector->soundorg;
			return;
		}
		
	I_Error("P_StartButton: no button slots left!");
}

void func_2ADCC(line_t *line)
{
	int i;
	mobj_t *th;
	angle_t angle;
	int x, y;
	int x2, y2;
	degenmobj_t *mm;


	mm = &line->frontsector->soundorg;

	x = (line->v1->x + line->v2->x) / 2;
	y = (line->v1->y + line->v2->y) / 2;

	x2 = x + (mm->x - x) / 5;
	y2 = y + (mm->y - y) / 5;


	for (i = 0; i < 7; i++)
	{
		th = P_SpawnMobj(x2, y2, ONFLOORZ, MT_296);
		th->z += 24*FRACUNIT;
		th->flags |= MF_27 | MF_SHADOW;
		P_SetMobjState(th, S_SHRD_284+(P_Random()%3));

		angle = (P_Random() << 13) / 255;

		th->angle = angle << ANGLETOFINESHIFT;

		th->momx = FixedMul((P_Random()&3)*FRACUNIT, finecosine[angle]);
		th->momy = FixedMul((P_Random()&3)*FRACUNIT, finesine[angle]);
		th->momz = (P_Random()&7)*FRACUNIT;
		th->tics += (P_Random()+7)&7;
	}
}

//==================================================================
//
//	Function that changes wall texture.
//	Tell it if switch is ok to use again (1=yes, it's a button).
//
//==================================================================
void P_ChangeSwitchTexture(line_t *line,int useAgain)
{
	int	texTop;
	int	texMid;
	int	texBot;
	int	i;
	int	sound;
	int v14;

	v14 = 0;

	texTop = sides[line->sidenum[0]].toptexture;
	texMid = sides[line->sidenum[0]].midtexture;
	texBot = sides[line->sidenum[0]].bottomtexture;

	if (line->special == 182)
	{
		line->flags &= ~ML_BLOCKING;
		v14 = 1;
		if (useAgain)
		{
			texBot= 0;
			texTop = 0;
		}
		if (texMid)
			useAgain = 0;
		sound = sfx_bglass;
	}
	else
		sound = sfx_swtchn;

	if (!useAgain)
		line->special = 0;
	
	for (i = 0;i < numswitches*2;i++)
		if (switchlist[i] == texTop)
		{
			if (alphSwitchList[i/2].f_14)
				sound = alphSwitchList[i/2].f_14;
			S_StartSound(&line->frontsector->soundorg,sound);
			sides[line->sidenum[0]].toptexture = switchlist[i^1];
			if (useAgain)
				P_StartButton(line,top,switchlist[i],BUTTONTIME);
			if (v14)
				func_2ADCC(line);
			return;
		}
		else
		if (switchlist[i] == texMid)
		{
			if (alphSwitchList[i/2].f_14)
				sound = alphSwitchList[i/2].f_14;
			S_StartSound(&line->frontsector->soundorg,sound);
			sides[line->sidenum[0]].midtexture = switchlist[i^1];
			if (line->flags & ML_TWOSIDED)
				sides[line->sidenum[1]].midtexture = switchlist[i^1];

			if (useAgain)
				P_StartButton(line, middle,switchlist[i],BUTTONTIME);
			if (sound == sfx_firxpl)
			{
				v14 = 1;
				players[0].f_4d |= 0x10000000;
				if (!netgame)
				{
					func_2DF40(&players[0], 126, 306);
					func_2DF40(&players[0], 126, 307);
				}
			}
			if (v14 || sound == sfx_bglass)
				func_2ADCC(line);
			return;
		}
		else
		if (switchlist[i] == texBot)
		{
			if (alphSwitchList[i/2].f_14)
				sound = alphSwitchList[i/2].f_14;
			S_StartSound(&line->frontsector->soundorg,sound);
			sides[line->sidenum[0]].bottomtexture = switchlist[i^1];
			if (useAgain)
				P_StartButton(line, bottom,switchlist[i],BUTTONTIME);
			if (v14)
				func_2ADCC(line);
			return;
		}
}

void func_2B1F4(int *l, mobj_t *thing)
{
	line_t *line;
	vertex_t *v1, *v2;

	line = (line_t*)l;
	v1 = line->v1;
	v2 = line->v2;

	S_StartSound(thing, sfx_stnmov);
	if (l[2])
	{
		if (thing->x < v1->x)
		{
			v1->y += 8*FRACUNIT;
			v2->y += 8*FRACUNIT;
		}
		else
		{
			v1->y -= 8*FRACUNIT;
			v2->y -= 8*FRACUNIT;
		}
	}
	else
	{
		if (thing->y < v1->y)
		{
			v1->x += 8*FRACUNIT;
			v2->x += 8*FRACUNIT;
		}
		else
		{
			v1->x -= 8*FRACUNIT;
			v2->x -= 8*FRACUNIT;
		}
	}

	if (v1->x < v2->x)
	{
		line->bbox[2] = v1->x;
		line->bbox[3] = v2->x;
	}
	else
	{
		line->bbox[2] = v2->x;
		line->bbox[3] = v1->x;
	}

	if (v1->y < v2->y)
	{
		line->bbox[1] = v1->y;
		line->bbox[0] = v2->y;
	}
	else
	{
		line->bbox[1] = v2->y;
		line->bbox[0] = v1->y;
	}

}

/*
==============================================================================
=
= P_UseSpecialLine
=
= Called when a thing uses a special line
= Only the front sides of lines are usable
===============================================================================
*/

boolean P_UseSpecialLine ( mobj_t *thing, line_t *line, int side)
{	
	int l;
	static char buffer[80];
	//
    // Use the back sides of VERY SPECIAL lines...
	//
	if (side)
	{
		switch(line->special)
		{
			case 148:	// Sliding door open&close
				break;

			default:
				return false;
		}
	}

	//
	//	Switches that other things can activate
	//
	if (!thing->player)
	{
		if (line->flags & ML_SECRET)
			return false;		// never open secret doors
		switch(line->special)
		{
			case 1:		// MANUAL DOOR RAISE
			case 31:
			case 144:
				break;
			default:
				return false;
		}
	}
	
	//
	// do something
	//	
	switch (line->special)
	{
		case 666:
			func_2B1F4((int*)line, thing);
			break;
		//===============================================
		//	MANUALS
		//===============================================
		case 1:			// Vertical Door

		case 26:
		case 27:
		case 28:

		case 31:		// Manual door open
		case 32:		// Blue locked door open
		case 33:		// Red locked door open
		case 34:		// Yellow locked door open

		case 117:		// Blazing door raise
		case 118:		// Blazing door open

		case 156:
		case 157:
		case 158:
		case 159:
		case 160:
		case 161:
		case 165:
		case 166:
		case 169:
		case 170:
		case 190:
		case 205:
		case 213:
		case 217:
		case 221:
		case 224:
		case 225:
		case 232:
			EV_VerticalDoor (line, thing);
			break;
		case 144:		// Door Slide Open&Close
			EV_SlidingDoor (line, thing);
			break;
		case 148:
			P_DamageMobj(thing, NULL, NULL, 16);
			P_Thrust(thing->player, thing->angle+ANG180, 0x7d000);
			break;
		case 211:
			if (thing->player == &players[consoleplayer] && thing->player->powers[pw_4])
			{
				sprintf(buffer, "voc%i", line->tag);
				I_StartVoice(buffer);
				line->special = 0;
			}
			break;
		//===============================================
		//	SWITCHES
		//===============================================
		case 7:			// Build Stairs
			if (EV_BuildStairs(line,build8))
				P_ChangeSwitchTexture(line,0);
			break;
		case 9:			// Change Donut
			if (EV_DoDonut(line))
				P_ChangeSwitchTexture(line,0);
			break;
#if 0
		case 11:		// Exit level
			P_ChangeSwitchTexture(line,0);
			//G_ExitLevel ();
			break;
#endif
		case 14:		// Raise Floor 32 and change texture
			if (EV_DoPlat(line,raiseAndChange,32))
				P_ChangeSwitchTexture(line,0);
			break;
		case 15:		// Raise Floor 24 and change texture
			if (EV_DoPlat(line,raiseAndChange,24))
				P_ChangeSwitchTexture(line,0);
			break;
		case 18:		// Raise Floor to next highest floor
			if (EV_DoFloor(line, raiseFloorToNearest))
				P_ChangeSwitchTexture(line,0);
			break;
		case 20:		// Raise Plat next highest floor and change texture
			if (EV_DoPlat(line,raiseToNearestAndChange,0))
				P_ChangeSwitchTexture(line,0);
			break;
		case 21:		// PlatDownWaitUpStay
			if (EV_DoPlat(line,downWaitUpStay,0))
				P_ChangeSwitchTexture(line,0);
			break;
		case 23:		// Lower Floor to Lowest
			if (EV_DoFloor(line,lowerFloorToLowest))
				P_ChangeSwitchTexture(line,0);
			break;
		case 29:		// Raise Door
			if (EV_DoDoor(line,normal))
				P_ChangeSwitchTexture(line,0);
			break;
		case 40:
			if (EV_DoDoor(line,door_10))
				P_ChangeSwitchTexture(line,0);
			break;
		case 189:
			if (EV_DoLockedDoor(line,door_10,thing))
				P_ChangeSwitchTexture(line,0);
			break;
		case 41:		// Lower Ceiling to Floor
			if (EV_DoCeiling(line,lowerToFloor))
				P_ChangeSwitchTexture(line,0);
			break;
		case 71:		// Turbo Lower Floor
			if (EV_DoFloor(line,turboLower))
				P_ChangeSwitchTexture(line,0);
			break;
		case 49:		// Ceiling Crush And Raise
			if (EV_DoCeiling(line,crushAndRaise))
				P_ChangeSwitchTexture(line,0);
			break;
		case 50:		// Close Door
			if (EV_DoDoor(line,close))
				P_ChangeSwitchTexture(line,0);
			break;
		case 51:		// Secret EXIT
			P_ChangeSwitchTexture(line,0);
			G_ExitLevel4 ();
			break;
		case 55:		// Raise Floor Crush
			if (EV_DoFloor(line,raiseFloorCrush))
				P_ChangeSwitchTexture(line,0);
			break;
		case 101:		// Raise Floor
			if (EV_DoFloor(line,raiseFloor))
				P_ChangeSwitchTexture(line,0);
			break;
		case 102:		// Lower Floor to Surrounding floor height
			if (EV_DoFloor(line,lowerFloor))
				P_ChangeSwitchTexture(line,0);
			break;
		case 103:		// Open Door
			if (EV_DoDoor(line,open))
				P_ChangeSwitchTexture(line,0);
			break;
		case 111:		// Blazing Door Raise (faster than TURBO!)
			if (EV_DoDoor (line,blazeRaise))
				P_ChangeSwitchTexture(line,0);
			break;
		case 112:		// Blazing Door Open (faster than TURBO!)
			if (EV_DoDoor (line,blazeOpen))
				P_ChangeSwitchTexture(line,0);
			break;
		case 113:		// Blazing Door Close (faster than TURBO!)
			if (EV_DoDoor (line,blazeClose))
				P_ChangeSwitchTexture(line,0);
			break;
		case 122:		// Blazing PlatDownWaitUpStay
			if (EV_DoPlat(line,blazeDWUS,0))
				P_ChangeSwitchTexture(line,0);
			break;
		case 127:		// Build Stairs Turbo 16
			if (EV_BuildStairs(line,turbo16))
				P_ChangeSwitchTexture(line,0);
			break;
		case 131:		// Raise Floor Turbo
			if (EV_DoFloor(line,raiseFloorTurbo))
				P_ChangeSwitchTexture(line,0);
			break;
		case 133:		// BlzOpenDoor BLUE
		case 135:		// BlzOpenDoor RED
		case 137:		// BlzOpenDoor YELLOW
		case 162:
		case 163:
		case 164:
		case 167:
			if (EV_DoLockedDoor (line,blazeOpen,thing))
				P_ChangeSwitchTexture(line,0);
			break;
		case 171:
			if (EV_DoLockedDoor (line,open,thing))
				P_ChangeSwitchTexture(line,0);
			break;
		case 140:		// Raise Floor 512
			if (EV_DoFloor(line,raiseFloor512))
				P_ChangeSwitchTexture(line,0);
			break;
		case 146:
			if (EV_BuildStairs(line, stair_2))
				P_ChangeSwitchTexture(line,0);
			break;
		case 147:
			if (func_1DC90(line))
				P_ChangeSwitchTexture(line,0);
			break;
		case 181:
			if (EV_DoFloor(line,floor_13))
				P_ChangeSwitchTexture(line,0);
			break;
		case 194:
			if (EV_DoDoor(line,open))
			{
				P_ChangeSwitchTexture(line,0);
				func_20F44(thing);
			}
			break;
		case 199:
			if (EV_DoCeiling(line,lowerAndCrush))
			{
				P_ChangeSwitchTexture(line,0);
				func_20F98(thing);
			}
			break;
		case 209:
			if (func_2DC68(thing->player, 174))
			{
				if (EV_BuildStairs(line, stair_2))
					P_ChangeSwitchTexture(line, 0);
			}
			else
			{
				sprintf(buffer,"You need the chalice!");
				thing->player->message = buffer;
				S_StartSound(thing, sfx_oof);
			}
			break;
		case 219:
			if (!thing->player->cards[it_18])
			{
				thing->player->message = "You need the Blue Crystal";
				S_StartSound(thing, sfx_oof);
				break;
			}
			if (EV_DoFloor(line,lowerFloor))
				P_ChangeSwitchTexture(line,0);
			break;
		case 220:
			if (!thing->player->cards[it_17])
			{
				thing->player->message = "You need the Red Crystal";
				S_StartSound(thing, sfx_oof);
				break;
			}
			if (EV_DoFloor(line,lowerFloor))
				P_ChangeSwitchTexture(line,0);
			break;
		case 226:
			if (EV_DoFloor(line, lowerFloor))
			{
				func_2DF40(thing->player, 126, 306);
				func_2DF40(thing->player, 126, 307);
				P_ChangeSwitchTexture(line,0);
				sprintf(buffer,"Congratulations! You have completed the training area.");
				thing->player->message = buffer;
			}
			break;
		case 235:
			if (thing->player->f_45 != 4)
				break;
			if (EV_DoDoor(line, door_10))
				P_ChangeSwitchTexture(line,0);
			break;
		//===============================================
		//	BUTTONS
		//===============================================
		case 11:
			P_ChangeSwitchTexture(line,1);
			if (levelTimer && levelTimeCount)
				break;
			G_ExitLevel3(line->tag);
			break;
		case 42:		// Close Door
			if (EV_DoDoor(line,close))
				P_ChangeSwitchTexture(line,1);
			break;
		case 43:		// Lower Ceiling to Floor
			if (EV_DoCeiling(line,lowerToFloor))
				P_ChangeSwitchTexture(line,1);
			break;
		case 45:		// Lower Floor to Surrounding floor height
			if (EV_DoFloor(line,lowerFloor))
				P_ChangeSwitchTexture(line,1);
			break;
		case 60:		// Lower Floor to Lowest
			if (EV_DoFloor(line,lowerFloorToLowest))
				P_ChangeSwitchTexture(line,1);
			break;
		case 61:		// Open Door
			if (EV_DoDoor(line,open))
				P_ChangeSwitchTexture(line,1);
			break;
		case 62:		// PlatDownWaitUpStay
			if (EV_DoPlat(line,downWaitUpStay,1))
				P_ChangeSwitchTexture(line,1);
			break;
		case 63:		// Raise Door
			if (EV_DoDoor(line,normal))
				P_ChangeSwitchTexture(line,1);
			break;
		case 64:		// Raise Floor to ceiling
			if (EV_DoFloor(line,raiseFloor))
				P_ChangeSwitchTexture(line,1);
			break;
		case 66:		// Raise Floor 24 and change texture
			if (EV_DoPlat(line,raiseAndChange,24))
				P_ChangeSwitchTexture(line,1);
			break;
		case 67:		// Raise Floor 32 and change texture
			if (EV_DoPlat(line,raiseAndChange,32))
				P_ChangeSwitchTexture(line,1);
			break;
		case 65:		// Raise Floor Crush
			if (EV_DoFloor(line,raiseFloorCrush))
				P_ChangeSwitchTexture(line,1);
			break;
		case 68:		// Raise Plat to next highest floor and change texture
			if (EV_DoPlat(line,raiseToNearestAndChange,0))
				P_ChangeSwitchTexture(line,1);
			break;
		case 69:		// Raise Floor to next highest floor
			if (EV_DoFloor(line, raiseFloorToNearest))
				P_ChangeSwitchTexture(line,1);
			break;
		case 70:		// Turbo Lower Floor
			if (EV_DoFloor(line,turboLower))
				P_ChangeSwitchTexture(line,1);
			break;
		case 114:		// Blazing Door Raise (faster than TURBO!)
			if (EV_DoDoor (line,blazeRaise))
				P_ChangeSwitchTexture(line,1);
			break;
		case 115:		// Blazing Door Open (faster than TURBO!)
			if (EV_DoDoor (line,blazeOpen))
				P_ChangeSwitchTexture(line,1);
			break;
		case 116:		// Blazing Door Close (faster than TURBO!)
			if (EV_DoDoor (line,blazeClose))
				P_ChangeSwitchTexture(line,1);
			break;
		case 123:		// Blazing PlatDownWaitUpStay
			if (EV_DoPlat(line,blazeDWUS,0))
				P_ChangeSwitchTexture(line,1);
			break;
		case 132:		// Raise Floor Turbo
			if (EV_DoFloor(line,raiseFloorTurbo))
				P_ChangeSwitchTexture(line,1);
			break;
		case 99:
		case 134:
		case 136:
		case 151:
		case 152:
		case 153:
		case 168:
			if (EV_DoLockedDoor (line,blazeOpen,thing))
				P_ChangeSwitchTexture(line,1);
			break;
		case 172:
		case 173:
		case 176:
		case 191:
		case 192:
		case 223:
			if (EV_DoLockedDoor (line,normal,thing))
				P_ChangeSwitchTexture(line,1);
			break;
		case 138:		// Light Turn On
			EV_LightTurnOn(line,255);
			P_ChangeSwitchTexture(line,1);
			break;
		case 139:		// Light Turn Off
			EV_LightTurnOn(line,35);
			P_ChangeSwitchTexture(line,1);
			break;
		case 154:
			if (!thing->player->cards[it_9])
			{
				thing->player->message = "You need a gold key";
				S_StartSound(thing, sfx_oof);
				break;
			}
			if (EV_DoPlat(line,downWaitUpStay,1))
				P_ChangeSwitchTexture(line,1);
			break;
		case 155:
			if (EV_DoPlat(line,plat_6,0))
				P_ChangeSwitchTexture(line,1);
			break;
		case 177:
			if (!thing->player->cards[it_8])
			{
				thing->player->message = "You don't have the key";
				S_StartSound(thing, sfx_oof);
				break;
			}
			if (EV_DoPlat(line,downWaitUpStay,1))
				P_ChangeSwitchTexture(line,1);
			break;
		case 207:
			if (func_1E7D8(line, thing))
				P_ChangeSwitchTexture(line,1);
			break;
		case 214:
			if (EV_DoPlat(line,plat_2,1))
				P_ChangeSwitchTexture(line,1);
			break;
		case 229:
			if (thing->player->f_45 != 4)
				break;
			if (func_1E7D8(line, thing))
				P_ChangeSwitchTexture(line,1);
			break;
		case 233:
			if (EV_DoDoor(line, door_10))
			{
				P_ChangeSwitchTexture(line,1);
				I_StartVoice("voc70");
				l = W_CheckNumForName("log70");
				if (l > 0)
				{
					strncpy(_char_A1300, W_CacheLumpNum(l, PU_CACHE), 300);
					sprintf(buffer, "Incoming Message from BlackBird...");
					thing->player->message = buffer;
				}
			}
			break;
		case 234:
			if (thing->player->f_4d&4)
			{
				if (EV_DoDoor (line,normal))
					P_ChangeSwitchTexture(line,1);
				break;
			}
			sprintf(buffer, "That doesn't seem to work!");
			thing->player->message = buffer;
			break;
	}
	
	return true;
}

