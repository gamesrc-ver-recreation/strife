//
// Copyright (C) 1993-1996 Id Software, Inc.
// Copyright (C) 2022 Nuke.YKT
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
//

// HU_stuff.c
#include "DoomDef.h"
#include "HU_lib.h"
#include "HU_stuff.h"

extern int _wp1, _wp2, _wp3, _wp4;

char *chat_macros[] =
{
	HUSTR_CHATMACRO0,
	HUSTR_CHATMACRO1,
	HUSTR_CHATMACRO2,
	HUSTR_CHATMACRO3,
	HUSTR_CHATMACRO4,
	HUSTR_CHATMACRO5,
	HUSTR_CHATMACRO6,
	HUSTR_CHATMACRO7,
	HUSTR_CHATMACRO8,
	HUSTR_CHATMACRO9
};

char player_names[][16] =
{
	"1: ",
	"2: ",
	"3: ",
	"4: ",
	"5: ",
	"6: ",
	"7: ",
	"8: ",
};


char *nickname;

static player_t *plr;
patch_t *hu_font[HU_FONTSIZE];
patch_t *hu_font2[HU_FONTSIZE];
static hu_textline_t w_title;
boolean chat_on;
static hu_itext_t w_chat;
static boolean always_off = false;
static char chat_dest[MAXPLAYERS];
static hu_itext_t w_inputbuffer[MAXPLAYERS];

static boolean message_on;
boolean message_dontfuckwithme;
static boolean message_nottobefuckedwith;

static hu_stext_t w_message;
static int message_counter;

extern int showMessages;
extern boolean automapactive;

static boolean headsupactive = false;

//
// Builtin map names.
// The actual names can be found in DStrings.h.
//

char *mapnames[] =	// DOOM shareware/registered/retail (Ultimate) names.
{
	HUSTR_1,
	HUSTR_2,
	HUSTR_3,
	HUSTR_4,
	HUSTR_5,
	HUSTR_6,
	HUSTR_7,
	HUSTR_8,
	HUSTR_9,
	HUSTR_10,
	HUSTR_11,
	HUSTR_12,
	HUSTR_13,
	HUSTR_14,
	HUSTR_15,
	HUSTR_16,
	HUSTR_17,
	HUSTR_18,
	HUSTR_19,
	HUSTR_20,
	HUSTR_21,
	HUSTR_22,
	HUSTR_23,
	HUSTR_24,
	HUSTR_25,
	HUSTR_26,
	HUSTR_27,
	HUSTR_28,
	HUSTR_29,
	HUSTR_30,
	HUSTR_31,
	HUSTR_32,
	HUSTR_33,
	HUSTR_34
};

const char shiftxform[] =
{

	0,
	1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
	11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
	21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
	31,
	' ', '!', '"', '#', '$', '%', '&',
	'"', // shift-'
	'(', ')', '*', '+',
	'<', // shift-,
	'_', // shift--
	'>', // shift-.
	'?', // shift-/
	')', // shift-0
	'!', // shift-1
	'@', // shift-2
	'#', // shift-3
	'$', // shift-4
	'%', // shift-5
	'^', // shift-6
	'&', // shift-7
	'*', // shift-8
	'(', // shift-9
	':',
	':', // shift-;
	'<',
	'+', // shift-=
	'>', '?', '@',
	'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N',
	'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
	'[', // shift-[
	'!', // shift-backslash - OH MY GOD DOES WATCOM SUCK
	']', // shift-]
	'"', '_',
	'\'', // shift-`
	'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N',
	'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
	'{', '|', '}', '~', 127
};

void HU_Init(void)
{

  int i, j;
  char buffer[9];

// load the heads-up font
  j = HU_FONTSTART;
  for (i=0;i<HU_FONTSIZE;i++)
  {
    sprintf(buffer, "STCFN%.3d", j++);
    hu_font[i] = (patch_t *) W_CacheLumpName(buffer, PU_STATIC);
	buffer[2] = 'B';
    hu_font2[i] = (patch_t *) W_CacheLumpName(buffer, PU_STATIC);
  }

}

void HU_Stop(void)
{
  headsupactive = false;
}

void HU_Start(void)
{

  int i;
  char *s;

// create the map title widget
  HUlib_initTextLine(&w_title, HU_TITLEX, HU_TITLEY,
    hu_font, HU_FONTSTART);

  s = HU_TITLE;
    
  while (*s)
    HUlib_addCharToTextLine(&w_title, *(s++));

  if (headsupactive)
    return;

  plr = &players[consoleplayer];
  message_on = false;
  message_dontfuckwithme = false;
  message_nottobefuckedwith = false;
  chat_on = false;

// create the message widget
  HUlib_initSText(&w_message, HU_MSGX, HU_MSGY, HU_MSGHEIGHT,
    hu_font, HU_FONTSTART, &message_on);

// create the chat widget
  HUlib_initIText(&w_chat, HU_INPUTX, HU_INPUTY,
    hu_font, HU_FONTSTART, &chat_on);

// create the inputbuffer widgets
  for (i=0 ; i<MAXPLAYERS ; i++)
    HUlib_initIText(&w_inputbuffer[i], 0, 0, 0, 0, &always_off);

  headsupactive = true;

  if (player_names[consoleplayer] != nickname && *nickname)
  {
	printf("have one\n");
	nickname = player_names[consoleplayer];
  }

}

void HU_Drawer(void)
{

  HUlib_drawSText(&w_message);
  HUlib_drawIText(&w_chat);
  if (automapactive)
    HUlib_drawTextLine(&w_title, false);

}

void HU_Erase(void)
{

  HUlib_eraseSText(&w_message);
  HUlib_eraseIText(&w_chat);
  HUlib_eraseTextLine(&w_title);

}

void func_39B30(hu_stext_t *a1, char *a2, char *a3)
{
	int c;
	int vd;

	char buffer[50];

	char *vb, *vc;

	vd = 0;
	if (a2)
	{
		vb = a2;
		while (*vb)
		{
			c = toupper(*vb++) - HU_FONTSTART;
			if (c < 0 || c >= HU_FONTSIZE)
				vd += 4;
			else
				vd += SHORT(hu_font[c]->width);
		}
	}
	vb = buffer;
	vc = a3;
	while (*vc && ((*vc != ' ' && *vc != '-') || vd <= 285))
	{
		*vb++ = *vc;
		c = toupper(*vc++);
		if (c != ' ' && c >= '!' && c < '_')
			vd += SHORT(hu_font[c-HU_FONTSTART]->width);
		else
			vd += 4;
	}
	if (vd > 320)
	{
		vb--;
		vc--;
	}
	if (*vc && *vc != ' ')
	{
		while (*vb != ' ')
		{
			vb--;
			vc--;
		}
	}
	*vb = 0;
	HUlib_addMessageToSText(&w_message, a2, buffer);
	HUlib_addMessageToSText(&w_message, NULL, vc);
}

void HU_Ticker(void)
{

  int i, rc;
  char c;

  // tick down message counter if message is up
  if (message_counter && !--message_counter)
  {
    message_on = false;
    message_nottobefuckedwith = false;
  }

  // display message if necessary
  if ((plr->message && !message_nottobefuckedwith)
   || (plr->message && message_dontfuckwithme))
  {
    func_39B30(&w_message, 0, plr->message);
    plr->message = 0;
    message_on = true;
    message_counter = HU_MSGTIMEOUT;
    message_nottobefuckedwith = message_dontfuckwithme;
    message_dontfuckwithme = 0;
  }

  // check for incoming chat characters
  if (netgame)
  {
    for (i=0 ; i<MAXPLAYERS; i++)
    {
      if (!playeringame[i])
	continue;
      if (i != consoleplayer && (c = players[i].cmd.chatchar))
      {
	if (c <= HU_CHANGENAME)
	  chat_dest[i] = c;
	else
	{
	  if (c >= 'a' && c <= 'z')
	    c = (char) shiftxform[(unsigned char) c];
	  rc = HUlib_keyInIText(&w_inputbuffer[i], c);
	  if (rc && c == KEY_ENTER)
	  {
	    if (w_inputbuffer[i].l.len && (chat_dest[i] == consoleplayer+1
            || chat_dest[i] == HU_BROADCAST))
            {
	      func_39B30(&w_message,
	      player_names[i],
	      w_inputbuffer[i].l.l);
			    
	      message_nottobefuckedwith = true;
	      message_on = true;
	      message_counter = HU_MSGTIMEOUT;
	      S_StartSound(0, sfx_radio);
	    }
		else if (chat_dest[i] == HU_CHANGENAME)
			sprintf(player_names[i], "%.13s: ", w_inputbuffer[i].l.l);
	    HUlib_resetIText(&w_inputbuffer[i]);
	  }
	}
	players[i].cmd.chatchar = 0;
      }
    }
  }

}

#define QUEUESIZE 64

static char chatchars[QUEUESIZE];
static int head = 0;
static int tail = 0;


void HU_queueChatChar(char c)
{
  chatchars[head] = c;
  if (((head + 1) & (QUEUESIZE-1)) == tail)
  {
	return;
  }
  head = (head + 1) & (QUEUESIZE-1);
}

char HU_dequeueChatChar(void)
{
  char c;

  if (head != tail)
  {
    c = chatchars[tail];
    tail = (tail + 1) & (QUEUESIZE-1);
  }
  else
  {
    c = 0;
  }

  return c;
}

boolean HU_Responder(event_t *ev)
{

  static char lastmessage[HU_MAXLINELENGTH+1];
  char *macromessage;
  boolean eatkey = false;
  static boolean _int_88760 = false;
  static boolean shiftdown = false;
  static boolean altdown = false;
  unsigned char c;
  int i, numplayers;
  int p;
  static int num_nobrainers = 0;

  if (ev->data1 == KEY_RSHIFT)
  {
    shiftdown = ev->type == ev_keydown;
    return false;
  }
  else if (ev->data1 == KEY_RALT || ev->data1 == KEY_LALT)
  {
    altdown = ev->type == ev_keydown;
    return false;
  }

  if (ev->type != ev_keydown)
    return false;

  if (!chat_on)
  {
    if (ev->data1 == HU_MSGREFRESH)
    {
      message_on = true;
      message_counter = HU_MSGTIMEOUT;
      eatkey = true;
    }
    else if (netgame && ev->data1 == HU_INPUTTOGGLE)
    {
      eatkey = chat_on = true;
      HUlib_resetIText(&w_chat);
      HU_queueChatChar(HU_BROADCAST);
    }
  }
  else
  {
    c = ev->data1;
    // send a macro
    if (altdown)
    {
      c = c - '0';
      if (c > 9)
	return false;
      // fprintf(stderr, "got here\n");
      macromessage = chat_macros[c];
	    
      // kill last message with a '\n'
      HU_queueChatChar(KEY_ENTER); // DEBUG!!!
	    
      // send the macro message
      while (*macromessage)
	HU_queueChatChar(*macromessage++);
      HU_queueChatChar(KEY_ENTER);
	    
      // leave chat mode and notify that it was sent
      chat_on = false;
      strncpy(lastmessage, chat_macros[c], HU_MAXLINELENGTH+1);
      plr->message = lastmessage;
      eatkey = true;
    }
    else
    {
      if (shiftdown || (c >= 'a' && c <= 'z'))
	c = shiftxform[c];
		if (!w_chat.l.len)
		{
			if (c >= '1' && c <= '8')
			{
				i = c - '0';
				if (i == consoleplayer)
				{
					num_nobrainers++;
					if (num_nobrainers < 3)
						plr->message = HUSTR_TALKTOSELF1;
					else if (num_nobrainers < 6)
						plr->message = HUSTR_TALKTOSELF2;
					else if (num_nobrainers < 9)
						plr->message = HUSTR_TALKTOSELF3;
					else if (num_nobrainers < 32)
						plr->message = HUSTR_TALKTOSELF4;
					else
						plr->message = HUSTR_TALKTOSELF5;
				}
				else
				{
					HU_queueChatChar(i);
					sprintf(lastmessage, "Talking to: %c", c);
					plr->message = lastmessage;
				}
				eatkey = true;
			}
			else if (c == '$')
			{
				HU_queueChatChar(HU_CHANGENAME);
				eatkey = true;
				sprintf(lastmessage, "Changing Name:");
				plr->message = lastmessage;
				_int_88760 = true;
			}
			else
			{
				eatkey = HUlib_keyInIText(&w_chat, c);
				if (eatkey)
					HU_queueChatChar(c);
			}
		}
		else
		{
      eatkey = HUlib_keyInIText(&w_chat, c);
      if (eatkey)
      {
	// static unsigned char buf[20]; // DEBUG
	HU_queueChatChar(c);
		
	// sprintf(buf, "KEY: %d => %d", ev->data1, c);
	//      plr->message = buf;
      }
		}
      if (c == KEY_ENTER)
      {
	chat_on = false;
	if (w_chat.l.len)
	{
		if (_int_88760)
		{
			sprintf(lastmessage, "%s now %.13s", player_names[consoleplayer], w_chat.l.l);
			sprintf(player_names[consoleplayer], "%.13s: ", w_chat.l.l);
			_int_88760 = false;
		}
		else
	  strcpy(lastmessage, w_chat.l.l);
	  plr->message = lastmessage;
	}
      }
      else if (c == KEY_ESCAPE)
	chat_on = false;
    }
  }

  return eatkey;

}

int func_3A2F8(int a1, int a2, char *a3)
{
	int x = a1;
	int c;
	int w;
	do
	{
		c = *a3++;
		if (!c)
			break;
		if (c == '\n')
		{
			x = a1;
			a2 += 12;
			continue;
		}
		if (c == '_')
			c = ' ';
		else if (c == ' ' && x == a1)
			continue;
		c = toupper(c) - HU_FONTSTART;
		if (c < 0 || c >= HU_FONTSIZE)
		{
			x += 4;
			continue;
		}
		w = SHORT(hu_font2[c]->width);
		if (x + w > 300)
		{
			a3--;
			x = a1;
			a2 += 12;
			continue;
		}
		if (!V_DrawPatchDirect(x, a2, 0, hu_font2[c]))
			break;
		x += w;
	} while (1);
	return a2+12;
}
