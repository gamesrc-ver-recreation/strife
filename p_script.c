//
// Copyright (C) 1993-1996 Id Software, Inc.
// Copyright (C) 2022 Nuke.YKT
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
//

#include "DoomDef.h"
#include "P_local.h"

script_t *_int_87CA8 = NULL;
script_t *_int_87CAC = NULL;
unkstruct1 *_int_87CB0 = NULL;
boolean _int_87CB4 = 0;

unkstruct1 _int_87CB8[5] = {
    {1, 0, 0, 0},
    {2, 340, 314, 315},
    {3, 340, 314, 315},
    {4, 340, 314, 315},
    {5, 340, 314, 315}
};

int _int_87D08 = 5;

unkstruct2 _int_87D0C[5] = {
    {
        "PEASANT",
        10,
        "PLEASE DON'T HURT ME.",
        "IF YOU'RE LOOKING TO HURT ME, I'M \nNOT REALLY WORTH THE EFFORT.",
        "I DON'T KNOW ANYTHING.",
        "GO AWAY OR I'LL CALL THE GUARDS!",
        "I WISH SOMETIMES THAT ALL THESE \nREBELS WOULD JUST LEARN THEIR \nPLACE AND STOP THIS NONSENSE.",
        "JUST LEAVE ME ALONE, OK?",
        "I'M NOT SURE, BUT SOMETIMES I THINK \nTHAT I KNOW SOME OF THE ACOLYTES.",
        "THE ORDER'S GOT EVERYTHING AROUND HERE PRETTY WELL LOCKED UP TIGHT.",
        "THERE'S NO WAY THAT THIS IS JUST A \nSECURITY FORCE.",
        "I'VE HEARD THAT THE ORDER IS REALLY \nNERVOUS ABOUT THE FRONT'S \nACTIONS AROUND HERE."
    },
    {
        "REBEL",
        10,
        "THERE'S NO WAY THE ORDER WILL \nSTAND AGAINST US.",
        "WE'RE ALMOST READY TO STRIKE. \nMACIL'S PLANS ARE FALLING IN PLACE.",
        "WE'RE ALL BEHIND YOU, DON'T WORRY.",
        "DON'T GET TOO CLOSE TO ANY OF THOSE BIG ROBOTS. THEY'LL MELT YOU DOWN \nFOR SCRAP!",
        "THE DAY OF OUR GLORY WILL SOON \nCOME, AND THOSE WHO OPPOSE US WILL \nBE CRUSHED!",
        "DON'T GET TOO COMFORTABLE. WE'VE \nSTILL GOT OUR WORK CUT OUT FOR US.",
        "MACIL SAYS THAT YOU'RE THE NEW \nHOPE. BEAR THAT IN MIND.",
        "ONCE WE'VE TAKEN THESE CHARLATANS DOWN, WE'LL BE ABLE TO REBUILD THIS WORLD AS IT SHOULD BE.",
        "REMEMBER THAT YOU AREN'T FIGHTING \nJUST FOR YOURSELF, BUT FOR \nEVERYONE HERE AND OUTSIDE.",
        "AS LONG AS ONE OF US STILL STANDS, \nWE WILL WIN."
    },
    {
        "AGUARD",
        10,
        "MOVE ALONG,  PEASANT.",
        "FOLLOW THE TRUE FAITH, ONLY THEN \nWILL YOU BEGIN TO UNDERSTAND.",
        "ONLY THROUGH DEATH CAN ONE BE \nTRULY REBORN.",
        "I'M NOT INTERESTED IN YOUR USELESS \nDRIVEL.",
        "IF I HAD WANTED TO TALK TO YOU I \nWOULD HAVE TOLD YOU SO.",
        "GO AND ANNOY SOMEONE ELSE!",
        "KEEP MOVING!",
        "IF THE ALARM GOES OFF, JUST STAY OUT OF OUR WAY!",
        "THE ORDER WILL CLEANSE THE WORLD \nAND USHER IT INTO THE NEW ERA.",
        "PROBLEM?  NO, I THOUGHT NOT."
    },
    {
        "BEGGAR",
        10,
        "ALMS FOR THE POOR?",
        "WHAT ARE YOU LOOKING AT, SURFACER?",
        "YOU WOULDN'T HAVE ANY EXTRA FOOD, WOULD YOU?",
        "YOU  SURFACE PEOPLE WILL NEVER \n                                                                                                       UNDERSTAND US.",
        "HA, THE GUARDS CAN'T FIND US.  THOSE \nIDIOTS DON'T EVEN KNOW WE EXIST. ",
        "ONE DAY EVERYONE BUT THOSE WHO SERVE THE ORDER WILL BE FORCED TO   JOIN US.",
        "STARE NOW,  BUT YOU KNOW THAT THIS WILL BE YOUR OWN FACE ONE DAY.",
        "THERE'S NOTHING THING MORE \nANNOYING THAN A SURFACER WITH AN ATTITUDE!",
        "THE ORDER WILL MAKE SHORT WORK OF YOUR PATHETIC FRONT.",
        "WATCH YOURSELF SURFACER. WE KNOW OUR ENEMIES!"
    },
    {
        "PGUARD",
        10,
        "WE ARE THE HANDS OF FATE. TO EARN \nOUR WRATH IS TO FIND OBLIVION!",
        "THE ORDER WILL CLEANSE THE WORLD \nOF THE WEAK AND CORRUPT!",
        "OBEY THE WILL OF THE MASTERS!",
        "LONG LIFE TO THE BROTHERS OF THE \nORDER!",
        "FREE WILL IS AN ILLUSION THAT BINDS \nTHE WEAK MINDED.",
        "POWER IS THE PATH TO GLORY. TO \nFOLLOW THE ORDER IS TO WALK THAT \nPATH!",
        "TAKE YOUR PLACE AMONG THE \nRIGHTEOUS, JOIN US!",
        "THE ORDER PROTECTS ITS OWN.",
        "ACOLYTES?  THEY HAVE YET TO SEE THE FULL GLORY OF THE ORDER.",
        "IF THERE IS ANY HONOR INSIDE THAT \nPATHETIC SHELL OF A BODY, \nYOU'LL ENTER INTO THE ARMS OF THE \nORDER."
    }
};

int _int_87DFC = 5;

int _int_87E00[] = {
    -100, -75, -50, -50, -100
};

void func_2EB54(int choice);
void func_2E92C(void);

menuitem_t _int_87E14[] = {
    {1, "", func_2EB54, '1'},
    {1, "", func_2EB54, '2'},
    {1, "", func_2EB54, '3'},
    {1, "", func_2EB54, '4'},
    {1, "", func_2EB54, '5'},
    {1, "", func_2EB54, '6'}
};

menu_t _int_87E7C = {
    6,
    0,
    _int_87E14,
    func_2E92C,
    42,75,
    0
};


short _short_A1462;
short _short_A1464;

script_t *_int_A142C;
char _char_A1430[50];

extern boolean talkactive;
extern int showMessages;
extern menu_t *currentMenu;
extern short itemOn;

int _int_A12E0_bka;
int _int_A12E4_bkb;
int _int_A12E8_bkc;
angle_t _int_A12EC_bkd;
player_t *_int_A12F0_bke;
mobj_t *_int_A12F4_bkf;
char *_int_A12F8_bkg;
char *_int_A12FC_bkh;
char _char_A1300[300];

void func_2DBC0 (void)
{
    char name[9];
    int l;
    sprintf(name, "script%02d", gamemap);
    l = W_CheckNumForName(name);
    if (l != -1)
    {
        _int_87CA8 = (script_t*)W_CacheLumpNum(l, PU_LEVEL);
        _short_A1464 = (unsigned int)W_LumpLength(l) / sizeof(script_t);
    }
    else
        _short_A1464 = 0;

    if (!_int_87CB4)
    {
        _int_87CB4 = 1;
        l = W_CheckNumForName("script00");
        _int_87CAC = (script_t*)W_CacheLumpNum(l, PU_STATIC);
        _short_A1462 = (unsigned int)W_LumpLength(l) / sizeof(script_t);
    }
}

int func_2DC68 (player_t *player, int a2)
{
    int i;
    if (a2 <= 0)
        return 0;
    if (a2 >= 133 && a2 < 160)
        return player->cards[a2-133];
    if (a2 >= 196 && a2 <= 200)
        return (a2-196) <= player->f_45;
    if (a2 >= 312 && a2 <= 342)
        return player->f_4d & (1<<(a2-312));
    for (i = 0; i < player->f_1dd; i++)
    {
        if (player->f_59[i].f_4 == a2)
            return player->f_59[i].f_8;
    }
    return 0;
}

script_t *func_2DD08(mobjtype_t a1, int a2)
{
    int i;
    int j = a2;
    for (i = 0; i < _short_A1464; i++)
    {
        if (_int_87CA8[i].f_0 == a1)
        {
            if (j <= 1)
            {
                return &_int_87CA8[i];
            }
            j--;
        }
    }
    for (i = 0; i < _short_A1462; i++)
    {
        if (_int_87CAC[i].f_0 == a1)
        {
            return &_int_87CAC[i];
        }
    }
    return &_int_87CAC[0];
}

unkstruct1 *func_2DD74(int a1)
{
    int i;
    for (i = 0; i < _int_87D08; i++)
    {
        if (_int_87CB8[i].f_0 == a1)
            return &_int_87CB8[i];
    }
    return &_int_87CB8[0];
}

char *func_2DDA8(char *a1)
{
    int i;
    char *a2 = a1+7;
    if (!strnicmp(a1, "RANDOM", 6))
    {
        for (i = 0; i < _int_87DFC; i++)
        {
            if (!strnicmp(a2, _int_87D0C[i].f_0, 4))
            {
                return _int_87D0C[i].f_8[M_Random() % _int_87D0C[i].f_4];
            }
        }
    }
    return a1;
}

int func_2DE1C(player_t *player, spritenum_t a2, int a3)
{
    int i, j;
    player->f_1d9 = 1;
    for (i = 0; i <= player->f_1dd; i++)
    {
        if (a3 < player->f_59[i].f_4)
        {
            if (i == 30)
                return 0;

            for (j = player->f_1dd; j >= i+1; j--)
            {
                player->f_59[j] = player->f_59[j-1];
            }
            player->f_59[i].f_0 = a2;
            player->f_59[i].f_4 = a3;
            player->f_59[i].f_8 = 1;
            if (player->f_1dd && i <= player->f_1df)
                player->f_1df++;
            player->f_1dd++;
            break;
        }
        if (a3 == player->f_59[i].f_4)
        {
            if (player->f_59[i].f_8 >= mobjinfo[a3].mass)
                return 0;
            player->f_59[i].f_8++;
            break;
        }
    }
    return 1;
}

int func_2DF40(player_t *player, int a2, int a3)
{
    int sound;
    line_t junk;
    int i;

    static char buffer[40];

    sound = sfx_itemup;

    if (mobjinfo[a3].flags & MF_11)
    {
        player->f_4d |= 1<<(mobjinfo[a3].speed-1);
    }

    if (a3 >= 133 && a3 < 160)
    {
        P_GiveCard(player, a3 - 133);
        return 1;
    }
    if (a3 >= 312 && a3 <= 342)
    {
        if (mobjinfo[a3].name)
        {
            strncpy(buffer, mobjinfo[a3].name, 39);
            player->message = buffer;
        }
        player->f_4d |= 1<<(a3-312);
    }
    else
    {
        switch (a2)
        {
            case 126:
                switch (a3)
                {
                    case 343:
                        gameaction = ga_victory;
                        if (gamemap == 10)
                            func_2DF40(player, 126, 328);
                        break;
                    case 298:
                        if (player->ammo[am_0] >= 50)
                            return 0;
                        player->ammo[am_0] = 50;
                        break;
                    case 299:
                        if (!P_GiveBody(player, _int_87E00[gameskill]))
                            return 0;
                        break;
                    case 301:
                        P_NoiseAlert(player->mo, player->mo);
                        func_203DC(_int_A12F4_bkf);
                        break;
                    case 302:
                        junk.tag = 222;
                        EV_DoDoor(&junk, open);
                        break;
                    case 303:
                        junk.tag = 222;
                        EV_DoDoor(&junk, close);
                        break;
                    case 304:
                        junk.tag = 223;
                        EV_DoDoor(&junk, open);
                        if (gamemap==2)
                            func_2DE1C(player, a2, a3);
                        break;
                    case 305:
                        junk.tag = 224;
                        EV_DoDoor(&junk, open);
                        break;
                    case 172:
                    {
#if (APPVER_STRIFEREV >= AV_SR_STRF12)
                        int i;
#endif
                        for (i = 0; i < 300; i++)
                        {
                            func_2DE1C(player, 182, 168);
                        }
                        break;
                    }
                    case 138:
                        P_GiveCard(player, it_5);
                        break;
                    case 306:
                        if (player->f_1e3 >= 100)
                            return 0;
                        player->f_1e3 += 10;
                        P_GiveBody(player, 200);
                        break;
                    case 307:
                        if (player->f_1e1 >= 100)
                            return 0;
                        player->f_1e1 += 10;
                        break;
                    default:
                        func_2DE1C(player, a2, a3);
                        break;
                }
                break;
            case 128:
                func_2DE1C(player, 128, 309);
                func_2DE1C(player, 129, 310);
#if (APPVER_STRIFEREV < AV_SR_STRF12)
                for (i = 0; i < 1; i++);
#endif
                for (i = 0; i < player->f_1e1*5+300; i++)
                    func_2DE1C(player, 182, 168);
                break;
            case 190:
                if (!P_GivePower(player, pw_3))
                    return 0;
                sound = sfx_yeah;
                break;
            case 111:
                if (!P_GivePower(player, pw_4))
                    return 0;
                sound = sfx_yeah;
                break;
            case 192:
                if (!P_GiveAmmo(player, am_0, 1))
                    return 0;
                break;
            case 193:
                if (!P_GiveAmmo(player, am_0, 5))
                    return 0;
                break;
            case 194:
                if (!P_GiveAmmo(player, am_4, 1))
                    return 0;
                break;
            case 195:
                if (!P_GiveAmmo(player, am_4, 5))
                    return 0;
                break;
            case 196:
                if (!P_GiveAmmo(player, am_3, 1))
                    return 0;
                break;
            case 197:
                if (!P_GiveAmmo(player, am_3, 5))
                    return 0;
                break;
            case 199:
                if (!P_GiveAmmo(player, am_1, 5))
                    return 0;
                break;
            case 198:
                if (!P_GiveAmmo(player, am_2, 5))
                    return 0;
                break;
            case 200:
                if (!P_GiveAmmo(player, am_5, 1))
                    return 0;
                break;
            case 201:
                if (!P_GiveAmmo(player, am_6, 1))
                    return 0;
                break;
            case 202:
                if (!player->backpack)
                {
                    for (i = 0; i < NUMAMMO; i++)
                        player->maxammo[i] *= 2;
                    player->backpack = 1;
                }
                for (i = 0; i < NUMAMMO; i++)
                    P_GiveAmmo(player, i, 1);
                break;
            case 204:
                if (player->weaponowned[wp_2])
                    return 0;
                if (!P_GiveWeapon(player, wp_2, 0))
                    return 0;
                sound = sfx_wpnup;
                break;
            case 205:
                if (player->weaponowned[wp_5])
                    return 0;
                if (!P_GiveWeapon(player, wp_5, 0))
                    return 0;
                sound = sfx_wpnup;
                break;
            case 207:
                if (player->weaponowned[wp_3])
                    return 0;
                if (!P_GiveWeapon(player, wp_3, 0))
                    return 0;
                sound = sfx_wpnup;
                break;
            case 208:
                if (player->weaponowned[wp_6])
                    return 0;
                if (!P_GiveWeapon(player, wp_6, 0))
                    return 0;
                sound = sfx_wpnup;
                break;
            case 210:
                if (player->weaponowned[wp_1])
                    return 0;
                if (!P_GiveWeapon(player, wp_1, 0))
                    return 0;
                sound = sfx_wpnup;
                break;
            case 182:
                func_2DE1C(player, 182, 168);
                break;
            case 183:
                for (i = 0; i < 1; i++);
                for (i = 0; i < 10; i++)
                    func_2DE1C(player, 182, 168);
                break;
            case 184:
#if (APPVER_STRIFEREV >= AV_SR_STRF12)
                for (i = 0; i < 1; i++);
#endif
                for (i = 0; i < 25; i++)
                    func_2DE1C(player, 182, 168);
                break;
            case 185:
#if (APPVER_STRIFEREV >= AV_SR_STRF12)
                for (i = 0; i < 1; i++);
#endif
                for (i = 0; i < 50; i++)
                    func_2DE1C(player, 182, 168);
                break;
#if (APPVER_STRIFEREV >= AV_SR_STRF12)
            case 136:
                if (!P_GiveArmor(player, -2))
                {
                    func_2DE1C(player, a2, a3);
                }
                break;
            case 137:
                if (!P_GiveArmor(player, -1))
                {
                    func_2DE1C(player, a2, a3);
                }
                break;
#endif
            default:
                if (!func_2DE1C(player, a2, a3))
                    return 0;
                break;
        }
    }

    if (player == &players[consoleplayer])
        S_StartSound(NULL, sound);

    return 1;
}

void func_2E858(player_t *player, int a2, int a3)
{
    int i, j;
    if (a3 <= 0)
        return;
    for (i = 0; i < player->f_1dd; i++)
    {
        if (a2 == player->f_59[i].f_4)
        {
            player->f_59[i].f_8 -= a3;
            if (player->f_59[i].f_8 < 1)
            {
                for (j = i+1; j <= player->f_1dd; j++)
                {
                    player->f_59[j-1] = player->f_59[j];
                }
                player->f_59[player->f_1dd].f_4 = NUMMOBJTYPES;
                player->f_59[player->f_1dd--].f_0 = -1;
                if (player->f_1df >= player->f_1dd && player->f_1df)
                    player->f_1df--;
            }
            break;
        }
    }
}

void func_2E92C(void)
{
    angle_t an;
    char buffer[50];
    int i;
    int vd;
    int t;

    if (_int_A12F0_bke->bonuscount)
    {
        _int_A12F0_bke->bonuscount -= 3;
        if (_int_A12F0_bke->bonuscount < 0)
            _int_A12F0_bke->bonuscount = 0;
    }

    an = R_PointToAngle2(_int_A12F0_bke->mo->x, _int_A12F0_bke->mo->y, _int_A12F4_bkf->x, _int_A12F4_bkf->y)
        - _int_A12F0_bke->mo->angle;

    if ((an > ANG45 && an < -ANG45) || (_int_A12F4_bkf->flags & MF_15))
    {
        func_2EB54(_int_87E7C.numitems-1);
    }
    _int_A12F4_bkf->reactiontime = 2;

    if (_int_A12E8_bkc != -1)
    {
        V_DrawPatchDirect(0, 0, 0, W_CacheLumpNum(_int_A12E8_bkc, PU_CACHE));
        if (_int_A12E4_bkb > gametic)
            return;
    }

    if (talkactive && _int_A12E4_bkb + 3 < gametic)
        menu2active = 1;

    M_WriteText(12, 18, _int_A12F8_bkg);

    vd = 28;
    if (showMessages || !_int_A142C->f_28[0])
    {
        vd = M_WriteText(20, 28, _int_A12FC_bkh);
    }

    i = 175 - _int_87E7C.numitems * 20;
    if (vd > i)
        i = 199 - _int_87E7C.numitems * 20;

    M_WriteText(42, i-6, "______________________________");
    _int_87E7C.y = i + 6;
    for (i = 0; i < _int_87E7C.numitems-1; i++)
    {
        sprintf(buffer, "%d) %s", i+1, _int_A142C->f_178[i].f_1c);
        if (_int_A142C->f_178[i].f_10[0] > 0)
            sprintf(buffer, "%s for %d", buffer, _int_A142C->f_178[i].f_10[0]);
        M_WriteText(_int_87E7C.x, _int_87E7C.y+3+i*19, buffer);
    }
    
    M_WriteText(_int_87E7C.x, _int_87E7C.y+3+i*19, _char_A1430);
}

void func_2EB54(int a1)
{
    int vs;
    int v4;
    int i;

    char *vdi;

    v4 = 0;

    vs = 1;


    if (a1==-1)
        a1 = _int_87E7C.numitems-1;

    I_StartVoice(0);
    for (i = 0; i < 3; i++)
    {
        if (func_2DC68(_int_A12F0_bke, _int_A142C->f_178[a1].f_4[i]) < _int_A142C->f_178[a1].f_10[i])
            vs = 0;
    }

    if (a1 == _int_87E7C.numitems-1 || !vs)
    {
        vdi = _int_A142C->f_178[a1].f_94;
        if (_int_87CB0->f_c)
            P_SetMobjState(_int_A12F4_bkf, _int_87CB0->f_c);
    }
    else
    {
        vdi = _int_A142C->f_178[a1].f_3c;
        if (_int_87CB0->f_8)
            P_SetMobjState(_int_A12F4_bkf, _int_87CB0->f_8);

        if (_int_A142C->f_178[a1].f_0 < 0 ||
            func_2DF40(_int_A12F0_bke,
                states[mobjinfo[_int_A142C->f_178[a1].f_0].spawnstate].sprite,
                _int_A142C->f_178[a1].f_0))
        {
            for (i = 0; i < 3; i++)
            {
                func_2E858(_int_A12F0_bke, _int_A142C->f_178[a1].f_4[i], _int_A142C->f_178[a1].f_10[i]);
            }
        }
        else
            vdi = "You seem to have enough!";

        v4 = _int_A142C->f_178[a1].f_8c;

        if (v4)
            _int_A12F4_bkf->f_9a = abs(_int_A142C->f_178[a1].f_8c);
    }

    if (a1 != _int_87E7C.numitems-1)
    {
        if (_int_A142C->f_178[a1].f_90)
        {
            sprintf(_char_A1300, "log%i", _int_A142C->f_178[a1].f_90);
            strncpy(_char_A1300, W_CacheLumpName(_char_A1300, PU_CACHE), 300);
        }
#if (APPVER_STRIFEREV >= AV_SR_STRF13)
        if (*vdi == '.')
            *vdi = 0;
#endif
        _int_A12F0_bke->message = vdi;
    }
    _int_A12F4_bkf->angle = _int_A12EC_bkd;
    _int_A12F0_bke->f_1d9 = 1;
    M_ClearMenus();
    if (v4 < 0 && gameaction != ga_victory)
        func_2EDEC(_int_A12F0_bke);
    else
        talkactive = 0;
}

void func_2EDE4(void)
{
    func_2EDEC(&players[0]);
}

void func_2EDEC(player_t *player)
{
    angle_t an;
    short i;
    if (menuactive || netgame)
        return;

    an = player->mo->angle;
    P_AimLineAttack(player->mo, an, 0x800000);
    if (!linetarget)
    {
        an += 0x4000000;
        P_AimLineAttack(player->mo, an, 0x800000);
        if (!linetarget)
        {
            an -= 0x8000000;
            P_AimLineAttack(player->mo, an, 0x800000);
        }
    }
    if (!linetarget)
        return;

    if (linetarget->flags & MF_15)
        return;

    _int_A12F4_bkf = linetarget;

    if (player == &players[consoleplayer])
        S_StartSound(NULL, sfx_radio);

    linetarget->target = player->mo;
    _int_A12F4_bkf->reactiontime = 2;
    _int_A12EC_bkd = _int_A12F4_bkf->angle;
    A_FaceTarget(linetarget);
    player->mo->angle = R_PointToAngle2(player->mo->x, player->mo->y,
        linetarget->x, linetarget->y);
    _int_A12F0_bke = player;

    _int_A142C = func_2DD08(linetarget->type, linetarget->f_9a);
    while (_int_A142C->f_14)
    {
        for (i = 0; i < 3; i++)
        {
            if (_int_A142C->f_8[i])
            {
                if (func_2DC68(_int_A12F0_bke, _int_A142C->f_8[i]) < 1)
                    break;
            }
        }
        if (i < 3)
            break;
        _int_A142C = func_2DD08(linetarget->type, _int_A142C->f_14);
    }

    func_19A70(20, 28, _int_A142C->f_38, 0);
    _int_A12FC_bkh = func_2DDA8(_int_A142C->f_38);
    _int_87CB0 = func_2DD74(linetarget->type);
    if (_int_87CB0->f_4)
        P_SetMobjState(_int_A12F4_bkf, _int_87CB0->f_4);
    if (_int_A142C->f_18[0])
        _int_A12F8_bkg = _int_A142C->f_18;
    else if (mobjinfo[linetarget->type].name)
        _int_A12F8_bkg = mobjinfo[linetarget->type].name;
    else
        _int_A12F8_bkg = "Person";

    for (i = 0; (i < 5) && _int_A142C->f_178[i].f_0; i++)
        ;
    _int_87E7C.numitems = i + 1;
    switch (M_Random() % 3)
    {
        case 0:
            sprintf(_char_A1430, "%d) %s", i+1, "BYE!");
            break;
        case 1:
            sprintf(_char_A1430, "%d) %s", i+1, "Thanks, Bye!");
            break;
        case 2:
            sprintf(_char_A1430, "%d) %s", i+1, "See you later!");
            break;
    }
    M_StartControlPanel();
    menu2active = 0;
    talkactive = 1;
    currentMenu = &_int_87E7C;
    itemOn = (i < _int_87E7C.lastOn) ? 0 : _int_87E7C.lastOn;

    _int_A12E8_bkc = W_CheckNumForName(_int_A142C->f_30);
    if (_int_A12E8_bkc != -1)
    {
        V_DrawPatchDirect(0, 0, 0, W_CacheLumpNum(_int_A12E8_bkc, PU_CACHE));
    }
    I_StartVoice(_int_A142C->f_28);
    _int_A12E4_bkb = gametic + 17;
}
