//
// Copyright (C) 1993-1996 Id Software, Inc.
// Copyright (C) 2022 Nuke.YKT
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
//

#ifndef __STLIB__
#define __STLIB__

//
// Background and foreground screen numbers
//
#define BG 4
#define FG 0

//
// Typedefs of widgets
//

// Number widget

typedef struct
{
  // upper right-hand corner
  //  of the number (right-justified)
  int x, y;

  // max # of digits in number
  int width;    
    
  // pointer to current value
  int *num;

  // list of patches for 0-9
  patch_t **p;

  // user data
  int data;
    
} st_number_t;



// Percent widget ("child" of number widget,
//  or, more precisely, contains a number widget.)
typedef struct
{
  // number information
  st_number_t n;

  // percent sign graphic
  patch_t *p;
    
} st_percent_t;



// Multiple Icon widget
typedef struct
{
  // center-justified location of icons
  int x, y;

  // last icon number
  int oldinum;

  // pointer to current icon
  int *inum;

  // pointer to boolean stating
  //  whether to update icon
  boolean *on;

  // list of icons
  patch_t **p;
    
  // user data
  int data;
    
} st_multicon_t;




// Binary Icon widget

typedef struct
{
  // center-justified location of icon
  int x, y;

  // last icon value
  int oldval;

  // pointer to current icon status
  boolean *val;

  // pointer to boolean
  //  stating whether to update icon
  boolean *on;  


  patch_t *p;	// icon
  int data;   // user data
    
} st_binicon_t;



//
// Widget creation, access, and update routines
//

// Initializes widget library.
// More precisely, initialize STMINUS,
//  everything else is done somewhere else.
//
void STlib_init(void);



// Number widget routines
void STlib_initNum (st_number_t *n, int x, int y, patch_t **pl, int *num, int width);

void STlib_updateNum (st_number_t *n);

#endif
