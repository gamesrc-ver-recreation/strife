//
// Copyright (C) 1993-1996 Id Software, Inc.
// Copyright (C) 2022 Nuke.YKT
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
//

// P_doors.c

#include "DoomDef.h"
#include "P_local.h"
#include "soundst.h"

//
// Sliding door frame information
//
slidename_t	slideFrameNames[MAXSLIDEDOORS] =
{
	{"SIGLDR01","SIGLDR02","SIGLDR03","SIGLDR04",	// front
	 "SIGLDR05","SIGLDR06","SIGLDR07","SIGLDR08"},	// back
	{"DORSTN01","DORSTN02","DORSTN03","DORSTN04",	// front
	 "DORSTN05","DORSTN06","DORSTN07","DORSTN08"},	// back
	{"DORQTR01","DORQTR02","DORQTR03","DORQTR04",	// front
	 "DORQTR05","DORQTR06","DORQTR07","DORQTR08"},	// back
	{"DORCRG01","DORCRG02","DORCRG03","DORCRG04",	// front
	 "DORCRG05","DORCRG06","DORCRG07","DORCRG08"},	// back
	{"DORCHN01","DORCHN02","DORCHN03","DORCHN04",	// front
	 "DORCHN05","DORCHN06","DORCHN07","DORCHN08"},	// back
	{"DORIRS01","DORIRS02","DORIRS03","DORIRS04",	// front
	 "DORIRS05","DORIRS06","DORIRS07","DORIRS08"},	// back
	{"DORALN01","DORALN02","DORALN03","DORALN04",	// front
	 "DORALN05","DORALN06","DORALN07","DORALN08"},	// back

	{"\0","\0","\0","\0"}
};

int _int_87118[MAXSLIDEDOORS] =
{
	sfx_drlmto,
	sfx_drston,
	sfx_airlck,
	sfx_drsmto,
	sfx_drchno,
	sfx_airlck,
	sfx_airlck
};

int _int_87138[MAXSLIDEDOORS] =
{
	sfx_drlmtc,
	sfx_drston,
	sfx_airlck,
	sfx_drsmtc,
	sfx_drchnc,
	sfx_airlck,
	sfx_airlck
};

void func_322D8 (vldoor_t *door);


//==================================================================
//==================================================================
//
//							VERTICAL DOORS
//
//==================================================================
//==================================================================

//==================================================================
//
//	T_VerticalDoor
//
//==================================================================
void T_VerticalDoor(vldoor_t *door)
{
	result_e res, res2;

	switch(door->direction)
	{
		case 0: // WAITING
			if(!--door->topcountdown)
				switch(door->type)
				{
					case blazeRaise:
						door->direction = -1; // time to go back down
						S_StartSound((mobj_t *)
							&door->sector->soundorg, sfx_bdcls);
						break;
					case normal:
						door->direction = -1; // time to go back down
						S_StartSound((mobj_t *)
							&door->sector->soundorg, door->f_2c);
						break;
					case close30ThenOpen:
						door->direction = 1;
						S_StartSound((mobj_t *)
							&door->sector->soundorg, door->f_28);
						break;
					case door_8:
						door->direction = 1;
						door->speed = VDOORSPEED;
						S_StartSound((mobj_t *)
							&door->sector->soundorg, door->f_28);
						break;
					default:
						break;
				}
			break;
		case 2: // INITIAL WAIT
			if(!--door->topcountdown)
			{
				switch(door->type)
				{
					case raiseIn5Mins:
						door->direction = 1;
						door->type = normal;
						S_StartSound((mobj_t *)
							&door->sector->soundorg, door->f_28);
						break;
					default:
						break;
				}
			}
			break;
		case -2:
			res = T_MovePlane(door->sector, door->speed,
				door->topheight, false, 1, 1);
			res2 = T_MovePlane(door->sector, door->speed,
				door->topwait, false, 0, -1);
			if(res == pastdest && res2 == pastdest)
			{
				door->sector->specialdata = NULL;
				P_RemoveThinker (&door->thinker); // unlink and free
			}
			break;
		case -1: // DOWN
			res = T_MovePlane(door->sector, door->speed,
				door->sector->floorheight, false, 1, door->direction);
			if(res == pastdest)
			{
				switch(door->type)
				{
					case blazeRaise:
					case blazeClose:
						door->sector->specialdata = NULL;
						P_RemoveThinker(&door->thinker);  // unlink and free
						// S_StartSound((mobj_t *)
						// 	&door->sector->soundorg, sfx_bdcls);
						break;
					case normal:
					case close:
						door->sector->specialdata = NULL;
						P_RemoveThinker(&door->thinker);  // unlink and free
						break;
					case close30ThenOpen:
						door->direction = 0;
						door->topcountdown = 35*30;
						break;
					case door_8:
						door->direction = 0;
						door->topcountdown = 35*120;
						break;
					default:
						break;
				}
			}
			else if(res == crushed)
			{
				switch(door->type)
				{
					case blazeClose:
					case close: // DON'T GO BACK UP!
					case door_8:
						break;
					default:
						door->direction = 1;
						S_StartSound((mobj_t *)
							&door->sector->soundorg,door->f_28);
						break;
				}
			}
			break;
		case 1: // UP
			res = T_MovePlane(door->sector, door->speed,
				door->topheight, false, 1, door->direction);
			if(res == pastdest)
			{
				switch(door->type)
				{
					case blazeRaise:
					case normal:
						door->direction = 0; // wait at top
						door->topcountdown = door->topwait;
						break;
					case close30ThenOpen:
					case blazeOpen:
					case open:
					case door_8:
						door->sector->specialdata = NULL;
						P_RemoveThinker (&door->thinker); // unlink and free
						break;
					default:
						break;
				}
			}
			break;
	}
}

//----------------------------------------------------------------------------
//
// EV_DoLockedDoor
//
// Move a locked door up/down
//
//----------------------------------------------------------------------------

int EV_DoLockedDoor (line_t *line, vldoor_e type, mobj_t *thing)
{
	player_t *p;

	p = thing->player;

	if (!p)
		return 0;

	switch(line->special)
	{
		case 99:	// Blue Lock
		case 133:
			if (!p->cards[it_3])
			{
				p->message = "You need an id card";
				S_StartSound(NULL,sfx_oof);
				return 0;
			}
			break;

		case 151:
		case 164:
			if (!p->cards[it_9])
			{
				p->message = "You need a gold key";
				S_StartSound(NULL,sfx_oof);
				return 0;
			}
			break;

		case 134: // Red Lock
		case 135:
			if (!p->cards[it_10])
			{
				p->message = "You need an id badge";
				S_StartSound(NULL,sfx_oof);
				return 0;
			}
			break;

		case 153:
		case 163:
			if (!p->cards[it_11])
			{
				p->message = "You need a silver key";
				S_StartSound(NULL,sfx_oof);
				return 0;
			}
			break;

		case 136: // Yellow Lock
		case 137:
			if (!p->cards[it_2])
			{
				p->message = "You need a pass card";
				S_StartSound(NULL,sfx_oof);
				return 0;
			}
			break;

		case 152:
		case 162:
			if (!p->cards[it_16])
			{
				p->message = "You need a brass key";
				S_StartSound(NULL,sfx_oof);
				return 0;
			}
			break;

		case 167:
		case 168:
			if (!p->cards[it_5])
			{
				p->message = "Hand print not on file";
				S_StartSound(NULL,sfx_oof);
				return 0;
			}
			break;

		case 171:
			if (!p->cards[it_4])
			{
				p->message = "You don't have the key to the prison";
				S_StartSound(NULL,sfx_oof);
				return 0;
			}
			break;

		case 172:
			if (!p->cards[it_6])
			{
				p->message = "You don't have the key";
				S_StartSound(NULL,sfx_oof);
				return 0;
			}
			break;

		case 173:
			if (!p->cards[it_7])
			{
				p->message = "You don't have the key";
				S_StartSound(NULL,sfx_oof);
				return 0;
			}
			break;

		case 176:
			if (!p->cards[it_8])
			{
				p->message = "You don't have the key";
				S_StartSound(NULL,sfx_oof);
				return 0;
			}
			break;

		case 189:
			if (!p->cards[it_12])
			{
				p->message = "You don't have the key";
				S_StartSound(NULL,sfx_oof);
				return 0;
			}
			break;

		case 191:
			if (!p->cards[it_13])
			{
				p->message = "You don't have the key";
				S_StartSound(NULL,sfx_oof);
				return 0;
			}
			break;

		case 192:
			if (!p->cards[it_15])
			{
				p->message = "You don't have the key";
				S_StartSound(NULL,sfx_oof);
				return 0;
			}
			break;

		case 223:
			if (!p->cards[it_25])
			{
				p->message = "You don't have the key";
				S_StartSound(NULL,sfx_oof);
				return 0;
			}
			break;
	}

	return EV_DoDoor(line,type);
}

//----------------------------------------------------------------------------
//
// EV_DoDoor
//
// Move a door up/down
//
//----------------------------------------------------------------------------

int EV_DoDoor (line_t *line, vldoor_e type)
{
	int	secnum;
	int retcode;
	sector_t *sec;
	vldoor_t *door;

	secnum = -1;
	retcode = 0;
	while((secnum = P_FindSectorFromLineTag(line, secnum)) >= 0)
	{
		sec = &sectors[secnum];
		if(sec->specialdata)
		{
			continue;
		}
		// Add new door thinker
		retcode = 1;
		door = Z_Malloc(sizeof(*door), PU_LEVSPEC, 0);
		P_AddThinker(&door->thinker);
		sec->specialdata = door;
		door->thinker.function = T_VerticalDoor;
		door->sector = sec;
		door->type = type;
		door->topwait = VDOORWAIT;
		door->speed = VDOORSPEED;
		func_322D8(door);
		switch(type)
		{
			case door_10:
				door->direction = -2;
				door->topheight = P_FindLowestCeilingSurrounding(sec);
				door->topheight -= 4*FRACUNIT;
				door->topwait = P_FindLowestFloorSurrounding(sec);
				door->speed = VDOORSPEED / 2;
				if (door->topheight != sec->ceilingheight)
					S_StartSound((mobj_t *)&door->sector->soundorg, door->f_28);
				break;
			case door_9:
				door->direction = -2;
				door->topheight = P_FindLowestCeilingSurrounding(sec);
				door->topheight -= 4*FRACUNIT;
				door->topwait = P_FindHighestFloorSurrounding(sec);
				door->speed = VDOORSPEED / 2;
				if (door->topheight != sec->ceilingheight)
					S_StartSound((mobj_t *)&door->sector->soundorg, door->f_28);
				break;
			case blazeClose:
			case door_8:
				door->direction = -1;
				door->topheight = P_FindLowestCeilingSurrounding(sec);
				door->topheight -= 4*FRACUNIT;
				door->speed = VDOORSPEED * 4;
				if (sec->ceilingheight != sec->floorheight)
					S_StartSound((mobj_t *)&door->sector->soundorg, sfx_bdcls);
				break;
			case close:
				door->topheight = P_FindLowestCeilingSurrounding(sec);
				door->topheight -= 4*FRACUNIT;
				door->direction = -1;
				if (sec->ceilingheight != sec->floorheight)
					S_StartSound((mobj_t *)&door->sector->soundorg, door->f_28);
				break;
			case close30ThenOpen:
				door->topheight = sec->ceilingheight;
				door->direction = -1;
				S_StartSound((mobj_t *)&door->sector->soundorg, door->f_2c);
				break;
			case blazeRaise:
			case blazeOpen:
				door->direction = 1;
				door->topheight = P_FindLowestCeilingSurrounding(sec);
				door->topheight -= 4*FRACUNIT;
				door->speed = VDOORSPEED * 4;
				if(door->topheight != sec->ceilingheight)
				{
					S_StartSound((mobj_t *)&door->sector->soundorg,
						sfx_bdopn);
				}
				break;
			case normal:
			case open:
				door->direction = 1;
				door->topheight = P_FindLowestCeilingSurrounding(sec);
				door->topheight -= 4*FRACUNIT;
				if(door->topheight != sec->ceilingheight)
				{
					S_StartSound((mobj_t *)&door->sector->soundorg,
						door->f_28);
				}
				break;
			default:
				break;
		}
	}
	return(retcode);
}

int func_1DC90(line_t *line)
{
	int			secnum;
	int			retcode;
	int			i;
	sector_t	*sec;

	secnum = -1;
	retcode = 0;
	while ((secnum = P_FindSectorFromLineTag (line, secnum)) >= 0)
	{
		sec = &sectors[secnum];

		retcode = 1;
		sec->special = 0;
		for (i = 0; i < sec->linecount; i++)
		{
			if ((sec->lines[i]->flags & ML_TWOSIDED) && sec->lines[i]->special == 148)
			{
				sec->lines[i]->flags &= ~ML_BLOCKING;
				sec->lines[i]->special = 0;
				sides[sec->lines[i]->sidenum[0]].midtexture = 0;
				sides[sec->lines[i]->sidenum[1]].midtexture = 0;
			}
		}
	}
	return retcode;
}

//==================================================================
//
//	EV_VerticalDoor : open a door manually, no tag value
//
//==================================================================
void EV_VerticalDoor(line_t *line, mobj_t *thing)
{
	player_t		*player;
	int				secnum;
	sector_t		*sec;
	vldoor_t		*door;
	int				side;
//
//	Check for locks
//
	player = thing->player;
	switch(line->special)
	{
		case 205:
			player->message = "THIS AREA IS ONLY AVAILABLE IN THE RETAIL VERSION OF STRIFE";
			S_StartSound(NULL, sfx_oof);
			return;
		case 165:
			player->message = "That doesn't seem to work";
			S_StartSound(NULL, sfx_oof);
			return;
		case 26: // Blue Lock
		case 32:
			if(!player->cards[it_3])
			{
				player->message = "You need an id card to open this door";
				S_StartSound(NULL, sfx_oof);
				return;
			}
			break;
		case 158:
		case 159:
			if(!player->cards[it_9])
			{
				player->message = "You need a gold key";
				S_StartSound(NULL, sfx_oof);
				return;
			}
			break;
		case 27: // Yellow Lock
		case 34:
			if(!player->cards[it_2])
			{
				player->message = "You need a pass card key to open this door";
				S_StartSound(NULL, sfx_oof);
				return;
			}
			break;
		case 156:
		case 161:
			if(!player->cards[it_16])
			{
				player->message = "You need a brass key";
				S_StartSound(NULL, sfx_oof);
				return;
			}
			break;
		case 28: // Red Lock
		case 33:
			if(!player->cards[it_10])
			{
				player->message = "You need an id badge to open this door";
				S_StartSound(NULL, sfx_oof);
				return;
			}
			break;
		case 157:
		case 160:
			if(!player->cards[it_11])
			{
				player->message = "You need a silver key";
				S_StartSound(NULL, sfx_oof);
				return;
			}
			break;
		case 166:
			if(!player->cards[it_5])
			{
				player->message = "Hand print not on file";
				S_StartSound(NULL, sfx_oof);
				return;
			}
			break;
		case 169:
			if(!player->cards[it_0])
			{
				player->message = "You don't have the key";
				S_StartSound(NULL, sfx_oof);
				return;
			}
			break;
		case 170:
			if(!player->cards[it_1])
			{
				player->message = "You don't have the key";
				S_StartSound(NULL, sfx_oof);
				return;
			}
			break;
		case 190:
			if(!player->cards[it_14])
			{
				player->message = "You don't have the key";
				S_StartSound(NULL, sfx_oof);
				return;
			}
			break;
		case 213:
			if(!func_2DC68(player, 174))
			{
				player->message = "You need the chalice!";
				S_StartSound(NULL, sfx_oof);
				return;
			}
			break;
		case 217:
			if(!player->cards[it_22])
			{
				player->message = "You don't have the key";
				S_StartSound(NULL, sfx_oof);
				return;
			}
			break;
		case 221:
			if(!player->cards[it_23])
			{
				player->message = "You don't have the key";
				S_StartSound(NULL, sfx_oof);
				return;
			}
			break;
		case 224:
			if(!player->cards[it_19])
			{
				player->message = "You don't have the key";
				S_StartSound(NULL, sfx_oof);
				return;
			}
			break;
		case 225:
			if(!player->cards[it_20])
			{
				player->message = "You don't have the key";
				S_StartSound(NULL, sfx_oof);
				return;
			}
			break;
		case 232:
			if(!(player->f_4d & 0x20000))
			{
				player->message = "You need the Oracle Pass!";
				S_StartSound(NULL, sfx_oof);
				return;
			}
			break;
	}

	// if the sector has an active thinker, use it
	sec = sides[line->sidenum[1]].sector;
	secnum = sec-sectors;
	if(sec->specialdata)
	{
		door = sec->specialdata;
		switch(line->special)
		{
			case 1: // ONLY FOR "RAISE" DOORS, NOT "OPEN"s
			case 26:
			case 27:
			case 28:
			case 117:
			case 159:
			case 160:
			case 161:
			case 166:
			case 169:
			case 170:
			case 190:
			case 213:
			case 232:
				if(door->direction == -1)
				{
					door->direction = 1; // go back up
				}
				else
				{
					if(!thing->player)
					{ // Monsters don't close doors
						return;
					}
					door->direction = -1; // start going down immediately
				}
				return;
		}
	}

	//
	// new door thinker
	//
	door = Z_Malloc (sizeof(*door), PU_LEVSPEC, 0);
	P_AddThinker(&door->thinker);
	sec->specialdata = door;
	door->thinker.function = T_VerticalDoor;
	door->sector = sec;
	door->direction = 1;
	door->speed = VDOORSPEED;
	door->topwait = VDOORWAIT;
	func_322D8(door);

	// for proper sound
	switch(line->special)
	{
		case 117: // BLAZING DOOR RAISE
		case 118: // BLAZING DOOR OPEN
			S_StartSound((mobj_t *)&sec->soundorg, sfx_bdopn);
			break;
		case 1: // NORMAL DOOR SOUND
		case 31:
			S_StartSound((mobj_t *)&sec->soundorg, door->f_28);
			//S_StartSound((mobj_t *)&sec->soundorg, sfx_dormov);
			break;
		default: // LOCKED DOOR SOUND
			S_StartSound((mobj_t *)&sec->soundorg, door->f_28);
			//S_StartSound((mobj_t *)&sec->soundorg, sfx_dormov);
			break;
	}
	switch(line->special)
	{
		case 1:
		case 26:
		case 27:
		case 28:
			door->type = normal;
			break;
		case 31:
		case 32:
		case 33:
		case 34:
		case 156:
		case 157:
		case 158:
			door->type = open;
			line->special = 0;
			break;
		case 117: // blazing door raise
			door->type = blazeRaise;
			door->speed = VDOORSPEED*4;
			break;
		case 118: // blazing door open
			door->type = blazeOpen;
			line->special = 0;
			door->speed = VDOORSPEED*4;
			break;
		default:
			door->type = normal;
			break;
	}
	
	//
	// find the top and bottom of the movement range
	//
	door->topheight = P_FindLowestCeilingSurrounding(sec);
	door->topheight -= 4*FRACUNIT;
}

//==================================================================
//
//	Spawn a door that closes after 30 seconds
//
//==================================================================
void P_SpawnDoorCloseIn30(sector_t *sec)
{
	vldoor_t *door;

	door = Z_Malloc(sizeof(*door), PU_LEVSPEC, 0);
	P_AddThinker(&door->thinker);
	sec->specialdata = door;
	sec->special = 0;
	door->thinker.function = T_VerticalDoor;
	door->sector = sec;
	door->direction = 0;
	door->type = normal;
	door->speed = VDOORSPEED;
	door->topcountdown = 30*35;
}

//==================================================================
//
//	Spawn a door that opens after 5 minutes
//
//==================================================================
void P_SpawnDoorRaiseIn5Mins(sector_t *sec, int secnum)
{
	vldoor_t *door;

	door = Z_Malloc(sizeof(*door), PU_LEVSPEC, 0);
	P_AddThinker(&door->thinker);
	sec->specialdata = door;
	sec->special = 0;
	door->thinker.function = T_VerticalDoor;
	door->sector = sec;
	door->direction = 2;
	door->type = raiseIn5Mins;
	door->speed = VDOORSPEED;
	door->topheight = P_FindLowestCeilingSurrounding(sec);
	door->topheight -= 4*FRACUNIT;
	door->topwait = VDOORWAIT;
	door->topcountdown = 5*60*35;
}

//
// EV_SlidingDoor : slide a door horizontally
// (animate midtexture, then set noblocking line)
//


slideframe_t slideFrames[MAXSLIDEDOORS];

void P_InitSlidingDoorFrames(void)
{
	int		i;

	memset(slideFrames, -1, sizeof(slideFrames));

	for (i = 0;i < MAXSLIDEDOORS; i++)
	{
		if (!slideFrameNames[i].frontFrame1[0])
			break;

		if (R_CheckTextureNumForName(slideFrameNames[i].frontFrame1) == -1)
			continue;
		
		slideFrames[i].frontFrames[0] = R_TextureNumForName(slideFrameNames[i].frontFrame1);
		slideFrames[i].frontFrames[1] = R_TextureNumForName(slideFrameNames[i].frontFrame2);
		slideFrames[i].frontFrames[2] = R_TextureNumForName(slideFrameNames[i].frontFrame3);
		slideFrames[i].frontFrames[3] = R_TextureNumForName(slideFrameNames[i].frontFrame4);
		slideFrames[i].frontFrames[4] = R_TextureNumForName(slideFrameNames[i].frontFrame5);
		slideFrames[i].frontFrames[5] = R_TextureNumForName(slideFrameNames[i].frontFrame6);
		slideFrames[i].frontFrames[6] = R_TextureNumForName(slideFrameNames[i].frontFrame7);
		slideFrames[i].frontFrames[7] = R_TextureNumForName(slideFrameNames[i].frontFrame8);
	}
}


//
// Return index into "slideFrames" array
// for which door type to use
//
int P_FindSlidingDoorType(line_t *line)
{
	int		i;
	int		val;

	val = sides[line->sidenum[0]].toptexture;
	for (i = 0;i < MAXSLIDEDOORS;i++)
	{
		if (val == slideFrames[i].frontFrames[0])
			return i;
	}
	
	return -1;
}

void T_SlidingDoor (slidedoor_t *door)
{
	fixed_t		vd, vb;
	int			ret;
	short		t1, t2;
	switch(door->status)
	{
		case sd_opening:
			if (!door->timer--)
			{
				if (++door->frame == SNUMFRAMES)
				{
					// IF DOOR IS DONE OPENING...
					door->line->flags &= ML_BLOCKING^0xff;
					door->line2->flags &= ML_BLOCKING^0xff;
					
					if (door->type == sdt_openOnly)
					{
						door->frontsector->specialdata = NULL;
						P_RemoveThinker (&door->thinker);
						break;
					}
					
					door->timer = SDOORWAIT;
					door->status = sd_waiting;
				}
				else
				{
					// IF DOOR NEEDS TO ANIMATE TO NEXT FRAME...
					door->timer = SWAITTICS;

					sides[door->line->sidenum[0]].midtexture = sides[door->line->sidenum[1]].midtexture =
					sides[door->line2->sidenum[0]].midtexture =sides[door->line2->sidenum[1]].midtexture = slideFrames[door->whichDoorIndex].frontFrames[door->frame];
				}
			}
			break;
			
			case sd_waiting:
				// IF DOOR IS DONE WAITING...
				if (!door->timer--)
				{
					// CAN DOOR CLOSE?
					if (door->frontsector->thinglist != NULL)
					{
						door->timer = SDOORWAIT;
						break;
					}

					vd = door->frontsector->ceilingheight;
					vb = door->frontsector->floorheight;

					ret = T_MovePlane(door->frontsector, vd - vb - 10*FRACUNIT,
						vb, false, 1, -1);
					if (ret == crushed)
					{
						door->timer = SDOORWAIT;
						break;
					}
					
					T_MovePlane(door->frontsector, 128*FRACUNIT, vd, false, 1, 1);
					door->line->flags |= ML_BLOCKING;
					door->line2->flags |= ML_BLOCKING;
					S_StartSound((mobj_t*)&door->frontsector->soundorg, _int_87138[door->whichDoorIndex]);
					//door->frame = SNUMFRAMES-1;
					door->status = sd_closing;
					door->timer = SWAITTICS;
				}
				break;
			
			case sd_closing:
				if (!door->timer--)
				{
					if (--door->frame < 0)
					{
						// IF DOOR IS DONE CLOSING...
						T_MovePlane(door->frontsector, 128*FRACUNIT, door->frontsector->floorheight, false, 1, -1);
						door->frontsector->specialdata = NULL;
						P_RemoveThinker (&door->thinker);
						break;
					}
					else
					{
						// IF DOOR NEEDS TO ANIMATE TO NEXT FRAME...
						door->timer = SWAITTICS;

						sides[door->line->sidenum[0]].midtexture = sides[door->line->sidenum[1]].midtexture =
						sides[door->line2->sidenum[0]].midtexture =sides[door->line2->sidenum[1]].midtexture = slideFrames[door->whichDoorIndex].frontFrames[door->frame];
					}
				}
				break;
	}
}


int func_1E7D8(line_t *line, mobj_t *thing)
{
	int			secnum;
	int			retcode;
	int			i;
	sector_t	*sec;
	line_t		*l;

	secnum = -1;
	retcode = 0;
	while ((secnum = P_FindSectorFromLineTag (line, secnum)) >= 0)
	{
		sec = &sectors[secnum];

		if (sec->specialdata)
			continue;

		for (i = 0; i < 4; i++)
		{
			l = sec->lines[i];
			if (P_FindSlidingDoorType(l) >= 0)
			{
				EV_SlidingDoor(l, thing);
				retcode = 1;
				break;
			}
		}
	}
	return retcode;
}


void EV_SlidingDoor(line_t *line, mobj_t *thing)
{
	sector_t	*sec;
	slidedoor_t	*door;
	line_t		*l;
	int			i;
    
	// Make sure door isn't already being animated
	sec = sides[line->sidenum[1]].sector;
	if (sec->specialdata)
	{
		if (!thing->player)
			return;
			
		door = sec->specialdata;
		if (door->type == sdt_openAndClose)
		{
			if (door->status == sd_waiting)
			{
				door->timer = SWAITTICS;
				door->status = sd_closing;
			}
		}
		return;
	}
    
	// Init sliding door vars
	door = Z_Malloc (sizeof(*door), PU_LEVSPEC, 0);
	P_AddThinker (&door->thinker);
	sec->specialdata = door;
		
	door->type = sdt_openAndClose;
	door->status = sd_opening;
	door->whichDoorIndex = P_FindSlidingDoorType(line);

	if (door->whichDoorIndex < 0)
		I_Error("EV_SlidingDoor: Textures are not defined for sliding door!");
			
	sides[line->sidenum[0]].midtexture = sides[line->sidenum[0]].toptexture;
	door->line2 = line;
	door->line = line;
	for (i = 0; i < 4; i++)
	{
		l = sec->lines[i];
		if (l != line)
		{
			if (sides[l->sidenum[0]].toptexture == sides[line->sidenum[0]].toptexture)
				door->line2 = l;
		}
	}
	door->frontsector = sec;
	door->thinker.function = T_SlidingDoor;
	//door->backsector = line->backsector;
	door->timer = SWAITTICS;
	door->frame = 0;

	door->line->flags |= ML_BLOCKING;
	door->line2->flags |= ML_BLOCKING;

	T_MovePlane(door->frontsector, 128 * FRACUNIT, P_FindLowestCeilingSurrounding(door->frontsector), false, 1, 1);
	S_StartSound((mobj_t*)&sec->soundorg, _int_87118[door->whichDoorIndex]);
}
