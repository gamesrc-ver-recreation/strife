//
// Copyright (C) 1993-1996 Id Software, Inc.
// Copyright (C) 2022 Nuke.YKT
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
//

// P_mobj.c

#include "DoomDef.h"
#include "P_local.h"
#include "sounds.h"
#include "soundst.h"

void G_PlayerReborn (int player);
void P_SpawnMapThing (mapthing_t *mthing);

/*
===============
=
= P_SetMobjState
=
= Returns true if the mobj is still present
===============
*/
int test;

boolean P_SetMobjState (mobj_t *mobj, statenum_t state)
{
	state_t	*st;
	
    do
    {
		if (state == S_NULL)
		{
			if (mobj->player && mobj->player->mo == mobj)
			{
				mobj->tics = 30;
				return true;
			}
			mobj->state = S_NULL;
			P_RemoveMobj (mobj);
			return false;
		}
	
		st = &states[state];

		mobj->state = st;
		mobj->tics = st->tics;
		mobj->sprite = st->sprite;
		mobj->frame = st->frame;

		if (st->action)		// call action functions when the state is set
			st->action (mobj);	

		state = st->nextstate;
	} while (!mobj->tics);
	
	return true;
}

/* 
=================== 
= 
= P_ExplodeMissile  
=
=================== 
*/ 

void P_ExplodeMissile (mobj_t *mo)
{
	mo->momx = mo->momy = mo->momz = 0;
	P_SetMobjState (mo, mobjinfo[mo->type].deathstate);
	mo->flags &= ~MF_MISSILE;
	if (mo->info->deathsound)
		S_StartSound (mo, mo->info->deathsound);
}

/* 
=================== 
= 
= P_XYMovement  
=
=================== 
*/ 

#define	STOPSPEED		0x1000
#define	FRICTION		0xe800

#define	MAXSPECIALCROSS		8
extern	line_t	*spechit[MAXSPECIALCROSS];
extern	int			 numspechit;

void P_XYMovement (mobj_t *mo)
{
	fixed_t		ptryx, ptryy;
	player_t	*player;
	fixed_t		xmove, ymove;

	player = mo->player;
	if (mo->momx > MAXMOVE)
		mo->momx = MAXMOVE;
	else if (mo->momx < -MAXMOVE)
		mo->momx = -MAXMOVE;
	if (mo->momy > MAXMOVE)
		mo->momy = MAXMOVE;
	else if (mo->momy < -MAXMOVE)
		mo->momy = -MAXMOVE;
	xmove = mo->momx;
	ymove = mo->momy;
	do
	{
		if (xmove > MAXMOVE/2 || ymove > MAXMOVE/2)
		{
			ptryx = mo->x+xmove/2;
			ptryy = mo->y+ymove/2;
			xmove >>= 1;
			ymove >>= 1;
		}
		else
		{
			ptryx = mo->x + xmove;
			ptryy = mo->y + ymove;
			xmove = ymove = 0;
		}
		if (!P_TryMove(mo, ptryx, ptryy))
		{	// blocked move
			if (player)
			{	// try to slide along it
				P_SlideMove (mo);
			}
			else if (mo->flags & MF_24)
			{
				mo->momx >>= 3;
				mo->momy >>= 3;
				if (!P_TryMove(mo, mo->x-xmove, mo->y+ymove))
					mo->momx = -mo->momx;
				else
					mo->momy = -mo->momy;
				xmove = ymove = 0;
			}
			else if (mo->flags & MF_MISSILE)
			{	// explode a missile
				if (ld_p && ld_p->special)
					P_ShootSpecialLine(mo, ld_p);
				if (numspechit)
					P_ShootSpecialLine(mo, spechit[numspechit-1]);
				if(ceilingline && ceilingline->backsector
					&& ceilingline->backsector->ceilingpic == skyflatnum)
				{	// hack to prevent missiles exploding against the sky
					P_RemoveMobj (mo);
					return;
				}
				P_ExplodeMissile(mo);
			}
			else
				mo->momx = mo->momy = 0;
		}
	} while (xmove || ymove);

//
// slow down
//
	if (player && player->cheats & CF_NOMOMENTUM && !player->cmd.forwardmove && !player->cmd.sidemove)
	{	// debug option for no sliding at all
		mo->momx = mo->momy = 0;
		return;
	}

	if (mo->flags & (MF_MISSILE | MF_24 ) )
		return; 	// no friction for missiles ever

	if (mo->z > mo->floorz)
	{
		if (player)
		{
			mo->momx = FixedMul (mo->momx, 0xfff0);
			mo->momy = FixedMul (mo->momy, 0xfff0);
		}
		return;		// no friction when airborne
	}

	if (mo->flags & MF_CORPSE)
	{	// do not stop sliding if halfway off a step with some momentum
		if (mo->momx > FRACUNIT/4 || mo->momx < -FRACUNIT/4
			|| mo->momy > FRACUNIT/4 || mo->momy < -FRACUNIT/4)
		{
			if (mo->floorz != mo->subsector->sector->floorheight)
				return;
		}
	}

	if (mo->momx > -STOPSPEED && mo->momx < STOPSPEED
		&& mo->momy > -STOPSPEED && mo->momy < STOPSPEED
		&& (!player || (player->cmd.forwardmove== 0
		&& player->cmd.sidemove == 0 ) ) )
	{	// if in a walking frame, stop moving
		if ( player&&(unsigned)((player->mo->state - states)- S_PLAY_288) < 4)
			P_SetMobjState (player->mo, S_PLAY_287);

		mo->momx = 0;
		mo->momy = 0;
	}
	else
	{
		mo->momx = FixedMul (mo->momx, FRICTION);
		mo->momy = FixedMul (mo->momy, FRICTION);
	}
}


/*
===============
=
= P_ZMovement
=
===============
*/

void P_ZMovement (mobj_t *mo)
{
	int		dist;
	int		delta;
//
// check for smooth step up
//
	if (mo->player && mo->z < mo->floorz)
	{
		mo->player->viewheight -= mo->floorz-mo->z;
		mo->player->deltaviewheight = (VIEWHEIGHT - mo->player->viewheight)>>3;
	}
//
// adjust height
//
	if (!func_24AC0(mo, mo->z + mo->momz))
	{
		if (mo->momz < 0)
			mo->floorz = mo->z;
		else
			mo->ceilingz = mo->z + mo->height;
	}
	if ( mo->flags & MF_FLOAT && mo->target)
	{	// float down towards target if too close
		if ( !(mo->flags & MF_INFLOAT) )
		{
			dist = P_AproxDistance (mo->x-mo->target->x, mo->y-mo->target->y);
			delta =(mo->target->z + (mo->height>>1)) - mo->z;
			if (delta<0 && dist < -(delta*3) )
				mo->z -= FLOATSPEED;
			else if (delta>0 && dist < (delta*3) )
				mo->z += FLOATSPEED;
		}
	}

//
// clip movement
//
	if (mo->z <= mo->floorz)
	{	// hit the floor

		// Note (id):
		//  somebody left this after the setting momz to 0,
		//  kinda useless there.
		if (mo->flags & MF_24)
		{	// the skull slammed into something
			mo->momz = (-mo->momz) >> 1;
			mo->reactiontime >>= 1;
			if (func_28FA8(mo) != 2)
				mo->flags &= ~MF_24;
		}

		if (mo->momz < 0)
		{
			if (mo->player && mo->momz < -GRAVITY*8)       // squat down
			{
				mo->player->deltaviewheight = mo->momz>>3;
				if (mo->momz < -GRAVITY*20)
					P_DamageMobj(mo, NULL, mo, mo->momz / -25000);
				mo->player->f_55 = 1;
				S_StartSound(mo, sfx_oof);
			}
			mo->momz = 0;
		}
		mo->z = mo->floorz;

		if ( (mo->flags & MF_MISSILE) && !(mo->flags & (MF_NOCLIP|MF_24)) )
		{
			P_ExplodeMissile (mo);
		}
		return;
	}
	else if (! (mo->flags & MF_NOGRAVITY) )
	{
		if (mo->momz == 0)
			mo->momz = -GRAVITY*2;
		else
			mo->momz -= GRAVITY;
	}

	if (mo->z + mo->height > mo->ceilingz)
	{	// hit the ceiling
		if (mo->flags & MF_24)
		{
			mo->momz = (-mo->momz)>>1;
			mo->reactiontime >>= 1;
		}
		if (mo->momz > 0)
			mo->momz = 0;
		mo->z = mo->ceilingz - mo->height;
		if ( (mo->flags & MF_MISSILE) && !(mo->flags & (MF_NOCLIP|MF_24)) )
		{
			if (mo->subsector->sector->ceilingpic == skyflatnum)
				P_RemoveMobj(mo);
			else
				P_ExplodeMissile(mo);
			return;
		}
	}
}


/*
================
=
= P_NightmareRespawn
=
================
*/

void P_NightmareRespawn (mobj_t *mobj)
{
	fixed_t			x,y,z;
	subsector_t		*ss;
	mobj_t			*mo;
	mapthing_t		*mthing;
		
	x = mobj->spawnpoint.x << FRACBITS;
	y = mobj->spawnpoint.y << FRACBITS;
	
	if (!P_CheckPosition (mobj, x, y) )
		return; // somthing is occupying it's position


// spawn a teleport fog at old spot

	mo = P_SpawnMobj (mobj->x, mobj->y,
		mobj->subsector->sector->floorheight , MT_118);
	S_StartSound (mo, sfx_telept);

// spawn a teleport fog at the new spot
	mo = P_SpawnMobj (x, y, ONFLOORZ , MT_118);
	S_StartSound (mo, sfx_telept);

// spawn the new monster
	mthing = &mobj->spawnpoint;
	
// spawn it
	if (mobj->info->flags & MF_SPAWNCEILING)
		z = ONCEILINGZ;
	else
		z = ONFLOORZ;
	mo = P_SpawnMobj (x,y,z, mobj->type);
	mo->spawnpoint = mobj->spawnpoint;      
	mo->angle = ANG45 * (mthing->angle/45);
	if (mthing->options & MTF_5)
		mobj->flags |= MF_23;
	if (mthing->options & MTF_3)
		mo->flags |= MF_5;
	if (mthing->options & MTF_6)
		mo->flags |= MF_26;
	if (mthing->options & MTF_8)
		mobj->flags |= MF_SHADOW;
	if (mthing->options & MTF_9)
		mobj->flags |= MF_27;

	mo->reactiontime = 18;
	
// remove the old monster
	P_RemoveMobj (mobj);
}

/*
================
=
= P_MobjThinker
=
================
*/

void P_MobjThinker (mobj_t *mobj)
{
	mobj_t	*onmo;
	
	// momentum movement
	if (mobj->momx || mobj->momy )
	{
		P_XYMovement (mobj);
		if (mobj->thinker.function == (think_t)-1)
			return;		// mobj was removed
		if (func_28FA8(mobj) != 2)
			mobj->flags |= MF_13;
		else
			mobj->flags &= ~MF_13;
	}
	if ( (mobj->z != mobj->floorz && !(mobj->flags & MF_NOGRAVITY)) || mobj->momz )
	{
		P_ZMovement (mobj);
		if (mobj->thinker.function == (think_t)-1)
			return;		// mobj was removed
		if (func_28FA8(mobj) != 2)
		{
			S_StartSound(mobj, sfx_wsplsh);
			mobj->flags |= MF_13;
		}
		else
			mobj->flags &= ~MF_13;
	}

//
// cycle through states, calling action functions at transitions
//
	if (mobj->tics != -1)
	{
		mobj->tics--;
		if (killfl && (mobj->flags & MF_COUNTKILL))
			P_DamageMobj(mobj, mobj, mobj, 10);
		// you can cycle through multiple states in a tic
		if (!mobj->tics)
			if (!P_SetMobjState (mobj, mobj->state->nextstate))
				return;		// freed itself
	}
	else
	{	// check for nightmare respawn
		if (! (mobj->flags & MF_COUNTKILL) )
			return;
		if (!respawnmonsters)
			return;
		mobj->movecount++;
		if (mobj->movecount < 16*35)
			return;
		if ( leveltime&31 )
			return;
		if (P_Random () > 4)
			return;
		if (mobj->flags & MF_NOTDMATCH)
			return;
		P_NightmareRespawn (mobj);
	}

}

/*
===============
=
= P_SpawnMobj
=
===============
*/

mobj_t *P_SpawnMobj (fixed_t x, fixed_t y, fixed_t z, mobjtype_t type)
{
	mobj_t		*mobj;
	state_t		*st;
	mobjinfo_t	*info;

	mobj = Z_Malloc (sizeof(*mobj), PU_LEVEL, NULL);
	memset (mobj, 0, sizeof (*mobj));
	info = &mobjinfo[type];
	
    mobj->type = type;
    mobj->info = info;
    mobj->x = x;
    mobj->y = y;
    mobj->radius = info->radius;
    mobj->height = info->height;
    mobj->flags = info->flags;
    mobj->health = info->spawnhealth;
	mobj->reactiontime = info->reactiontime;
	mobj->lastlook = P_Random () % MAXPLAYERS;

// do not set the state with P_SetMobjState, because action routines can't
// be called yet
	st = &states[info->spawnstate];
	mobj->state = st;
	mobj->tics = st->tics;
	mobj->sprite = st->sprite;
	mobj->frame = st->frame;

// set subsector and/or block links
	P_SetThingPosition (mobj);

	mobj->floorz = mobj->subsector->sector->floorheight;
	mobj->ceilingz = mobj->subsector->sector->ceilingheight;
	if (z == ONFLOORZ)
	{
		mobj->z = mobj->floorz;
		if (func_28FA8(mobj) != 2)
			mobj->flags |= MF_13;
	}
	else if (z == ONCEILINGZ)
		mobj->z = mobj->ceilingz - mobj->info->height;
	else 
		mobj->z = z;

	mobj->thinker.function = P_MobjThinker;
	P_AddThinker (&mobj->thinker);
	return mobj;
}

/*
===============
=
= P_RemoveMobj
=
===============
*/
mapthing_t	itemrespawnque[ITEMQUESIZE];
int			itemrespawntime[ITEMQUESIZE];
int			iquehead, iquetail;


void P_RemoveMobj(mobj_t *mobj)
{
	int t, o;
	if ((mobj->flags & MF_SPECIAL)
		&& !(mobj->flags & MF_DROPPED))
	{
		itemrespawnque[iquehead] = mobj->spawnpoint;
		itemrespawntime[iquehead] = leveltime + 35*30;
		if (randomp && iquehead != iquetail)
		{
			t = itemrespawnque[iquehead].type;
			o = itemrespawnque[iquehead].options;
			itemrespawnque[iquehead].type = itemrespawnque[iquetail].type;
			itemrespawnque[iquehead].options = itemrespawnque[iquetail].options;
			itemrespawnque[iquetail].type = t;
			itemrespawnque[iquetail].options = o;
		}
		iquehead = (iquehead+1)&(ITEMQUESIZE-1);

		// lose one off the end?
		if (iquehead == iquetail)
			iquetail = (iquetail+1)&(ITEMQUESIZE-1);
	}
// unlink from sector and block lists
	P_UnsetThingPosition (mobj);
// stop any playing sound
	S_StopSound (mobj);
// free block
	P_RemoveThinker ((thinker_t *)mobj);
}

/*
===============
=
= P_RespawnSpecials
=
===============
*/

void P_RespawnSpecials (void)
{
	fixed_t         x,y,z; 
	subsector_t 	*ss; 
	mobj_t			*mo;
	mapthing_t		*mthing;
	int				i;
	
	if (deathmatch != 2)
		return;			// only respawn items in deathmatch
		
	if (iquehead == iquetail)
		return;			// nothing left to respawn
	
	if (leveltime < itemrespawntime[iquetail])
		return;			// wait at least 30 seconds

	mthing = &itemrespawnque[iquetail];

// pull it from the que
	iquetail = (iquetail+1)&(ITEMQUESIZE-1);
	
	x = mthing->x << FRACBITS; 
	y = mthing->y << FRACBITS; 
	  
// spawn a teleport fog at the new spot
	ss = R_PointInSubsector (x,y); 
	mo = P_SpawnMobj (x, y, ss->sector->floorheight , MT_119); 

	S_StartSound (mo, sfx_itmbk);

// find which type to spawn
	for (i=0 ; i< NUMMOBJTYPES ; i++)
		if (mthing->type == mobjinfo[i].doomednum)
			break;

// spawn it
	if (mobjinfo[i].flags & MF_SPAWNCEILING)
		z = ONCEILINGZ;
	else
		z = ONFLOORZ;
	mo = P_SpawnMobj (x,y,z, i);
	mo->spawnpoint = *mthing;	
	mo->angle = ANG45 * (mthing->angle/45);
}

//=============================================================================


/*
============
=
= P_SpawnPlayer
=
= Called when a player is spawned on the level 
= Most of the player structure stays unchanged between levels
============
*/

void P_SpawnPlayer (mapthing_t *mthing)
{
	player_t	*p;
	fixed_t		x,y,z;
	mobj_t		*mobj;
	int	i;

	if (!playeringame[mthing->type-1])
		return;						// not playing
		
	p = &players[mthing->type-1];

	if (p->playerstate == PST_REBORN)
		G_PlayerReborn (mthing->type-1);

	x = mthing->x << FRACBITS;
	y = mthing->y << FRACBITS;
	z = ONFLOORZ;
	mobj = P_SpawnMobj (x,y,z, MT_1);
	if (mthing->type > 1)	// set color translations for player sprites
		mobj->flags |= (mthing->type-1)<<MF_TRANSSHIFT;
	
	mobj->angle = ANG45 * (mthing->angle/45);

	mobj->player = p;
	mobj->health = p->health;
	p->mo = mobj;
	p->playerstate = PST_LIVE;	
	p->refire = 0;
	p->damagecount = 0;
	p->bonuscount = 0;
	p->extralight = 0;
	p->fixedcolormap = 0;
	p->viewheight = VIEWHEIGHT;
	P_SetupPsprites (p);		// setup gun psprite	

	killfl = 0;
	if (gamemap == 10)
		p->weaponowned[wp_7] = 1;
	
	if (deathmatch)
	{
		p->f_1e1 = 50;
		for (i=0 ; i<NUMCARDS ; i++)
			p->cards[i] = true;		// give all cards in death match mode			
		p->f_4d = MAXINT;
	}

	if (testgame)
		p->cheats |= CF_GODMODE;
			
	if (mthing->type-1 == consoleplayer)
	{
		// wake up the status bar
		ST_Start ();
		// wake up the heads up text
		HU_Start ();		
	}
}



/*
=================
=
= P_SpawnMapThing
=
= The fields of the mapthing should already be in host byte order
==================
*/

void P_SpawnMapThing (mapthing_t *mthing)
{
	int			i, bit;
	mobj_t		*mobj;
	fixed_t		x,y,z;
		
// count deathmatch start positions
	if (mthing->type == 11)
	{
		if (deathmatch_p < &deathmatchstarts[16])
		{
			memcpy (deathmatch_p, mthing, sizeof(*mthing));
			deathmatch_p++;
		}
		return;
	}
	
// check for players specially
	if (mthing->type <= 8)
	{
		// save spots for respawning in network games
		playerstarts[mthing->type-1] = *mthing;
		if (!deathmatch)
			P_SpawnPlayer (mthing);

		return;
	}

// check for apropriate skill level
	if ((mthing->options & 16) && !netgame)
		return;
		
	if (gameskill == sk_baby)
		bit = 1;
	else if (gameskill == sk_nightmare)
		bit = 4;
	else
		bit = 1<<(gameskill-1);
	if (!(mthing->options & bit) )
		return;

// find which type to spawn
	for (i=0 ; i< NUMMOBJTYPES ; i++)
		if (mthing->type == mobjinfo[i].doomednum)
			break;
	
	if (i==NUMMOBJTYPES)
		I_Error ("P_SpawnMapThing: Unknown type %i at (%i, %i)",mthing->type
		, mthing->x, mthing->y);


		
// don't spawn keycards and players in deathmatch
	if (deathmatch && mobjinfo[i].flags & MF_NOTDMATCH)
		return;
		
// don't spawn any monsters if -nomonsters
	if (nomonsters && (mobjinfo[i].flags & MF_COUNTKILL) )
		return;
		
	
// spawn it

	x = mthing->x << FRACBITS;
	y = mthing->y << FRACBITS;
	if (mobjinfo[i].flags & MF_SPAWNCEILING)
		z = ONCEILINGZ;
	else
		z = ONFLOORZ;
	mobj = P_SpawnMobj (x,y,z, i);
	mobj->spawnpoint = *mthing;
	if (mobj->tics > 0)
		mobj->tics = 1 + (P_Random () % mobj->tics);
	if (mobj->flags & MF_COUNTKILL)
		totalkills++;
		
	mobj->angle = ANG45 * (mthing->angle/45);
	if (mthing->options & MTF_5)
		mobj->flags |= MF_23;
	if (mthing->options & MTF_3)
		mobj->flags |= MF_5;
	if (mthing->options & MTF_6)
		mobj->flags |= MF_26;
	if (mthing->options & MTF_8)
		mobj->flags |= MF_SHADOW;
	if (mthing->options & MTF_9)
		mobj->flags |= MF_27;
}

/*
===============================================================================

						GAME SPAWN FUNCTIONS

===============================================================================
*/

/*
================
=
= P_SpawnPuff
=
================
*/

extern fixed_t attackrange;

void P_SpawnPuff (fixed_t x, fixed_t y, fixed_t z)
{
	mobj_t	*th;
	
	z += ((P_Random()-P_Random())<<10);
	th = P_SpawnMobj (x,y,z, MT_115);
// don't make punches spark on the wall
	if (attackrange == 80*FRACUNIT)
		P_SetMobjState (th, S_POW2_141);
}

void func_2D4A4 (int x, int y, int z)
{
	z += ((P_Random()-P_Random())<<10);
	P_SpawnMobj (x,y,z, MT_116);
}

/*
================
=
= P_SpawnBlood
=
================
*/

void P_SpawnBlood (fixed_t x, fixed_t y, fixed_t z, int damage)
{
	mobj_t	*th;
	
	z += ((P_Random()-P_Random())<<10);
	th = P_SpawnMobj (x,y,z, MT_117);
	th->momz = FRACUNIT*2;
	if (damage >= 10 && damage <= 13)
		P_SetMobjState (th,S_BLOD_172);
	else if (damage < 10 && damage >= 7)
		P_SetMobjState (th,S_BLOD_173);
	else if (damage < 7)
		P_SetMobjState (th,S_BLOD_174);
}

/*
================
=
= P_CheckMissileSpawn
=
= Moves the missile forward a bit and possibly explodes it right there
=
================
*/

void P_CheckMissileSpawn (mobj_t *th)
{
	th->x += (th->momx>>1);
	th->y += (th->momy>>1);	// move a little forward so an angle can
							// be computed if it immediately explodes
	th->z += (th->momz>>1);
	if (!P_TryMove (th, th->x, th->y))
		P_ExplodeMissile (th);
}

/*
================
=
= P_SpawnMissile
=
================
*/

mobj_t *P_SpawnMissile (mobj_t *source, mobj_t *dest, mobjtype_t type)
{
	mobj_t		*th;
	angle_t		an;
	int			dist;
	
	th = P_SpawnMobj (source->x,source->y, source->z + 4*8*FRACUNIT, type);
	if (th->info->seesound)
		S_StartSound (th, th->info->seesound);
	th->target = source;		// where it came from
	an = R_PointToAngle2 (source->x, source->y, dest->x, dest->y);	
	// fuzzy player
	if (dest->flags & MF_SHADOW)
		an += (P_Random()-P_Random())<<21;	
	else if (dest->flags & MF_27)
		an += (P_Random()-P_Random())<<22;	
	th->angle = an;
	an >>= ANGLETOFINESHIFT;
	th->momx = FixedMul (th->info->speed, finecosine[an]);
	th->momy = FixedMul (th->info->speed, finesine[an]);
	
	dist = P_AproxDistance (dest->x - source->x, dest->y - source->y);
	dist = dist / th->info->speed;
	if (dist < 1)
		dist = 1;
	th->momz = (dest->z - source->z) / dist;
	P_CheckMissileSpawn (th);
	return th;
}


mobj_t *func_2D6F8 (mobj_t *source, mobj_t *dest, mobjtype_t type)
{
	mobj_t		*th;
	angle_t		an;
	int			dist;
	
	th = P_SpawnMobj (source->x,source->y, source->z + 4*8*FRACUNIT, type);
	if (th->info->seesound)
		S_StartSound (th, th->info->seesound);
	th->target = source;		// where it came from
	an = source->angle;	
	// fuzzy player
	if (dest->flags & MF_SHADOW)
		an += (P_Random()-P_Random())<<21;	
	else if (dest->flags & MF_27)
		an += (P_Random()-P_Random())<<22;	
	th->angle = an;
	an >>= ANGLETOFINESHIFT;
	th->momx = FixedMul (th->info->speed, finecosine[an]);
	th->momy = FixedMul (th->info->speed, finesine[an]);
	
	dist = P_AproxDistance (dest->x - source->x, dest->y - source->y);
	dist = dist / th->info->speed;
	if (dist < 1)
		dist = 1;
	th->momz = (dest->z - source->z) / dist;
	P_CheckMissileSpawn (th);
	return th;
}

/*
================
=
= P_SpawnPlayerMissile
=
= Tries to aim at a nearby monster
================
*/

mobj_t *P_SpawnPlayerMissile (mobj_t *source, mobjtype_t type)
{
	mobj_t			*th;
	angle_t			an;
	fixed_t			x,y,z, slope;
			
//
// see which target is to be aimed at
//
	an = source->angle;
	slope = P_AimLineAttack (source, an, 16*64*FRACUNIT);
	if (!linetarget)
	{
		an += 1<<26;
		slope = P_AimLineAttack (source, an, 16*64*FRACUNIT);
		if (!linetarget)
		{
			an -= 2<<26;
			slope = P_AimLineAttack (source, an, 16*64*FRACUNIT);
		}
		if (!linetarget)
		{
			an = source->angle;
		}
	}
	if (linetarget)
		source->target = linetarget;
	
	x = source->x;
	y = source->y;
	z = source->z + 4*8*FRACUNIT;

	if (source->flags & MF_13)
		z -= 10*FRACUNIT;
	
	th = P_SpawnMobj (x,y,z, type);
	if (th->info->seesound)
		S_StartSound (th, th->info->seesound);
	th->target = source;
	th->angle = an;
	th->momx = FixedMul( th->info->speed, finecosine[an>>ANGLETOFINESHIFT]);
	th->momy = FixedMul( th->info->speed, finesine[an>>ANGLETOFINESHIFT]);
	th->momz = FixedMul( th->info->speed, slope);

	P_CheckMissileSpawn (th);

	return th;
}

mobj_t *func_2D96C (mobj_t *source, mobjtype_t type)
{
	mobj_t			*th;
	angle_t			an;
	fixed_t			x,y,z, slope;
	
	an = source->angle;
	x = source->x;
	y = source->y;
	z = source->z;
	
	th = P_SpawnMobj (x,y,z, type);
	th->target = source;
	th->angle = an;
	th->momx = FixedMul( th->info->speed, finecosine[an>>ANGLETOFINESHIFT]);
	th->momy = FixedMul( th->info->speed, finesine[an>>ANGLETOFINESHIFT]);

	P_CheckMissileSpawn (th);

	slope = P_AimLineAttack (source, an, 16*64*FRACUNIT);
	
	th->momz = FixedMul( th->info->speed, slope);

	if (source->flags & MF_13)
		z -= 10*FRACUNIT;

	return th;
}

