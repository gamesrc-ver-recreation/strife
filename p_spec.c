//
// Copyright (C) 1993-1996 Id Software, Inc.
// Copyright (C) 2022 Nuke.YKT
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
//

// P_Spec.c

#include <stdlib.h>
#include "DoomDef.h"
#include "P_local.h"
#include "soundst.h"

animstruct1_t _int_87208[] =
{
	{"F_WATR03", 0, -1},
	{"F_WATR02", 0, -1},
	{"F_WATR01", 0, -1},
	{"F_VWATR3", 0, -1},
	{"F_VWATR2", 0, -1},
	{"P_VWATR1", 0, -1},
	{"F_HWATR3", 0, -1},
	{"F_HWATR2", 0, -1},
	{"F_HWATR1", 0, -1},
	{"F_PWATR3", 1, -1},
	{"F_PWATR2", 1, -1},
	{"F_PWATR1", 1, -1},
	{"END", -1, -1}
};

/*
===================
=
= P_InitPicAnims
=
===================
*/

animdef_t		animdefs[] =
{
	{false, "F_SCANR8", "F_SCANR5", 4},
	{false, "F_WATR03", "F_WATR01", 8},
	{false, "F_PWATR3", "F_PWATR1", 11},
	{false, "F_SCANR4", "F_SCANR1", 4},
	{true, "SCAN08", "SCAN05", 4},
	{true, "SWTRMG03", "SWTRMG01", 4},
	{true, "SCAN04", "SCAN01", 4},
	{true, "COMP04", "COMP01", 4},
	{true, "COMP08", "COMP05", 6},
	{true, "COMP12", "COMP09", 11},
	{true, "COMP16", "COMP13", 12},
	{true, "COMP20", "COMP17", 12},
	{true, "COMP24", "COMP21", 12},
	{true, "COMP28", "COMP25", 12},
	{true, "COMP32", "COMP29", 12},
	{true, "COMP37", "COMP33", 12},
	{true, "COMP41", "COMP38", 12},
	{true, "COMP49", "COMP42", 10},
	{true, "BRKGRY16", "BRKGRY13", 10},
	{true, "BRNSCN04", "BRNSCN01", 10},
	{true, "CONCRT12", "CONCRT09", 11},
	{true, "CONCRT25", "CONCRT22", 11},
	{true, "WALPMP02", "WALPMP01", 16},
	{true, "WALTEK17", "WALTEK16", 8},
	{true, "FORCE04", "FORCE01", 4},
	{true, "FORCE08", "FORCE05", 4},
	{true, "FAN02", "FAN01", 4},
	{false, "F_VWATR3", "P_VWATR1", 4},
	{false, "F_HWATR3", "F_HWATR1", 4},
	{false, "F_TELE2", "F_TELE1", 4},
	{false, "F_FAN2", "F_FAN1", 4},
	{false, "F_CONVY2", "F_CONVY1", 4},
	{false, "F_RDALN4", "F_RDALN1", 4},

	{-1}
};

anim_t	anims[MAXANIMS];
anim_t	*lastanim;


void P_InitPicAnims (void)
{
	int		i;
	
//
//	Init animation
//
	lastanim = anims;
	for (i=0 ; animdefs[i].istexture != -1 ; i++)
	{
		if (i == MAXANIMS)
			I_Error("max animated lines (%i) reached: %s", MAXANIMS, animdefs[i].startname);
		if (animdefs[i].istexture)
		{
			if (R_CheckTextureNumForName(animdefs[i].startname) == -1)
				continue;	// different episode
			lastanim->picnum = R_TextureNumForName (animdefs[i].endname);
			lastanim->basepic = R_TextureNumForName (animdefs[i].startname);
		}
		else
		{
			if (W_CheckNumForName(animdefs[i].startname) == -1)
				continue;
			lastanim->picnum = R_FlatNumForName (animdefs[i].endname);
			lastanim->basepic = R_FlatNumForName (animdefs[i].startname);
		}
		lastanim->istexture = animdefs[i].istexture;
		lastanim->numpics = lastanim->picnum - lastanim->basepic + 1;
		if (lastanim->numpics < 2)
			I_Error ("P_InitPicAnims: bad cycle from %s to %s"
			, animdefs[i].startname, animdefs[i].endname);
		lastanim->speed = animdefs[i].speed;
		lastanim++;
	}

}

int func_28FA8(mobj_t *a1)
{
	sector_t *sec;
	int i;
	int pic;
	sec = a1->subsector->sector;
	pic = sec->floorpic;
	if (a1->z <= sec->floorheight)
	{
		for (i = 0; _int_87208[i].f_4 != -1; i++)
		{
			if (_int_87208[i].f_8 == pic)
			{
				return _int_87208[i].f_4;
			}
		}
	}
	return 2;
}

void func_28FF8(void)
{
	int i;
	for (i = 0; _int_87208[i].f_4 != -1; i++)
	{
		_int_87208[i].f_8 = R_FlatNumForName(_int_87208[i].f_0);
	}
}

/*
==============================================================================

							UTILITIES

==============================================================================
*/

//
//	Will return a side_t* given the number of the current sector,
//		the line number, and the side (0/1) that you want.
//
side_t *getSide(int currentSector,int line, int side)
{
	return &sides[ (sectors[currentSector].lines[line])->sidenum[side] ];
}

//
//	Will return a sector_t* given the number of the current sector,
//		the line number and the side (0/1) that you want.
//
sector_t *getSector(int currentSector,int line,int side)
{
	return sides[ (sectors[currentSector].lines[line])->sidenum[side] ].sector;
}

//
//	Given the sector number and the line number, will tell you whether
//		the line is two-sided or not.
//
int	twoSided(int sector,int line)
{
	return (sectors[sector].lines[line])->flags & ML_TWOSIDED;
}

//==================================================================
//
//	Return sector_t * of sector next to current. NULL if not two-sided line
//
//==================================================================
sector_t *getNextSector(line_t *line,sector_t *sec)
{
	if (!(line->flags & ML_TWOSIDED))
		return NULL;
		
	if (line->frontsector == sec)
		return line->backsector;
	
	return line->frontsector;
}

//==================================================================
//
//	FIND LOWEST FLOOR HEIGHT IN SURROUNDING SECTORS
//
//==================================================================
fixed_t	P_FindLowestFloorSurrounding(sector_t *sec)
{
	int			i;
	line_t		*check;
	sector_t	*other;
	fixed_t		floor = sec->floorheight;
	
	for (i=0 ;i < sec->linecount ; i++)
	{
		check = sec->lines[i];
		other = getNextSector(check,sec);
		if (!other)
			continue;
		if (other->floorheight < floor)
			floor = other->floorheight;
	}
	return floor;
}

//==================================================================
//
//	FIND HIGHEST FLOOR HEIGHT IN SURROUNDING SECTORS
//
//==================================================================
fixed_t	P_FindHighestFloorSurrounding(sector_t *sec)
{
	int			i;
	line_t		*check;
	sector_t	*other;
	fixed_t		floor = -500*FRACUNIT;
	
	for (i=0 ;i < sec->linecount ; i++)
	{
		check = sec->lines[i];
		other = getNextSector(check,sec);
		if (!other)
			continue;			
		if (other->floorheight > floor)
			floor = other->floorheight;
	}
	return floor;
}

//==================================================================
//
//	FIND NEXT HIGHEST FLOOR IN SURROUNDING SECTORS
//
//==================================================================
fixed_t	P_FindNextHighestFloor(sector_t *sec,int currentheight)
{
	int			i;
	int			h;
	int			min;
	line_t		*check;
	sector_t	*other;
	fixed_t		height = currentheight;
	fixed_t		heightlist[20];		// 20 adjoining sectors max!
	
	for (i =0,h = 0 ;i < sec->linecount ; i++)
	{
		check = sec->lines[i];
		other = getNextSector(check,sec);
		if (!other)
			continue;
		if (other->floorheight > height)
			heightlist[h++] = other->floorheight;
	}
	
	//
	// Find lowest height in list
	//
	if (!h)
		return currentheight;
	min = heightlist[0];
	for (i = 1;i < h;i++)
		if (heightlist[i] < min)
			min = heightlist[i];
			
	return min;
}

//==================================================================
//
//	FIND LOWEST CEILING IN THE SURROUNDING SECTORS
//
//==================================================================
fixed_t	P_FindLowestCeilingSurrounding(sector_t *sec)
{
	int			i;
	line_t		*check;
	sector_t	*other;
	fixed_t		height = MAXINT;
	
	for (i=0 ;i < sec->linecount ; i++)
	{
		check = sec->lines[i];
		other = getNextSector(check,sec);
		if (!other)
			continue;
		if (other->ceilingheight < height)
			height = other->ceilingheight;
	}
	return height;
}

//==================================================================
//
//	FIND HIGHEST CEILING IN THE SURROUNDING SECTORS
//
//==================================================================
fixed_t	P_FindHighestCeilingSurrounding(sector_t *sec)
{
	int	i;
	line_t	*check;
	sector_t	*other;
	fixed_t	height = 0;
	
	for (i=0 ;i < sec->linecount ; i++)
	{
		check = sec->lines[i];
		other = getNextSector(check,sec);
		if (!other)
			continue;
		if (other->ceilingheight > height)
			height = other->ceilingheight;
	}
	return height;
}

//==================================================================
//
//	RETURN NEXT SECTOR # THAT LINE TAG REFERS TO
//
//==================================================================
int	P_FindSectorFromLineTag(line_t	*line,int start)
{
	int	i;
	
	for (i=start+1;i<numsectors;i++)
		if (sectors[i].tag == line->tag)
			return i;
	return -1;
}

//==================================================================
//
//	Find minimum light from an adjacent sector
//
//==================================================================
int	P_FindMinSurroundingLight(sector_t *sector,int max)
{
	int			i;
	int			min;
	line_t		*line;
	sector_t	*check;
	
	min = max;
	for (i=0 ; i < sector->linecount ; i++)
	{
		line = sector->lines[i];
		check = getNextSector(line,sector);
		if (!check)
			continue;
		if (check->lightlevel < min)
			min = check->lightlevel;
	}
	return min;
}

/*
==============================================================================

							EVENTS

Events are operations triggered by using, crossing, or shooting special lines, or by timed thinkers

==============================================================================
*/



/*
===============================================================================
=
= P_CrossSpecialLine - TRIGGER
=
= Called every time a thing origin is about to cross
= a line with a non 0 special
=
===============================================================================
*/

extern char *mapnames[];

void P_CrossSpecialLine (int linenum, int side, mobj_t *thing)
{
	line_t		*line;
	int			ok;
	static char buffer[80];
	int v;
	int m;
	int sc, mn;

	line = &lines[linenum];

	if (thing->flags&(MF_MISSILE | MF_CORPSE))
	{
		ok = 0;
		switch(line->special)
		{
			case 182:
				ok = 1;
				break;
		}
		if (!ok)
			return;
	}

	//
	//	Triggers that other things can activate
	//
	if (!thing->player)
	{

		ok = 0;
		switch(line->special)
		{
			case 39:	// TELEPORT TRIGGER
			case 97:	// TELEPORT RETRIGGER
			case 125:	// TELEPORT MONSTERONLY TRIGGER
			case 126:	// TELEPORT MONSTERONLY RETRIGGER
			case 4:	// RAISE DOOR
			case 10:	// PLAT DOWN-WAIT-UP-STAY TRIGGER
			case 88:	// PLAT DOWN-WAIT-UP-STAY RETRIGGER
			case 182:
			case 185:
			case 195:
			case 231:
				ok = 1;
				break;
		}
		if (!ok)
			return;
	}

	switch (line->special)
	{
		//====================================================
		// TRIGGERS
		//====================================================
#if 0
		case 40:		// RaiseCeilingLowerFloor
			EV_DoCeiling(line, raiseToHighest);
			EV_DoFloor(line, lowerFloorToLowest);
			line->special = 0;
			break;
#endif
		case 2:			// Open Door
			EV_DoDoor(line,open);
			line->special = 0;
			break;
		case 3:			// Close Door
			EV_DoDoor(line,close);
			line->special = 0;
			break;
		case 4:			// Raise Door
			EV_DoDoor(line,normal);
			line->special = 0;
			break;
		case 5:			// Raise Floor
			EV_DoFloor(line,raiseFloor);
			line->special = 0;
			break;
		case 6:			// Fast Ceiling Crush & Raise
			EV_DoCeiling(line,fastCrushAndRaise);
			line->special = 0;
			break;
		case 8:			// Build Stairs
			EV_BuildStairs(line,build8);
			line->special = 0;
			break;
		case 10:		// PlatDownWaitUp
			EV_DoPlat(line,downWaitUpStay,0);
			line->special = 0;
			break;
		case 12:		// Light Turn On - brightest near
			EV_LightTurnOn(line,0);
			line->special = 0;
			break;
		case 13:		// Light Turn On 255
			EV_LightTurnOn(line,255);
			line->special = 0;
			break;
		case 16:		// Close Door 30
			EV_DoDoor(line,close30ThenOpen);
			line->special = 0;
			break;
		case 17:		// Start Light Strobing
			EV_StartLightStrobing(line);
			line->special = 0;
			break;
		case 19:		// Lower Floor
			EV_DoFloor(line,lowerFloor);
			line->special = 0;
			break;
		case 22:		// Raise floor to nearest height and change texture
			EV_DoPlat(line,raiseToNearestAndChange,0);
			line->special = 0;
			break;
		case 25:		// Ceiling Crush and Raise
			EV_DoCeiling(line,crushAndRaise);
			line->special = 0;
			break;
		case 30:		// Raise floor to shortest texture height
						// on either side of lines
			EV_DoFloor(line,raiseToTexture);
			line->special = 0;
			break;
		case 35:		// Lights Very Dark
			EV_LightTurnOn(line,35);
			line->special = 0;
			break;
		case 36:		// Lower Floor (TURBO)
			EV_DoFloor(line,turboLower);
			line->special = 0;
			break;
		case 37:		// LowerAndChange
			EV_DoFloor(line,lowerAndChange);
			line->special = 0;
			break;
		case 38:		// Lower Floor To Lowest
			EV_DoFloor( line, lowerFloorToLowest );
			line->special = 0;
			break;
		case 196:
			if (thing->player->f_45 > 0)
			{
				EV_DoFloor(line,lowerFloorToLowest);
				line->special = 0;
			}
			break;
		case 212:
			if (thing->player->weaponowned[wp_5])
			{
				EV_DoFloor(line,lowerFloorToLowest);
				line->special = 0;
			}
			break;
		case 39:		// TELEPORT!
			EV_Teleport( line, side, thing, 0 );
			line->special = 0;
			break;
		case 44:		// Ceiling Crush
			EV_DoCeiling( line, lowerAndCrush );
			line->special = 0;
			break;
		case 52:		// EXIT!
			G_ExitLevel3 (line->tag / 100);
			break;
		case 53:		// Perpetual Platform Raise
			EV_DoPlat(line,perpetualRaise,0);
			line->special = 0;
			break;
		case 54:		// Platform Stop
			EV_StopPlat(line);
			line->special = 0;
			break;
		case 56:		// Raise Floor Crush
			EV_DoFloor(line,raiseFloorCrush);
			line->special = 0;
			break;
		case 57:		// Ceiling Crush Stop
			EV_CeilingCrushStop(line);
			line->special = 0;
			break;
		case 58:		// Raise Floor 24
			EV_DoFloor(line,raiseFloor24);
			line->special = 0;
			break;
		case 59:		// Raise Floor 24 And Change
			EV_DoFloor(line,raiseFloor24AndChange);
			line->special = 0;
			break;
		case 104:		// Turn lights off in sector(tag)
			EV_TurnTagLightsOff(line);
			line->special = 0;
			break;
		case 108:		// Blazing Door Raise (faster than TURBO!)
			EV_DoDoor (line,blazeRaise);
			line->special = 0;
			break;
		case 109:		// Blazing Door Open (faster than TURBO!)
			EV_DoDoor (line,blazeOpen);
			line->special = 0;
			break;
		case 100:		// Build Stairs Turbo 16
			EV_BuildStairs(line,turbo16);
			line->special = 0;
			break;
		case 110:		// Blazing Door Close (faster than TURBO!)
			EV_DoDoor (line,blazeClose);
			line->special = 0;
			break;
		case 197:
			if (thing->player->f_45 > 0)
			{
				EV_DoDoor (line,blazeClose);
				line->special = 0;
			}
			break;
		case 119:		// Raise floor to nearest surr. floor
			EV_DoFloor(line,raiseFloorToNearest);
			line->special = 0;
			break;
		case 121:		// Blazing PlatDownWaitUpStay
			EV_DoPlat(line,blazeDWUS,0);
			line->special = 0;
			break;
		case 124:		// Secret EXIT
			G_ExitLevel4 ();
			break;
		case 125:		// TELEPORT MonsterONLY
			if (!thing->player)
			{
				EV_Teleport( line, side, thing, 0 );
				line->special = 0;
			}
			break;
		case 130:		// Raise Floor Turbo
			EV_DoFloor(line,raiseFloorTurbo);
			line->special = 0;
			break;
		case 141:		// Silent Ceiling Crush & Raise
			EV_DoCeiling(line,silentCrushAndRaise);
			line->special = 0;
			break;
		case 174:
			EV_DoDoor (line,door_10);
			line->special = 0;
			break;
		case 183:
			EV_DoDoor (line,door_9);
			line->special = 0;
			break;
		case 178:
			EV_BuildStairs(line, stair_2);
			line->special = 0;
			break;
		case 179:
			EV_DoCeiling(line, lowerToFloor);
			line->special = 0;
			break;
		case 182:
			if (thing->flags & MF_MISSILE)
				P_ChangeSwitchTexture(line,1);
			break;
		case 193:
			if (thing->player->f_4d & (1<<((sides[line->sidenum[0]].rowoffset>>16)-1)))
			{
				EV_DoFloor(line, lowerFloorToLowest);
				line->special = 0;
			}
			break;
		case 187:
			if (thing->player->f_4d & (1<<((sides[line->sidenum[0]].rowoffset>>16)-1)))
			{
				func_1DC90(line);
				line->special = 0;
			}
			break;
		case 188:
			if (thing->player->f_4d & 0x8000)
			{
				EV_DoDoor(line, open);
				line->special = 0;
			}
			break;
		case 200:
			if (thing->player->weaponowned[wp_7])
			{
				EV_DoDoor(line, open);
				line->special = 0;
			}
			break;
		case 201:
			if (side == 1)
				break;
		case 202:
			if (thing->player == &players[consoleplayer] && thing->player->powers[pw_4])
			{
				sprintf(buffer, "voc%i", line->tag);
				I_StartVoice(buffer);
				sprintf(buffer, "log%i", line->tag);
				v = W_CheckNumForName(buffer);
				if (v > 0)
				{
					strncpy(_char_A1300, W_CacheLumpNum(v, PU_CACHE), 300);
					thing->player->message = "Incoming Message...";
				}
				line->special = 0;
			}
			break;
		case 210:
			if (thing->player == &players[consoleplayer] && thing->player->powers[pw_4]
				&& thing->player->weaponowned[wp_5])
			{
				sprintf(buffer, "voc%i", line->tag);
				I_StartVoice(buffer);
				sprintf(buffer, "log%i", line->tag);
				v = W_CheckNumForName(buffer);
				if (v > 0)
				{
					strncpy(_char_A1300, W_CacheLumpNum(v, PU_CACHE), 300);
					thing->player->message = "Incoming Message from BlackBird...";
				}
				line->special = 0;
			}
			break;
		case 215:
			if (thing->player == &players[consoleplayer] && thing->player->powers[pw_4]
				&& (line->tag == 0 || (thing->player->f_4d&(1<<((line->tag%100)-1)))))
			{
				sprintf(buffer, "voc%i", line->tag/100);
				I_StartVoice(buffer);
				sprintf(buffer, "log%i", line->tag/100);
				v = W_CheckNumForName(buffer);
				if (v > 0)
				{
					strncpy(_char_A1300, W_CacheLumpNum(v, PU_CACHE), 300);
					thing->player->message = "Incoming Message from BlackBird...";
				}
				line->special = 0;
			}
			break;
		case 216:
			if (thing->player->f_4d & (1<<((sides[line->sidenum[0]].rowoffset>>16)-1)))
				EV_DoDoor(line, normal);
			break;
		case 204:
			if (thing->player == &players[consoleplayer])
			{
				S_ChangeMusic(line->tag, 1);
				line->special = 0;
			}
			break;
		case 227:
			if (thing->player->f_4d & (1<<((sides[line->sidenum[0]].rowoffset>>16)-1)))
			{
				EV_DoDoor(line, close);
				line->special = 0;
			}
			break;
		case 230:
			if (thing->player->f_4d & (1<<((sides[line->sidenum[0]].rowoffset>>16)-1)))
			{
				EV_DoDoor(line, open);
				line->special = 0;
			}
			break;
		case 228:
			if (thing->player->f_4d & 0x800000)
			{
				if (thing->player->f_4d & 0x8000000)
					I_StartVoice("voc130");
				else
					I_StartVoice("voc128");
				line->special = 0;
			}
			break;

	//====================================================
	// RE-DOABLE TRIGGERS
	//====================================================

		case 72:		// Ceiling Crush
			EV_DoCeiling( line, lowerAndCrush );
			break;
		case 73:		// Ceiling Crush and Raise
			EV_DoCeiling(line,crushAndRaise);
			break;
		case 74:		// Ceiling Crush Stop
			EV_CeilingCrushStop(line);
			break;
		case 75:			// Close Door
			EV_DoDoor(line,close);
			break;
		case 76:		// Close Door 30
			EV_DoDoor(line,close30ThenOpen);
			break;
		case 77:			// Fast Ceiling Crush & Raise
			EV_DoCeiling(line,fastCrushAndRaise);
			break;
		case 79:		// Lights Very Dark
			EV_LightTurnOn(line,35);
			break;
		case 80:		// Light Turn On - brightest near
			EV_LightTurnOn(line,0);
			break;
		case 81:		// Light Turn On 255
			EV_LightTurnOn(line,255);
			break;
		case 82:		// Lower Floor To Lowest
			EV_DoFloor( line, lowerFloorToLowest );
			break;
		case 83:		// Lower Floor
			EV_DoFloor(line,lowerFloor);
			break;
		case 84:		// LowerAndChange
			EV_DoFloor(line,lowerAndChange);
			break;
		case 86:			// Open Door
			EV_DoDoor(line,open);
			break;
		case 87:		// Perpetual Platform Raise
			EV_DoPlat(line,perpetualRaise,0);
			break;
		case 88:		// PlatDownWaitUp
			EV_DoPlat(line,downWaitUpStay,0);
			break;
		case 89:		// Platform Stop
			EV_StopPlat(line);
			break;
		case 90:			// Raise Door
			EV_DoDoor(line,normal);
			break;
		case 91:			// Raise Floor
			EV_DoFloor(line,raiseFloor);
			break;
		case 92:		// Raise Floor 24
			EV_DoFloor(line,raiseFloor24);
			break;
		case 93:		// Raise Floor 24 And Change
			EV_DoFloor(line,raiseFloor24AndChange);
			break;
		case 94:		// Raise Floor Crush
			EV_DoFloor(line,raiseFloorCrush);
			break;
		case 95:		// Raise floor to nearest height and change texture
			EV_DoPlat(line,raiseToNearestAndChange,0);
			break;
		case 96:		// Raise floor to shortest texture height
						// on either side of lines
			EV_DoFloor(line,raiseToTexture);
			break;
		case 97:		// TELEPORT!
			EV_Teleport( line, side, thing, 0 );
			break;
		case 98:		// Lower Floor (TURBO)
			EV_DoFloor(line,turboLower);
			break;
		case 105:		// Blazing Door Raise (faster than TURBO!)
			EV_DoDoor (line,blazeRaise);
			break;
		case 106:		// Blazing Door Open (faster than TURBO!)
			EV_DoDoor (line,blazeOpen);
			break;
		case 107:		// Blazing Door Close (faster than TURBO!)
			EV_DoDoor (line,blazeClose);
			break;
		case 120:		// Blazing PlatDownWaitUpStay
			EV_DoPlat(line,blazeDWUS,0);
			break;
		case 126:		// TELEPORT MonsterONLY
			if (!thing->player)
				EV_Teleport( line, side, thing, 0 );
			break;
		case 128:		// Raise To Nearest Floor
			EV_DoFloor(line,raiseFloorToNearest);
			break;
		case 129:		// Raise Floor Turbo
			EV_DoFloor(line,raiseFloorTurbo);
			break;
		case 186:
			if (side == 1)
				break;
		case 145:
			thing->momz = thing->momx = thing->momy = 0;
			m = line->tag / 100;
			if (players[0].weaponowned[wp_7])
			{
				if (m == 7)
					m = 10;
				else if (m == 3)
					m = 30;
			}
			sprintf(buffer, "Entering%s", mapnames[m - 1] + 8);
			thing->player->message = buffer;
			if (netgame && deathmatch)
			{
				if (levelTimer == 1 && levelTimeCount)
				{
					sc = levelTimeCount / 35;
					mn = sc / 60;
					sprintf(buffer, "%d min left", mn);
				}
				else
				{
					line->special = 0;
					EV_DoFloor(line, raiseFloor24);
				}
			}
			else if (thing->health > 0)
			{
				G_ExitLevel(line->tag / 100, line->tag % 100, thing->angle);
			}
			break;
		case 175:
			if (thing->floorz + 16*FRACUNIT > thing->z)
				P_NoiseAlert(thing->player->mo, thing->player->mo);
			break;
		case 198:
			if (!func_2DC68(thing->player, 163))
				P_NoiseAlert(thing->player->mo, thing->player->mo);
			break;
		case 150:
			P_NoiseAlert(thing->player->mo, thing->player->mo);
			break;
		case 208:
			if (thing->player->weaponowned[wp_5])
				P_NoiseAlert(thing->player->mo, thing->player->mo);
			break;
		case 206:
			if (func_2DC68(thing->player, 174))
				P_NoiseAlert(thing->player->mo, thing->player->mo);
			break;
		case 184:
			if (EV_DoPlat(line, plat_6, 0))
				P_ChangeSwitchTexture(line, 1);
			break;
		case 185:
			EV_Teleport(line, side, thing, 51);
			break;
		case 195:
			EV_Teleport(line, side, thing, 51);
			P_SetMobjState(thing, S_AGRD_419);
			break;
		case 203:
			if (thing->player == &players[consoleplayer])
				S_ChangeMusic(line->tag, 1);
			break;
		case 231:
			EV_Teleport(line, side, thing, 33);
			break;
	}
}



/*
===============================================================================
=
= P_ShootSpecialLine - IMPACT SPECIALS
=
= Called when a thing shoots a special line
=
===============================================================================
*/

void	P_ShootSpecialLine ( mobj_t *thing, line_t *line)
{
	int		ok;
	
	//
	//	Impacts that other things can activate
	//
	if (!thing->player)
	{
		ok = 0;
		switch(line->special)
		{
			case 46:		// OPEN DOOR IMPACT
			case 182:
				ok = 1;
				break;
		}
		if (!ok)
			return;
	}

	switch(line->special)
	{
		case 24:		// RAISE FLOOR
			EV_DoFloor(line,raiseFloor);
			P_ChangeSwitchTexture(line,0);
			break;
		case 46:		// OPEN DOOR
			EV_DoDoor(line,open);
			P_ChangeSwitchTexture(line,1);
			break;
		case 47:		// RAISE FLOOR NEAR AND CHANGE
			EV_DoPlat(line,raiseToNearestAndChange,0);
			P_ChangeSwitchTexture(line,0);
			break;
		case 180:
			EV_DoFloor(line,floor_13);
			P_ChangeSwitchTexture(line,0);
			break;
		case 182:
			P_ChangeSwitchTexture(line,0);
			break;
	}
}


/*
===============================================================================
=
= P_PlayerInSpecialSector
=
= Called every tic frame that the player origin is in a special sector
=
===============================================================================
*/

void P_PlayerInSpecialSector(player_t *player)
{
	sector_t	*sector;
	int t;
	
	sector = player->mo->subsector->sector;
	if (player->mo->z != sector->floorheight)
		return;		// not all the way down yet
		
	switch (sector->special)
	{
		case 15:
			P_DamageMobj (player->mo, NULL, NULL, 999);
			break;
		case 18:
			t = sector->tag - 100;
			if (!(player->cheats&CF_NOCLIP))
			{
				P_Thrust(player, (t / 10)<<29, (t%10)<<12);
			}
			break;
		case 5:		// HELLSLIME DAMAGE
			if (!player->powers[pw_2])
				player->f_49 += 2;
			break;
		case 16:	// SUPER HELLSLIME DAMAGE
			if (!player->powers[pw_2])
				player->f_49 += 4;
			break;
		case 7:		// NUKAGE DAMAGE
		case 4:		// STROBE HURT
			if (!player->powers[pw_2] )
				if (!(leveltime&0x1f))
					P_DamageMobj (player->mo, NULL, NULL, 5);
			break;
			
		case 9:		// SECRET SECTOR
			//player->secretcount++;
			sector->special = 0;
			if (player == &players[consoleplayer])
				S_StartSound(NULL, sfx_yeah);
			break;
		case 11:	// EXIT SUPER DAMAGE! (for E1M8 finale)
			player->cheats &= ~CF_GODMODE;

			if (!(leveltime&0x1f))
				P_DamageMobj (player->mo, NULL, NULL, 20);

			if (player->health <= 10)
				G_ExitLevel3(0);
			break;
			
		default:
			I_Error ("P_PlayerInSpecialSector: "
					"unknown special %i",sector->special);
	};
}


/*
===============================================================================
=
= P_UpdateSpecials
=
= Animate planes, scroll walls, etc
===============================================================================
*/
boolean		levelTimer;
int		levelTimeCount;

void P_UpdateSpecials (void)
{
	anim_t	*anim;
	int		pic;
	int		i;
	line_t	*line;
	
	//
	//	LEVEL TIMER
	//
	if (levelTimer == true)
	{
		if (levelTimeCount)
			levelTimeCount--;
		//if (!levelTimeCount)
		//	G_ExitLevel();
	}
	
	//
	//	ANIMATE FLATS AND TEXTURES GLOBALY
	//
	for (anim = anims ; anim < lastanim ; anim++)
	{
		for (i = anim->basepic; i < anim->basepic + anim->numpics; i++)
		{
			pic = anim->basepic + ( (leveltime/anim->speed + i)%anim->numpics );
			if (anim->istexture)
				texturetranslation[i] = pic;
			else
				flattranslation[i] = pic;
		}
	}
	
	//
	//	ANIMATE LINE SPECIALS
	//
	for (i = 0; i < numlinespecials; i++)
	{
		line = linespeciallist[i];
		switch(line->special)
		{
			case 48:	// EFFECT FIRSTCOL SCROLL +
				sides[line->sidenum[0]].textureoffset += FRACUNIT;
				break;
			case 149:
				sides[line->sidenum[0]].textureoffset -= FRACUNIT;
				break;
			case 142:
				sides[line->sidenum[0]].rowoffset += FRACUNIT;
				break;
			case 143:
				sides[line->sidenum[0]].rowoffset -= 3*FRACUNIT;
				break;
		}
	}
	
	//
	//	DO BUTTONS
	//
	for (i = 0; i < MAXBUTTONS; i++)
		if (buttonlist[i].btimer)
		{
			buttonlist[i].btimer--;
			if (!buttonlist[i].btimer)
			{
				switch(buttonlist[i].where)
				{
					case top:
						sides[buttonlist[i].line->sidenum[0]].toptexture =
							buttonlist[i].btexture;
						break;
					case middle:
						sides[buttonlist[i].line->sidenum[0]].midtexture =
							buttonlist[i].btexture;
						break;
					case bottom:
						sides[buttonlist[i].line->sidenum[0]].bottomtexture =
							buttonlist[i].btexture;
						break;
				}
				S_StartSound((mobj_t *)&buttonlist[i].soundorg,sfx_swtchn);
				memset(&buttonlist[i],0,sizeof(button_t));
			}
		}
	
}

//============================================================
//
//	Special Stuff that can't be categorized
//
//============================================================
int EV_DoDonut(line_t *line)
{
	sector_t	*s1;
	sector_t	*s2;
	sector_t	*s3;
	int			secnum;
	int			rtn;
	int			i;
	floormove_t		*floor;
	
	secnum = -1;
	rtn = 0;
	while ((secnum = P_FindSectorFromLineTag(line,secnum)) >= 0)
	{
		s1 = &sectors[secnum];
		
		//	ALREADY MOVING?  IF SO, KEEP GOING...
		if (s1->specialdata)
			continue;
			
		rtn = 1;
		s2 = getNextSector(s1->lines[0],s1);
		for (i = 0;i < s2->linecount;i++)
		{
			if ((!s2->lines[i]->flags & ML_TWOSIDED) ||
				(s2->lines[i]->backsector == s1))
				continue;
			s3 = s2->lines[i]->backsector;

			//
			//	Spawn rising slime
			//
			floor = Z_Malloc (sizeof(*floor), PU_LEVSPEC, 0);
			P_AddThinker (&floor->thinker);
			s2->specialdata = floor;
			floor->thinker.function = T_MoveFloor;
			floor->type = donutRaise;
			floor->crush = false;
			floor->direction = 1;
			floor->sector = s2;
			floor->speed = FLOORSPEED / 2;
			floor->texture = s3->floorpic;
			floor->newspecial = 0;
			floor->floordestheight = s3->floorheight;
			
			//
			//	Spawn lowering donut-hole
			//
			floor = Z_Malloc (sizeof(*floor), PU_LEVSPEC, 0);
			P_AddThinker (&floor->thinker);
			s1->specialdata = floor;
			floor->thinker.function = T_MoveFloor;
			floor->type = lowerFloor;
			floor->crush = false;
			floor->direction = -1;
			floor->sector = s1;
			floor->speed = FLOORSPEED / 2;
			floor->floordestheight = s3->floorheight;
			break;
		}
	}
	return rtn;
}

/*
==============================================================================

							SPECIAL SPAWNING

==============================================================================
*/
/*
================================================================================
= P_SpawnSpecials
=
= After the map has been loaded, scan for specials that
= spawn thinkers
=
===============================================================================
*/

short	numlinespecials;
line_t	*linespeciallist[MAXLINEANIMS];

void P_SpawnSpecials (void)
{
	sector_t	*sector;
	int		i;
	int		episode;

	episode = 1;
	if (W_CheckNumForName("texture2") >= 0)
		episode = 2;
		
	//
	// See if -TIMER needs to be used
	//
	levelTimer = false;
	
	i = M_CheckParm("-timer");
	if (i && deathmatch)
	{
		int	time;
		time = atoi(myargv[i+1]) * 60 * 35;
		levelTimer = true;
		levelTimeCount = time;
	}

	//
	//	Init special SECTORs
	//
	sector = sectors;
	for (i=0 ; i<numsectors ; i++, sector++)
	{
		if (!sector->special)
			continue;
		switch (sector->special)
		{
			case 1:		// FLICKERING LIGHTS
				P_SpawnLightFlash (sector);
				break;
			case 2:		// STROBE FAST
				P_SpawnStrobeFlash(sector,FASTDARK,0);
				break;
			case 3:		// STROBE SLOW
				P_SpawnStrobeFlash(sector,SLOWDARK,0);
				break;
			case 4:		// STROBE FAST/DEATH SLIME
				P_SpawnStrobeFlash(sector,FASTDARK,0);
				sector->special = 4;
				break;
			case 8:		// GLOWING LIGHT
				P_SpawnGlowingLight(sector);
				break;
			case 9:		// SECRET SECTOR
				totalsecret++;
				break;
			case 10:	// DOOR CLOSE IN 30 SECONDS
				P_SpawnDoorCloseIn30 (sector);
				break;
			case 12:	// SYNC STROBE SLOW
				P_SpawnStrobeFlash (sector, SLOWDARK, 1);
				break;
			case 13:	// SYNC STROBE FAST
				P_SpawnStrobeFlash (sector, FASTDARK, 1);
				break;
			case 14:	// DOOR RAISE IN 5 MINUTES
				P_SpawnDoorRaiseIn5Mins (sector, i);
				break;
			case 17:
				P_SpawnFireFlicker(sector);
				break;
		}
	}
		
	
	//
	//	Init line EFFECTs
	//
	numlinespecials = 0;
	for (i = 0;i < numlines; i++)
		switch(lines[i].special)
		{
			case 48:	// EFFECT FIRSTCOL SCROLL+
			case 142:
			case 143:
			case 149:
				linespeciallist[numlinespecials] = &lines[i];
				numlinespecials++;
				break;
		}
		
	//
	//	Init other misc stuff
	//
	for (i = 0;i < MAXCEILINGS;i++)
		activeceilings[i] = NULL;
	for (i = 0;i < MAXPLATS;i++)
		activeplats[i] = NULL;
	for (i = 0;i < MAXBUTTONS;i++)
		memset(&buttonlist[i],0,sizeof(button_t));

	P_InitSlidingDoorFrames();
}

int _int_a0740; // incorrectly defined enum?
