//
// Copyright (C) 1993-1996 Id Software, Inc.
// Copyright (C) 2022 Nuke.YKT
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
//

// P_inter.c

#include "DoomDef.h"
#include "P_local.h"
#include "soundst.h"

#define BONUSADD 6

// a weapon is found with two clip loads, a big item has five clip loads
int		maxammo[NUMAMMO] = {250, 50, 25, 400, 100, 30, 16};
int		clipammo[NUMAMMO] = {10, 4, 2, 20, 4, 6, 4};


/*
===============================================================================
							GET STUFF
===============================================================================
*/

/* 
=================== 
= 
= P_GiveAmmo
=
= Num is the number of clip loads, not the individual count (0= 1/2 clip)
= Returns false if the ammo can't be picked up at all
=================== 
*/ 

boolean P_GiveAmmo (player_t *player, ammotype_t ammo, int num)
{
	int		oldammo;
	
	if (ammo == am_noammo)
		return false;
		
	if (ammo > NUMAMMO)
		I_Error ("P_GiveAmmo: bad type %i", ammo);
		
	if ( player->ammo[ammo] == player->maxammo[ammo]  )
		return false;
			
	if (num)
		num *= clipammo[ammo];
	else
		num = clipammo[ammo]/2;
	if (gameskill == sk_baby || gameskill == sk_nightmare)
		num <<= 1;			// give double ammo in trainer mode, you'll need in nightmare
		
	oldammo = player->ammo[ammo];
	player->ammo[ammo] += num;
	if (player->ammo[ammo] > player->maxammo[ammo])
		player->ammo[ammo] = player->maxammo[ammo];
	
	if (oldammo)
		return true;		// don't change up weapons, player was lower on
							// purpose

	if (player->readyweapon != wp_0)
		return true;

	switch (ammo)
	{
	case am_0:
		if (player->weaponowned[wp_2])
			player->pendingweapon = wp_2;
		break;
	case am_1:
		if (player->weaponowned[wp_1])
			player->pendingweapon = wp_1;
		break;
	case am_3:
		if (player->weaponowned[wp_6])
			player->pendingweapon = wp_6;
		break;
	case am_4:
		if (player->weaponowned[wp_3])
			player->pendingweapon = wp_3;
		break;
	default:
		break;
	}
	
	return true;
}


/* 
=================== 
= 
= P_GiveWeapon
=
= The weapon name may have a MF_DROPPED flag ored in
=================== 
*/ 

boolean P_GiveWeapon (player_t *player, weapontype_t weapon, boolean dropped)
{
	boolean		gaveammo, gaveweapon;

	if (player->weaponowned[weapon])
		gaveweapon = false;
	else
	{
		gaveweapon = true;
		player->weaponowned[weapon] = true;

		if (weapon == wp_1)
			player->weaponowned[wp_8] = true;
		else if (weapon == wp_4)
			player->weaponowned[wp_9] = true;
		else if (weapon == wp_6)
			player->weaponowned[wp_10] = true;

		if (weapon > player->readyweapon && weapon <= wp_7)
			player->pendingweapon = weapon;
	}
	
	if (netgame && (deathmatch!=2) && !dropped)
	{	// leave placed weapons forever on cooperative net games
		if (!gaveweapon)
			return false;
		player->bonuscount += BONUSADD;
		if (deathmatch)
			P_GiveAmmo (player, weaponinfo[weapon].ammo, 5);
		else
			P_GiveAmmo (player, weaponinfo[weapon].ammo, 2);
		if (player == &players[consoleplayer])
			S_StartSound (NULL, sfx_wpnup);
		return false;
	}
	
	if (weaponinfo[weapon].ammo != am_noammo)
	{	// give one clip with a dropped weapon, two clips with a found weapon
		if (dropped)
			gaveammo = P_GiveAmmo (player, weaponinfo[weapon].ammo, 1);
		else
			gaveammo = P_GiveAmmo (player, weaponinfo[weapon].ammo, 2);
	}
	else
		gaveammo = false;
	
	return (gaveweapon || gaveammo);
}



/* 
=================== 
= 
= P_GiveBody
=
= Returns false if the body isn't needed at all
=================== 
*/ 

boolean P_GiveBody (player_t *player, int num)
{
	if (num < 0)
	{
		num = (player->f_1e3 + MAXHEALTH) * -num / 100;
		if (num <= player->health)
			return false;
		player->health = num;
		return true;
	}

	if (player->health >= MAXHEALTH + player->f_1e3)
		return false;
		
	player->health += num;
	if (player->health > MAXHEALTH + player->f_1e3)
		player->health = MAXHEALTH + player->f_1e3;
	player->mo->health = player->health;
	
	return true;
}


/* 
=================== 
= 
= P_GiveArmor
=
= Returns false if the armor is worse than the current armor
=================== 
*/ 

boolean P_GiveArmor (player_t *player, int armortype)
{
	int		hits;

	if (armortype < 0)
	{
		if (player->armorpoints)
			return false;
		armortype = -armortype;
	}
	
	hits = armortype*100;
	if (player->armorpoints >= hits)
		return false;		// don't pick up
		
	player->armortype = armortype;
	player->armorpoints = hits;
	
	return true;
}


/* 
=================== 
= 
= P_GiveCard
=
=================== 
*/ 

boolean P_GiveCard (player_t *player, card_t card)
{
	if (player->cards[card])
		return false;		
	player->bonuscount = BONUSADD*2;
	player->cards[card] = 1;
	return true;
}


/* 
=================== 
= 
= P_GivePower
=
=================== 
*/ 

boolean P_GivePower (player_t *player, powertype_t power)
{
	if (player->powers[power])
	{
		if (power == pw_1)
		{
			if (player->mo->flags & MF_27)
				return false;
			player->mo->flags &= ~MF_SHADOW;
			player->mo->flags |= MF_27;
			player->powers[pw_1] = 55*35;
			return true;
		}
		return false;
	}

	switch (power)
	{
		case pw_5:
			player->powers[pw_5] = 160*35;
			P_SetPsprite(player, ps_2, S_TRGT_10);
			P_SetPsprite(player, ps_3, S_TRGT_11);
			P_SetPsprite(player, ps_4, S_TRGT_12);
			player->psprites[ps_2].sx = 160<<FRACBITS;
			player->psprites[ps_2].sy = player->psprites[ps_3].sy
				= player->psprites[ps_4].sy = 100 << FRACBITS;
			break;
		case pw_1:
			player->powers[pw_1] = 55*35;
			player->mo->flags |= MF_SHADOW;
			break;
		case pw_2:
			player->powers[pw_2] = 80*35;
			break;
		case pw_0:
			P_GiveBody(player, 100);
			player->powers[pw_0] = 1;
			break;
		case pw_3:
			if (gamemap < 40)
				player->f_361[gamemap] = 1;
			player->powers[pw_3] = 1;
			break;
		default:
			player->powers[power] = 1;
			break;
	}
	return true;
}



/*
==================
=
= P_TouchSpecialThing
=
==================
*/

void P_TouchSpecialThing (mobj_t *special, mobj_t *toucher)
{
	player_t	*player;
	int			i;
	fixed_t		delta;
	int			sound;
	static char buffer[80];
		
	delta = special->z - toucher->z;
	if (delta > toucher->height || delta < -8*FRACUNIT)
		return;			// out of reach
	
	if (toucher->health <= 0)
		return;						// can happen with a sliding player corpse
	sound = sfx_itemup;	
	player = toucher->player;
	switch (special->type)
	{
		case MT_67:
		case MT_70:
		case MT_71:
		case MT_72:
		case MT_73:
		case MT_74:
		case MT_75:
			P_DamageMobj(player->mo, NULL, NULL, 5);
			return;
	}
	buffer[0] = 0;
	switch (special->sprite)
	{
	case SPR_TOKN:
		func_2DF40(player, special->sprite, special->type);
		break;
	case SPR_COMM:
		if (!P_GivePower(player, pw_4))
			return;
		sound = sfx_yeah;
		break;
	case SPR_PMAP:
		if (!P_GivePower(player, pw_3))
			return;
		sound = sfx_yeah;
		break;
	case SPR_SHD1:
		if (!func_2DE1C(player, special->sprite, special->type))
			buffer[0] = '!';
		break;
	case SPR_BLIT:
		if (special->flags & MF_DROPPED)
		{
			if (!P_GiveAmmo(player, am_0, 0))
				return;
		}
		else
		{
			if (!P_GiveAmmo(player, am_0, 1))
				return;
		}
		break;
	case SPR_BBOX:
		if (!P_GiveAmmo(player, am_0, 5))
			return;
		break;
	case SPR_MSSL:
		if (!P_GiveAmmo(player, am_4, 1))
			return;
		break;
	case SPR_ROKT:
		if (!P_GiveAmmo(player, am_4, 5))
			return;
		break;
	case SPR_BRY1:
		if (!P_GiveAmmo(player, am_3, 1))
			return;
		break;
	case SPR_CPAC:
		if (!P_GiveAmmo(player, am_3, 5))
			return;
		break;
	case SPR_XQRL:
		if (!P_GiveAmmo(player, am_1, 5))
			return;
		break;
	case SPR_PQRL:
		if (!P_GiveAmmo(player, am_2, 5))
			return;
		break;
	case SPR_GRN1:
		if (!P_GiveAmmo(player, am_5, 1))
			return;
		break;
	case SPR_GRN2:
		if (!P_GiveAmmo(player, am_6, 1))
			return;
		break;
	case SPR_BKPK:
		if (!player->backpack)
		{
			for (i=0 ; i<NUMAMMO ; i++)
				player->maxammo[i] *= 2;
			player->backpack = true;
		}
		for (i=0 ; i<NUMAMMO ; i++)
			P_GiveAmmo (player, i, 1);
		break;
	case SPR_RIFL:
		if (!P_GiveWeapon(player, wp_2, special->flags&MF_DROPPED))
			return;
		sound = sfx_wpnup;
		break;
	case SPR_FLAM:
		if (!P_GiveWeapon(player, wp_5, false))
			return;
		P_GiveAmmo(player, am_3, 3);
		sound = sfx_wpnup;
		break;
	case SPR_MMSL:
		if (!P_GiveWeapon(player, wp_3, false))
			return;
		sound = sfx_wpnup;
		break;
	case SPR_TRPD:
		if (!P_GiveWeapon(player, wp_6, false))
			return;
		sound = sfx_wpnup;
		break;
	case SPR_CBOW:
		if (!P_GiveWeapon(player, wp_1, special->flags&MF_DROPPED))
			return;
		sound = sfx_wpnup;
		break;
	case SPR_GRND:
		if (!P_GiveWeapon(player, wp_4, special->flags&MF_DROPPED))
			return;
		sound = sfx_wpnup;
		break;
	case SPR_SIGL:
		if (!P_GiveWeapon(player, wp_7, special->flags&MF_DROPPED))
		{
			player->f_45 = special->frame;
#if (APPVER_STRIFEREV >= AV_SR_STRF12)
			return;
#endif
		}
		if (netgame)
			player->f_45 = 4;
		player->pendingweapon = wp_7;
		player->f_1d9 = 1;
#if (APPVER_STRIFEREV >= AV_SR_STRF12)
		if (deathmatch == 1)
			return;
#endif
		sound = sfx_wpnup;
		break;
	case SPR_COIN:
		func_2DE1C(player, SPR_COIN, 168);
		break;
	case SPR_CRED:
		for (i = 0; i < 10; i++)
			func_2DE1C(player, SPR_COIN, 168);
		break;
	case SPR_SACK:
		if (special->health < 0)
		{
			for (i = special->health; i != 0; i++)
				func_2DE1C(player, SPR_COIN, 168);
		}
		else
		{
			for (i = 0; i < 25; i++)
				func_2DE1C(player, SPR_COIN, 168);
		}
		break;
	case SPR_CHST:
		for (i = 0; i < 50; i++)
			func_2DE1C(player, SPR_COIN, 168);
		break;
//
// armor
//
	case SPR_ARM1:
		if (!P_GiveArmor (player, -2))
		{
			if (!func_2DE1C(player, special->sprite, special->type))
				buffer[0] = '!';
		}
		break;
	case SPR_ARM2:
		if (!P_GiveArmor (player, -1))
		{
			if (!func_2DE1C(player, special->sprite, special->type))
				buffer[0] = '!';
		}
		break;
		
	default:
		if (special->type >= MT_133 && special->type < MT_133+NUMCARDS)
		{
			P_GiveCard(player, special->type-MT_133);
		}
		else if (!func_2DE1C(player, special->sprite, special->type))
			buffer[0]='!';
		break;
	}

	if (!buffer[0])
	{
		if (special->info->name)
			sprintf(buffer, "You picked up the %s.", special->info->name);
		else
			sprintf(buffer, "You picked up the item.");
	}
	else if (buffer[0] == '!')
	{
		sprintf(buffer, "You cannot hold any more.");
		player->message = buffer;
		return;
	}

	if ((special->flags & MF_11) && (special->info->speed != 8 || !(player->f_4d & 0x20)))
		player->f_4d |= 1<<(special->info->speed-1);
	
	player->message = buffer;
	P_RemoveMobj (special);
	player->bonuscount = BONUSADD;
	if (player == &players[consoleplayer])
		S_StartSound (NULL, sound);
}

/*
==============
=
= KillMobj
=
==============
*/

void P_KillMobj (mobj_t *source, mobj_t *target)
{
	mobjtype_t		item;
	mobj_t			*mo;
	int i;
	script_t *t;
	line_t junk;
	int l;
	static char buffer[80];
	
	target->flags &= ~(MF_SHOOTABLE|MF_FLOAT|MF_24);
	target->flags |= MF_CORPSE|MF_DROPOFF;
	target->height = FRACUNIT;

	if (source && source->player)
	{
		if (target->flags & MF_COUNTKILL)
			source->player->killcount++;	// count for intermission

		if (target->player)
		{
			extern char player_names[MAXPLAYERS][16];
			source->player->frags[target->player-players]++;

			sprintf(buffer, "%s killed %s", player_names[source->player->f_30f],
				player_names[target->player->f_30f]);
			if (netgame)
				players[consoleplayer].message = buffer;
		}
	}
	else if (!netgame && (target->flags & MF_COUNTKILL) )
		players[0].killcount++;			// count all monster deaths, even
										// those caused by other monsters
	
	if (target->player)
	{
		if (!source)	// count environment kills against you
			target->player->frags[target->player-players]++;
		if (gamemap == 29 && !netgame)
		{
			F_StartFinale();
			return;
		}
		if (netgame)
		{
			while (target->player->f_59[0].f_8 > 0)
			{
				switch (target->player->f_59[0].f_4)
				{
					case MT_168:
						i = target->player->f_59[0].f_8;
						mo = P_SpawnMobj(target->x, target->y, target->z + 24 * FRACUNIT, MT_170);
						mo->health = -i;
						func_30950(target->player, 0, i);
						break;
					default:
						mo = P_SpawnMobj(target->x, target->y, target->z + 24 * FRACUNIT, target->player->f_59[0].f_4);
						func_30950(target->player, 0, 1);
						break;
				}
				mo->momx += ((P_Random()&7)-(P_Random()&7)) * FRACUNIT;
				mo->momy += ((P_Random()&7)+1) * FRACUNIT;
				mo->flags |= MF_DROPPED;
			}
		}
		target->player->playerstate = PST_DEAD;
		target->player->mo->momz += 5*FRACUNIT;
		P_DropWeapon (target->player);

		if (target->player == &players[consoleplayer] && automapactive)
			AM_Stop ();					// don't die in auto map,
										// switch view prior to dying
	}

	if (target->state != &states[S_BURN_372])
	{
		if (target->health == -6666)
			P_SetMobjState(target, S_DISR_373);
		else if (target->health <= -target->info->spawnhealth
			&& target->info->xdeathstate)
			P_SetMobjState (target, target->info->xdeathstate);
		else
			P_SetMobjState (target, target->info->deathstate);
	}

	t = func_2DD08(target->type, target->f_9a);
	item = t->f_4;

	if (!item)
	{
		switch (target->type)
		{
			case MT_288:
				junk.tag = 225;
				EV_DoDoor(&junk, close);
				junk.tag = 44;
				EV_DoFloor(&junk, lowerFloor);
				I_StartVoice("VOC13");
				l = W_CheckNumForName("LOG13");
				if (l>0)
					strncpy(_char_A1300, W_CacheLumpNum(l, PU_CACHE), 300);
				players[0].f_4d |= 1<<(mobjinfo[MT_288].speed-1);
				item = 289;
				break;
			case MT_43:
			case MT_53:
			case MT_61:
				item = 179;
				break;
			case MT_64:
				item = 182;
				break;
			case MT_65:
				item = 295;
				break;
			case MT_49:
				item = 180;
				break;
			case MT_66:
				item = 296;
				break;
			case MT_62:
			case MT_63:
				item = 183;
				break;
			case MT_95:
				item = 196;
				break;
			case MT_70:
				item = 197;
				break;
			case MT_71:
				item = 198;
				break;
			case MT_72:
				item = 199;
				break;
			case MT_73:
				item = 200;
				break;
			default:
				return;
		}
	}

	switch (item)
	{
		case 303:
			junk.tag = 222;
			EV_DoDoor(&junk, close);
			sprintf(buffer, "You're dead!  You set off the alarm.");
			P_NoiseAlert(players[0].mo, players[0].mo);
			if (!deathmatch)
				players[consoleplayer].message = buffer;
			break;
		case 301:
			sprintf(buffer, "You Fool!  You've set off the alarm");
			P_NoiseAlert(players[0].mo, players[0].mo);
			if (!deathmatch)
				players[consoleplayer].message = buffer;
			break;
		case 304:
			junk.tag = 223;
			EV_DoDoor(&junk, open);
			break;
		case 305:
			junk.tag = 224;
			EV_DoDoor(&junk, open);
			break;
		case 196:
		case 197:
		case 198:
		case 199:
		case 200:
			for (i = 0; i < 8; i++)
			{
				if (!P_GiveWeapon(&players[i], wp_7, false))
				{
					players[i].f_45++;
					if (players[i].f_45 > 4)
						players[i].f_45 = 4;
				}
				players[i].pendingweapon = wp_7;
				players[i].f_1d9 = 1;
			}
			break;
		default:
			if (!deathmatch || !(mobjinfo[item].flags & MF_NOTDMATCH))
			{
				mo = P_SpawnMobj(target->x, target->y, target->z+24*FRACUNIT, item);
				mo->momx += ((P_Random()&7)-(P_Random()&7))*FRACUNIT;
				mo->momy += ((P_Random()&7)-(P_Random()&7))*FRACUNIT;
				mo->flags |= MF_DROPPED | MF_SPECIAL;
			}
			break;
	}
}

boolean func_2387C(mobjtype_t t)
{
	if (t == MT_95)
		return true;
	else if (t == MT_64)
		return true;
	else if (t == MT_49)
		return true;
	else if (t == MT_65)
		return true;
	else if (t == MT_66)
		return true;
	return false;
}



/*
=================
=
= P_DamageMobj
=
= Damages both enemies and players
= inflictor is the thing that caused the damage
= 		creature or missile, can be NULL (slime, etc)
= source is the thing to target after taking damage
=		creature or NULL
= Source and inflictor are the same for melee attacks
= source can be null for barrel explosions and other environmental stuff
==================
*/

void P_DamageMobj (mobj_t *target, mobj_t *inflictor, mobj_t *source, int damage)
{
	unsigned	ang;
	int			saved;
	player_t	*player;
	fixed_t		thrust;
	int			temp;
	int vb;
	int dist;
		
	if (target->health <= 0)
		return;
		
	if ( !(target->flags & MF_SHOOTABLE) )
		return;						// shouldn't happen...

	if (inflictor && (inflictor->flags & MF_31))
	{
		if (target->type == MT_1 && inflictor->health == -1)
			return;
		if ((target->flags & MF_31) && inflictor->health == -2)
			return;
#if (APPVER_STRIFEREV >= AV_SR_STRF12)
		if ((target->type == MT_50 || target->type == MT_65 || target->type == MT_71) && source->player->f_45 < 1)
#else
		if ((target->type == MT_50 || target->type == MT_65) && source->player->f_45 < 1)
#endif
			return;
	}

	if (inflictor)
	{
		if (inflictor->type == MT_112 || inflictor->type == MT_113 || inflictor->type == MT_109)
		{
			if (func_2387C(target->type) == 1)
				damage /= 2;
			else if (inflictor->type == MT_109)
			{
				damage /= 2;
				if (target->flags & MF_NOBLOOD)
					damage /= 2;
			}
		}
		else if (inflictor->type == MT_97)
		{
			ang = R_PointToAngle2(target->x, target->y
				, source->x, source->y);

			ang >>= ANGLETOFINESHIFT;
			thrust = 1020*(FRACUNIT>>3)*100/target->info->mass;
		
			target->momx += FixedMul (thrust, finecosine[ang]);
			target->momy += FixedMul (thrust, finesine[ang]);
			target->reactiontime += 10;

			dist = P_AproxDistance(target->x-source->x, target->y-source->y);

			dist /= thrust;
			if (dist < 1)
				dist = 1;

			target->momz = (source->z - target->z) / dist;
		}
		else if (inflictor->type == MT_103)
		{
			if (target->flags & MF_NOBLOOD)
				return;
			damage = target->health + 10;
		}
		else if ((target->flags & MF_31) && !(inflictor->flags & MF_31))
		{
			P_SetMobjState(target, target->info->missilestate);
			return;
		}
	}

	if ((target->type >= MT_2 && target->type <= MT_5) || target->type == MT_49)
	{
		if (source)
			target->target = source;
		P_SetMobjState(target, target->info->painstate);
		return;
	}

	if (target->type == MT_0)
	{
		if (inflictor && inflictor->type == MT_128)
			damage = target->health;
		else
			return;
	}
	
	player = target->player;
	if (player && gameskill == sk_baby)
		damage >>= 1;				// take half damage in trainer mode

//
// kick away unless using the chainsaw
//
	if (inflictor && !(target->flags & MF_NOCLIP) && (!source || !source->player
	|| source->player->readyweapon != wp_5))
	{
		ang = R_PointToAngle2 ( inflictor->x, inflictor->y
			,target->x, target->y);
		
		thrust = damage*(FRACUNIT>>3)*100/target->info->mass;

		// make fall forwards sometimes
		if ( damage < 40 && damage > target->health && target->z - inflictor->z > 64*FRACUNIT && (P_Random ()&1) )
		{
			ang += ANG180;
			thrust *= 4;
		}
		
		ang >>= ANGLETOFINESHIFT;
		target->momx += FixedMul (thrust, finecosine[ang]);
		target->momy += FixedMul (thrust, finesine[ang]);
	}
	
//
// player specific
//
	if (player)
	{
		// end of game hell hack
		if (target->subsector->sector->special == 11
			&& damage >= target->health)
		{
			damage = target->health - 1;
		}

		if ( damage < 1000 && (player->cheats&CF_GODMODE) )
		{
			return;
		}
		if (player->powers[pw_2] && inflictor
			&& (inflictor->type == MT_112 || inflictor->type == MT_113 || inflictor->type == MT_109))
		{
			damage = 0;
		}

		if (player->armortype)
		{
			if (player->armortype == 1)
				saved = damage/3;
			else
				saved = damage/2;
			if (player->armorpoints <= saved)
			{	// armor is used up
				saved = player->armorpoints;
				player->armortype = 0;
				func_30F48(player, 136);
				func_30F48(player, 137);
			}
			player->armorpoints -= saved;
			damage -= saved;
		}
		player->health -= damage;		// mirror mobj health here for Dave
		player->attacker = source;
		if (target != source)
			target->target = source;
		player->damagecount += damage;	// add damage after armor / invuln
		if (player->damagecount > 100)
			player->damagecount = 100;	// teleport stomp does 10k points...
	
		temp = damage < 100 ? damage : 100;

		// if (player == &players[consoleplayer])
		// 	I_Tactile (40,10,40+temp*2);
	}

//
// do the damage
//	
	target->health -= damage;	

	if (player && player->health < 50 && (deathmatch || (player->cheats & CF_4)))
	{
		while (player->health < 50 && func_30F48(player, 181))
		{
		}
		while (player->health < 50 && func_30F48(player, 180))
		{
		}
	}

	if (target->health <= 0)
	{
		if (inflictor && inflictor->type == MT_106)
			target->health = -target->info->spawnhealth;
		else if (!(target->flags & MF_NOBLOOD) && inflictor &&
			(inflictor->type == MT_114 || inflictor->type == MT_105
				|| inflictor->type == MT_110 || inflictor->type == MT_111))
		{
			S_StartSound(target, sfx_dsrptr);
			target->health = -6666;
		}
		if (!(target->flags & MF_NOBLOOD) && inflictor &&
			(inflictor->type == MT_112 || inflictor->type == MT_113
				|| inflictor->type == MT_109))
		{
			target->flags &= ~(MF_SHOOTABLE|MF_FLOAT|MF_SHADOW|MF_27);
			if (target->player)
			{
				target->player->cheats |= CF_3;
				target->player->powers[pw_1] = 0;
				target->player->readyweapon = wp_0;
				P_SetPsprite(target->player, ps_weapon, S_WAVE_2);
				P_SetPsprite(target->player, ps_flash, S_NULL);
			}
			P_SetMobjState(target, S_BURN_349);
			S_StartSound(target, sfx_burnme);
			return;
		}
		P_KillMobj(source, target);
		return;
	}

	if (target->health <= 6 && target->info->f_30)
	{
		P_SetMobjState(target, target->info->f_30);
		return;
	}

	if ( damage && (P_Random () < target->info->painchance) )
	{
		target->flags |= MF_JUSTHIT;		// fight back!
		P_SetMobjState (target, target->info->painstate);
	}
			
	target->reactiontime = 0;		// we're awake now...	

	if (target->type == MT_95)
		return;

	if ( (!target->threshold || target->type == MT_74)
		&& source && source != target && source->type != MT_74
		&& (source->flags & MF_26) != (target->flags & MF_26) )
	{	// if not intent on another player, chase after this one
		target->target = source;
		target->threshold = BASETHRESHOLD;
		if (target->state == &states[target->info->spawnstate]
		&& target->info->seestate != S_NULL)
			P_SetMobjState (target, target->info->seestate);
	}

}
