//
// Copyright (C) 1993-1996 Id Software, Inc.
// Copyright (C) 2022 Nuke.YKT
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
//

// F_finale.c

#include "DoomDef.h"
#include "P_local.h"
#include "soundst.h"
#include <ctype.h>

extern int _wp1, _wp2, _wp3, _wp4, _wp5;
//extern int _wp1, _wp2, _wp3, _wp4, _wp5, _wp6, _wp7, _wp8, _wp9, _wp10, _wp11, _wp12;
//extern int _wp13, _wp14, _wp15, _wp16, _wp17, _wp18;

int             finalestage;            // 0 = text, 1 = art screen, 2 = character cast
int             finalecount;

#define TEXTSPEED       3
#define TEXTWAIT        250

int		finaletic;
int		finalenext;
char    *finaletext;
char    *finaleflat;

void	F_StartCast (void);
void	F_CastTicker (void);
boolean F_CastResponder (event_t *ev);
void	F_CastDrawer (void);

char *_unk_86290[3] = {"2", "3", "4"};

/*
=======================
=
= F_StartFinale
=
=======================
*/
void F_StartFinale (void)
{
	char *textsegmenthack;
    gameaction = ga_nothing;
    gamestate = GS_FINALE;
    viewactive = false;
    automapactive = false;
	wipegamestate = -1;

	V_DrawPatch(0, 0, 0, W_CacheLumpName("PANEL0", PU_CACHE));

	finaleflat = "PANEL0";

	textsegmenthack = "PANEL7";

	switch(gamemap)
	{
		case 34:
			finalenext = -1;
#if (APPVER_STRIFEREV >= AV_SR_STRF13)
			if (shareware)
				finalenext = 25;
#endif
			break;
		case 3:
			finalenext = 1;
			break;
		case 9:
			finalenext = -99;
			break;
		case 10:
			finalenext = 5;
			break;
		case 29:
			if (!netgame)
			{
				if (players[0].health <= 0)
					finalenext = 14;
				else if ((players[0].f_4d & 0x1000000) != 0 && (players[0].f_4d & 0x4000000) != 0)
					finalenext = 10;
				else
					finalenext = 17;
			}
			break;
	}


	S_ChangeMusic(mus_dark_21, true);
    
	finaletic = 7;
    finalestage = 0;
    finalecount = 0;
	
}


boolean F_Responder (event_t *event)
{
	if (finalestage == 2)
		return F_CastResponder(event);

	return false;
}

void func_17BA8(void)
{
	finalecount++;
	if (finalecount >= 250)
	{
		gamestate = GS_FINALE;
		finalestage = 0;
		finalecount = 0;
	}
}

void func_17BD8(void)
{
	switch (finalenext)
		{
		case -99:
			gamestate = GS_LEVEL;
			func_2EDE4();
			break;
		case -9:
			S_StartSound(NULL, sfx_rifle);
			finaletic = 35*90;
			break;
		case -1:
			finalecount = 0;
			finalestage = GS_INTERMISSION;
			wipegamestate = -1;
			S_StartMusic(mus_fast_5);
			break;
		case 0:
			finaletic = 35*20;
			finalenext = -1;
		case 1:
			finaleflat = "SS2F1";
			I_StartVoice("MAC10");
			finalenext = 2;
			finaletic = 35*9;
			break;
		case 2:
			finaleflat = "SS2F2";
			I_StartVoice("MAC11");
			finalenext = 3;
			finaletic = 35*10;
			break;
		case 3:
			finaleflat = "SS2F3";
			I_StartVoice("MAC12");
			finalenext = 4;
			finaletic = 35*12;
			break;
		case 4:
			finaleflat = "SS2F4";
			I_StartVoice("MAC13");
			finalenext = -99;
			finaletic = 35*17;
			break;
		case 5:
			finaleflat = "SS3F1";
			I_StartVoice("MAC16");
			finalenext = 6;
			finaletic = 35*10;
			break;
		case 6:
			finaleflat = "SS3F2";
			I_StartVoice("MAC17");
			finalenext = 7;
			finaletic = 35*12;
			break;
		case 7:
			finaleflat = "SS3F3";
			I_StartVoice("MAC18");
			finalenext = 8;
			finaletic = 35*12;
			break;
		case 8:
			finaleflat = "SS3F4";
			I_StartVoice("MAC19");
			finalenext = -99;
			finaletic = 35*11;
			break;
		case 10:
			finaleflat = "SS4F1";
			S_StartMusic(mus_happy_33);
			I_StartVoice("RIE01");
			finalenext = 11;
			finaletic = 35*13;
			break;
		case 11:
			finaleflat = "SS4F2";
			I_StartVoice("BBX01");
			finalenext = 12;
			finaletic = 35*11;
			break;
		case 12:
			finaleflat = "SS4F3";
			I_StartVoice("BBX02");
			finalenext = 13;
			finaletic = 35*14;
			break;
		case 13:
			finaleflat = "SS4F4";
			finalenext = -1;
			finaletic = 35*28;
			break;
		case 14:
			S_StartMusic(mus_sad_26);
			finaleflat = "SS5F1";
			I_StartVoice("SS501b");
			finalenext = 15;
			finaletic = 35*11;
			break;
		case 15:
			finaleflat = "SS5F2";
			I_StartVoice("SS502b");
			finalenext = 16;
			finaletic = 35*10;
			break;
		case 16:
			finaleflat = "SS5F3";
			I_StartVoice("SS503b");
			finalenext = -1;
			finaletic = 35*11;
			break;
		case 17:
			S_StartMusic(mus_end_34);
			finaleflat = "SS6F1";
			I_StartVoice("SS601A");
			finalenext = 18;
			finaletic = 35*8;
			break;
		case 18:
			S_StartMusic(mus_end_34);
			finaleflat = "SS6F2";
			I_StartVoice("SS602A");
			finalenext = 19;
			finaletic = 35*8;
			break;
		case 19:
			S_StartMusic(mus_end_34);
			finaleflat = "SS6F3";
			I_StartVoice("SS603A");
			finalenext = -1;
			finaletic = 35*9;
			break;
#if (APPVER_STRIFEREV >= AV_SR_STRF13)
		case 25:
			finaleflat = "PANEL7";
			finaletic = 35*5;
			finalenext = 26;
			break;
		case 26:
			finaleflat = "VELLOGO";
			finaletic = 35*5;
			finalenext = -1;
			break;
#endif
	}
	finalecount = 0;
#if (APPVER_STRIFEREV < AV_SR_STRF13)
	V_DrawPatch(0, 0, 0, W_CacheLumpName("PANEL0", PU_CACHE));
#endif
}


/*
=======================
=
= F_Ticker
=
=======================
*/

void F_Ticker (void)
{
	int		i;

	// check for skipping
	if (finalecount > 50)
	{
	  // go on to the next level
	  for (i=0 ; i<MAXPLAYERS ; i++)
		if (players[i].cmd.buttons)
		  break;

	  if (i < MAXPLAYERS)
	  {
		finalecount = finaletic;
	  }
	}
	finalecount++;
	
	if (finalestage == 2)
	{
		F_CastTicker ();
		return;
	}

	if (finalecount > finaletic)
		func_17BD8();
}

/*
=======================
=
= Final DOOM 2 animation
= Casting by id Software.
=   in order of appearance
=
=======================
*/

typedef struct
{
	int		f_0;
	mobjtype_t	type;
} castinfo_t;

castinfo_t	castorder[] = {
	{1, MT_1},
	{1, MT_38},
	{1, MT_6},
	{1, MT_43},
	{1, MT_53},
	{1, MT_63},
	{1, MT_50},
	{0, MT_91},
	{0, MT_92},
	{0, MT_95},
	{0, MT_52},
	{0, MT_62},
	{0, MT_93},
	{0, MT_66},
	{0, MT_67},
	{0, MT_64},
	{0, MT_74},
	{1, NUMMOBJTYPES}
};

int			castnum;
int			casttics;
state_t		*caststate;
boolean		castdeath;
int			castframes;
int			castonmelee;
boolean		castattacking;


/*
=======================
=
= F_StartCast
=
=======================
*/
extern	gamestate_t     wipegamestate;


void F_StartCast (void)
{
	usergame = false;
	gameaction = ga_nothing;
	gamestate = GS_FINALE;
	viewactive = false;
	automapactive = false;

	wipegamestate = -1;		// force a screen wipe
	castnum = 0;
	caststate = &states[mobjinfo[castorder[castnum].type].seestate];
	casttics = caststate->tics;
	if (casttics > 50)
		casttics = 50;
	castdeath = false;
	finalestage = 2;	
	castframes = 0;
	castonmelee = 0;
	castattacking = false;
}


/*
=======================
=
= F_CastTicker
=
=======================
*/
void F_CastTicker (void)
{
	int		st;
	int		sfx;
	
	if (--casttics > 0)
		return;			// not time to change state yet
		
	if (caststate->tics == -1 || caststate->nextstate == S_NULL)
	{
		// switch from deathstate to next monster
		castnum++;
		castdeath = false;
		if (shareware && castorder[castnum].f_0 == 0)
			castnum = 0;
		if (castorder[castnum].type == NUMMOBJTYPES)
			castnum = 0;
		if (mobjinfo[castorder[castnum].type].seesound)
			S_StartSound (NULL, mobjinfo[castorder[castnum].type].seesound);
		caststate = &states[mobjinfo[castorder[castnum].type].seestate];
		castframes = 0;
	}
	else
	{
		// just advance to next state in animation
		if (caststate == &states[S_PLAY_292])
			goto stopattack;	// Oh, gross hack!
		st = caststate->nextstate;
		caststate = &states[st];
		castframes++;

		// sound hacks....
		if (st == mobjinfo[castorder[castnum].type].meleestate
			|| st == mobjinfo[castorder[castnum].type].missilestate)
			sfx = mobjinfo[castorder[castnum].type].attacksound;
		else if (st == S_PLAY_292)
			sfx = sfx_rifle;
		else
			sfx = 0;
		
		if (sfx)
			S_StartSound (NULL, sfx);
	}
	
	if (!castdeath && castframes == 12)
	{
		// go into attack frame
		castattacking = true;
		if (castonmelee)
			caststate=&states[mobjinfo[castorder[castnum].type].meleestate];
		else
			caststate=&states[mobjinfo[castorder[castnum].type].missilestate];
		castonmelee ^= 1;
		if (caststate == &states[S_NULL])
		{
		if (castonmelee)
			caststate=
				&states[mobjinfo[castorder[castnum].type].meleestate];
		else
			caststate=
				&states[mobjinfo[castorder[castnum].type].missilestate];
		}
	}
	
	if (castattacking)
	{
		if (castframes == 24
			||	caststate == &states[mobjinfo[castorder[castnum].type].seestate] )
		{
		  stopattack:
			castattacking = false;
			castframes = 0;
		caststate = &states[mobjinfo[castorder[castnum].type].seestate];
		}
	}
	
	casttics = caststate->tics;
	if (casttics > 50)
		casttics = 50;
	else if (casttics == -1)
		casttics = 15;
}


/*
=======================
=
= F_CastResponder
=
=======================
*/

boolean F_CastResponder (event_t* ev)
{
	if (ev->type != ev_keydown)
		return false;
	
	if (castdeath)
		return true;			// already in dying frames
	
	// go into death frame
	castdeath = true;
	caststate = &states[mobjinfo[castorder[castnum].type].deathstate];
	casttics = caststate->tics;
	if (casttics > 50)
		casttics = 50;
	castframes = 0;
	castattacking = false;
	if (mobjinfo[castorder[castnum].type].deathsound)
		S_StartSound (NULL, mobjinfo[castorder[castnum].type].deathsound);

	return true;
}


#include "hu_stuff.h"
extern	patch_t *hu_font[HU_FONTSIZE];

void F_CastPrint (char* text)
{
	char	*ch;
	int		c;
	int		cx;
	int		w;
	int		width;

	// find width
	ch = text;
	width = 0;

	while (ch)
	{
		c = *ch++;
		if (!c)
			break;
		c = toupper(c) - HU_FONTSTART;
		if (c < 0 || c> HU_FONTSIZE)
		{
			width += 4;
			continue;
		}
	
		w = SHORT (hu_font[c]->width);
		width += w;
	}

	// draw it
	cx = 160-width/2;
	ch = text;
	while (ch)
	{
		c = *ch++;
		if (!c)
			break;
		c = toupper(c) - HU_FONTSTART;
		if (c < 0 || c> HU_FONTSIZE)
		{
			cx += 4;
			continue;
		}
	
		w = SHORT (hu_font[c]->width);
		V_DrawPatch(cx, 180, 0, hu_font[c]);
		cx+=w;
	}
	
}


/*
==================
=
= F_DrawPatchCol
=
==================
*/

void F_DrawPatchCol (int x, patch_t *patch, int col)
{
}


/*
==================
=
= F_BunnyScroll
=
==================
*/

void F_BunnyScroll (void)
{
	int                     scrolled, x;
	patch_t         *p1, *p2;
	char            name[10];
	int                     stage;
	static int      laststage;
		
	p1 = W_CacheLumpName ("credit", PU_LEVEL);
	p2 = W_CacheLumpName ("vellogo", PU_LEVEL);

	V_MarkRect (0, 0, SCREENWIDTH, SCREENHEIGHT);

	scrolled = 320 - (finalecount-430)/2;
	if (scrolled > 320)
		scrolled = 320;
	if (scrolled < 0)
		scrolled = 0;
	
	for ( x=0 ; x<SCREENWIDTH ; x++)
	{
		if (x+scrolled < 320)
			F_DrawPatchCol (x, p1, x+scrolled);
		else
			F_DrawPatchCol (x, p2, x+scrolled - 320);		
	}
}


/*
=======================
=
= F_Drawer
=
=======================
*/

void F_Drawer (void)
{
	if (finalestage == 2)
	{
		//F_CastDrawer ();
		return;
	}

	if (!finalestage)
		V_DrawPatch(0,0,0,W_CacheLumpName(finaleflat, PU_CACHE));
	else
	{
		switch (gamemap)
		{
			case 29:
				V_DrawPatch(0,0,0,W_CacheLumpName("CREDIT",PU_CACHE));
				break;
			case 34:
				F_BunnyScroll();
				break;
		}
	}
			
}
