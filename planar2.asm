;
; Copyright (C) 1993-1996 Id Software, Inc.
; Copyright (C) 2022 Nuke.YKT
;
; This program is free software; you can redistribute it and/or
; modify it under the terms of the GNU General Public License
; as published by the Free Software Foundation; either version 2
; of the License, or (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program. If not, see <https://www.gnu.org/licenses/>.
;

	.386
	.MODEL  small
	INCLUDE defs.inc
.DATA

EXTRN	_xlatable:DWORD

.CODE

PROC   R_DrawFuzzColumnLoop_
PUBLIC	R_DrawFuzzColumnLoop_
	PUSHR
	mov esi,eax
	mov ebp,edx
	mov edi,ebx
	mov edx,[_xlatable]
	mov ebx,[_dc_source]
	mov ecx,[_dc_colormap]
	add edi,1
	jmp floop
ALIGN 4
floop:
	mov eax,ebp
	sar eax,16
	and eax,127
	add eax,ebx
	mov cl,[eax]
	mov dl,[ecx]
	mov dh,[esi]
	mov dl,[edx]
	mov [esi],dl
	add esi,PLANEWIDTH
	add ebp,[_dc_iscale]
	sub edi,1
	jnz floop
	POPR
	ret
ENDP

	mov eax, eax
PROC   R_DrawAltFuzzColumnLoop_
PUBLIC	R_DrawAltFuzzColumnLoop_
	PUSHR
	mov esi,eax
	mov ebp,edx
	mov edi,ebx
	mov edx,[_xlatable]
	mov ebx,[_dc_source]
	mov ecx,[_dc_colormap]
	add edi,1
	jmp faloop
ALIGN 4
faloop:
	mov eax,ebp
	sar eax,16
	and eax,127
	add eax,ebx
	mov cl,[eax]
	mov dh,[ecx]
	mov dl,[esi]
	mov dl,[edx]
	mov [esi],dl
	add esi,PLANEWIDTH
	add ebp,[_dc_iscale]
	sub edi,1
	jnz faloop
	mov eax, eax
	POPR
	ret
ENDP
	mov eax, eax

END