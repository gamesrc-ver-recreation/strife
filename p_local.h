//
// Copyright (C) 1993-1996 Id Software, Inc.
// Copyright (C) 2022 Nuke.YKT
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
//

// P_local.h

#ifndef __P_LOCAL__
#define __P_LOCAL__

extern int _pp1, _pp2, _pp3, _pp4, _pp5, _pp6, _pp7, _pp8, _pp9, _pp10, _pp11; // move to doomdef.h?
#ifndef __R_LOCAL__
#include "R_local.h"
#endif

#define FLOATSPEED		(FRACUNIT*5)


#define MAXHEALTH		100
#define VIEWHEIGHT		(41*FRACUNIT)

// mapblocks are used to check movement against lines and things
#define MAPBLOCKUNITS	128
#define MAPBLOCKSIZE	(MAPBLOCKUNITS*FRACUNIT)
#define MAPBLOCKSHIFT	(FRACBITS+7)
#define MAPBMASK		(MAPBLOCKSIZE-1)
#define MAPBTOFRAC		(MAPBLOCKSHIFT-FRACBITS)

// player radius for movement checking
#define PLAYERRADIUS	16*FRACUNIT

// MAXRADIUS is for precalculated sector block boxes
// the spider demon is larger, but we don't have any moving sectors
// nearby
#define MAXRADIUS		32*FRACUNIT

#define GRAVITY		FRACUNIT
#define MAXMOVE		(30*FRACUNIT)

#define USERANGE		(64*FRACUNIT)
#define MELEERANGE		(64*FRACUNIT)
#define MISSILERANGE	(32*64*FRACUNIT)

typedef enum
{
	DI_EAST,
	DI_NORTHEAST,
	DI_NORTH,
	DI_NORTHWEST,
	DI_WEST,
	DI_SOUTHWEST,
	DI_SOUTH,
	DI_SOUTHEAST,
	DI_NODIR,
	NUMDIRS
} dirtype_t;

#define	BASETHRESHOLD	 	100		// follow a player exlusively for 3 seconds

/*
===============================================================================

							P_TICK

===============================================================================
*/

extern	thinker_t	thinkercap;	// both the head and tail of the thinker list


void P_InitThinkers (void);
void P_AddThinker (thinker_t *thinker);
void P_RemoveThinker (thinker_t *thinker);

/*
===============================================================================

							P_PSPR

===============================================================================
*/

void P_SetupPsprites (player_t *curplayer);
void P_MovePsprites (player_t *curplayer);

void P_DropWeapon (player_t *player);

void P_BulletSlope (mobj_t *mo);

void P_SetPsprite (player_t *player, int position, statenum_t stnum);

/*
===============================================================================

							P_USER

===============================================================================
*/

void	P_PlayerThink (player_t *player);

void	P_Thrust (player_t *player, angle_t angle, fixed_t move) ;

char	*func_30950 (player_t *player, int a2, int a3);

boolean	func_30F48 (player_t *player, int a2);

/*
===============================================================================
							P_MOBJ
===============================================================================
*/

#define ONFLOORZ	MININT
#define	ONCEILINGZ	MAXINT

// Time interval for item respawning.
#define ITEMQUESIZE		128

extern mapthing_t	itemrespawnque[ITEMQUESIZE];
extern int		itemrespawntime[ITEMQUESIZE];
extern int		iquehead, iquetail;


void P_RespawnSpecials (void);

mobj_t *P_SpawnMobj (fixed_t x, fixed_t y, fixed_t z, mobjtype_t type);

void 	P_RemoveMobj (mobj_t *th);
boolean	P_SetMobjState (mobj_t *mobj, statenum_t state);
void 	P_MobjThinker (mobj_t *mobj);

void	P_SpawnPuff (fixed_t x, fixed_t y, fixed_t z);
void	func_2D4A4 (int x, int y, int z);
void 	P_SpawnBlood (fixed_t x, fixed_t y, fixed_t z, int damage);
void	P_CheckMissileSpawn (mobj_t *th);
mobj_t *P_SpawnMissile (mobj_t *source, mobj_t *dest, mobjtype_t type);
mobj_t *func_2D6F8 (mobj_t *source, mobj_t *dest, mobjtype_t type);
mobj_t	*P_SpawnPlayerMissile (mobj_t *source, mobjtype_t type);
mobj_t	*func_2D96C (mobj_t *source, mobjtype_t type);

void	P_ExplodeMissile (mobj_t *mo);

/*
===============================================================================

							P_ENEMY

===============================================================================
*/

void P_NoiseAlert (mobj_t *target, mobj_t *emmiter);
void A_FaceTarget (mobj_t *actor);
void func_1EAE0 (mobj_t *a1, mobj_t *a2);
void func_203DC (mobj_t *actor);
void func_20F44 (mobj_t *actor);
void func_20F98 (mobj_t *actor);
void func_21B50 (mobj_t *actor);

/*
===============================================================================

							P_MAPUTL

===============================================================================
*/

extern int _mp1, _mp2, _mp3, _mp4, _mp5, _mp6, _mp7, _mp8, _mp9, _mp10, _mp11;
extern int _mp12, _mp13, _mp14, _mp15;

typedef struct
{
	fixed_t x, y, dx, dy;
} divline_t;

typedef struct
{
	fixed_t		frac;		// along trace line
	boolean		isaline;
	union {
		mobj_t	*thing;
		line_t	*line;
	}			d;
} intercept_t;

#define	MAXINTERCEPTS	128
extern	intercept_t		intercepts[MAXINTERCEPTS], *intercept_p;
typedef boolean (*traverser_t) (intercept_t *in);


fixed_t P_AproxDistance (fixed_t dx, fixed_t dy);
int 	P_PointOnLineSide (fixed_t x, fixed_t y, line_t *line);
int 	P_PointOnDivlineSide (fixed_t x, fixed_t y, divline_t *line);
void 	P_MakeDivline (line_t *li, divline_t *dl);
fixed_t P_InterceptVector (divline_t *v2, divline_t *v1);
int 	P_BoxOnLineSide (fixed_t *tmbox, line_t *ld);

extern	fixed_t opentop, openbottom, openrange;
extern	fixed_t	lowfloor;
void 	P_LineOpening (line_t *linedef);

extern	line_t *ld_p;
boolean P_BlockLinesIterator (int x, int y, boolean(*func)(line_t*) );
boolean P_BlockThingsIterator (int x, int y, boolean(*func)(mobj_t*) );

#define PT_ADDLINES		1
#define	PT_ADDTHINGS	2
#define	PT_EARLYOUT		4

extern	divline_t 	trace;
boolean P_PathTraverse (fixed_t x1, fixed_t y1, fixed_t x2, fixed_t y2,
	int flags, boolean (*trav) (intercept_t *));

void 	P_UnsetThingPosition (mobj_t *thing);
void	P_SetThingPosition (mobj_t *thing);

/*
===============================================================================

							P_MAP

===============================================================================
*/

extern boolean		floatok;				// if true, move would be ok if
extern fixed_t		tmfloorz, tmceilingz;	// within tmfloorz - tmceilingz

extern line_t *ceilingline;


boolean P_CheckPosition (mobj_t *thing, fixed_t x, fixed_t y);
boolean P_TryMove (mobj_t *thing, fixed_t x, fixed_t y);
boolean P_TeleportMove (mobj_t *thing, fixed_t x, fixed_t y);
void	P_SlideMove (mobj_t *mo);
boolean P_CheckSight (mobj_t *t1, mobj_t *t2);
void 	P_UseLines (player_t *player);
boolean func_24AC0 (mobj_t *thing, fixed_t a2);

boolean P_ChangeSector (sector_t *sector, boolean crunch);

extern	mobj_t		*linetarget;			// who got hit (or NULL)
fixed_t P_AimLineAttack (mobj_t *t1, angle_t angle, fixed_t distance);

void P_LineAttack (mobj_t *t1, angle_t angle, fixed_t distance, fixed_t slope, int damage);

void P_RadiusAttack (mobj_t *spot, mobj_t *source, int damage);

/*
===============================================================================

							P_SETUP

===============================================================================
*/

extern byte		*rejectmatrix;			// for fast sight rejection
extern short		*blockmaplump;		// offsets in blockmap are from here
extern short		*blockmap;
extern int			bmapwidth, bmapheight;	// in mapblocks
extern fixed_t		bmaporgx, bmaporgy;		// origin of block map
extern mobj_t		**blocklinks;			// for thing chains

/*
===============================================================================

							P_INTER

===============================================================================
*/

extern int		maxammo[NUMAMMO];
extern int		clipammo[NUMAMMO];

void P_TouchSpecialThing (mobj_t *special, mobj_t *toucher);

void P_DamageMobj (mobj_t *target, mobj_t *inflictor, mobj_t *source, int damage);

void P_SetMessage(player_t *player, char *message, boolean ultmsg);
void P_TouchSpecialThing(mobj_t *special, mobj_t *toucher);
void P_DamageMobj(mobj_t *target, mobj_t *inflictor, mobj_t *source,
	int damage);
boolean P_GiveAmmo(player_t *player, ammotype_t ammo, int count);
boolean P_GiveBody(player_t *player, int num);
boolean P_GivePower(player_t *player, powertype_t power);
boolean P_GiveWeapon (player_t *player, weapontype_t weapon, boolean dropped);
boolean P_GiveCard (player_t *player, card_t card);
boolean P_GiveArmor (player_t *player, int armortype);
void P_KillMobj (mobj_t *source, mobj_t *target);

/*
===============================================================================

							P_SCRIPT

===============================================================================
*/



typedef struct {
	int f_0;
	int f_4[3];
	int f_10[3];
	char f_1c[32];
	char f_3c[80];
	int f_8c;
	int f_90;
	char f_94[80];
} unkstruct3;

typedef struct {
	int f_0;
	int f_4;
	int f_8[3];
	int f_14;
	char f_18[16];
	char f_28[8];
	char f_30[8];
	char f_38[320];
	unkstruct3 f_178[5];
} script_t;

typedef struct {
	int f_0;
	int f_4;
	int f_8;
	int f_c;
} unkstruct1;

typedef struct {
	char *f_0;
	int f_4;
	char *f_8[10];
} unkstruct2;

extern char _char_A1300[300];
extern script_t *_int_A142C;

void func_2DBC0 (void);

int func_2DC68 (player_t *player, int a2);

script_t *func_2DD08(mobjtype_t a1, int a2);

int func_2DE1C (player_t *player, spritenum_t a2, int a3);

int func_2DF40 (player_t *player, int a2, int a3);

void func_2EDE4 (void);

void func_2EDEC (player_t *player);

extern int _spp1, _spp2, _spp3, _spp4, _spp5, _spp6, _spp7, _spp8, _spp9, _spp10;
extern int _spp11, _spp12;

#include "P_spec.h"

#endif // __P_LOCAL__
