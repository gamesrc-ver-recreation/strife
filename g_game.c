//
// Copyright (C) 1993-1996 Id Software, Inc.
// Copyright (C) 2022 Nuke.YKT
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
//

// G_game.c

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "DoomDef.h"
#include "P_local.h"
#include "soundst.h"

extern int _wp1, _wp2, _wp3, _wp4, _wp5, _wp6, _wp7, _wp8, _wp9, _wp10, _wp11, _wp12;
extern int _wp13, _wp14, _wp15, _wp16, _wp17, _wp18, _wp19, _wp20;

#if (APPVER_STRIFEREV >= AV_SR_STRF12)
#define GAMESTATIC static
#else
#define GAMESTATIC
#endif

boolean G_CheckDemoStatus (void);
void G_ReadDemoTiccmd (ticcmd_t *cmd);
void G_WriteDemoTiccmd (ticcmd_t *cmd);
void G_PlayerReborn (int player);
void G_InitNew (skill_t skill, int map);

GAMESTATIC void G_DoReborn (int playernum);

GAMESTATIC void G_DoLoadLevel (boolean unk);
GAMESTATIC void G_DoNewGame (void);
GAMESTATIC void G_DoLoadGame (boolean a1);
GAMESTATIC void G_DoPlayDemo (void);
GAMESTATIC void G_DoCompleted (void);
GAMESTATIC void G_DoVictory (void);
GAMESTATIC void G_DoWorldDone (void);
GAMESTATIC void G_DoSaveGame (char *dir);

void D_PageTicker(void);
void D_AdvanceDemo(void);


int spawndir;
int spawnspot;

gameaction_t    gameaction;
gamestate_t     gamestate;
skill_t         gameskill = sk_medium;
boolean         respawnmonsters;
int             gamemap;

boolean         paused;
boolean         sendpause;              // send a pause event next tic
boolean         sendsave;               // send a save event next tic
boolean         usergame;               // ok to save / end game

boolean         timingdemo;             // if true, exit with report on completion
boolean         nodrawers;              // for comparative timing purposes 
boolean         noblit;                 // for comparative timing purposes 
int             starttime;              // for comparative timing purposes

boolean         viewactive;

boolean         deathmatch;             // only if started as net death
boolean         netgame;                // only true if packets are broadcast
boolean         playeringame[MAXPLAYERS];
player_t        players[MAXPLAYERS];

int             consoleplayer;          // player taking events and displaying
int             displayplayer;          // view being displayed
int             gametic;
int             levelstarttic;          // gametic at level start
int             totalkills, totalsecret;    // for intermission

char            demoname[32];
boolean         demorecording;
boolean         demoplayback;
boolean			netdemo;
byte            *demobuffer, *demo_p, *demoend;
boolean         singledemo;             // quit after playing a demo from cmdline

boolean         precache = true;        // if true, load all graphics at start

short            consistancy[MAXPLAYERS][BACKUPTICS];

byte            *savebuffer, *save_p;


//
// controls (have defaults)
//
int             key_right, key_left, key_up, key_down;
int             key_strafeleft, key_straferight;
int             key_fire, key_use, key_strafe, key_speed;

int             key_usehealth, key_jump, key_invquery;
int             key_mission, key_invpop, key_invkey;
int             key_invH, key_invE, key_invL, key_invR;
int             key_invU, key_invD, key_lookup, key_lookdown;

int             mousebfire;
int             mousebstrafe;
int             mousebforward;
int             mousebjump;

int             joybfire;
int             joybstrafe;
int             joybuse;
int             joybspeed;
int             joybjump;



#define MAXPLMOVE       (forwardmove[1])
 
#define TURBOTHRESHOLD	0x32

fixed_t         forwardmove[2] = {0x19, 0x32};
fixed_t         sidemove[2] = {0x18, 0x28};
fixed_t         angleturn[3] = {640, 1280, 320};     // + slow turn
#define SLOWTURNTICS    6

#define NUMKEYS 256
boolean         gamekeydown[NUMKEYS];
int             turnheld;                   // for accelerative turning


boolean         mousearray[4];
boolean         *mousebuttons = &mousearray[1];
	// allow [-1]
int             mousex, mousey;             // mouse values are used once
int             dclicktime, dclickstate, dclicks;
int             dclicktime2, dclickstate2, dclicks2;

int             joyxmove, joyymove;         // joystick values are repeated
boolean         joyarray[5];
boolean         *joybuttons = &joyarray[1];     // allow [-1]

int     savegameslot = 6;
char    savedescription[32];

char curdir[128];
char userdir[128];
char savedir[128];

 
void *statcopy;				// for statistics driver


/*
====================
=
= G_BuildTiccmd
=
= Builds a ticcmd from all of the available inputs or reads it from the
= demo buffer.
= If recording a demo, write it out
====================
*/

int mousefreeze = 0;

void G_BuildTiccmd (ticcmd_t *cmd)
{
	int             i;
	boolean         strafe, bstrafe;
	int             speed, tspeed;
	int             forward, side;

	ticcmd_t		*base;

	base = I_BaseTiccmd ();		// empty, or external driver
	memcpy (cmd,base,sizeof(*cmd)); 
	//cmd->consistancy =
	//      consistancy[consoleplayer][(maketic*ticdup)%BACKUPTICS];
	cmd->consistancy =
		consistancy[consoleplayer][maketic%BACKUPTICS];

	if (gamekeydown[key_lookup])
		cmd->f_8 |= BT2_0;
	if (gamekeydown[key_lookdown])
		cmd->f_8 |= BT2_1;
#if (APPVER_STRIFEREV >= AV_SR_STRF12)
	if (gamekeydown[key_invU] && players[consoleplayer].f_1dd > 0)
#else
	if (gamekeydown[key_invU])
#endif
	{
		cmd->f_8 |= BT2_3;
		cmd->f_9 = players[consoleplayer].f_59[players[consoleplayer].f_1df].f_0;
	}
#if (APPVER_STRIFEREV >= AV_SR_STRF12)
	if (gamekeydown[key_invD] && players[consoleplayer].f_1dd > 0)
#else
	if (!netgame && gamekeydown[key_invD])
#endif
	{
		cmd->f_8 |= BT2_4;
		cmd->f_9 = players[consoleplayer].f_59[players[consoleplayer].f_1df].f_0;
	}
	if (gamekeydown[key_usehealth])
		cmd->f_8 |= BT2_7;

//printf ("cons: %i\n",cmd->consistancy);

	strafe = gamekeydown[key_strafe] || mousebuttons[mousebstrafe]
		|| joybuttons[joybstrafe];
	speed = gamekeydown[key_speed] || joybuttons[joybspeed];

	if (speed)
		cmd->f_8 |= BT2_2;

	if (players[consoleplayer].health <= 15)
		speed = 0;

	forward = side = 0;

//
// use two stage accelerative turning on the keyboard and joystick
//
	if (joyxmove < 0 || joyxmove > 0
	|| gamekeydown[key_right] || gamekeydown[key_left])
		turnheld += ticdup;
	else
		turnheld = 0;
	if (turnheld < SLOWTURNTICS)
		tspeed = 2;             // slow turn
	else
		tspeed = speed;

//
// let movement keys cancel each other out
//
	if(strafe)
	{
		if (gamekeydown[key_right])
			side += sidemove[speed];
		if (gamekeydown[key_left])
			side -= sidemove[speed];
		if (joyxmove > 0)
			side += sidemove[speed];
		if (joyxmove < 0)
			side -= sidemove[speed];
	}
	else
	{
		if (gamekeydown[key_right])
			cmd->angleturn -= angleturn[tspeed];
		if (gamekeydown[key_left])
			cmd->angleturn += angleturn[tspeed];
		if (joyxmove > 0)
			cmd->angleturn -= angleturn[tspeed];
		if (joyxmove < 0)
			cmd->angleturn += angleturn[tspeed];
	}

	if (gamekeydown[key_up])
		forward += forwardmove[speed];
	if (gamekeydown[key_down])
		forward -= forwardmove[speed];
	if (joyymove < 0)
		forward += forwardmove[speed];
	if (joyymove > 0)
		forward -= forwardmove[speed];
	if (gamekeydown[key_straferight])
		side += sidemove[speed];
	if (gamekeydown[key_strafeleft])
		side -= sidemove[speed];

//
// buttons
//
	cmd->chatchar = HU_dequeueChatChar();

	if (gamekeydown[key_jump] || mousebuttons[mousebjump]
		|| joybuttons[joybjump])
		cmd->f_8 |= BT2_5;

	if (gamekeydown[key_fire] || joybuttons[joybfire])
		cmd->buttons |= BT_ATTACK;

	if (mousebuttons[mousebfire])
	{
		if (mousefreeze > 0)
			mousefreeze--;
		else
			cmd->buttons |= BT_ATTACK;
	}

	if (gamekeydown[key_use] || joybuttons[joybuse] )
	{
		cmd->buttons |= BT_USE;
		dclicks = 0;                    // clear double clicks if hit use button
	}

//
// chainsaw overrides 
//
	for(i = 0; i < NUMWEAPONS-1; i++)
	{
		if(gamekeydown['1'+i])
		{
			cmd->buttons |= BT_CHANGE;
			cmd->buttons |= i<<BT_WEAPONSHIFT;
			break;
		}
	}

//
// mouse
//
	if (mousebuttons[mousebforward])
	{
		forward += forwardmove[speed];
	}

//
// forward double click
//
	if (mousebuttons[mousebforward] != dclickstate && dclicktime > 1 )
	{
		dclickstate = mousebuttons[mousebforward];
		if (dclickstate)
			dclicks++;
		if (dclicks == 2)
		{
			cmd->buttons |= BT_USE;
			dclicks = 0;
		}
		else
			dclicktime = 0;
	}
	else
	{
		dclicktime += ticdup;
		if (dclicktime > 20)
		{
			dclicks = 0;
			dclickstate = 0;
		}
	}

//
// strafe double click
//
	bstrafe = mousebuttons[mousebstrafe]
|| joybuttons[joybstrafe];
	if (bstrafe != dclickstate2 && dclicktime2 > 1 )
	{
		dclickstate2 = bstrafe;
		if (dclickstate2)
			dclicks2++;
		if (dclicks2 == 2)
		{
			cmd->buttons |= BT_USE;
			dclicks2 = 0;
		}
		else
			dclicktime2 = 0;
	}
	else
	{
		dclicktime2 += ticdup;
		if (dclicktime2 > 20)
		{
			dclicks2 = 0;
			dclickstate2 = 0;
		}
	}

	forward += mousey;
	if (strafe)
		side += mousex*2;
	else
		cmd->angleturn -= mousex*0x8;

	mousex = mousey = 0;

	if (forward > MAXPLMOVE)
		forward = MAXPLMOVE;
	else if (forward < -MAXPLMOVE)
		forward = -MAXPLMOVE;
	if (side > MAXPLMOVE)
		side = MAXPLMOVE;
	else if (side < -MAXPLMOVE)
		side = -MAXPLMOVE;

	cmd->forwardmove += forward;
	cmd->sidemove += side;

//
// special buttons
//
	if (sendpause)
	{
		sendpause = false;
		cmd->buttons = BT_SPECIAL | BTS_PAUSE;
	}

	if (sendsave)
	{
		sendsave = false;
		cmd->buttons = BT_SPECIAL | BTS_SAVEGAME | (savegameslot<<BTS_SAVESHIFT);
	}

}


/*
==============
=
= G_DoLoadLevel
=
==============
*/

GAMESTATIC void G_DoLoadLevel (boolean unk)
{
	int             i;

	levelstarttic = gametic;        // for time calculation
	
	if (wipegamestate == GS_LEVEL) 
		wipegamestate = -1;             // force a wipe 

	gamestate = GS_LEVEL;
	for (i=0 ; i<MAXPLAYERS ; i++)
	{
		if (playeringame[i] && (players[i].playerstate == PST_DEAD || players[i].health <= 0))
			players[i].playerstate = PST_REBORN;
	}

	P_SetupLevel (gamemap);
	displayplayer = consoleplayer;      // view the guy you are playing
	starttime = I_GetTime ();
	gameaction = ga_nothing;
	Z_CheckHeap ();

//
// clear cmd building stuff
//

	memset (gamekeydown, 0, sizeof(gamekeydown));
	joyxmove = joyymove = 0;
	mousex = mousey = 0;
	sendpause = sendsave = paused = false;
	memset (mousebuttons, 0, sizeof(mousebuttons));
	memset (joybuttons, 0, sizeof(joybuttons));

	func_2DBC0 ();
}


/*
===============================================================================
=
= G_Responder
=
= get info needed to make ticcmd_ts for the players
=
===============================================================================
*/

boolean G_Responder (event_t *ev)
{
	// allow spy mode changes even during the demo
	if (gamestate == GS_LEVEL && ev->type == ev_keydown
		&& ev->data1 == KEY_F12 && (singledemo || gameskill == sk_baby) )
	{
		// spy mode 
		do
		{
			displayplayer++;
			if (displayplayer == MAXPLAYERS)
				displayplayer = 0;
		} while (!playeringame[displayplayer] && displayplayer != consoleplayer);
		return true;
	}
    
	// any other key pops up menu if in demos
	if (gameaction == ga_nothing && !singledemo && 
		(demoplayback || gamestate == GS_DEMOSCREEN) 
		) 
	{ 
		if (ev->type == ev_keydown ||  
			(ev->type == ev_mouse && ev->data1) || 
			(ev->type == ev_joystick && ev->data1) ) 
		{ 
			if (devparm && ev->data1 == 'g')
				D_PageTicker ();
			else
				M_StartControlPanel (); 
			return true; 
		} 
		return false; 
	} 

	if (gamestate == GS_LEVEL)
	{
#if 0 
		if (devparm && ev->type == ev_keydown && ev->data1 == ';') 
		{ 
			G_DeathMatchSpawnPlayer (0); 
			return true; 
		} 
#endif 
		if (HU_Responder (ev))
			return true;	// chat ate the event
		if (ST_Responder (ev))
			return true;	// status window ate it
		if (AM_Responder (ev))
			return true;	// automap ate it
	}

	if (gamestate == GS_FINALE) 
	{ 
		if (F_Responder (ev))
			return true;	// finale ate the event
	} 

	switch(ev->type)
	{
		case ev_keydown:
			if (ev->data1 == KEY_PAUSE)
			{
				sendpause = true;
				return true;
			}
			if (ev->data1 < NUMKEYS)
				gamekeydown[ev->data1] = true;
			return true; // eat key down events

		case ev_keyup:
			if (ev->data1 < NUMKEYS)
				gamekeydown[ev->data1] = false;
			return false; // always let key up events filter down

		case ev_mouse:
			mousebuttons[0] = ev->data1&1;
			mousebuttons[1] = ev->data1&2;
			mousebuttons[2] = ev->data1&4;
			mousex = ev->data2*(mouseSensitivity+5)/10;
			mousey = ev->data3*(mouseSensitivity+5)/10;
			return true; // eat events

		case ev_joystick:
			joybuttons[0] = ev->data1&1;
			joybuttons[1] = ev->data1&2;
			joybuttons[2] = ev->data1&4;
			joybuttons[3] = ev->data1&8;
			joyxmove = ev->data2;
			joyymove = ev->data3;
			return true; // eat events

		default:
			break;
	}
	return false;
}

/*
===============================================================================
=
= G_Ticker
=
===============================================================================
*/

void G_Ticker (void)
{
	int                     i, buf;
	ticcmd_t        *cmd;

//
// do player reborns if needed
//
	for (i=0 ; i<MAXPLAYERS ; i++)
		if (playeringame[i] && players[i].playerstate == PST_REBORN)
			G_DoReborn (i);

//
// do things to change the game state
//
	while (gameaction != ga_nothing)
	{
		switch (gameaction)
		{
		case ga_loadlevel:
			G_DoLoadLevel (true);
			break;
		case ga_newgame:
			G_DoNewGame ();
			break;
		case ga_loadgame:
			G_DoLoadGame (true);
			func_1B68C ();
			func_1B734 ();
			// G_DoLoadGame ();
			break;
		case ga_savegame:
			func_1B614 ();
			func_1B704 (userdir);
			G_DoSaveGame (userdir);
			// G_DoSaveGame ();
			break;
		case ga_playdemo:
			G_DoPlayDemo ();
			break;
		case ga_completed:
			G_DoCompleted ();
			break;
		case ga_victory:
			F_StartFinale();
			break;
		case ga_worlddone:
			G_DoWorldDone();
			break;
		case ga_screenshot:
			M_ScreenShot ();
			gameaction = ga_nothing;
			break;
		case ga_nothing:
			break;
		}
	}


//
// get commands, check consistancy, and build new consistancy check
//
	buf = (gametic/ticdup)%BACKUPTICS;

	for (i=0 ; i<MAXPLAYERS ; i++)
		if (playeringame[i])
		{
			cmd = &players[i].cmd;

			memcpy (cmd, &netcmds[i][buf], sizeof(ticcmd_t));

			if (demoplayback)
				G_ReadDemoTiccmd (cmd);
			if (demorecording)
				G_WriteDemoTiccmd (cmd);
	    
			//
			// check for turbo cheats
			//
			if (cmd->forwardmove > TURBOTHRESHOLD && !(gametic&31) && ((gametic>>5)&3) == i )
			{
				static char turbomessage[80];
				extern char player_names[][16];
				sprintf (turbomessage, "%s is turbo!",player_names[i]);
				players[consoleplayer].message = turbomessage;
			}

			if (netgame && !netdemo && !(gametic%ticdup) )
			{
				if (gametic > BACKUPTICS
				&& consistancy[i][buf] != cmd->consistancy)
				{
					I_Error ("Player %i consistency failure (%i should be %i)", i+1, cmd->consistancy, consistancy[i][buf]);
				}
				if (players[i].mo)
					consistancy[i][buf] = players[i].mo->x;
				else
					consistancy[i][buf] = rndindex;
			}
		}

//
// check for special buttons
//
	for (i=0 ; i<MAXPLAYERS ; i++)
		if (playeringame[i])
		{
			if (players[i].cmd.buttons & BT_SPECIAL)
			{
				switch (players[i].cmd.buttons & BT_SPECIALMASK)
				{
				case BTS_PAUSE:
					paused ^= 1;
					if (paused)
						S_PauseSound ();
					else
						S_ResumeSound ();
					break;

				case BTS_SAVEGAME:
					if (!savedescription[0])
						strcpy (savedescription, "NET GAME");
					savegameslot =
						(players[i].cmd.buttons & BTS_SAVEMASK)>>BTS_SAVESHIFT;
					gameaction = ga_savegame;
					break;
				}
			}
		}
//
// do main actions
//
	switch (gamestate)
	{
		case GS_LEVEL:
			P_Ticker ();
			ST_Ticker ();
			AM_Ticker ();
			HU_Ticker ();
			break;
		case GS_INTERMISSION:
			func_17BA8 ();
			break;
		case GS_FINALE:
			F_Ticker ();
			break;
		case GS_DEMOSCREEN:
			D_PageTicker ();
			break;
	}
}


/*
==============================================================================

						PLAYER STRUCTURE FUNCTIONS

also see P_SpawnPlayer in P_Things
==============================================================================
*/

/*
====================
=
= G_InitPlayer
=
= Called at the start
= Called by the game initialization functions
====================
*/
void G_InitPlayer (int player) 
{ 
	player_t	*p;

// set up the saved info         
	p = &players[player];
	 
// clear everything else to defaults
	G_PlayerReborn (player);
}

/*
====================
=
= G_PlayerReborn
=
= Called after a player dies
= almost everything is cleared and initialized
====================
*/

void G_PlayerReborn (int player)
{
	player_t *p;
	int i;
	int frags[MAXPLAYERS];
	int killcount, v10;

	memcpy(frags, players[player].frags, sizeof(frags));
	killcount = players[player].killcount;
	v10 = players[player].f_30f;

	p = &players[player];
	memset(p, 0, sizeof(*p));

	players[player].cheats |= CF_4;
	memcpy(players[player].frags, frags, sizeof(players[player].frags));
	players[player].killcount = killcount;
	players[player].f_30f = v10;

	p->f_25d = p->usedown = p->attackdown = true; // don't do anything immediately
	p->playerstate = PST_LIVE;
	p->health = MAXHEALTH;
	p->weaponowned[wp_0] = true;
	p->readyweapon = p->pendingweapon = wp_0;
	for(i = 0; i < NUMAMMO; i++)
	{
		p->maxammo[i] = maxammo[i];
	}
	p->f_55 = 1;
	for(i = 0; i < 32; i++)
	{
		p->f_59[i].f_4 = NUMMOBJTYPES;
	}
	strncpy(_char_A1300, "Find help", 300);
}

/*
====================
=
= G_CheckSpot
=
= Returns false if the player cannot be respawned at the given mapthing_t spot
= because something is occupying it
====================
*/

void P_SpawnPlayer (mapthing_t *mthing);

boolean G_CheckSpot (int playernum, mapthing_t *mthing)
{
	fixed_t         x,y;
	subsector_t *ss;
	unsigned        an;
	mobj_t      *mo;
	int         i;

	x = mthing->x << FRACBITS;
	y = mthing->y << FRACBITS;
	
	if (!players[playernum].mo)
	{
		if (leveltime > 0)
			players[playernum].message = "you didn't have a body!";
		// first spawn of level, before corpses
		for (i=0 ; i<playernum ; i++)
			if (players[i].mo->x == x && players[i].mo->y == y)
				return false;	
		return true;
	}

	if (!P_CheckPosition (players[playernum].mo, x, y) )
		return false;

// spawn a teleport fog
	ss = R_PointInSubsector (x,y);
	an = ( ANG45 * (mthing->angle/45) ) >> ANGLETOFINESHIFT;

	mo = P_SpawnMobj (x+20*finecosine[an], y+20*finesine[an]
	, ss->sector->floorheight, MT_118);

	if (players[consoleplayer].viewz != 1)
		S_StartSound (mo, sfx_telept);  // don't start sound on first frame

	return true;
}

/*
====================
=
= G_DeathMatchSpawnPlayer
=
= Spawns a player at one of the random death match spots
= called at level load and each death
====================
*/

void G_DeathMatchSpawnPlayer (int playernum)
{
	int             i,j;
	int             selections;

	selections = deathmatch_p - deathmatchstarts;
	if (selections < 4)
		I_Error ("Only %i deathmatch spots, at least 4 required!", selections);

	for (j=0 ; j<20 ; j++)
	{
		i = P_Random() % selections;
		if (G_CheckSpot (playernum, &deathmatchstarts[i]) )
		{
			deathmatchstarts[i].type = playernum+1;
			P_SpawnPlayer (&deathmatchstarts[i]);
			return;
		}
	}

// no good spot, so the player will probably get stuck
	P_SpawnPlayer (&playerstarts[playernum]);
}

/*
====================
=
= G_DoReborn
=
====================
*/

void func_16784 (int a1)
{
	sprintf(curdir, "%s%d", savedir, a1);
}

GAMESTATIC void G_DoReborn (int playernum)
{
	int                             i;

	if (!netgame)
	{
		func_16784 (gamemap);
		gameaction = ga_loadgame;                      // reload the level from scratch
	}
	else
	{       // respawn at the start
		if (players[playernum].mo)
			players[playernum].mo->player = NULL;   // dissasociate the corpse

		// spawn at random spot if in death match
		if (deathmatch)
		{
			G_DeathMatchSpawnPlayer (playernum);
			return;
		}

		if (G_CheckSpot (playernum, &playerstarts[playernum]) )
		{
			P_SpawnPlayer (&playerstarts[playernum]);
			return;
		}
		// try to spawn at one of the other players spots
		for (i=0 ; i<MAXPLAYERS ; i++)
			if (G_CheckSpot (playernum, &playerstarts[i]) )
			{
				playerstarts[i].type = playernum+1;             // fake as other player
				P_SpawnPlayer (&playerstarts[i]);
				playerstarts[i].type = i+1;                             // restore
				return;
			}
		// he's going to be inside something.  Too bad.
		P_SpawnPlayer (&playerstarts[playernum]);
	}
}


void G_ScreenShot (void)
{
	gameaction = ga_screenshot;
}


/*
====================
=
= G_DoCompleted
=
====================
*/

int		_int_9F074;
//extern char     *pagename;

void G_ExitLevel (int a1, int a2, int a3)
{
	gameaction = ga_completed;
	if (players[0].weaponowned[wp_7])
	{
		switch (a1)
		{
			case 3:
				_int_9F074 = 30;
				break;
			case 7:
				_int_9F074 = 10;
				break;
			default:
				_int_9F074 = a1;
				break;
		}
	}
	else
		_int_9F074 = a1;
	if (deathmatch)
		spawnspot = 0;
	else
		spawnspot = a2;
	spawndir = a3;
}

void G_ExitLevel2 (int a1, int a2)
{
	gameaction = ga_completed;
	_int_9F074 = gamemap;
	spawnspot = a1;
	spawndir = a2;
}

void G_ExitLevel3 (int a1)
{
    _int_9F074 = !a1 ? gamemap + 1 : a1;
    spawnspot = 0;
    gameaction = ga_completed;
}

void G_ExitLevel4 (void)
{
	gameaction = ga_victory;
}

GAMESTATIC void G_DoCompleted(void)
{
	int i;

	for(i = 0; i < MAXPLAYERS; i++)
	{
		if(playeringame[i])
		{
			if (_int_9F074 < 40)
				players[i].powers[pw_3] = players[i].f_361[_int_9F074];
			if (netgame)
				players[i].powers[pw_1] = 0;
		}
	}

	killfl = false;

	if (automapactive)
		AM_Stop();

	if (!deathmatch)
		G_DoSaveGame (savedir);

	gameaction = ga_worlddone;
}

//============================================================================
//
// G_WorldDone
//
//============================================================================

void G_WorldDone (void)
{
}

void func_169DC(void)
{
	mapthing_t *va;
	if (!spawnspot)
		return;
	va = &_char_A0170_ddd[spawnspot-1];
	P_TeleportMove(players[0].mo, va->x<<16, va->y<<16);
	players[0].mo->angle = spawndir;
	players[0].mo->health = players[0].health;
}

void func_16A2C(int a1)
{
	mapthing_t *va;
	va = &_char_A0170_ddd[a1-1];
	P_TeleportMove(players[0].mo, va->x<<16, va->y<<16);
}

GAMESTATIC void G_DoWorldDone(void)
{
	int vd, vb;
	int vc;
	vc = leveltime;
	gamestate = GS_LEVEL;
	gamemap = _int_9F074;
	func_16784(gamemap);
	if (!deathmatch)
	{
		vd = (players[0].mo->flags & MF_SHADOW) > 0;
		vb = (players[0].mo->flags & MF_27) > 0;
	}
	G_DoLoadGame(false);
	leveltime = vc;
	if (!deathmatch)
	{
		players[0].mo->flags &= ~(MF_27|MF_SHADOW);
		if (vd)
			players[0].mo->flags |= MF_SHADOW;
		if (vb)
			players[0].mo->flags |= MF_27;
		func_169DC();
		G_DoSaveGame(savedir);
		func_1B704(savedir);
	}
	gameaction = ga_nothing;
	viewactive = true;
}

void func_16B24 (void)
{
	gamestate = GS_LEVEL;
	gameaction = ga_nothing;
	viewactive = true;
}

/*
====================
=
= G_InitFromSavegame
=
= Can be called by the startup code or the menu task. 
=
====================
*/

//extern boolean setsizeneeded;
//void R_ExecuteSetViewSize (void);

void G_LoadGame (char* name) 
{
	int length;
	char buffer[100];

	sprintf(buffer, "%s\\current", name);

	length = M_ReadFile(buffer, &savebuffer);
	if (length > 0)
	{
		save_p = savebuffer;
		gamemap = *save_p;
		gameaction = ga_loadgame;
		Z_Free(savebuffer);
	}
	else
		gameaction = ga_newgame;
	func_16784(gamemap);
}

#define VERSIONSIZE		8

GAMESTATIC void G_DoLoadGame(boolean a1)
{
	int length;
	int i;
	int a, b, c;
	char vcheck[VERSIONSIZE];

	gameaction = ga_nothing;

	length = M_ReadFile(curdir, &savebuffer);

	if (length == 0)
	{
		G_DoLoadLevel(true);
		return;
	}

	save_p = savebuffer;
	// Skip the description field
	memset(vcheck, 0, sizeof(vcheck));
	sprintf(vcheck, "ver %i", VERSION);
	if (strcmp (save_p, vcheck))
	{ // Bad version
		return;
	}
	save_p += VERSIONSIZE;
	gameskill = *save_p++;
	for(i = 0; i < MAXPLAYERS; i++)
	{
		playeringame[i] = *save_p++;
	}
	// Load a base level
	if (a1)
		G_InitNew(gameskill, gamemap);
	else
		G_DoLoadLevel(true);

	// Get the times
	a = *save_p++;
	b = *save_p++;
	c = *save_p++;
	leveltime = (a<<16)+(b<<8)+c;

	// De-archive all the modifications
	P_UnArchivePlayers(a1);
	P_UnArchiveWorld();
	P_UnArchiveThinkers();
	P_UnArchiveSpecials();

	if(*save_p != 0x1d)
	{
		I_Error("Bad savegame");
	}
	Z_Free(savebuffer);

	// Draw the pattern into the back screen
	R_FillBackScreen();
}


//==========================================================================
//
// G_SaveGame
//
// Called by the menu task.  <description> is a 24 byte text string.
//
//==========================================================================

void G_SaveGame(int slot, char *description)
{
	char buffer[128];
	savegameslot = slot;
	if (M_CheckParm("-cdrom"))
	{
		sprintf(savedir, "c:\\strife.cd\\strfsav%d.ssg\\", 6);
		sprintf(userdir, "c:\\strife.cd\\strfsav%d.ssg\\", savegameslot);
	}
	else
	{
		sprintf(savedir, "strfsav%d.ssg\\", 6);
		sprintf(userdir, "strfsav%d.ssg\\", savegameslot);
	}
	savedescription[0] = 0;
	strcpy(savedescription, description);
	sprintf(buffer, "%sname", savedir);
	M_WriteFile(buffer, savedescription, 32);
}

//==========================================================================
//
// G_DoSaveGame
//
// Called by G_Ticker based on gameaction.
//
//==========================================================================

GAMESTATIC void G_DoSaveGame(char *dir)
{
	static char name[80];
	char name2[VERSIONSIZE];
	int length;
	int i;

	sprintf(name, "%scurrent", dir);
	M_WriteFile(name, &gamemap, 4);

	sprintf(name, "%s%d", dir, gamemap);

	save_p = savebuffer = screens[1]+0x4000;

	memset (name2,0,sizeof(name2));
	sprintf (name2,"ver %i",VERSION);
	memcpy (save_p, name2, VERSIONSIZE);
	save_p += VERSIONSIZE;

	*save_p++ = gameskill;
	for(i = 0; i < MAXPLAYERS; i++)
	{
		*save_p++ = playeringame[i];
	}
	*save_p++ = leveltime>>16;
	*save_p++ = leveltime>>8;
	*save_p++ = leveltime;

	P_ArchivePlayers();
	P_ArchiveWorld();
	P_ArchiveThinkers();
	P_ArchiveSpecials();
	
	*save_p++ = 0x1d;		// consistancy marker
	 
	length = save_p - savebuffer;
	if (length > SAVEGAMESIZE)
		I_Error ("Savegame buffer overrun");
	M_WriteFile (name, savebuffer, length);
	gameaction = ga_nothing;

	if (strcmp(dir, userdir) != 0)
		return;
	sprintf(name, GGSAVED, savedescription);
	players[consoleplayer].message = name;

	// Draw the pattern into the back screen
	R_FillBackScreen();
}


/*
====================
=
= G_InitNew
=
= Can be called by the startup code or the menu task
= consoleplayer, displayplayer, playeringame[] should be set
====================
*/

extern int __wp1, __wp2;

skill_t d_skill;
int     d_map;

void G_DeferedInitNew (skill_t skill, int map)
{
	d_skill = skill;
	d_map = map;
	gameaction = ga_newgame;
}

GAMESTATIC void G_DoNewGame (void)
{
	demoplayback = false;
	netdemo = false;
	netgame = false;
	deathmatch = false;
	playeringame[1] = playeringame[2] = playeringame[3] = 0;
	respawnparm = false;
	fastparm = false;
	killfl = false;
	consoleplayer = 0;
	G_InitNew (d_skill, d_map);
	gameaction = ga_nothing;
}

extern  int                     skytexture;

void G_InitNew(skill_t skill, int map)
{
	int i;

	if(paused)
	{
		paused = false;
		S_ResumeSound();
	}


	if(skill > sk_nightmare)
		skill = sk_nightmare;
	M_ClearRandom();
	if(skill == sk_nightmare || respawnparm)
	{
		respawnmonsters = true;
	}
	else
	{
		respawnmonsters = false;
	}
	
	if (skill == sk_baby && gameskill != sk_baby)
	{
		for (i=S_AGRD_612 ; i<=S_AGRD_622 ; i++)
			states[i].tics <<= 1;
		for (i=S_ROB1_577 ; i<=S_ROB1_582 ; i++)
			states[i].tics <<= 1;
		for (i=S_TURT_1149 ; i<=S_TURT_1150 ; i++)
			states[i].tics <<= 1;
		for (i=S_ROB2_690 ; i<=S_ROB2_700 ; i++)
			states[i].tics <<= 1;
		mobjinfo[MT_101].speed <<= 1;
		for (i=S_SPID_1012 ; i<=S_SPID_1019 ; i++)
			states[i].tics <<= 1;
	}
	else if (skill != sk_baby && gameskill == sk_baby)
	{
		for (i=S_AGRD_612 ; i<=S_AGRD_622 ; i++)
			states[i].tics >>= 1;
		for (i=S_ROB1_577 ; i<=S_ROB1_582 ; i++)
			states[i].tics >>= 1;
		for (i=S_TURT_1149 ; i<=S_TURT_1150 ; i++)
			states[i].tics >>= 1;
		for (i=S_ROB2_690 ; i<=S_ROB2_700 ; i++)
			states[i].tics >>= 1;
		mobjinfo[MT_101].speed >>= 1;
		for (i=S_SPID_1012 ; i<=S_SPID_1019 ; i++)
			states[i].tics >>= 1;
	}
	if (fastparm || (skill == sk_nightmare && gameskill != sk_nightmare))
	{
		for (i=S_AGRD_612 ; i<=S_AGRD_622 ; i++)
			states[i].tics >>= 1;
		mobjinfo[MT_101].speed >>= 1;
	}
	else if (skill != sk_nightmare && gameskill == sk_nightmare)
	{
		for (i=S_AGRD_612 ; i<=S_AGRD_622 ; i++)
			states[i].tics <<= 1;
		mobjinfo[MT_101].speed <<= 1;
	}


	// Force players to be initialized upon first level load
	for(i = 0; i < MAXPLAYERS; i++)
	{
		players[i].playerstate = PST_REBORN;
	}

	usergame = true; // will be set false if a demo
	paused = false;
	demoplayback = false;
	automapactive = false;
	viewactive = true;
	gamemap = map;
	gameskill = skill;

	viewactive = true;

	spawnspot = 0;

	// Set the sky map for the episode
	if (gamemap >= 32 || gamemap < 9)
		skytexture = R_TextureNumForName("skymnt02");
	else
		skytexture = R_TextureNumForName("skymnt01");

//
// give one null ticcmd_t
//
#if 0
	gametic = 0;
	maketic = 1;
	for (i=0 ; i<MAXPLAYERS ; i++)
		nettics[i] = 1;                 // one null event for this gametic
	memset (localcmds,0,sizeof(localcmds));
	memset (netcmds,0,sizeof(netcmds));
#endif
	func_16784(gamemap);
	G_DoLoadLevel(true);
}


/*
===============================================================================

							DEMO RECORDING

===============================================================================
*/

#define DEMOMARKER      0x80

void G_ReadDemoTiccmd (ticcmd_t *cmd)
{
	if (*demo_p == DEMOMARKER)
	{       // end of demo data stream
		G_CheckDemoStatus ();
		return;
	}
	cmd->forwardmove = ((signed char)*demo_p++);
	cmd->sidemove = ((signed char)*demo_p++);
	cmd->angleturn = ((unsigned char)*demo_p++)<<8;
	cmd->buttons = (unsigned char)*demo_p++;
	cmd->f_8 = (unsigned char)*demo_p++;
	cmd->f_9 = (unsigned char)*demo_p++;
}

void G_WriteDemoTiccmd (ticcmd_t *cmd)
{
	if (gamekeydown['q'])           // press q to end demo recording
		G_CheckDemoStatus ();
	*demo_p++ = cmd->forwardmove;
	*demo_p++ = cmd->sidemove;
	*demo_p++ = (cmd->angleturn+128)>>8;
	*demo_p++ = cmd->buttons;
	*demo_p++ = cmd->f_8;
	*demo_p++ = cmd->f_9;
	demo_p -= 6;
	if (demo_p > demoend - 16)
	{
		// no more space 
		G_CheckDemoStatus();
		return;
	}

	G_ReadDemoTiccmd (cmd);         // make SURE it is exactly the same
}



/*
===================
=
= G_RecordDemo
=
===================
*/

void G_RecordDemo (char *name)
{
	int             i;
	int             maxsize;

	usergame = false;
	strcpy (demoname, name);
	strcat (demoname, ".lmp");
	maxsize = 0x20000;
	i = M_CheckParm ("-maxdemo");
	if (i && i<myargc-1)
		maxsize = atoi(myargv[i+1])*1024;
	demobuffer = Z_Malloc (maxsize,PU_STATIC,NULL);
	demoend = demobuffer + maxsize;

	demorecording = true;
}


void G_BeginRecording(void)
{
	int             i;

	demo_p = demobuffer;

	*demo_p++ = VERSION;
	*demo_p++ = gameskill;
	*demo_p++ = gamemap;
	*demo_p++ = deathmatch;
	*demo_p++ = respawnparm;
	*demo_p++ = fastparm;
	*demo_p++ = nomonsters;
	*demo_p++ = consoleplayer;

	for (i=0 ; i<MAXPLAYERS; i++)
		*demo_p++ = playeringame[i];
}


/*
===================
=
= G_PlayDemo
=
===================
*/

char    *defdemoname;

void G_DeferedPlayDemo (char *name)
{
	defdemoname = name;
	gameaction = ga_playdemo;
}

GAMESTATIC void G_DoPlayDemo (void)
{
	skill_t skill;
	int             i, map;

	gameaction = ga_nothing;
	demobuffer = demo_p = W_CacheLumpName (defdemoname, PU_STATIC);
	if (*demo_p++ != VERSION)
		I_Error("Demo is from a different game version!");

	skill = *demo_p++;
	map = *demo_p++;
	deathmatch = *demo_p++;
	respawnparm = *demo_p++;
	fastparm = *demo_p++;
	nomonsters = *demo_p++;
	consoleplayer = *demo_p++;

	for (i=0 ; i<MAXPLAYERS ; i++)
		playeringame[i] = *demo_p++;
	if (playeringame[1])
	{
		netgame = true;
		netdemo = true;
	}

	precache = false;               // don't spend a lot of time in loadlevel
	G_InitNew (skill, map);
	precache = true;
	usergame = false;
	demoplayback = true;
}


/*
===================
=
= G_TimeDemo
=
===================
*/

void G_TimeDemo (char *name)
{
	nodrawers = M_CheckParm ("-nodraw");
	noblit = M_CheckParm ("-noblit");
	timingdemo = true;
	singletics = true;

	defdemoname = name;
	gameaction = ga_playdemo;
}


/*
===================
=
= G_CheckDemoStatus
=
= Called after a death or level completion to allow demos to be cleaned up
= Returns true if a new demo loop action will take place
===================
*/

boolean G_CheckDemoStatus (void)
{
	int             endtime;

	if (timingdemo)
	{
		endtime = I_GetTime ();
		I_Error ("timed %i gametics in %i realtics",gametic
		, endtime-starttime);
	}

	if (demoplayback)
	{
		if (singledemo)
			I_Quit ();

		Z_ChangeTag (demobuffer, PU_CACHE);
		demoplayback = false;
		netdemo = false;
		netgame = false;
		deathmatch = false;
		playeringame[1] = playeringame[2] = playeringame[3] = 0;
		respawnparm = false;
		fastparm = false;
		nomonsters = false;
		consoleplayer = 0;
		D_AdvanceDemo ();
		return true;
	}

	if (demorecording)
	{
		*demo_p++ = DEMOMARKER;
		M_WriteFile (demoname, demobuffer, demo_p - demobuffer);
		Z_Free (demobuffer);
		demorecording = false;
		I_Error ("Demo %s recorded",demoname);
	}

	return false;
}


