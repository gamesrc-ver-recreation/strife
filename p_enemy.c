//
// Copyright (C) 1993-1996 Id Software, Inc.
// Copyright (C) 2022 Nuke.YKT
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
//

// P_enemy.c

#include "DoomDef.h"
#include "P_local.h"
#include "soundst.h"


/*
===============================================================================

							ENEMY THINKING

enemies are allways spawned with targetplayer = -1, threshold = 0
Most monsters are spawned unaware of all players, but some can be made preaware

===============================================================================
*/


//
// Called by P_NoiseAlert.
// Recursively traverse adjacent sectors,
// sound blocking lines cut off traversal.
//

mobj_t *soundtarget;

void P_RecursiveSound (sector_t *sec, int soundblocks)
{
	int			i;
	line_t		*check;
	sector_t	*other;

// wake up all monsters in this sector
	if (sec->validcount == validcount && sec->soundtraversed <= soundblocks+1)
		return;		// already flooded
	sec->validcount = validcount;
	sec->soundtraversed = soundblocks+1;
	sec->soundtarget = soundtarget;
	for (i=0 ;i<sec->linecount ; i++)
	{
		check = sec->lines[i];
		if(! (check->flags&ML_TWOSIDED) )
			continue;

		P_LineOpening (check);

		if(openrange <= 0)
			continue;	// closed door

		if( sides[ check->sidenum[0] ].sector == sec )
			other = sides[ check->sidenum[1] ].sector;
		else
			other = sides[ check->sidenum[0] ].sector;

		if (check->flags & ML_SOUNDBLOCK)
		{
			if(!soundblocks)
				P_RecursiveSound (other, 1);
		}
		else
			P_RecursiveSound (other, soundblocks);
	}
}

/*
================
=
= P_NoiseAlert
=
= If a monster yells at a player, it will alert other monsters to the
= player.
=
================
*/

void P_NoiseAlert(mobj_t *target, mobj_t *emmiter)
{
	soundtarget = target;
	validcount++;
	P_RecursiveSound (emmiter->subsector->sector, 0);
}

void func_1EAB0(mobj_t *a1, mobj_t *a2)
{
	if (a2->flags & MF_15)
		return;
	a2->target = a1;
	if (a2->info->seesound)
		S_StartSound(a2, a2->info->seesound);
	P_SetMobjState(a2, a2->info->seestate);
}

void func_1EAE0(mobj_t *a1, mobj_t *a2)
{
	mobj_t *vb;
	if (a2->subsector->sector->soundtarget)
		return;
	if (a2->health <= 0)
		return;
	if (!(a2->flags & MF_COUNTKILL))
		return;
	if (a2->flags & MF_15)
		return;
	a2->target = a1;
	P_SetMobjState(a2, a2->info->painstate);
	vb = a2->snext;
	while (vb)
	{
		if (vb->health > 0 && vb->type >= MT_53 && vb->type <= MT_62)
		{
			if (P_CheckSight(vb, a1) || P_CheckSight(vb, a2))
			{
				func_1EAB0(a1, vb);
				vb->flags |= MF_15;
			}
		}
		vb = vb->snext;
	}
	vb = a2->sprev;
	while (vb)
	{
		if (vb->health > 0 && vb->type >= MT_53 && vb->type <= MT_62)
		{
			if (P_CheckSight(vb, a1) || P_CheckSight(vb, a2))
			{
				func_1EAB0(a1, vb);
				vb->flags |= MF_15;
			}
		}
		vb = vb->sprev;
	}
}

/*
================
=
= P_CheckMeleeRange
=
================
*/

boolean P_CheckMeleeRange(mobj_t *actor)
{
	mobj_t		*pl;
	fixed_t		dist;
	fixed_t		h;

	if (!actor->target)
		return false;

	pl = actor->target;

	h = (actor->height * 3) / 2;
	if (actor->z+h < pl->z)
		return false;

	dist = P_AproxDistance (pl->x-actor->x, pl->y-actor->y);

	if (dist >= MELEERANGE-20*FRACUNIT+pl->info->radius)
		return false;

	if (! P_CheckSight (actor, actor->target) )
		return false;

	return true;
}

/*
================
=
= P_CheckMissileRange
=
================
*/

boolean P_CheckMissileRange(mobj_t *actor)
{
	fixed_t		dist;

	if (! P_CheckSight (actor, actor->target) )
		return false;

	if(actor->flags&MF_JUSTHIT)
	{ // the target just hit the enemy, so fight back!
		actor->flags &= ~MF_JUSTHIT;
		return true;
	}

	if (actor->reactiontime)
		return false;		// don't attack yet

	dist = P_AproxDistance ( actor->x-actor->target->x, actor->y-actor->target->y) - 64*FRACUNIT;
	if(!actor->info->meleestate)
		dist -= 128*FRACUNIT;		// no melee attack, so fire more

	dist >>= 16;

	switch (actor->type)
	{
		case MT_61:
		case MT_63:
		case MT_91:
			dist >>= 1;
			break;
		case MT_53:
		case MT_54:
		case MT_55:
		case MT_56:
		case MT_57:
		case MT_58:
			dist >>= 4;
			break;
	}

	if (dist > 150)
		dist = 150;

	if (actor->type == MT_63 && dist > 120)
		dist = 120;

	return dist < P_Random();
}

boolean func_1ED64(mobj_t *actor)
{
	fixed_t		dist;

	if (! P_CheckSight (actor, actor->target) )
		return false;

	if (actor->reactiontime)
		return false;		// don't attack yet

	dist = P_AproxDistance ( actor->x-actor->target->x, actor->y-actor->target->y) - 64*FRACUNIT;
	dist >>= 16;

	return dist < 200;
}


/*
================
=
= P_Move
=
= Move in the current direction
= returns false if the move is blocked
================
*/

fixed_t	xspeed[8] = {FRACUNIT,47000,0,-47000,-FRACUNIT,-47000,0,47000};
fixed_t yspeed[8] = {0,47000,FRACUNIT,47000,0,-47000,-FRACUNIT,-47000};

#define	MAXSPECIALCROSS		8
extern	line_t	*spechit[MAXSPECIALCROSS];
extern	int			 numspechit;

boolean P_Move(mobj_t *actor)
{
	fixed_t tryx, tryy;
	line_t *ld;
	boolean try, good;

	if (actor->movedir == DI_NODIR)
		return false;
	
	if ((unsigned)actor->movedir >= 8)
		I_Error ("Weird actor->movedir!");

	tryx = actor->x+actor->info->speed*xspeed[actor->movedir];
	tryy = actor->y+actor->info->speed*yspeed[actor->movedir];

	try = P_TryMove (actor, tryx, tryy);

	if (!try)
	{ // open any specials
		if (actor->flags&MF_FLOAT && floatok)
		{ // must adjust height
			if (actor->z < tmfloorz)
				actor->z += FLOATSPEED;
			else
				actor->z -= FLOATSPEED;
			actor->flags |= MF_INFLOAT;
			return true;
		}

		if(!numspechit)
			return false;

		actor->movedir = DI_NODIR;
		good = false;
		while (numspechit--)
		{
			ld = spechit[numspechit];
			// if the special isn't a door that can be opened, return false
			if (P_UseSpecialLine (actor, ld,0))
				good = true;
		}
		return good;
	}
	else
	{
		actor->flags &= ~(MF_INFLOAT | MF_13);
		if (func_28FA8(actor) != 2)
			actor->flags |= MF_13;
	}

	return true;
}


/*
==================================
=
= TryWalk
=
= Attempts to move actoron in its current (ob->moveangle) direction.
=
= If blocked by either a wall or an actor returns FALSE
= If move is either clear or blocked only by a door, returns TRUE and sets
= If a door is in the way, an OpenDoor call is made to start it opening.
=
==================================
*/

boolean P_TryWalk(mobj_t *actor)
{
	if (!P_Move (actor))
	{
		return false;
	}
	actor->movecount = P_Random()&15;
	return true;
}

/*
================
=
= P_NewChaseDir
=
================
*/

dirtype_t opposite[] =
{DI_WEST, DI_SOUTHWEST, DI_SOUTH, DI_SOUTHEAST, DI_EAST, DI_NORTHEAST,
DI_NORTH, DI_NORTHWEST, DI_NODIR};

dirtype_t diags[] = {DI_NORTHWEST,DI_NORTHEAST,DI_SOUTHWEST,DI_SOUTHEAST};

void P_NewChaseDir (mobj_t *actor)
{
	fixed_t		deltax,deltay;
	dirtype_t	d[3];
	dirtype_t	tdir, olddir, turnaround;

	if (!actor->target)
	{
		P_SetMobjState(actor, actor->info->spawnstate);
		return;
	}
		
	olddir = actor->movedir;
	turnaround=opposite[olddir];

	deltax = actor->target->x - actor->x;
	deltay = actor->target->y - actor->y;

	if (deltax>10*FRACUNIT)
		d[1]= DI_EAST;
	else if (deltax<-10*FRACUNIT)
		d[1]= DI_WEST;
	else
		d[1]=DI_NODIR;
	if (deltay<-10*FRACUNIT)
		d[2]= DI_SOUTH;
	else if (deltay>10*FRACUNIT)
		d[2]= DI_NORTH;
	else
		d[2]=DI_NODIR;

// try direct route
	if (d[1] != DI_NODIR && d[2] != DI_NODIR)
	{
		actor->movedir = diags[((deltay<0)<<1)+(deltax>0)];
		if (actor->movedir != turnaround && P_TryWalk(actor))
			return;
	}

// try other directions
	if (P_Random() > 200 ||  abs(deltay)>abs(deltax))
	{
		tdir=d[1];
		d[1]=d[2];
		d[2]=tdir;
	}

	if (d[1]==turnaround)
		d[1]=DI_NODIR;
	if (d[2]==turnaround)
		d[2]=DI_NODIR;
	
	if (d[1]!=DI_NODIR)
	{
		actor->movedir = d[1];
		if (P_TryWalk(actor))
			return;     /*either moved forward or attacked*/
	}

	if (d[2]!=DI_NODIR)
	{
		actor->movedir =d[2];
		if (P_TryWalk(actor))
			return;
	}

/* there is no direct path to the player, so pick another direction */

	if (olddir!=DI_NODIR)
	{
		actor->movedir =olddir;
		if (P_TryWalk(actor))
			return;
	}

	if (P_Random()&1) 	/*randomly determine direction of search*/
	{
		for (tdir=DI_EAST ; tdir<=DI_SOUTHEAST ; tdir++)
		{
			if (tdir!=turnaround)
			{
				actor->movedir =tdir;
				if ( P_TryWalk(actor) )
					return;
			}
		}
	}
	else
	{
		for (tdir=DI_SOUTHEAST ; tdir != (DI_EAST-1);tdir--)
		{
			if (tdir!=turnaround)
			{
				actor->movedir =tdir;
				if ( P_TryWalk(actor) )
				return;
			}
		}
	}

	if (turnaround !=  DI_NODIR)
	{
		actor->movedir =turnaround;
		if ( P_TryWalk(actor) )
			return;
	}

	actor->movecount = 0;
	actor->movedir = DI_NODIR;		// can't move
}

void func_1F17C(mobj_t *actor)
{
	fixed_t		deltax,deltay;
	dirtype_t	d[3];
	dirtype_t	tdir, olddir, turnaround;

	olddir = actor->movedir;
	turnaround=opposite[olddir];
	
	if (P_Random()&1) 	/*randomly determine direction of search*/
	{
		for (tdir=DI_EAST ; tdir<=DI_SOUTHEAST ; tdir++)
		{
			if (tdir!=turnaround)
			{
				actor->movedir =tdir;
				if ( (P_Random()&1) && P_TryWalk(actor) )
					return;
			}
		}
	}
	else
	{
		for (tdir=DI_SOUTHEAST ; tdir != (DI_EAST-1);tdir--)
		{
			if (tdir!=turnaround)
			{
				actor->movedir =tdir;
				if ( P_TryWalk(actor) )
				return;
			}
		}
	}

	if (turnaround !=  DI_NODIR)
	{
		actor->movedir =turnaround;
		if ( P_TryWalk(actor) )
			return;
	}

	actor->movedir = DI_NODIR;		// can't move
}

/*
================
=
= P_LookForPlayers
=
= If allaround is false, only look 180 degrees in front
= returns true if a player is targeted
================
*/

boolean P_LookForPlayers(mobj_t *actor, boolean allaround)
{
	int			c;
	int			stop;
	player_t	*player;
	sector_t	*sector;
	angle_t		an;
	fixed_t		dist;
	mobj_t		*vb;

	vb = players[actor->f_9a].mo;
	if (actor->flags & MF_26)
	{
#if (APPVER_STRIFEREV >= AV_SR_STRF12)
		if (!netgame)
		{
			if (vb && vb->target && !(vb->target->flags & MF_26))
			{
				actor->target = vb->target;
				return true;
			}
			P_BulletSlope(actor);
			if (!linetarget || (actor->target->flags & MF_26))
			{
				actor->target = NULL;
				return false;
			}
			return true;
		}
		else
		{
			if (vb && vb->target && !(vb->target->type == MT_43 && vb->target->f_9a == actor->f_9a))
			{
				actor->target = vb->target;
				return true;
			}
			P_BulletSlope(actor);
			if (!linetarget || (actor->target->type == MT_43 && actor->target->f_9a == actor->f_9a) || vb == actor->target)
			{
				actor->target = NULL;
				return false;
			}
			return true;
		}
#else
		if (vb && vb->target && (actor->flags & MF_26) != (vb->target->flags & MF_26))
		{
			actor->target = vb->target;
			return true;
		}
		P_BulletSlope(actor);
		if (!linetarget || (actor->flags & MF_26) == (actor->target->flags & MF_26))
		{
			actor->target = NULL;
			return false;
		}
		return true;
#endif
	}

	sector = actor->subsector->sector;
	c = 0;
	stop = (actor->lastlook-1)&3;
	for( ; ; actor->lastlook = (actor->lastlook+1)&3 )
	{
		if (!playeringame[actor->lastlook])
			continue;
			
		if (c++ == 2 || actor->lastlook == stop)
			return false;		// done looking

		player = &players[actor->lastlook];
		if (player->health <= 0)
			continue;		// dead
		if (!P_CheckSight (actor, player->mo))
			continue;		// out of sight
			
		if (!allaround)
		{
			an = R_PointToAngle2 (actor->x, actor->y, 
			player->mo->x, player->mo->y) - actor->angle;
			if (an > ANG90 && an < ANG270)
			{
				dist = P_AproxDistance (player->mo->x - actor->x,
					player->mo->y - actor->y);
				// if real close, react anyway
				if (dist > MELEERANGE)
					continue;		// behind back
			}
		}

		actor->target = player->mo;
		return true;
	}
	return false;
}

/*
===============================================================================

						ACTION ROUTINES

===============================================================================
*/

/*
==============
=
= A_Look
=
= Stay in state until a player is sighted
=
==============
*/

void A_Look (mobj_t *actor)
{
	mobj_t		*targ;
	
	actor->threshold = 0;		// any shot will wake up
	targ = actor->subsector->sector->soundtarget;
	if (targ && (targ->flags & MF_SHOOTABLE) )
	{
		if (actor->flags & MF_26)
			func_1F4A0(actor);
		else
		{
			actor->target = targ;
			if ( actor->flags & MF_23 )
			{
				if (P_CheckSight (actor, actor->target))
					goto seeyou;
			}
			else
				goto seeyou;
		}
	}
	
	
	if (!P_LookForPlayers (actor, actor->flags & MF_11) )
		return;
		
// go into chase state
seeyou:
	if (actor->info->seesound)
	{
		int		sound;
		
		sound = actor->info->seesound;
		if (actor->type == MT_93)
		{ // full volume
			S_StartSound (NULL, sound);
		}
		else
		{
			S_StartSound (actor, sound);
		}
	}
	actor->threshold = 20;
	P_SetMobjState (actor, actor->info->seestate);
}

void func_1F4A0(mobj_t *actor)
{
	int delta;

	if (actor->flags & MF_5)
		return;

	if (actor->reactiontime)
	{
		actor->reactiontime--;
		return;
	}

//
// turn towards movement direction if not there yet
//
	if(actor->movedir < 8)
	{
		actor->angle &= (7<<29);
		delta = actor->angle-(actor->movedir << 29);
		if(delta > 0)
			actor->angle -= ANG90/2;
		else if(delta < 0)
			actor->angle += ANG90/2;
	}
	
//
// chase towards player
//
	if (--actor->movecount<0 || !P_Move (actor))
	{
		func_1F17C (actor);
		actor->movecount += 5;
	}
}

void func_1F51C(mobj_t *actor)
{
	mobj_t		*targ;
	
	actor->threshold = 0;
	targ = actor->subsector->sector->soundtarget;
	if (targ && (targ->flags & MF_SHOOTABLE) )
	{
		if ((actor->flags & MF_26) != (targ->flags & MF_26) || gamemap == 3 || gamemap == 34)
		{
			actor->target = targ;
			if ( ( actor->flags & MF_23 ) && !P_CheckSight(actor, actor->target) )
				goto test;
			actor->threshold = 10;
			P_SetMobjState (actor, actor->info->seestate);
		}
		else if (P_LookForPlayers (actor, actor->flags & MF_11) )
		{
			P_SetMobjState (actor, actor->info->seestate);
			actor->flags |= MF_15;
		}
		else
			goto test;
	}
	else
		goto test;
	return;

test:
	if (P_Random() < 30)
	{
		P_SetMobjState (actor, actor->info->spawnstate + (P_Random()&1) + 1);
	}
	if (!(actor->flags & MF_5) && P_Random() < 40)
	{
		P_SetMobjState (actor, actor->info->spawnstate + 3);
	}
}

void func_1F608(mobj_t *actor)
{
	mobj_t		*targ;
	
	actor->threshold = 0;
	targ = actor->subsector->sector->soundtarget;
	if (targ && (targ->flags & MF_SHOOTABLE) )
	{
		if ((actor->flags & MF_26) != (targ->flags & MF_26))
		{
			actor->target = targ;
			if ( !( actor->flags & MF_23 ) || P_CheckSight(actor, actor->target) )
			{
				if (actor->info->seesound)
					S_StartSound (actor, actor->info->seesound);
				actor->threshold = 10;
				P_SetMobjState (actor, actor->info->seestate);
			}
		}
	}
}


/*
==============
=
= A_Chase
=
= Actor has a melee attack, so it tries to close as fast as possible
=
==============
*/

void A_Chase (mobj_t *actor)
{
	int delta;

	if (actor->reactiontime)
		actor->reactiontime--;

//
// modify target threshold
//
	if (actor->threshold)
	{
		if (!actor->target)
		{
			actor->threshold = 0;
		}
		else
			actor->threshold--;
	}

//
// turn towards movement direction if not there yet
//
	if(actor->movedir < 8)
	{
		actor->angle &= (7<<29);
		delta = actor->angle-(actor->movedir << 29);
		if(delta > 0)
			actor->angle -= ANG90/2;
		else if(delta < 0)
			actor->angle += ANG90/2;
	}

	if (!actor->target || !(actor->target->flags&MF_SHOOTABLE))
	{	// look for a new target
		if(P_LookForPlayers(actor, true))
			return;		// got a new target

		P_SetMobjState (actor, actor->info->spawnstate);
		return;
	}
	
//
// don't attack twice in a row
//
	if (actor->flags & MF_JUSTATTACKED)
	{
		actor->flags &= ~MF_JUSTATTACKED;
		if (!fastparm)
			P_NewChaseDir (actor);
		return;
	}
	
//
// check for melee attack
//	
	if (actor->info->meleestate && P_CheckMeleeRange (actor))
	{
		if (actor->info->attacksound)
			S_StartSound (actor, actor->info->attacksound);
		P_SetMobjState (actor, actor->info->meleestate);
		return;
	}

//
// check for missile attack
//
	if (actor->info->missilestate)
	{
		if (!fastparm && actor->movecount)
		{
			goto nomissile;
		}
		if (!P_CheckMissileRange (actor))
			goto nomissile;
		P_SetMobjState (actor, actor->info->missilestate);
		actor->flags |= MF_JUSTATTACKED | MF_15;
		return;
	}
nomissile:

//
// possibly choose another target
//
	if (netgame && !actor->threshold && !P_CheckSight (actor, actor->target) )
	{
		if (P_LookForPlayers(actor,true))
			return;		// got a new target
	}
	
//
// chase towards player
//
	if (--actor->movecount<0 || !P_Move (actor))
	{
		P_NewChaseDir (actor);
	}

//
// make active sound
//
	if (actor->info->activesound && P_Random() < 38)
	{
		int		sound;
		switch (actor->info->activesound)
		{
			case sfx_agrac1:
			case sfx_agrac2:
			case sfx_agrac3:
			case sfx_agrac4:
				sound = sfx_agrac1 + (P_Random() % 4);
				break;
			default:
				sound = actor->info->activesound;
				break;
		}
		S_StartSound (actor, sound);
	}
}

/*============================================================================= */

/*
==============
=
= A_FaceTarget
=
==============
*/

void A_FaceTarget (mobj_t *actor)
{
	if(!actor->target)
		return;
	actor->flags &= ~MF_23;

	actor->angle = R_PointToAngle2 (actor->x, actor->y
	, actor->target->x, actor->target->y);

	if (actor->target->flags & MF_SHADOW)
		actor->angle += (P_Random()-P_Random())<<22;
	else if (actor->target->flags & MF_27)
		actor->angle += (P_Random()-P_Random())<<23;
}

void func_1F8DC(mobj_t *actor)
{
	int		damage;
	
	if (!actor->target)
		return;
		
	A_FaceTarget (actor);
	if (P_CheckMeleeRange (actor))
	{
		damage = ((P_Random()%5)+1)*2;
		P_DamageMobj (actor->target, actor, actor, damage);
	}
}

void func_1F91C(mobj_t *actor)
{
	int		i;
	int		angle, bangle, damage, slope;
	
	if (!actor->target)
		return;

	S_StartSound (actor, sfx_reavat);
	A_FaceTarget (actor);
	bangle = actor->angle;
	slope = P_AimLineAttack (actor, bangle, MISSILERANGE);

	for (i=0 ; i<3 ; i++)
	{
		angle = bangle + ((P_Random()-P_Random())<<20);
		damage = ((P_Random()&7)+1)*3;
		P_LineAttack (actor, angle, MISSILERANGE, slope, damage);
	}
}

void func_1F998(mobj_t *actor)
{
	int		angle, bangle, damage, slope;
	
	if (!actor->target)
		return;

	S_StartSound (actor, sfx_rifle);
	A_FaceTarget (actor);
	bangle = actor->angle;
	slope = P_AimLineAttack (actor, bangle, MISSILERANGE);

	angle = bangle + ((P_Random()-P_Random())<<19);
	damage = ((P_Random()%5)+1)*3;
	P_LineAttack (actor, angle, MISSILERANGE, slope, damage);
}

void func_1FA10(mobj_t *actor)
{
// keep firing unless target got out of sight
	A_FaceTarget (actor);
	if (P_Random () < 30)
		return;
	if (!actor->target || actor->target->health <= 0 || !P_CheckSight (actor, actor->target)
		|| P_Random() < 40 )
	{
		P_SetMobjState (actor, actor->info->seestate);
	}
}

void func_1FA58(mobj_t *actor)
{
	mobj_t *rt, *rt2;
	fixed_t x, y;
	fixed_t vc;
	angle_t angle;
	int i;

	x = actor->x;
	y = actor->y;
	angle = actor->angle >> ANGLETOFINESHIFT;
	vc = mobjinfo[MT_105].radius;

	rt = func_2D6F8(actor, actor->target, MT_105);

	if (rt->momx | rt->momy)
	{
		for (i = 8; i > 1; i--)
		{
			rt2 = P_SpawnMobj(x + FixedMul(vc*i, finecosine[angle]), y + FixedMul(vc*i, finesine[angle]),
				rt->z + (rt->momz >> 2) * i, MT_104);
			rt2->target = actor;
			rt2->momx = rt->momx;
			rt2->momy = rt->momy;
			rt2->momz = rt->momz;
			P_CheckMissileSpawn(rt2);
		}
	}
	rt->z += rt->momz >> 2;
}

void func_1FB64(mobj_t *actor)
{
	if (!(actor->flags & MF_NOGRAVITY) )
	{
		P_SetMobjState (actor, S_SPID_1027);
	}
	else if (actor->ceilingz - actor->info->height > actor->z)
	{
		P_SetMobjState (actor, S_SPID_1020);
	}
}

void func_1FB98(mobj_t *actor)
{
	if (!actor)
		return;
	if (actor->flags & MF_NOGRAVITY)
	{
		if (actor->state->nextstate != S_SPID_1010)
			P_SetMobjState (actor, S_SPID_1010);
	}
	else
	{
		if (actor->state->nextstate != S_SPID_1011)
			P_SetMobjState (actor, S_SPID_1011);
	}
}

void func_1FBD0(mobj_t *actor)
{
	actor->flags &= ~MF_NOGRAVITY;
}

void func_1FBD8(mobj_t *actor)
{
	int		damage;
	if (actor->flags & MF_NOGRAVITY)
	{
		P_SetMobjState (actor, S_SPID_1020);
		return;
	}

	if (!actor->target)
		return;

	A_FaceTarget(actor);
	if (P_CheckMeleeRange(actor))
	{
		damage = ((P_Random()%8)+1)*2;
		P_DamageMobj(actor->target, actor, actor, damage);
	}
}

void func_1FC30(mobj_t *actor)
{
	fixed_t vd;
	fixed_t vb;

	if (actor->threshold)
		return;

	if (actor->flags & MF_INFLOAT)
		return;

	vd = actor->floorz + 96*FRACUNIT;
	vb = actor->ceilingz - actor->info->height - 16*FRACUNIT;
	if (vd > vb)
		vd = vb;
	if (vd < actor->z)
		actor->momz -= FRACUNIT;
	else
		actor->momz += FRACUNIT;
	if (vd == actor->z)
		actor->threshold = 4;
	else
		actor->threshold = 8;
}

void func_1FC98(mobj_t *actor)
{
	int		damage;

	if (!actor->target)
		return;

	A_FaceTarget(actor);
	if (P_CheckMeleeRange(actor))
	{
		S_StartSound(actor, sfx_revbld);
		damage = ((P_Random()%8)+1)*3;
		P_DamageMobj(actor->target, actor, actor, damage);
	}
}

void func_1FCE8(mobj_t *actor)
{
	int		i;
	int		angle, bangle, damage, slope;
	
	if (!actor->target)
		return;

	S_StartSound (actor, sfx_pgrdat);
	A_FaceTarget (actor);
	bangle = actor->angle;
	slope = P_AimLineAttack (actor, bangle, MISSILERANGE);

	for (i=0 ; i<10 ; i++)
	{
		damage = (P_Random()&4)*2;
		angle = actor->angle;
		angle += ((P_Random()-P_Random())<<19);
		P_LineAttack (actor, angle, MISSILERANGE+64*FRACUNIT,
			slope+((P_Random()-P_Random())<<5), damage);
	}
}

void func_1FD6C(mobj_t *actor)
{
	if (!actor->target)
		return;

	actor->z += 8*FRACUNIT;
	if (func_1ED64(actor))
	{
		A_FaceTarget(actor);
		actor->angle -= 0x8000000;
		func_2D6F8(actor, actor->target, MT_113);
	}
	else if (P_CheckMissileRange(actor))
	{
		A_FaceTarget(actor);
		actor->z += 16*FRACUNIT;
		func_2D6F8(actor, actor->target, MT_100);
		actor->z -= 16*FRACUNIT;
		actor->angle -= 0x1000000;
		func_2D6F8(actor, actor->target, MT_100);
		actor->angle += 0x2000000;
		func_2D6F8(actor, actor->target, MT_100);

		P_SetMobjState(actor, actor->info->seestate);
		actor->reactiontime += 15;
	}
	else
		P_SetMobjState(actor, actor->info->seestate);
	actor->z -= 8*FRACUNIT;
}

void func_1FE5C(mobj_t *actor)
{
	mobj_t *th;
	actor->angle += 0x4000000;
	th = func_2D6F8(actor, actor->target, MT_113);
	th->z += 16*FRACUNIT;
	th->momz = FRACUNIT;
}

void func_1FE84(mobj_t *actor)
{
	mobj_t *th;
	actor->angle -= 0x4000000;
	th = func_2D6F8(actor, actor->target, MT_113);
	th->z += 16*FRACUNIT;
	th->momz = FRACUNIT;
}

void func_1FEB0(mobj_t *actor)
{
	if (!actor->target || actor->target->health <= 0 || !P_CheckSight(actor, actor->target))
		P_SetMobjState(actor, actor->info->seestate);
}

void func_1FEDC(mobj_t *actor)
{
	if (!actor->target)
		return;
	A_FaceTarget(actor);
	if (!func_1ED64(actor))
		P_SetMobjState(actor, S_ROB3_1061);
	if (actor->z != actor->target->z && actor->z+actor->height + 54*FRACUNIT < actor->ceilingz)
		P_SetMobjState(actor, S_ROB3_1064);
}

void func_1FF30(mobj_t *actor)
{
	mobj_t *th;
	if (!actor->target)
		return;
	A_FaceTarget(actor);
	actor->z += 32*FRACUNIT;
	actor->angle -= 0x1000000;
	th = func_2D6F8(actor, actor->target, MT_108);
	th->momz += 9*FRACUNIT;
	actor->angle += 0x2000000;
	th = func_2D6F8(actor, actor->target, MT_108);
	th->momz += 16*FRACUNIT;
	actor->z -= 32*FRACUNIT;
}

void func_1FF94(mobj_t *actor)
{
	angle_t angle;
	fixed_t vs = actor->info->speed * ((FRACUNIT*2) / 3);
	int vd;
	mobj_t *dest;

	if (!actor->target)
		return;

	S_StartSound(actor, sfx_inqjmp);
	actor->z += 64*FRACUNIT;
	A_FaceTarget(actor);
	angle = actor->angle;
	angle >>= ANGLETOFINESHIFT;
	actor->momx = FixedMul(vs, finecosine[angle]);
	actor->momy = FixedMul(vs, finesine[angle]);

	vd = P_AproxDistance(actor->target->x-actor->x, actor->target->y-actor->y);

	vd /= vs;

	if (vd < 1)
		vd = 1;
	actor->momz = (actor->target->z-actor->z) / vd;
	actor->reactiontime = 60;
	actor->flags |= MF_NOGRAVITY;
}

void func_20070(mobj_t *actor)
{
	if (!(leveltime&7))
		S_StartSound(actor, sfx_inqjmp);
	if (--actor->reactiontime < 0 || actor->momx == 0 || actor->momy == 0
		|| actor->z <= actor->floorz)
	{
		P_SetMobjState(actor, actor->info->seestate);
		actor->reactiontime = 0;
		actor->flags &= ~MF_NOGRAVITY;
	}
}

void func_200E0(mobj_t *actor)
{
	switch (P_Random() % 5)
	{
	case 0:
		func_201B4(actor);
		break;
	case 2:
		func_204D0(actor);
		break;
	case 3:
		func_20334(actor);
		break;
	case 4:
		func_204A4(actor);
		break;
	case 5:
		func_20314(actor);
		break;
	}
}

void func_201B4(mobj_t *actor)
{
	mobj_t *th;
	if (!actor->target)
		return;
	th = P_SpawnMobj(actor->target->x, actor->target->y, 0x80000000, MT_88);
	th->threshold = 25;
	th->target = actor;
	th->tracer = actor->target;
	th->health = -2;
}

void func_201FC(mobj_t *actor)
{
	mobj_t *th;
	fixed_t x, y;
	if (actor->threshold)
		actor->threshold--;
	actor->momx += ((P_Random()&3)-(P_Random()&3))*FRACUNIT;
	actor->momy += ((P_Random()&3)-(P_Random()&3))*FRACUNIT;
	x = actor->x + ((P_Random()&3)-(P_Random()&3))*50*FRACUNIT;
	y = actor->y + ((P_Random()&3)-(P_Random()&3))*50*FRACUNIT;
	if (actor->threshold > 25)
		th = P_SpawnMobj(x, y, 0x7fffffff, MT_87);
	else
		th = P_SpawnMobj(x, y, 0x7fffffff, MT_86);

	th->target = actor->target;
	th->momz = -18*FRACUNIT;
	th->health = actor->health;

	th = P_SpawnMobj(actor->x, actor->y, 0x7fffffff, MT_87);
	th->target = actor->target;
	th->momz = -18*FRACUNIT;
	th->health = actor->health;
}

void func_20314(mobj_t *actor)
{
	mobj_t *th;
	if (!actor->target)
		return;
	th = P_SpawnMissile(actor, actor->target, MT_85);
	th->health = -2;
}

void func_20334(mobj_t *actor)
{
	int i;
	mobj_t *th;
	fixed_t vdi;
	if (!actor->target)
		return;
	vdi = actor->z + 32*FRACUNIT;
	th = P_SpawnMobj(actor->x, actor->y, vdi, MT_87);
	th->momz = -18*FRACUNIT;
	th->target = actor;
	th->tracer = actor->target;
	th->health = -2;
	actor->angle -= ANG90;
	for (i = 0; i < 20; i++)
	{
		actor->angle += ANG90 / 10;
		th = func_2D96C(actor, MT_81);
		th->health = -2;
		th->z = vdi;
	}
	actor->angle -= ANG90;
}

void func_203DC(mobj_t *actor)
{
	thinker_t *th;
	mobj_t *mo2;

	for (th = thinkercap.next ; th != &thinkercap ; th=th->next)
	{
		if (th->function != P_MobjThinker)
			continue;

		mo2 = (mobj_t *)th;
		if (mo2->type == MT_71)
		{
			P_SetMobjState(mo2, mo2->info->seestate);
			mo2->target = actor->target;
			return;		
		}
	}
}

void func_20424(mobj_t *actor)
{
	actor->angle += ANG90;
	func_2D96C(actor, MT_82);
	actor->angle += ANG180;
	func_2D96C(actor, MT_82);
	actor->angle += ANG90;
	func_2D96C(actor, MT_82);
}

void func_2046C(mobj_t *actor)
{
	mobj_t *th;

	th = P_SpawnMobj(actor->x - actor->momx, actor->y - actor->momy, actor->z, MT_83);
	th->angle = actor->angle;
	th->health = actor->health;
}

void func_204A4(mobj_t *actor)
{
	mobj_t *th;
	if (!actor->target)
		return;
	th = P_SpawnMissile(actor, actor->target, MT_90);
	th->tracer = actor->target;
	th->health = -2;
}

void func_204D0(mobj_t *actor)
{
	mobj_t *th;
	if (!actor->target)
		return;
	th = P_SpawnMissile(actor, actor->target, MT_82);
	th->health = -2;
}

void func_204F0(mobj_t *actor)
{
	actor->flags &= ~(MF_27 | MF_SHADOW);
}

void func_204F8(mobj_t *actor)
{
	actor->flags |= MF_SHADOW;
	actor->flags &= ~MF_27;
}

void func_20510(mobj_t *actor)
{
	actor->flags |= MF_27 | MF_SHADOW;
}

void func_20518(mobj_t *actor)
{
	mapthing_t *sp;

	sp = &actor->spawnpoint;
	if (sp->options & MTF_8)
		actor->flags |= MF_SHADOW;
	if (sp->options & MTF_9)
		actor->flags |= MF_27;
}

void func_20538(mobj_t *actor)
{
	int damage;
	if (!actor->target)
		return;
	damage = (P_Random()&9)*10;
	P_DamageMobj(actor->target, actor, actor, damage);
}

void func_20564(mobj_t *actor)
{
	mobj_t *th;
	if (!actor->target)
		return;
	actor->z += 32*FRACUNIT;
	th = P_SpawnMissile(actor, actor->target, MT_101);
	th->tracer = actor->target;
	actor->z -= 32*FRACUNIT;
}

void func_20598(mobj_t *actor)
{
	if (!actor->target)
		return;
	P_SpawnMissile(actor, actor->target, MT_97);
}

void func_205B0(mobj_t *actor)
{
	S_StartSound(actor, sfx_tend);
	P_SpawnMobj(actor->x, actor->y, actor->z, MT_98);
	P_SpawnMobj(actor->x-(actor->momx>>1), actor->y-(actor->momy>>1), actor->z, MT_98);
	P_SpawnMobj(actor->x-actor->momx, actor->y-actor->momy, actor->z, MT_98);
}

void func_2061C(mobj_t *actor)
{
	mobj_t *th;
	S_StartSound(actor, sfx_rflite);
	P_SpawnPuff(actor->x, actor->y, actor->z);
	th = P_SpawnMobj(actor->x-actor->momx, actor->y-actor->momy, actor->z, MT_51);
	th->momz = FRACUNIT;
}

void func_20668(mobj_t *actor)
{
	fixed_t x, y;
	mobj_t *th;

	x = actor->x + ((P_Random()&3) - (P_Random()&3))*10*FRACUNIT;
	y = actor->y + ((P_Random()&3) - (P_Random()&3))*10*FRACUNIT;
	th = P_SpawnMobj(x, y, actor->z, MT_116);
	P_SetMobjState(th, S_BNG4_199);
	th->momz = FRACUNIT;
}

int	TRACEANGLE = 0xe000000;

void func_206E4(mobj_t *actor)
{
	angle_t	exact;
	fixed_t	dist;
	fixed_t	slope;
	mobj_t	*dest;
	mobj_t	*th;

//
// adjust direction
//
	dest = actor->tracer;
	
	if (!dest || dest->health <= 0)
		return;
    
//
// change angle
//
	exact = R_PointToAngle2 (actor->x, actor->y, dest->x, dest->y);

	if (exact != actor->angle)
	{
		if (exact - actor->angle > 0x80000000)
		{
			actor->angle -= TRACEANGLE;
			if (exact - actor->angle < 0x80000000)
				actor->angle = exact;
		}
		else
		{
			actor->angle += TRACEANGLE;
			if (exact - actor->angle > 0x80000000)
				actor->angle = exact;
		}
	}
	
	exact = actor->angle>>ANGLETOFINESHIFT;
	actor->momx = FixedMul (actor->info->speed, finecosine[exact]);
	actor->momy = FixedMul (actor->info->speed, finesine[exact]);
//
// change slope
//
	dist = P_AproxDistance (dest->x - actor->x,
	dest->y - actor->y);
    
	dist = dist / actor->info->speed;

	if (dist < 1)
		dist = 1;
	slope = (dest->z+40*FRACUNIT - actor->z) / dist;

	if (slope < actor->momz)
		actor->momz -= FRACUNIT/8;
	else
		actor->momz += FRACUNIT/8;
}

void func_207F8(mobj_t *actor)
{
	int damage;
	if (!actor->target)
		return;

	A_FaceTarget(actor);
	if (P_CheckMeleeRange(actor))
	{
		damage = ((P_Random()%10)+1)*6;
		S_StartSound(actor, sfx_mtalht);
		P_DamageMobj(actor->target, actor, actor, damage);
	}
}


void A_Scream (mobj_t *actor)
{
	int		sound;
	
	switch (actor->info->deathsound)
	{
	case 0:
		return;
	
	default:
		sound = actor->info->deathsound;
		break;
	}
//
// Check for bosses.
//
	if (actor->type == MT_74 || actor->type == MT_93)
	{ // full volume
		S_StartSound (NULL, sound);
	}
	else
		S_StartSound (actor, sound);
}


void A_XScream (mobj_t *actor)
{
	int		sound;
	if ((actor->flags & MF_NOBLOOD) && actor->info->deathsound)
		sound = actor->info->deathsound;
	else
		sound = sfx_slop;
	S_StartSound (actor, sound);	
}

void A_Pain (mobj_t *actor)
{
	int		sound;
	
	switch (actor->info->painsound)
	{
	case 0:
		return;

	case sfx_pespna:
	case sfx_pespnb:
	case sfx_pespnc:
	case sfx_pespnd:
		sound = sfx_pespna + (P_Random()%4);
		break;
	
	default:
		sound = actor->info->painsound;
		break;
	}
	S_StartSound (actor, sound);
}

void func_208DC(mobj_t *actor)
{
	actor->flags |= MF_15;

	if (!(P_Random()%5))
	{
		A_Pain(actor);
		actor->health--;
	}
	if (actor->health <= 0)
		P_KillMobj(actor->target, actor);
}

void A_Fall (mobj_t *actor)
{
// actor is on ground, it can be walked over
	actor->flags |= MF_15;
	actor->flags &= ~(MF_SOLID|MF_NOGRAVITY|MF_SHADOW);
}

void func_20960 (mobj_t *actor)
{
	line_t		junk;
	junk.tag = 999;
	EV_DoDoor(&junk, blazeClose);
	if (actor->target && actor->target->player)
		P_NoiseAlert(actor->target, actor);
}

void func_209BC (mobj_t *actor)
{
	line_t		junk;
	junk.tag = 999;
	EV_DoDoor(&junk, door_8);
	if (actor->target && actor->target->player)
		P_NoiseAlert(actor->target, actor);
}

void func_20A18(mobj_t *actor)
{
	mobj_t *th;
	angle_t angle;

	th = P_SpawnMobj(actor->x, actor->y, actor->z+24*FRACUNIT, MT_96);
	angle = actor->angle + ANG180 + ((P_Random()-P_Random())<<22);
	th->angle = angle;
	angle >>= ANGLETOFINESHIFT;
	th->momx = FixedMul (th->info->speed, finecosine[angle]);
	th->momy = FixedMul (th->info->speed, finesine[angle]) ;
	th->momz = P_Random() << 9;
}

void func_20A9C(mobj_t *actor)
{
	mobj_t *th;
	angle_t angle;

	th = P_SpawnMobj(actor->x, actor->y, actor->z+24*FRACUNIT, MT_94);
	angle = actor->angle - ANG90 + ((P_Random()-P_Random())<<22);
	th->angle = angle;
	angle >>= ANGLETOFINESHIFT;
	th->momx = (FixedMul (th->info->speed, finecosine[angle]) << 16) >> 3;
	th->momy = (FixedMul (th->info->speed, finesine[angle]) << 16) >> 3;
	th->momz = P_Random() << 10;
}

void func_20B2C(mobj_t *actor)
{
	mobj_t *th;

	th = P_SpawnMobj(actor->x, actor->y, actor->z, MT_67);
	th->momz = P_Random() << 9;
}

void func_20B54(mobj_t *actor)
{
	mobj_t *th;

	th = P_SpawnMobj(actor->x, actor->y, actor->z, MT_70);
	th->momz = P_Random() << 9;
}

void func_20B7C(mobj_t *actor)
{
	mobj_t *th;

	th = P_SpawnMobj(actor->x, actor->y, actor->z, MT_71);
	th->momz = P_Random() << 9;
}

void func_20BA4(mobj_t *actor)
{
	mobj_t *th;

	th = P_SpawnMobj(actor->x, actor->y, actor->z, MT_72);
	th->momz = P_Random() << 9;
}

void func_20BCC(mobj_t *actor)
{
	mobj_t *th;

	th = P_SpawnMobj(actor->x, actor->y, actor->z, MT_73);
	th->momz = P_Random() << 9;
}

fixed_t _int_9F860_0;
fixed_t _int_9F860_1;
fixed_t _int_9F860_2;

void func_20BF4(mobj_t *actor)
{
	mobj_t *th;

	th = P_SpawnMobj(actor->x, actor->y, actor->z+70*FRACUNIT, MT_74);
	th->momz = 5*FRACUNIT;

	_int_9F860_0 = th->x;
	_int_9F860_1 = th->y;
	_int_9F860_2 = th->z;
}

void func_20C38(mobj_t *actor, angle_t angle, int speed)
{
	angle >>= ANGLETOFINESHIFT;
	actor->momx += FixedMul(speed, finecosine[angle]);
	actor->momy += FixedMul(speed, finesine[angle]);
}

void func_20C74(mobj_t *actor)
{
	int radius;
	angle_t angle1;
	angle_t angle2;
	fixed_t tx, ty;
	mobj_t *th;

	radius = mobjinfo[MT_75].radius << 1;

	angle1 = actor->angle >> ANGLETOFINESHIFT;
	tx = FixedMul(radius, finecosine[angle1]);
	ty = FixedMul(radius, finesine[angle1]);

	th = P_SpawnMobj(_int_9F860_0+tx, _int_9F860_1+ty, _int_9F860_2, MT_75);
	th->target = actor->target;
	A_FaceTarget(th);
	func_20C38(th, th->angle, 5120000);

	angle2 = (actor->angle + ANG90) >> ANGLETOFINESHIFT;
	tx = FixedMul(radius, finecosine[angle2]);
	ty = FixedMul(radius, finesine[angle2]);

	th = P_SpawnMobj(_int_9F860_0+tx, _int_9F860_1+ty, _int_9F860_2, MT_75);
	th->target = actor->target;
	th->momx = tx << 2;
	th->momy = ty << 2;
	A_FaceTarget(th);

	angle2 = (actor->angle - ANG90) >> ANGLETOFINESHIFT;
	tx = FixedMul(radius, finecosine[angle2]);
	ty = FixedMul(radius, finesine[angle2]);

	th = P_SpawnMobj(_int_9F860_0+tx, _int_9F860_1+ty, _int_9F860_2, MT_75);
	th->target = actor->target;
	th->momx = tx << 2;
	th->momy = ty << 2;
	A_FaceTarget(th);
}

void func_20DF4(mobj_t *actor)
{
	P_SpawnMobj(actor->x, actor->y, actor->z, MT_28);
}

void func_20E10(mobj_t *actor)
{
	int t1;
	angle_t t2;
	sector_t *sec = actor->subsector->sector;
	if (actor->z != sec->floorheight)
		return;
	switch (sec->special)
	{
		case 15:
			P_DamageMobj(actor, NULL, NULL, 999);
			break;
		case 18:
			t1 = ((sec->tag - 100) % 10) << 12;
			t2 = ((sec->tag - 100) / 10) << 29;
            t2 >>= ANGLETOFINESHIFT;
			actor->momx += FixedMul(t1, finecosine[t2]);
			actor->momy += FixedMul(t1, finesine[t2]);
			break;
	}
}

void func_20EB8(mobj_t *actor)
{
	sector_t *sec;
	mobj_t *th;
	int i;
	mobjtype_t t;
	
	sec= actor->subsector->sector;
	sec->lightlevel = 0;
	sec->floorheight = P_FindLowestFloorSurrounding(sec);

	t = MT_255;
	for (i = 0; i < 8; i++)
	{
		th = P_SpawnMobj(actor->x, actor->y, actor->z, t++);
		th->momx = ((P_Random()&15) - (P_Random()&7)) * FRACUNIT;
		th->momy = ((P_Random()&7) - (P_Random()&7)) * FRACUNIT;
		th->momz = (P_Random()&3)*FRACUNIT + 7*FRACUNIT;
	}
}

char _char_9F810[80];

void func_20F44(mobj_t *actor)
{
	int i;
	sprintf(_char_9F810, "You've freed the prisoners!");
	for (i = 0; i < 8; i++)
	{
		func_2DF40(&players[i], 126, 324);
		players[i].message = _char_9F810;
	}
}

void func_20F98(mobj_t *actor)
{
	int i;
	sprintf(_char_9F810, "You've destroyed the Converter!");
	for (i = 0; i < 8; i++)
	{
		func_2DF40(&players[i], 126, 336);
		func_2DF40(&players[i], 126, 306);
		func_2DF40(&players[i], 126, 307);
		players[i].message = _char_9F810;
	}
}

extern char _char_9F810[];

void func_2100C(mobj_t *actor)
{
	int i;
	strcpy(_char_9F810, mobjinfo[actor->info->speed - 1 + MT_312].name);

	for (i = 0; i < 8; i++)
	{
		players[i].f_4d |= 1 << (actor->info->speed - 1);
		players[i].message = _char_9F810;
	}
}

void func_21100(mobj_t *actor)
{
	if (actor->target && actor->target->player)
		actor->target->player->extralight = 0;
}

void func_21124(mobj_t *actor)
{
	P_RadiusAttack(actor, actor->target, 512);
	if (actor->target && actor->target->player)
		actor->target->player->extralight = 5;
}

void func_21158(mobj_t *actor)
{
	P_RadiusAttack(actor, actor->target, 192);
	if (actor->target && actor->target->player)
		P_NoiseAlert(actor->target, actor);
}

void func_211A0(mobj_t *actor)
{
	P_RadiusAttack(actor, actor->target, 128);
	if (actor->target && actor->target->player)
		P_NoiseAlert(actor->target, actor);
}

void func_211E8(mobj_t *actor)
{
	P_RadiusAttack(actor, actor->target, 64);
	if (actor->target && actor->target->player)
		P_NoiseAlert(actor->target, actor);
}

void func_21230(mobj_t *actor)
{
	P_RadiusAttack(actor, actor->target, 32);
	if (actor->target && actor->target->player)
		P_NoiseAlert(actor->target, actor);
}

void func_21278(mobj_t *actor)
{
	if (actor->target && actor->target->player)
		P_NoiseAlert(actor->target, actor);
}

void func_212AC(mobj_t *actor)
{
	if (--actor->reactiontime <= 0)
	{
		P_ExplodeMissile(actor);
		actor->flags &= ~MF_24;
	}
}

void func_212C8(mobj_t *actor)
{
	P_SpawnMobj(actor->x, actor->y, actor->z, MT_109);
}

void func_212E4(mobj_t *actor)
{
	mobj_t *th;
	th = P_SpawnMobj(actor->x, actor->y, actor->z+10*FRACUNIT, MT_68);
	th->momx = ((P_Random()&15) - (P_Random()&7)) * FRACUNIT;
	th->momy = ((P_Random()&7) - (P_Random()&15)) * FRACUNIT;
	th->momz = (P_Random()&15)*FRACUNIT;
}

void func_2134C(mobj_t *actor)
{
	mobj_t *th;
	th = P_SpawnMobj(actor->x, actor->y, actor->z+10*FRACUNIT, MT_69);
	th->momx = ((P_Random()&7) - (P_Random()&15)) * FRACUNIT;
	th->momy = ((P_Random()&15) - (P_Random()&7)) * FRACUNIT;
	th->momz = (P_Random()&7)*FRACUNIT;
}

void func_213B4(mobj_t *actor)
{
	mobj_t *th;
	actor->momz -= 8 * FRACUNIT;
	actor->momx += ((P_Random()&3) - (P_Random()&3)) * FRACUNIT;
	actor->momy += ((P_Random()&3) - (P_Random()&3)) * FRACUNIT;
	S_StartSound(actor, sfx_lgfire);
	if (actor->flags & MF_DROPPED)
		return;

	th = P_SpawnMobj(actor->x + ((P_Random() + 12) & 31) * FRACUNIT,
		actor->y + ((P_Random() + 12) & 31) * FRACUNIT,
		actor->z + 4*FRACUNIT, MT_109);
	th->momx += ((P_Random()&7) - (P_Random()&7)) * FRACUNIT;
	th->momy += ((P_Random()&7) - (P_Random()&7)) * FRACUNIT;
	th->momz -= FRACUNIT;
	th->reactiontime = (P_Random()&3)+2;
	th->flags |= MF_DROPPED;
}

void func_214C0(mobj_t *actor)
{
	int i;
	thinker_t *th;
	mobj_t *mo;
	int l;

	switch (actor->type)
	{
		case MT_60:
			for (i = 0; i < 8; i++)
			{
				if (playeringame[i] && players[i].health > 0)
					break;
			}
			if (i == 8)
				break;

			for (th = thinkercap.next; th != &thinkercap; th = th->next)
			{
				if (th->function != P_MobjThinker)
					continue;
				mo = (mobj_t*)th;
				if (mo == actor)
					continue;
				if (mo->type == actor->type && mo->health > 0)
					return;
			}
			if (actor->type == MT_60)
			{
				for (i = 0; i < 8; i++)
				{
					func_2DF40(&players[i], 126, 318);
				}
				I_StartVoice("VOC14");
				l = W_CheckNumForName("LOG14");
				if (l > 0)
					strncpy(_char_A1300, W_CacheLumpNum(l, PU_CACHE), 300);
			}
			break;
	}
}

void A_BossDeath(mobj_t *actor)
{
	int i;
	thinker_t* th;
	mobj_t* mo;
	int l;
	line_t junk;
	switch (actor->type)
	{
		case MT_63:
		case MT_67:
		case MT_70:
		case MT_71:
		case MT_72:
		case MT_73:
		case MT_75:
		case MT_95:
			for (i = 0; i < 8; i++)
			{
				if (playeringame[i] && players[i].health > 0)
					break;
			}
			if (i == 8)
				break;

			for (th = thinkercap.next; th != &thinkercap; th = th->next)
			{
				if (th->function != P_MobjThinker)
					continue;
				mo = (mobj_t*)th;
				if (mo == actor)
					continue;
				if (mo->type == actor->type && mo->health > 0)
					return;
			}
			if (actor->type == MT_63)
			{
				junk.tag = 667;
				EV_DoFloor(&junk, lowerFloorToLowest);
			}
			else if (actor->type == MT_67)
			{
				I_StartVoice("VOC95");
				l = W_CheckNumForName("LOG95");
				if (l > 0)
					strncpy(_char_A1300, W_CacheLumpNum(l, PU_CACHE), 300);
				junk.tag = 999;
				EV_DoFloor(&junk, lowerFloorToLowest);
			}
			else if (actor->type == MT_70)
			{
				func_2DF40(&players[0], 126, 332);
				I_StartVoice("VOC74");
				l = W_CheckNumForName("LOG74");
				if (l > 0)
					strncpy(_char_A1300, W_CacheLumpNum(l, PU_CACHE), 300);
			}
			else if (actor->type == MT_71)
			{
				for (th = thinkercap.next; th != &thinkercap; th = th->next)
				{
					if (th->function != P_MobjThinker)
						continue;
					mo = (mobj_t*)th;
					if (mo == actor)
						continue;
					if (mo->type == MT_65 && mo->health > 0)
						P_KillMobj(actor, mo);
				}
				func_2DF40(&players[0], 126, 334);
				if (players[0].f_4d & 0x100000)
					func_2DF40(&players[0], 126, 333);
				if (!(players[0].f_4d & 0x800000))
				{
					I_StartVoice("VOC87");
					l = W_CheckNumForName("LOG87");
					if (l > 0)
						strncpy(_char_A1300, W_CacheLumpNum(l, PU_CACHE), 300);
				}
				else if ((players[0].f_4d & 0x800000) && (players[0].f_4d & 0x2000000))
				{
					I_StartVoice("VOC85");
					l = W_CheckNumForName("LOG85");
					if (l > 0)
						strncpy(_char_A1300, W_CacheLumpNum(l, PU_CACHE), 300);
				}
				junk.tag = 222;
				EV_DoDoor(&junk, open);
			}
			else if (actor->type == MT_72)
			{
				func_2DF40(&players[0], 126, 335);
				if (!(players[0].f_4d & 0x1000000))
				{
					I_StartVoice("VOC79");
					l = W_CheckNumForName("LOG79");
					if (l > 0)
						strncpy(_char_A1300, W_CacheLumpNum(l, PU_CACHE), 300);
				}
				else
				{
					I_StartVoice("VOC106");
					l = W_CheckNumForName("LOG106");
					if (l > 0)
						strncpy(_char_A1300, W_CacheLumpNum(l, PU_CACHE), 300);
				}
			}
			else if (actor->type == MT_73)
			{
				func_2DF40(&players[0], 126, 337);
				if (!netgame)
				{
					func_2DF40(&players[0], 126, 306);
					func_2DF40(&players[0], 126, 307);
				}
				if (players[0].f_45 == 4)
				{
					I_StartVoice("VOC85");
					l = W_CheckNumForName("LOG85");
					if (l > 0)
						strncpy(_char_A1300, W_CacheLumpNum(l, PU_CACHE), 300);
				}
				else
				{
					I_StartVoice("VOC83");
					l = W_CheckNumForName("LOG83");
					if (l > 0)
						strncpy(_char_A1300, W_CacheLumpNum(l, PU_CACHE), 300);
				}
				junk.tag = 666;
				EV_DoFloor(&junk, lowerFloorToLowest);
			}
			else if (actor->type == MT_75)
			{
				F_StartFinale();
			}
			else if (actor->type == MT_95)
			{
				F_StartFinale();
				G_ExitLevel3(false);
			}
			else if (actor->type)
				I_Error("Error: Unconnected BossDeath id %d", actor->type);
			break;
	}
}

void func_219C0(mobj_t *actor)
{
	S_StartSound(actor, sfx_inqact);
	A_Chase(actor);
}

void func_219D8(mobj_t *actor)
{
	S_StartSound(actor, sfx_spdwlk);
	A_Chase(actor);
}



void A_PlayerScream (mobj_t *mo)
{
//
// Default death sound.
//
	int		sound = sfx_pldeth;
	
	if (mo->health < -50)
	{
		// IF THE PLAYER DIES
		// LESS THAN -50% WITHOUT GIBBING
		sound = sfx_plxdth;
	}
    
	S_StartSound (mo, sound);
}

void func_21A0C(mobj_t *actor)
{
	mobj_t *th;
	mobj_t* th2;
	mobj_t *vp;
	angle_t angle;
	vp = actor->target;
#if (APPVER_STRIFEREV >= AV_SR_STRF12)
	if (actor->target != players[actor->f_9a].mo)
	{
		actor->target = vp = players[actor->f_9a].mo;
	}
#endif
	th = P_SpawnMobj(actor->x, actor->y, 0x80000000, MT_43);
#if (APPVER_STRIFEREV >= AV_SR_STRF12)
	th->f_9a = actor->f_9a;
#endif
	if (!P_TryMove(th, th->x, th->y))
	{
		P_RemoveMobj(th);
		return;
	}
	actor->flags &= ~MF_SPECIAL;
#if (APPVER_STRIFEREV >= AV_SR_STRF12)
	th->flags |= (actor->f_9a << MF_TRANSSHIFT) | MF_15;
#else
	th->flags |= (vp->player->f_30f << MF_TRANSSHIFT) | MF_15;
#endif
	th->threshold = 100;
#if (APPVER_STRIFEREV >= AV_SR_STRF12)
	th->target = NULL;
#else
	th->target = vp->target;
#endif
	if (deathmatch)
		th->health <<= 1;
#if (APPVER_STRIFEREV >= AV_SR_STRF12)
	if (vp && vp->target && (vp->target->type != MT_43 || vp->target->f_9a != th->f_9a))
		th->target = vp->target;
#else
	th->f_9a = vp->player->f_30f;
#endif
	P_SetMobjState(th, th->info->seestate);
	th->angle = actor->angle;
	angle = actor->angle >> ANGLETOFINESHIFT;
	th2 = P_SpawnMobj(th->x + finecosine[angle] * 20, th->y + finesine[angle] * 20, th->z,
		MT_118);
	S_StartSound(th2, sfx_telept);
	if (--actor->health < 0)
		P_RemoveMobj(actor);
}

void func_21B50(mobj_t *actor)
{
	angle_t angle;
	mobj_t *th;
	if (actor->flags & MF_NOBLOOD)
		th = P_SpawnMobj(actor->x, actor->y, actor->z+24*FRACUNIT, MT_296);
	else
		th = P_SpawnMobj(actor->x, actor->y, actor->z+24*FRACUNIT, MT_295);

	P_SetMobjState(th, th->info->spawnstate + (P_Random()%19));
	angle = (P_Random() << 13) / 255;
	th->angle = angle << ANGLETOFINESHIFT;
	th->momx = FixedMul((P_Random()&15)*FRACUNIT, finecosine[angle]);
	th->momy = FixedMul((P_Random()&15)*FRACUNIT, finesine[angle]);
	th->momz = (P_Random()&15)<<16;
}

void func_21C0C(mobj_t *actor)
{
	if (--actor->reactiontime < 0)
	{
		actor->target = NULL;
		actor->reactiontime = actor->info->reactiontime;
		func_1F608(actor);
		if (!actor->target)
			P_SetMobjState(actor, actor->info->spawnstate);
		else
			actor->reactiontime = 50;
	}
	if (actor->reactiontime == 2)
		func_21C9C(actor);
	else if (actor->reactiontime > 50)
		S_StartSound(actor, sfx_alarm);
}

void func_21C7C(mobj_t *actor)
{
	if (!actor->info->activesound)
		return;

	if (!(leveltime&7))
		S_StartSound(actor, actor->info->activesound);
}

void func_21C9C(mobj_t *actor)
{
	actor->subsector->sector->soundtarget = NULL;
}

void func_21CAC(mobj_t *actor)
{
	mobj_t *th;
	mobjtype_t ot = actor->type;

	th = P_SpawnMobj(actor->x, actor->y, actor->z+24*FRACUNIT, MT_297);
	th->momz = -FRACUNIT;
	actor->type = MT_112;
	P_RadiusAttack(actor, actor, 64);
	actor->type = ot;
}

void func_21CF4(mobj_t *actor)
{
	actor->flags |= MF_NOGRAVITY;
	actor->momz = (P_Random()&3)<<16;
}

void func_21D10(mobj_t *actor)
{
	int i;
	sector_t *sec;
	line_t *line;
	sec = actor->subsector->sector;
	actor->flags &= ~MF_SPECIAL;
	
	for (i = 0; i < sec->linecount; i++)
	{
		if (!(sec->lines[i]->flags & ML_TWOSIDED))
			continue;
		if (sec->lines[i]->special == 148)
		{
			sec->lines[i]->flags &= ~ML_BLOCKING;
			sec->lines[i]->special = 0;
			sides[sec->lines[i]->sidenum[0]].midtexture = 0;
			sides[sec->lines[i]->sidenum[1]].midtexture = 0;
		}
	}
}
